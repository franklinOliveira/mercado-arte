<?php require_once('_config.php'); ?>
<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

		<title>Mercado Arte | Artistas</title>

 		<meta name="viewport" content="width=1080" />
		<meta name="description" content="" />
	    <meta name="keywords" content="" />
	    <meta name="revisit" content="3 days" />
	    <meta name="robots" content="index, follow" />
	    <meta name="url" content="" />
	    <meta name="copyright" content="" />
	    <meta name="author" content="" />

	    <meta property="og:image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta property="og:title" content="" />
	    <meta property="og:url" content="" />
	    <meta property="og:description" content="" />
 
	    <meta itemprop="image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta itemprop="name" content="" />
	    <meta itemprop="url" content="" />

		<link rel="canonical" href="<?php echo base_url(); ?>artista" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/content/css/application.css" media="all" />
	    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>/content/images/favicon.png" />

		<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>/content/images//html5.js"></script>
		<![endif]-->
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="#">Home</a></li>
					<li>Artistas</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php include('includes/sidebar.php'); ?>

			<div class="content">

				<section class="primary-banner primary-banner-small">
					<div class="resume">
						<h2 class="winner">Artista do Mês</h2>
						<h3 class="name">Sou Kit Gom <span class="badge sprite-badge-gold-big"></span></h3>
						<img src="<?php echo base_url(); ?>/content/images/arte-banner.jpg" width="640" height="90" alt="" />
					</div> <!-- /resume -->

					<article class="artist artist-about">
						<div class="thumbnail">
							<a href="#">
								<img src="<?php echo base_url(); ?>/content/images/profile-big.jpg" width="180" height="180" alt="" />
							</a>
						</div> <!-- /thumbnail -->

						<div class="description">
							<p class="city"><span class="flag hide-text sprite-flag-br">Brasil</span> São Paulo/SP</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, sapiente, ipsum officiis obcaecati maxime mollitia alias molestiae dolore iure veniam voluptas vitae est perspiciatis natus fugiat dolorum voluptatibus eius iusto... <a href="#"><i class="icon icon-arrow-right-big"></i> Continue Lendo</a></p>
							<ul class="actions">
								<li class="first"><a href="#">Ver Obras <i class="icon icon-camera"></i></a></li>
								<li><a href="#">Adicionar aos Favoritos <i class="icon icon-add-favorite"></i></a></li>
								<li class="last"><a href="#">Entrar em Contato <i class="icon icon-comment"></i></a></li>
							</ul>
						</div> <!-- /description -->
					</article>
				</section> <!-- /primary-banner -->

				<section class="right-column">
					<div class="box">
						<?php include('includes/social.php'); ?>

						<h2 class="title">Perguntas Frequentes</h2>
						<div class="box-content">
							<ul class="quickly-faq">
								<li><a href="#">Como faço para ter minha página no Mercado Arte?</a></li>
								<li><a href="#">Vocês aceitam artistas do mercado primário?</a></li>
								<li><a href="#">Quando vender, terei de pegar comissão?</a></li>
							</ul>
						</div> <!-- /box-content -->
					</div> <!-- /box -->
				</section> <!-- /right-column -->

				<br class="clear" />

				<section class="main border-top">

					<h3 class="title page-title">Artistas</h3>
					<ul class="filter-letter">
						<li><a href="#">A</a></li>
						<li><a href="#">B</a></li>
						<li><a href="#">C</a></li>
						<li><a href="#">D</a></li>
						<li><a href="#">E</a></li>
						<li><a href="#">F</a></li>
						<li><a href="#">G</a></li>
						<li><a href="#">H</a></li>
						<li><a href="#">I</a></li>
						<li><a href="#">J</a></li>
						<li><a href="#">K</a></li>
						<li><a href="#">L</a></li>
						<li><a href="#">M</a></li>
						<li><a href="#">N</a></li>
						<li><a href="#">O</a></li>
						<li><a href="#">P</a></li>
						<li><a href="#">Q</a></li>
						<li><a href="#">R</a></li>
						<li class="current"><a href="#">S</a></li>
						<li><a href="#">T</a></li>
						<li><a href="#">U</a></li>
						<li><a href="#">V</a></li>
						<li><a href="#">W</a></li>
						<li><a href="#">X</a></li>
						<li><a href="#">Y</a></li>
						<li><a href="#">Z</a></li>
					</ul>

					<section class="list js-filter-scroll">

						<article class="partner artist">
							<div class="thumbnail">
								<a href="#">
									<img src="<?php echo base_url(); ?>/content/images/profile.jpg" width="110" height="110" alt="" />
								</a>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Sou Kit Gom</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<ul class="actions">
									<li><a href="#" class="favorite">Adicionado aos Favoritos <i class="icon icon-heart"><span>Remover dos Favoritos?</span></i></a></li>
									<li><a href="#">Entrar em Contato <i class="icon icon-comment"></i></a></li>
								</ul>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="">
									</a>
								</div>
								<div class="item view-more">
									<a href="#">
										<span>Ver</span> 
										<i class="icon icon-plus"><span>Mais</span></i>
									</a>
								</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner artist">
							<div class="thumbnail">
								<a href="#">
									<img src="<?php echo base_url(); ?>/content/images/profile.jpg" width="110" height="110" alt="" />
								</a>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Sou Kit Gom</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<ul class="actions">
									<li><a href="#">Adicionar aos Favoritos <i class="icon icon-add-favorite"></i></a></li>
									<li><a href="#">Entrar em Contato <i class="icon icon-comment"></i></a></li>
								</ul>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="">
									</a>
								</div>
								<div class="item view-more">
									<a href="#">
										<span>Ver</span> 
										<i class="icon icon-plus"><span>Mais</span></i>
									</a>
								</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner artist">
							<div class="thumbnail">
								<a href="#">
									<img src="<?php echo base_url(); ?>/content/images/profile.jpg" width="110" height="110" alt="" />
								</a>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Sou Kit Gom</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<ul class="actions">
									<li><a href="#">Adicionar aos Favoritos <i class="icon icon-add-favorite"></i></a></li>
									<li><a href="#">Entrar em Contato <i class="icon icon-comment"></i></a></li>
								</ul>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="">
									</a>
								</div>
								<div class="item view-more">
									<a href="#">
										<span>Ver</span> 
										<i class="icon icon-plus"><span>Mais</span></i>
									</a>
								</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner artist">
							<div class="thumbnail">
								<a href="#">
									<img src="<?php echo base_url(); ?>/content/images/profile.jpg" width="110" height="110" alt="" />
								</a>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Sou Kit Gom</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<ul class="actions">
									<li><a href="#">Adicionar aos Favoritos <i class="icon icon-add-favorite"></i></a></li>
									<li><a href="#">Entrar em Contato <i class="icon icon-comment"></i></a></li>
								</ul>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="">
									</a>
								</div>
								<div class="item view-more">
									<a href="#">
										<span>Ver</span> 
										<i class="icon icon-plus"><span>Mais</span></i>
									</a>
								</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner artist">
							<div class="thumbnail">
								<a href="#">
									<img src="<?php echo base_url(); ?>/content/images/profile.jpg" width="110" height="110" alt="" />
								</a>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Sou Kit Gom</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<ul class="actions">
									<li><a href="#">Adicionar aos Favoritos <i class="icon icon-add-favorite"></i></a></li>
									<li><a href="#">Entrar em Contato <i class="icon icon-comment"></i></a></li>
								</ul>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="">
									</a>
								</div>
								<div class="item view-more">
									<a href="#">
										<span>Ver</span> 
										<i class="icon icon-plus"><span>Mais</span></i>
									</a>
								</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner artist">
							<div class="thumbnail">
								<a href="#">
									<img src="<?php echo base_url(); ?>/content/images/profile.jpg" width="110" height="110" alt="" />
								</a>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Sou Kit Gom</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<ul class="actions">
									<li><a href="#">Adicionar aos Favoritos <i class="icon icon-add-favorite"></i></a></li>
									<li><a href="#">Entrar em Contato <i class="icon icon-comment"></i></a></li>
								</ul>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="">
									</a>
								</div>
								<div class="item view-more">
									<a href="#">
										<span>Ver</span> 
										<i class="icon icon-plus"><span>Mais</span></i>
									</a>
								</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner artist">
							<div class="thumbnail">
								<a href="#">
									<img src="<?php echo base_url(); ?>/content/images/profile.jpg" width="110" height="110" alt="" />
								</a>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Sou Kit Gom</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<ul class="actions">
									<li><a href="#">Adicionar aos Favoritos <i class="icon icon-add-favorite"></i></a></li>
									<li><a href="#">Entrar em Contato <i class="icon icon-comment"></i></a></li>
								</ul>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="">
									</a>
								</div>
								<div class="item view-more">
									<a href="#">
										<span>Ver</span> 
										<i class="icon icon-plus"><span>Mais</span></i>
									</a>
								</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner artist">
							<div class="thumbnail">
								<a href="#">
									<img src="<?php echo base_url(); ?>/content/images/profile.jpg" width="110" height="110" alt="" />
								</a>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Sou Kit Gom</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<ul class="actions">
									<li><a href="#">Adicionar aos Favoritos <i class="icon icon-add-favorite"></i></a></li>
									<li><a href="#">Entrar em Contato <i class="icon icon-comment"></i></a></li>
								</ul>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="">
									</a>
								</div>
								<div class="item view-more">
									<a href="#">
										<span>Ver</span> 
										<i class="icon icon-plus"><span>Mais</span></i>
									</a>
								</div>
							</div> <!-- /thumbs -->
						</article>

						<br class="clear" />

						<ul class="pagination">
							<li>
								<a href="#" class="first">
									<i class="icon icon-arrow-left-small"></i>
									<span>Anterior</span>
								</a>
							</li>
							<li><a href="#">1</a></li>
							<li><a href="#" class="current">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li>
								<a href="#" class="last">
									<i class="icon icon-arrow-right-small"></i>
									<span>Próxima</span>
								</a>
							</li>
						</ul>

					</section> <!-- /list -->

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>