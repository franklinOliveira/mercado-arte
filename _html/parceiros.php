<?php require_once('_config.php'); ?>
<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

		<title>Mercado Arte | Parceiros</title>

 		<meta name="viewport" content="width=1080" />
		<meta name="description" content="" />
	    <meta name="keywords" content="" />
	    <meta name="revisit" content="3 days" />
	    <meta name="robots" content="index, follow" />
	    <meta name="url" content="" />
	    <meta name="copyright" content="" />
	    <meta name="author" content="" />

	    <meta property="og:image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta property="og:title" content="" />
	    <meta property="og:url" content="" />
	    <meta property="og:description" content="" />
 
	    <meta itemprop="image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta itemprop="name" content="" />
	    <meta itemprop="url" content="" />

		<link rel="canonical" href="<?php echo base_url(); ?>parceiros" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/content/css/application.css" media="all" />
	    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>/content/images/favicon.png" />

		<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>/content/images//html5.js"></script>
		<![endif]-->
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="#">Home</a></li>
					<li>Parceiros</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php include('includes/sidebar.php'); ?>

			<div class="content">

				<section class="primary-banner primary-banner-small">
					<a href="#">
						<img src="<?php echo base_url(); ?>/content/images/catalogo.jpg" alt="" />
					</a>
				</section>

				<section class="right-column">
					<div class="box">
						<?php include('includes/social.php'); ?>

						<h2 class="title">Perguntas Frequentes</h2>
						<div class="box-content">
							<ul class="quickly-faq">
								<li><a href="#">Como faço para ter minha página no Mercado Arte?</a></li>
								<li><a href="#">Vocês aceitam artistas do mercado primário?</a></li>
								<li><a href="#">Quando vender, terei de pegar comissão?</a></li>
							</ul>
						</div> <!-- /box-content -->
					</div> <!-- /box -->
				</section>

				<br class="clear" />

				<section class="main border-top">

					<h3 class="title page-title">Nossos Parceiros</h3>
					<div class="filter-order">
						<label for="order-by">Filtro:</label>
						<select name="order-by" class="js-custom-select js-change-order">
						    <option value="">Todos os Parceiros</option>
						    <option value="">Arquitetura</option>
						    <option value="">Galeria de Arte</option>
						    <option value="">Molduraria</option>
						</select>
					</div>

					<section class="list js-filter-scroll">
						<!--
						//Aplicar as classes 'owl-carousel js-thumbs-slider' na div.thumb,
						//somente se tiver 5 ou mais imagens cadastradas, 
						//isso faz o slider funcionar.
						-->
						<article class="partner">
							<div class="thumbnail">
								<img src="<?php echo base_url(); ?>/content/images/cais-arquitetura.jpg" width="110" height="110" alt="" />
								<h4 class="category">Arquitetura</h4>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Cais Arquitetura</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<a href="http://www.caisarquitetura.com.br" target="_blank">www.caisarquitetura.com.br</a>
								<p>(11) 2594-5059</p>
								<a href="mailto:contato@caisarquitetura.com.br?Contato pelo Site [Mercado Arte]">contato@caisarquitetura.com.br</a>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">Foto 1</div>
								<div class="item">Foto 2</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner">
							<div class="thumbnail">
								<img src="<?php echo base_url(); ?>/content/images/james-lisboa.jpg" width="110" height="110" alt="" />
								<h4 class="category">Arquitetura</h4>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Cais Arquitetura</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<a href="http://www.caisarquitetura.com.br" target="_blank">www.caisarquitetura.com.br</a>
								<p>(11) 2594-5059</p>
								<a href="mailto:contato@caisarquitetura.com.br?Contato pelo Site [Mercado Arte]">contato@caisarquitetura.com.br</a>
							</div> <!-- /description -->

							<div class="thumbs owl-carousel js-thumbs-slider">
								<div class="item">Foto 1</div>
								<div class="item">Foto 2</div>
								<div class="item">Foto 3</div>
								<div class="item">Foto 4</div>
								<div class="item">Foto 5</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner">
							<div class="thumbnail">
								<img src="<?php echo base_url(); ?>/content/images/cais-arquitetura.jpg" width="110" height="110" alt="" />
								<h4 class="category">Arquitetura</h4>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Cais Arquitetura</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<a href="http://www.caisarquitetura.com.br" target="_blank">www.caisarquitetura.com.br</a>
								<p>(11) 2594-5059</p>
								<a href="mailto:contato@caisarquitetura.com.br?Contato pelo Site [Mercado Arte]">contato@caisarquitetura.com.br</a>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">Foto 1</div>
								<div class="item">Foto 2</div>
								<div class="item">Foto 3</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner">
							<div class="thumbnail">
								<img src="<?php echo base_url(); ?>/content/images/james-lisboa.jpg" width="110" height="110" alt="" />
								<h4 class="category">Arquitetura</h4>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Cais Arquitetura</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<a href="http://www.caisarquitetura.com.br" target="_blank">www.caisarquitetura.com.br</a>
								<p>(11) 2594-5059</p>
								<a href="mailto:contato@caisarquitetura.com.br?Contato pelo Site [Mercado Arte]">contato@caisarquitetura.com.br</a>
							</div> <!-- /description -->

							<div class="thumbs owl-carousel js-thumbs-slider">
								<div class="item">Foto 1</div>
								<div class="item">Foto 2</div>
								<div class="item">Foto 3</div>
								<div class="item">Foto 4</div>
							</div> <!-- /thumbs -->
						</article> 

						<article class="partner">
							<div class="thumbnail">
								<img src="<?php echo base_url(); ?>/content/images/cais-arquitetura.jpg" width="110" height="110" alt="" />
								<h4 class="category">Arquitetura</h4>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Cais Arquitetura</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<a href="http://www.caisarquitetura.com.br" target="_blank">www.caisarquitetura.com.br</a>
								<p>(11) 2594-5059</p>
								<a href="mailto:contato@caisarquitetura.com.br?Contato pelo Site [Mercado Arte]">contato@caisarquitetura.com.br</a>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">Foto 1</div>
								<div class="item">Foto 2</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner">
							<div class="thumbnail">
								<img src="<?php echo base_url(); ?>/content/images/james-lisboa.jpg" width="110" height="110" alt="" />
								<h4 class="category">Arquitetura</h4>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Cais Arquitetura</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<a href="http://www.caisarquitetura.com.br" target="_blank">www.caisarquitetura.com.br</a>
								<p>(11) 2594-5059</p>
								<a href="mailto:contato@caisarquitetura.com.br?Contato pelo Site [Mercado Arte]">contato@caisarquitetura.com.br</a>
							</div> <!-- /description -->

							<div class="thumbs owl-carousel js-thumbs-slider">
								<div class="item">Foto 1</div>
								<div class="item">Foto 2</div>
								<div class="item">Foto 3</div>
								<div class="item">Foto 4</div>
								<div class="item">Foto 5</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner">
							<div class="thumbnail">
								<img src="<?php echo base_url(); ?>/content/images/cais-arquitetura.jpg" width="110" height="110" alt="" />
								<h4 class="category">Arquitetura</h4>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Cais Arquitetura</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<a href="http://www.caisarquitetura.com.br" target="_blank">www.caisarquitetura.com.br</a>
								<p>(11) 2594-5059</p>
								<a href="mailto:contato@caisarquitetura.com.br?Contato pelo Site [Mercado Arte]">contato@caisarquitetura.com.br</a>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">Foto 1</div>
								<div class="item">Foto 2</div>
								<div class="item">Foto 3</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner">
							<div class="thumbnail">
								<img src="<?php echo base_url(); ?>/content/images/james-lisboa.jpg" width="110" height="110" alt="" />
								<h4 class="category">Arquitetura</h4>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Cais Arquitetura</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<a href="http://www.caisarquitetura.com.br" target="_blank">www.caisarquitetura.com.br</a>
								<p>(11) 2594-5059</p>
								<a href="mailto:contato@caisarquitetura.com.br?Contato pelo Site [Mercado Arte]">contato@caisarquitetura.com.br</a>
							</div> <!-- /description -->

							<div class="thumbs owl-carousel js-thumbs-slider">
								<div class="item">Foto 1</div>
								<div class="item">Foto 2</div>
								<div class="item">Foto 3</div>
								<div class="item">Foto 4</div>
							</div> <!-- /thumbs -->
						</article> 
						
						<article class="partner">
							<div class="thumbnail">
								<img src="<?php echo base_url(); ?>/content/images/cais-arquitetura.jpg" width="110" height="110" alt="" />
								<h4 class="category">Arquitetura</h4>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Cais Arquitetura</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<a href="http://www.caisarquitetura.com.br" target="_blank">www.caisarquitetura.com.br</a>
								<p>(11) 2594-5059</p>
								<a href="mailto:contato@caisarquitetura.com.br?Contato pelo Site [Mercado Arte]">contato@caisarquitetura.com.br</a>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">Foto 1</div>
								<div class="item">Foto 2</div>
								<div class="item">Foto 3</div>
								<div class="item">Foto 4</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner">
							<div class="thumbnail">
								<img src="<?php echo base_url(); ?>/content/images/james-lisboa.jpg" width="110" height="110" alt="" />
								<h4 class="category">Arquitetura</h4>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Cais Arquitetura</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<a href="http://www.caisarquitetura.com.br" target="_blank">www.caisarquitetura.com.br</a>
								<p>(11) 2594-5059</p>
								<a href="mailto:contato@caisarquitetura.com.br?Contato pelo Site [Mercado Arte]">contato@caisarquitetura.com.br</a>
							</div> <!-- /description -->

							<div class="thumbs owl-carousel js-thumbs-slider">
								<div class="item">Foto 1</div>
								<div class="item">Foto 2</div>
								<div class="item">Foto 3</div>
								<div class="item">Foto 4</div>
								<div class="item">Foto 5</div>
							</div> <!-- /thumbs -->
						</article>

						<article class="partner">
							<div class="thumbnail">
								<img src="<?php echo base_url(); ?>/content/images/cais-arquitetura.jpg" width="110" height="110" alt="" />
								<h4 class="category">Arquitetura</h4>
							</div> <!-- /thumbnail -->

							<div class="description">
								<h5 class="name"><a href="#">Cais Arquitetura</a></h5>
								<p class="city"><span class="flag sprite-flag-br">Brasil</span> São Paulo/SP</p>
								<a href="http://www.caisarquitetura.com.br" target="_blank">www.caisarquitetura.com.br</a>
								<p>(11) 2594-5059</p>
								<a href="mailto:contato@caisarquitetura.com.br?Contato pelo Site [Mercado Arte]">contato@caisarquitetura.com.br</a>
							</div> <!-- /description -->

							<div class="thumbs">
								<div class="item">Foto 1</div>
								<div class="item">Foto 2</div>
								<div class="item">Foto 3</div>
							</div> <!-- /thumbs -->
						</article>

						<br class="clear" />

						<ul class="pagination">
							<li>
								<a href="#" class="first">
									<i class="icon icon-arrow-left-small"></i>
									<span>Anterior</span>
								</a>
							</li>
							<li><a href="#">1</a></li>
							<li><a href="#" class="current">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li>
								<a href="#" class="last">
									<i class="icon icon-arrow-right-small"></i>
									<span>Próxima</span>
								</a>
							</li>
						</ul>

					</section> <!-- /list -->

				</section>
				
			</div>

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>