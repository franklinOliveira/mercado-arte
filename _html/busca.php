<?php require_once('_config.php'); ?>
<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

		<title>Mercado Arte | Resultado da busca por 'palavra'</title>

 		<meta name="viewport" content="width=1080" />
		<meta name="description" content="" />
	    <meta name="keywords" content="" />
	    <meta name="revisit" content="3 days" />
	    <meta name="robots" content="index, follow" />
	    <meta name="url" content="" />
	    <meta name="copyright" content="" />
	    <meta name="author" content="" />

	    <meta property="og:image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta property="og:title" content="" />
	    <meta property="og:url" content="" />
	    <meta property="og:description" content="" />
 
	    <meta itemprop="image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta itemprop="name" content="" />
	    <meta itemprop="url" content="" />

		<link rel="canonical" href="<?php echo base_url(); ?>index" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/content/css/application.css" media="all" />
	    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>/content/images/favicon.png" />

		<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>/content/images/html5.js"></script>
		<![endif]-->
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
			</div> <!-- /breadcrumb -->

			<?php include('includes/sidebar.php'); ?>

			<div class="content">

				<section class="main">
					
					<!--
					<h3 class="title margin-bottom">Desculpe, não encontramos nada relacionado a 'palavra'.</h3>
					<div class="not-found">
						<p>Tente buscar por outro termo ou navegue pelo site e conheça todas nossas obras e artistas.</p>
						<p>Aqui vão algumas dicas:</p>
					</div>
					Se não for dar muito trampo,
					faz um loop com umas 10 obras
					aleatórias aqui.
					-->
					
					<h3 class="title margin-bottom">Encontramos estes resultados para a busca por 'palavra' que você realizou:</h3>

					<section class="list">

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Sou Kit Gom <span class="badge sprite-badge-gold"></span></h4>
								<h5 class="description">Vista da janela - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 6.500</small>
									<p class="descount">R$ 5.000</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Alessandra Gois <span class="badge sprite-badge-silver"></span></h4>
								<h5 class="description">Flores - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 6.500</small>
									<p class="descount">R$ 6.200</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Vik Silva <span class="badge sprite-badge-gold"></span></h4>
								<h5 class="description">Nú Feminino - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 2.500</small>
									<p class="descount">R$ 2.000</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Patrícia Souza</h4>
								<h5 class="description">Vaso de Flor - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 950</small>
									<p class="descount">R$ 750</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art no-margin">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-5.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Joãozinho Castro</h4>
								<h5 class="description">Árvore da Vida - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 3.200</small>
									<p class="descount">R$ 2.800</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Sou Kit Gom <span class="badge sprite-badge-gold"></span></h4>
								<h5 class="description">Vista da janela - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 6.500</small>
									<p class="descount">R$ 5.000</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Alessandra Gois <span class="badge sprite-badge-silver"></span></h4>
								<h5 class="description">Flores - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 6.500</small>
									<p class="descount">R$ 6.200</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Vik Silva <span class="badge sprite-badge-gold"></span></h4>
								<h5 class="description">Nú Feminino - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 2.500</small>
									<p class="descount">R$ 2.000</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Patrícia Souza</h4>
								<h5 class="description">Vaso de Flor - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 950</small>
									<p class="descount">R$ 750</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art no-margin">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-5.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Joãozinho Castro</h4>
								<h5 class="description">Árvore da Vida - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 3.200</small>
									<p class="descount">R$ 2.800</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Sou Kit Gom <span class="badge sprite-badge-gold"></span></h4>
								<h5 class="description">Vista da janela - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 6.500</small>
									<p class="descount">R$ 5.000</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Alessandra Gois <span class="badge sprite-badge-silver"></span></h4>
								<h5 class="description">Flores - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 6.500</small>
									<p class="descount">R$ 6.200</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Vik Silva <span class="badge sprite-badge-gold"></span></h4>
								<h5 class="description">Nú Feminino - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 2.500</small>
									<p class="descount">R$ 2.000</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Patrícia Souza</h4>
								<h5 class="description">Vaso de Flor - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 950</small>
									<p class="descount">R$ 750</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art no-margin">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-5.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Joãozinho Castro</h4>
								<h5 class="description">Árvore da Vida - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 3.200</small>
									<p class="descount">R$ 2.800</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Sou Kit Gom <span class="badge sprite-badge-gold"></span></h4>
								<h5 class="description">Vista da janela - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 6.500</small>
									<p class="descount">R$ 5.000</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Alessandra Gois <span class="badge sprite-badge-silver"></span></h4>
								<h5 class="description">Flores - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 6.500</small>
									<p class="descount">R$ 6.200</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Vik Silva <span class="badge sprite-badge-gold"></span></h4>
								<h5 class="description">Nú Feminino - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 2.500</small>
									<p class="descount">R$ 2.000</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Patrícia Souza</h4>
								<h5 class="description">Vaso de Flor - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 950</small>
									<p class="descount">R$ 750</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art no-margin">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-5.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Joãozinho Castro</h4>
								<h5 class="description">Árvore da Vida - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 3.200</small>
									<p class="descount">R$ 2.800</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-1.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Sou Kit Gom <span class="badge sprite-badge-gold"></span></h4>
								<h5 class="description">Vista da janela - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 6.500</small>
									<p class="descount">R$ 5.000</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-2.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Alessandra Gois <span class="badge sprite-badge-silver"></span></h4>
								<h5 class="description">Flores - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 6.500</small>
									<p class="descount">R$ 6.200</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-3.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Vik Silva <span class="badge sprite-badge-gold"></span></h4>
								<h5 class="description">Nú Feminino - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 2.500</small>
									<p class="descount">R$ 2.000</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-4.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Patrícia Souza</h4>
								<h5 class="description">Vaso de Flor - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 950</small>
									<p class="descount">R$ 750</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art no-margin">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/arte-5.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="artist">Joãozinho Castro</h4>
								<h5 class="description">Árvore da Vida - Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 3.200</small>
									<p class="descount">R$ 2.800</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<br class="clear" />

						<ul class="pagination">
							<li>
								<a href="#" class="first">
									<i class="icon icon-arrow-left-small"></i>
									<span>Anterior</span>
								</a>
							</li>
							<li><a href="#">1</a></li>
							<li><a href="#" class="current">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li>
								<a href="#" class="last">
									<i class="icon icon-arrow-right-small"></i>
									<span>Próxima</span>
								</a>
							</li>
						</ul>

					</section> <!-- /list -->

				</section>
				
			</div>

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>