<?php require_once('_config.php'); ?>
<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

		<title>Mercado Arte | Cadastro</title>

 		<meta name="viewport" content="width=1080" />
		<meta name="description" content="" />
	    <meta name="keywords" content="" />
	    <meta name="revisit" content="3 days" />
	    <meta name="robots" content="index, follow" />
	    <meta name="url" content="" />
	    <meta name="copyright" content="" />
	    <meta name="author" content="" />

	    <meta property="og:image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta property="og:title" content="" />
	    <meta property="og:url" content="" />
	    <meta property="og:description" content="" />
 
	    <meta itemprop="image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta itemprop="name" content="" />
	    <meta itemprop="url" content="" />

		<link rel="canonical" href="<?php echo base_url(); ?>cadastro-1" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/content/css/application.css" media="all" />
	    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>/content/images/favicon.png" />

		<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>/content/images//html5.js"></script>
		<![endif]-->
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="#">Home</a></li>
					<li>Cadastro</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php include('includes/sidebar.php'); ?>

			<div class="content">

				<section class="main plans form-itens"> 

					<form action="#" class="js-register-form">
						<h3 class="title page-title">Qual plano atende melhor suas necessidades?</h3>
						<div class="bordered-content choose-plan">
							<table class="table-plans">
							    <thead>
							        <tr>
							            <td class="table-title align-right">
							            	<h4>Selecione um plano:</h4>
							            </td>
							            <td class="plan bronze">
							            	<div class="plan-title js-choose-plan">
								            	<label for="bronze">
								            		<i class="icon icon-radio-unchecked"></i>
								            		<span>Bronze</span>
									            	<p>R$ 55,00</p>
									            	<small>por mês</small>
								            	</label>
							            		<input type="radio" name="plano" id="bronze" data-price="55" class="validate[required]" />
							            	</div>
							            </td>
							            <td class="plan silver">
							            	<div class="plan-title js-choose-plan">
								            	<label for="prata">
								            		<i class="icon icon-radio-unchecked"></i>
								            		<span>Prata</span>
							            			<p>R$ 87,50</p>
							            			<small>por mês</small>
								            	</label>
								            	<input type="radio" name="plano" id="prata" data-price="87.5" class="validate[required]" />
								            </div>
							            </td>
							            <td class="plan gold"> 
							            	<div class="plan-title js-choose-plan">
								            	<label for="ouro">
								            		<i class="icon icon-radio-unchecked"></i> 
								            		<span>Ouro</span>
									            	<p>R$ 120,00</p>
									            	<small>por mês</small>
								            	</label>
								            	<input type="radio" name="plano" id="ouro" data-price="120" class="validate[required]" />
								            </div>
							            </td>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							            <td class="align-right">Número de obras publicadas</td>
							            <td>Até 10 obras</td>
							            <td>Até 50 obras</td>
							            <td>Ilimitado</td>
							        </tr>
							        <tr>
							            <td class="align-right">Espaço para biografia</td>
							            <td>Sim</td>
							            <td>Sim</td>
							            <td>Sim</td>
							        </tr>
							        <tr>
							            <td class="align-right">Espaço para textos críticos</td>
							            <td>Sim</td>
							            <td>Sim</td>
							            <td>Sim</td>
							        </tr>
							        <tr>
							            <td class="align-right">Subdomínio (ex.: suapagina.mercadoarte.com.br)</td>
							            <td><span class="red">Não</span></td>
							            <td>Sim</td>
							            <td>Sim</td>
							        </tr>
							        <tr>
							            <td class="align-right">Arte para cartão de visita personalizado</td>
							            <td><span class="red">Não</span></td>
							            <td>Sim</td>
							            <td>Sim</td>
							        </tr>
							        <tr>
							            <td class="align-right">Obras em destaque na página inicial</td>
							            <td><span class="red">Não</span></td>
							            <td>Sim</td>
							            <td>Sim</td>
							        </tr>
							        <tr>
							            <td class="align-right">Arte para cartão de visita personalizado</td>
							            <td><span class="red">Não</span></td>
							            <td>Sim</td>
							            <td>Sim</td>
							        </tr>
							        <tr>
							            <td class="align-right">Aliás para e-mail personalizado (ex.: seunome@mercadoarte.com.br)</td>
							            <td><span class="red">Não</span></td>
							            <td>Sim</td>
							            <td>Sim</td>
							        </tr>
							        <tr>
							            <td class="align-right">Participação em exposições coletivas do Mercado Arte <br />*sujeito a taxa de inscrição</td>
							            <td><span class="red">Não</span></td>
							            <td>Sim</td>
							            <td>Sim <br /> <small>(Desconto de 50% <br />na taxa de inscrição)</small></td>
							        </tr>
							        <tr>
							            <td class="align-right">Participação em eventuais publicidades em veículos de terceiros <br />*sujeito a taxa de inscrição</td>
							            <td><span class="red">Não</span></td>
							            <td><span class="red">Não</span></td>
							            <td>Sim <br /> <small>(Desconto de 50% <br />na taxa de inscrição)</small></td>
							        </tr>
							    </tbody>
							</table>
						</div> <!-- /choose-plan -->


						<h3 class="title page-title">Plano Escolhido</h3> 
						<div class="selected-plan bordered-content">
							<span class="column-half">
								<label for="plano">Plano escolhido:</label>
								<select name="plano" class="select-no-appearance js-select-plan validate[required]">
									<option selected>Selecione um plano...</option>
								    <option value="bronze">Bronze - R$ 55,00/mês</option>
								    <option value="prata">Prata - R$ 87,50/mês</option>
								    <option value="ouro">Ouro - R$ 120,00/mês</option>
								</select>

								<label for="periodicidade">Periodicidade:</label>
								<select name="periodicidade" class="select-no-appearance js-select-period validate[required]">
								    <option value="1" >Mensal</option> 
								    <option value="3" selected>Trimestral</option>
								    <option value="6">Semestral</option>
								    <option value="12">Anual</option>
								    <option value="24">Bienal</option>
								    <option value="36">Trienal</option>
								</select>

								<label for="desconto">Cupom de desconto:</label>
								<span class="cupon-code">
									<input type="text" class="input rounded small" />
									<button class="button rounded continue">Aplicar</button>
								</span>
							</span> <!-- /column-half -->

							<span class="column-half">
								<span class="error-message js-alert-message"></span>
								<div class="total js-total">
									<h4>Total a pagar</h4>
									<p class="result js-result-plan"><small>Selecione um plano <br />e um período de assinatura.</small></p>
									<span>Parcele em até 12x com PagSeguro</span>
								</div>
							</span> <!-- /column-half -->
							
						</div> <!-- /selected-plan -->


						<h3 class="title page-title">Pagamento</h3>
						<div class="payment bordered-content">
								
							<span class="label"><strong>Atenção:</strong> Sua nota será emitida pelo nome cadastrado no CPF e será enviada por e-mail.</span>
							<span class="radios payment-radios js-radios">
								
								<input type="radio" name="pagamento" id="pagseguro" checked class="validate[required]" />
								<label for="pagseguro">
									<i class="icon icon-radio-unchecked"></i>
									<span class="hide-text sprite-pagseguro">PagSeguro</span>
								</label>

								<br class="clear" />
							
								<input type="radio" name="pagamento" id="paypal" class="validate[required]" />
								<label for="paypal">
									<i class="icon icon-radio-unchecked"></i>
									<span class="hide-text sprite-paypal">PayPal</span>
								</label>
								
							</span>
						</div> <!-- /payment-->

						<h3 class="title page-title">Contrato</h3>
						<div class="contract bordered-content">
								
							<div class="contract-text bordered-content">
								<p>Prezado Cliente!</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, vitae, nihil, culpa, ad soluta sunt sapiente quibusdam velit voluptas doloremque quia adipisci amet. Dignissimos harum iusto labore cupiditate nostrum similique.</p>
								<p class="red uppercase"><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, vitae, nihil, culpa</strong></p>

								<p><strong>1. Lorem ipsum dolor sit amet, consectetur</strong></p>
								<p>Adipisicing elit. Odit, vitae, nihil, culpa, ad soluta sunt sapiente quibusdam velit voluptas doloremque quia adipisci amet. Dignissimos harum iusto labore cupiditate nostrum similique.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, vitae, nihil, culpa, ad soluta sunt sapiente quibusdam velit voluptas doloremque quia adipisci amet. Dignissimos harum iusto labore cupiditate nostrum similique.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, vitae, nihil, culpa, ad soluta sunt sapiente quibusdam velit voluptas doloremque quia adipisci amet. Dignissimos harum iusto labore cupiditate nostrum similique.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, vitae, nihil, culpa, ad soluta sunt sapiente quibusdam velit voluptas doloremque quia adipisci amet. Dignissimos harum iusto labore cupiditate nostrum similique.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, vitae, nihil, culpa, ad soluta sunt sapiente quibusdam velit voluptas doloremque quia adipisci amet. Dignissimos harum iusto labore cupiditate nostrum similique.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, vitae, nihil, culpa, ad soluta sunt sapiente quibusdam velit voluptas doloremque quia adipisci amet. Dignissimos harum iusto labore cupiditate nostrum similique.</p>
							</div>

							<div class="contract-option">
								<a href="#" class="print">
									<i class="icon icon-print"></i>
									Imprimir Contrato
								</a>
								<div class="accept-contract">
									<span class="label">Aceita este contrato?</span>
									<span class="radios js-radios">
										<input type="radio" name="aceitar" id="sim" checked class="validate[required]" />
										<label for="sim"><i class="icon icon-radio-unchecked"></i> Sim</label>

										<input type="radio" name="aceitar" id="nao" class="validate[required]" />
										<label for="nao"><i class="icon icon-radio-unchecked"></i> Não</label>
									</span>

									<button type="submit" class="button rounded continue js-submit-register">Continuar</button>
								</div>
							</div>

							<br class="clear" />
							
						</div> <!-- /contract -->

					</form>

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>