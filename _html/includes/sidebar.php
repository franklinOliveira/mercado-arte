<?php $url = basename($_SERVER['PHP_SELF']); ?>

<?php
	if ($url == 'artista-home.php' || $url == 'artista-biografia.php' || $url == 'artista-obras.php' || $url == 'artista-obras-detalhe.php'){
?>
		<aside id="sidebar">
			<div class="box">
				<h2 class="title align-left">Filtro</h2>
				<form action="#" class="js-filter-validation">
					<div class="box-content">
						<h3 class="subtitle">Tamanho</h3>
						<ul class="js-size-filter">
							<li> 
								<label for="ate-50-cm">
									<i class="icon icon-radio-unchecked"></i>
									<span>Até 50 cm</span>
								</label>
								<input type="radio" name="tamanho" id="ate-50" />
							</li>
							<li>
								<label for="de-51-a-100">
									<i class="icon icon-radio-unchecked"></i> 
									<span>51 cm ~ 100 cm</span>
								</label>
								<input type="radio" name="tamanho" id="de-51-a-100" />
							</li>
							<li> 
								<label for="maior-que-100">
									<i class="icon icon-radio-unchecked"></i>
									<span>Maior que 101 cm</span>
								</label>
								<input type="radio" name="tamanho" id="maior-que-100" />
							</li>
							<li> 
								<label for="indiferente">
									<i class="icon icon-radio-unchecked"></i>
									<span>- Indiferente -</span>
								</label>
								<input type="radio" name="tamanho" id="indiferente" />
							</li>
						</ul>

						<h3 class="subtitle">Preço</h3>
						<ul class="js-price-filter">
							<li>
								<label for="ate-100">
									<i class="icon icon-radio-unchecked"></i>
									<span>Até R$ 100</span>
								</label>
								<input type="radio" name="preco" id="ate-100" />
							</li>
							<li>
								<label for="entre-100-e-500">
									<i class="icon icon-radio-unchecked"></i>
									<span>R$ 100 - R$ 500</span>
								</label>
								<input type="radio" name="preco" id="entre-100-e-500" />
							</li>
							<li>
								<label for="entre-500-e-2500">
									<i class="icon icon-radio-unchecked"></i>
									<span>R$ 500 - R$ 2.500</span>
								</label>
								<input type="radio" name="preco" id="entre-500-e-2500" />
							</li>
							<li>
								<label for="acima-de-2500">
									<i class="icon icon-radio-unchecked"></i>
									<span>Acima de R$ 2.500</span>
								</label>
								<input type="radio" name="preco" id="acima-de-2500" />
							</li>
							<li>
								<label for="indiferente">
									<i class="icon icon-radio-unchecked"></i>
									<span>- Indiferente -</span>
								</label>
								<input type="radio" name="preco" id="indiferente" />
							</li>
						</ul>

						<button type="submit" class="button rounded js-submit-filter">Filtrar</button>
					</div> <!-- /box-content -->
				</form>
			</div>

			<div class="box facebook-box">
				<h2 class="title align-left">Facebook</h2>
				<div class="box-content">
					<!--
					No admin do usuário, coloca um campo só pra ele colocar a URL da página no Facebook, ai troca o HREF aqui e GG.
					-->
					<iframe src="//www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/Mercado.Arte.BR&amp;width=235&amp;height=560&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=true&amp;show_border=false&amp;appId=1456724621265401" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
				</div> <!-- /box-content -->
			</div> <!-- /box -->
		</aside>

<?php } else { ?>

		<aside id="sidebar">
			<div class="box">
				<h2 class="title">Encontre a obra perfeita</h2>
				<form action="#" class="js-filter-validation">
					<div class="box-content">
						<h3 class="subtitle">O que você procura?</h3> 
						<ul class="js-type-filter">
							<li>
								<label for="pintura">
									<i class="icon icon-radio-unchecked"></i>
									<span>Pintura</span>
								</label>
								<!-- <input type="radio" name="tipo" id="pintura" class="validate[required]" /> -->
								<input type="radio" name="tipo" id="pintura" />
							</li>
							<li>
								<label for="escultura">
									<i class="icon icon-radio-unchecked"></i>
									<span>Escultura</span>
								</label>
								<input type="radio" name="tipo" id="escultura" />
							</li> 
							<li>
								<label for="fotografia">
									<i class="icon icon-radio-unchecked"></i>
									<span>Fotografia</span>
								</label>
								<input type="radio" name="tipo" id="fotografia" />
							</li>
						</ul>

						<h3 class="subtitle">Tamanho</h3>
						<ul class="js-size-filter">
							<li> 
								<label for="ate-50-cm">
									<i class="icon icon-radio-unchecked"></i>
									<span>Até 50 cm</span>
								</label>
								<input type="radio" name="tamanho" id="ate-50" />
							</li>
							<li>
								<label for="de-51-a-100">
									<i class="icon icon-radio-unchecked"></i> 
									<span>51 cm ~ 100 cm</span>
								</label>
								<input type="radio" name="tamanho" id="de-51-a-100" />
							</li>
							<li> 
								<label for="maior-que-100">
									<i class="icon icon-radio-unchecked"></i>
									<span>Maior que 101 cm</span>
								</label>
								<input type="radio" name="tamanho" id="maior-que-100" />
							</li>
							<li> 
								<label for="indiferente">
									<i class="icon icon-radio-unchecked"></i>
									<span>- Indiferente -</span>
								</label>
								<input type="radio" name="tamanho" id="indiferente" />
							</li>
						</ul>

						<h3 class="subtitle">Técnica</h3>
						<ul class="js-checkbox-filter">
							<li>
								<label for="acrilica">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Acrílica</span>
								</label>
								<input type="checkbox" name="technique" id="acrilica" />
							</li>
							<li>
								<label for="aquarela">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Aquarela</span>
								</label>
								<input type="checkbox" name="technique" id="aquarela" />
							</li>
							<li>
								<label for="colagem">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Colagem</span>
								</label>
								<input type="checkbox" name="technique" id="colagem" />
							</li>
							<li>
								<label for="desenho">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Desenho</span>
								</label>
								<input type="checkbox" name="technique" id="desenho" />
							</li>
							<li>
								<label for="guache">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Guache</span>
								</label>
								<input type="checkbox" name="technique" id="guache" />
							</li>
							<li>
								<label for="gravura">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Gravura</span>
								</label>
								<input type="checkbox" name="technique" id="gravura" />
							</li>
							<li>
								<label for="oleo">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Óleo</span>
								</label>
								<input type="checkbox" name="technique" id="oleo" />
							</li>
							<li>
								<label for="tempera">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Têmpera</span>
								</label>
								<input type="checkbox" name="technique" id="tempera" />
							</li>
							<li>
								<label for="outras">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>- Outras -</span>
								</label>
								<input type="checkbox" name="technique" id="outras" />
							</li>
						</ul>

						<h3 class="subtitle">Formato</h3>
						<ul class="js-checkbox-filter">
							<li>
								<label for="">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Horizontal</span>
								</label>
								<input type="checkbox" name="format" id="horizontal" />
							</li>
							<li>
								<label for="">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Vertical</span>
								</label>
								<input type="checkbox" name="format" id="vertical" />
							</li>
							<li>
								<label for="">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Circular</span>
								</label>
								<input type="checkbox" name="format" id="circular" />
							</li>
							<li>
								<label for="">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Quadrado</span>
								</label>
								<input type="checkbox" name="format" id="quadrado" />
							</li>
							<li>
								<label for="">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>- Outros -</span>
								</label>
								<input type="checkbox" name="format" id="outros" />
							</li>
						</ul>

						<h3 class="subtitle">Preço</h3>
						<ul class="js-price-filter">
							<li>
								<label for="ate-100">
									<i class="icon icon-radio-unchecked"></i>
									<span>Até R$ 100</span>
								</label>
								<input type="radio" name="preco" id="ate-100" />
							</li>
							<li>
								<label for="entre-100-e-500">
									<i class="icon icon-radio-unchecked"></i>
									<span>R$ 100 - R$ 500</span>
								</label>
								<input type="radio" name="preco" id="entre-100-e-500" />
							</li>
							<li>
								<label for="entre-500-e-2500">
									<i class="icon icon-radio-unchecked"></i>
									<span>R$ 500 - R$ 2.500</span>
								</label>
								<input type="radio" name="preco" id="entre-500-e-2500" />
							</li>
							<li>
								<label for="acima-de-2500">
									<i class="icon icon-radio-unchecked"></i>
									<span>Acima de R$ 2.500</span>
								</label>
								<input type="radio" name="preco" id="acima-de-2500" />
							</li>
							<li>
								<label for="indiferente">
									<i class="icon icon-radio-unchecked"></i>
									<span>- Indiferente -</span>
								</label>
								<input type="radio" name="preco" id="indiferente" />
							</li>
						</ul>

						<h3 class="subtitle">Cor</h3>
						<ul class="js-color-filter">
							<li>
								<label for="colorida">
									<i class="icon icon-radio-unchecked"></i>
									<span>Colorida</span>
								</label>
								<input type="radio" name="cor" id="colorida" />
							</li>
							<li>
								<label for="preto-e-branco">
									<i class="icon icon-radio-unchecked"></i>
									<span>Preto &amp; Branco</span>
								</label>
								<input type="radio" name="cor" id="preto-e-branco" />
							</li>
						</ul>
						<span class="colors js-colors">
							
							<span class="color red">
								<label for="vermelho">
									<span>Vermelho</span>
									<input type="checkbox" id="vermelho" name="vermelho" />
								</label>
							</span>
							
							<span class="color orange">
								<label for="laranja">
									<span>Laranja</span>
									<input type="checkbox" id="laranja" name="laranja" />
								</label>
							</span>

							<span class="color yellow">
								<label for="amarelo">
									<span>Amarelo</span>
									<input type="checkbox" id="amarelo" name="amarelo" />
								</label>
							</span>

							<span class="color green">
								<label for="verde">
									<span>Verde</span>
									<input type="checkbox" id="verde" name="verde" />
								</label>
							</span>

							<span class="color blue-light">
								<label for="azul-claro">
									<span>Azul Claro</span>
									<input type="checkbox" id="azul-claro" name="azul-claro" />
								</label>
							</span>

							<span class="color blue">
								<label for="azul">
									<span>Azul</span>
									<input type="checkbox" id="azul" name="azul" />
								</label>
							</span>

							<span class="color purple">
								<label for="roxo">
									<span>Roxo</span>
									<input type="checkbox" id="roxo" name="roxo" />
								</label>
							</span>

							<span class="color pink">
								<label for="rosa">
									<span>Rosa</span>
									<input type="checkbox" id="rosa" name="rosa" />
								</label>
							</span>

							<span class="color white">
								<label for="branco">
									<span>Branco</span>
									<input type="checkbox" id="branco" name="branco" />
								</label>
							</span>

							<span class="color gray">
								<label for="cinza">
									<span>Cinza</span>
									<input type="checkbox" id="cinza" name="cinza" />
								</label>
							</span>

							<span class="color black">
								<label for="preto">
									<span>Preto</span>
									<input type="checkbox" id="preto" name="preto" />
								</label>
							</span>

							<span class="color brown">
								<label for="marrom">
									<span>Marrom</span>
									<input type="checkbox" id="marrom" name="marrom" />
								</label>
							</span>
						</span>

						<h3 class="subtitle">Categorias</h3>
						<ul class="js-checkbox-filter">
							<li>
								<label for="abstratas">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Abstratas</span>
								</label>
								<input type="checkbox" name="categorias" id="abstratas" />
							</li>
							<li>
								<label for="animais">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Animais</span>
								</label>
								<input type="checkbox" name="categorias" id="animais" />
							</li>
							<li>
								<label for="arquitetura-e-edificios">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Arquitectura &amp; Edifícios</span>
								</label>
								<input type="checkbox" name="categorias" id="arquitetura-e-edificios" />
							</li>
							<li>
								<label for="beleza-e-moda">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Beleza &amp; Moda</span>
								</label>
								<input type="checkbox" name="categorias" id="beleza-e-moda" />
							</li>
							<li>
								<label for="celebridade">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Celebridade</span>
								</label>
								<input type="checkbox" name="categorias" id="celebridade" />
							</li>
							<li>
								<label for="cidades">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Cidades</span>
								</label>
								<input type="checkbox" name="categorias" id="cidades" />
							</li>
							<li>
								<label for="comida-e-bebida">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Comida e Bebida</span>
								</label>
								<input type="checkbox" name="categorias" id="comida-e-bebida" />
							</li>
							<li>
								<label for="ensino">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Ensino</span>
								</label>
								<input type="checkbox" name="categorias" id="ensino" />
							</li>
							<li>
								<label for="esporte">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Esporte</span>
								</label>
								<input type="checkbox" name="categorias" id="esporte" />
							</li>
							<li>
								<label for="fauna-e-flora">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Fauna &amp; Flora</span>
								</label>
								<input type="checkbox" name="categorias" id="fauna-e-flora" />
							</li>
							<li>
								<label for="ferias-e-eventos">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Férias &amp; Eventos</span>
								</label>
								<input type="checkbox" name="categorias" id="ferias-e-eventos" />
							</li>
							<li>
								<label for="industrial">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Industrial</span>
								</label>
								<input type="checkbox" name="categorias" id="industrial" />
							</li>
							<li>
								<label for="joias-e-gemas">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Jóias &amp; Gemas</span>
								</label>
								<input type="checkbox" name="categorias" id="joias-e-gemas" />
							</li>
							<li>
								<label for="medicina-e-saude">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Medicina &amp; Saúde</span>
								</label>
								<input type="checkbox" name="categorias" id="medicina-e-saude" />
							</li>
							<li>
								<label for="natura">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Natureza</span>
								</label>
								<input type="checkbox" name="categorias" id="natura" />
							</li>
							<li>
								<label for="negocios-e-financas">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Negócios &amp; Finanças</span>
								</label>
								<input type="checkbox" name="categorias" id="negocios-e-financas" />
							</li>
							<li>
								<label for="objetos-e-ferramentas">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Objetos &amp; Ferramentas</span>
								</label>
								<input type="checkbox" name="categorias" id="objetos-e-ferramentas" />
							</li>
							<li>
								<label for="pessoas">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Pessoas</span>
								</label>
								<input type="checkbox" name="categorias" id="pessoas" />
							</li>
							<li>
								<label for="profissoes">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Profissões</span>
								</label>
								<input type="checkbox" name="categorias" id="profissoes" />
							</li>
							<li>
								<label for="religiao">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Religião</span>
								</label>
								<input type="checkbox" name="categorias" id="religiao" />
							</li>
							<li>
								<label for="tecnologia">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Tecnologia</span>
								</label>
								<input type="checkbox" name="categorias" id="tecnologia" />
							</li>
							<li>
								<label for="transporte-e-automoveis">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Transporte &amp; Automóveis</span>
								</label>
								<input type="checkbox" name="categorias" id="transporte-e-automoveis" />
							</li>
							<li>
								<label for="viagens">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Viagens</span>
								</label>
								<input type="checkbox" name="categorias" id="viagens" />
							</li>
							<li>
								<label for="vintage-e-retro">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>Vintage &amp; Retro</span>
								</label>
								<input type="checkbox" name="categorias" id="vintage-e-retro" />
							</li>
							<li>
								<label for="outras">
									<i class="icon icon-checkbox-unchecked"></i>
									<span>- Outras -</span>
								</label>
								<input type="checkbox" name="categorias" id="outras" />
							</li>
						</ul>

						<button type="submit" class="button rounded js-submit-filter">Filtrar</button>
					</div> <!-- /box-content -->
				</form>
				<?php if ($url == 'parceiros.php'){ ?>
					<?php include('includes/social.php'); ?>
				<?php } ?>
			</div> <!-- /box -->
		</aside>

<?php } ?>

