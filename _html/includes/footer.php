<footer id="footer">
			<div class="wrapper">
				<span class="footer-column column-one">
					<p>O <strong>Mercado Arte</strong> disponibiliza para os artistas a oportunidade de ter uma página na Web para exibir seus trabalhos e para o público em geral a chance de acessibilidade a um universo artístico criativo que vai muito além do que se apresenta em galerias, museus e sites atualmente. <a href="#">Cadastre-se</a></p>
				</span>
				<span class="footer-column column-two">
					<h4>Cadastre-se e receba as novidades por e-mail:</h4>
					<form action="#" class="js-newsletter-form">
						<input type="email" class="input email rounded validate[required,custom[email]]" placeholder="Seu e-mail" />
						<input type="text" class="input name rounded validate[required,custom[name]]" placeholder="Seu nome" />
						<button type="submit" class="button register rounded js-submit-newsletter">Cadastrar</button>
					</form>
				</span>
				<span class="footer-column column-three">
					<h4>Central do relacionamento</h4>
					<a href="#" class="button rounded">Tire suas dúvidas</a>
					<p>Se preferir, ligue para: <br /> (11) 2594-5059</p>
				</span>
			</div>

			<br class="clear" />

			<div class="bottom-links">
				<div class="wrapper">
					<p>Copyright &copy; 2014 Mercado Arte</p>
					<ul>
						<li><a href="#">Sobre Nós</a></li>
						<li><a href="#">Política de Privacidade</a></li>
						<li><a href="#">Como Comprar</a></li>
						<li><a href="#">Como Vender</a></li>
						<li><a href="#">Planos</a></li>
					</ul>
				</div>
			</div>

		</footer>

		<script src="<?php echo base_url(); ?>/content/js/_jquery.js"></script>
		<script src="<?php echo base_url(); ?>/content/js/_preloader.js"></script>
		<script src="<?php echo base_url(); ?>/content/js/_carousel.js"></script>
		<script src="<?php echo base_url(); ?>/content/js/_validate.js"></script>
		<script src="<?php echo base_url(); ?>/content/js/_portuguese.js"></script>
		<script src="<?php echo base_url(); ?>/content/js/_maskedinput.js"></script>
		<script src="<?php echo base_url(); ?>/content/js/_select.js"></script>
		<script src="<?php echo base_url(); ?>/content/js/_zoom.js"></script>
		<script src="<?php echo base_url(); ?>/content/js/_custom.js"></script>
		<!--<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>!-->