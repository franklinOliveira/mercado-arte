<h2 class="title">Mercado Arte na WEB</h2>
				<div class="box-content">
					<ul class="social">
						<li>
							<a href="#" class="rss">
								<i class="icon icon-rss"></i>
								<span>RSS</span>
							</a>
						</li>
						<li>
							<a href="#" class="google-plus">
								<i class="icon icon-google-plus"></i>
								<span>Google Plus</span>
							</a>
						</li>
						<li>
							<a href="#" class="facebook">
								<i class="icon icon-facebook"></i>
								<span>Facebook</span>
							</a>
						</li>
						<li>
							<a href="#" class="twitter">
								<i class="icon icon-twitter"></i>
								<span>Twitter</span>
							</a>
						</li>
						<li>
							<a href="#" class="youtube">
								<i class="icon icon-youtube"></i>
								<span>Youtube</span>
							</a>
						</li>
					</ul>
				</div> <!-- /box-content -->