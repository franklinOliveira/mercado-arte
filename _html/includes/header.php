<?php $url = basename($_SERVER['PHP_SELF']); ?>
<header id="header">

			<div class="top-links">

				<div class="wrapper">
					<?php if ($url == 'artista-home.php' || $url == 'artista-biografia.php' || $url == 'artista-obras.php' || $url == 'artista-obras-detalhe.php'){ ?>
					<h1 class="back-home"><a href="#">Mercado Arte</a></h1>
					<?php } else { ?>
					<a href="#" class="login">
						<i class="icon icon-user"></i>
						<!-- <span>Olá, <strong>Wallace Erick.</strong></span> -->
						<span>Faça login ou cadastre-se</span>
					</a>
					<?php }  ?>

					<ul>
						<li><a href="#">Ajuda</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Fórum</a></li>
						<li><a href="#">Serviços</a></li>
						<li><a href="#">Eventos</a></li>
						<li><a href="#">Contato</a></li>
					</ul>
				</div>

			</div> <!-- /quick-links -->

			<div class="header">

				<div class="wrapper">
					<?php if ($url == 'artista-home.php' || $url == 'artista-biografia.php' || $url == 'artista-obras.php' || $url == 'artista-obras-detalhe.php'){ ?>
						<div class="profile-name">
							<div class="thumbnail">
								<img src="<?php echo base_url(); ?>/content/images/artist-profile.jpg" alt="" />
							</div>
							<div class="name">
								<h2>Patrícia Azoni</h2>
								<p>Artes Visuais e Composições</p>
							</div>
						</div> <!-- /profile-name -->

						<div class="contact-info">  
							<p>
								<i class="icon hide-text sprite-phone">Telefone:</i> 
								<strong>(11) 2594-5059</strong>
								<span class="region">
									<span class="flag hide-text sprite-flag-br">Brasil</span>
									<span>São Paulo/SP</span>
								</span>
							</p>
						</div> <!-- /contact-info -->
					<?php } else { ?>
						<h1><a href="#" class="logo hide-text sprite-logo">Mercado Arte</a></h1>

						<div class="search-form">
							<p>O você está procurando?</p>
							<form action="#">
								<input type="text" class="input rounded-left search-input" placeholder="quadro abstrato, escultura em bronze, foto" />
								<button type="submit" class="button rounded-right search-button">
									<i class="icon icon-search"></i>
									<span>Buscar</span>
								</button>
							</form>
						</div> <!-- /search-form -->

						<div class="contact-info">  
							<p>Quer ajuda para escolher?</p>
							<p>
								<i class="icon hide-text sprite-phone">Telefone:</i> 
								<strong>(11) 2594-5059</strong>
							</p>
						</div> <!-- /contact-info -->
					<?php } ?>
				</div>

			</div> <!-- /header -->

			<div class="wrapper">
				<nav class="menu">
					<ul>
						<li>
							<a href="#">
								<i class="icon icon-home"></i>
								<span>Home</span>
							</a>
						</li>
						<?php if ($url == 'artista-home.php' || $url == 'artista-biografia.php' || $url == 'artista-obras.php' || $url == 'artista-obras-detalhe.php'){ ?>
						<li><a href="#">Obras</a></li>
						<li><a href="#">Artista</a></li>
						<li><a href="#">Contato</a></li>
						<?php } else { ?>
						<li><a href="#" class="current">Pintura</a></li>
						<li><a href="#">Escultura</a></li>
						<li><a href="#">Fotografia</a></li>
						<li><a href="#">Artistas</a></li>
						<li><a href="#">Parceiros</a></li>
						<?php } ?>
					</ul>
					<a href="#" class="liked">Favoritos <i class="icon icon-heart"></i></a>
				</nav>
			</div>
		</header>