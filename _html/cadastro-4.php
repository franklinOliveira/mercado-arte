<?php require_once('_config.php'); ?>
<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

		<title>Mercado Arte | Cadastro Efetuado com Sucesso</title>

 		<meta name="viewport" content="width=1080" />
		<meta name="description" content="" />
	    <meta name="keywords" content="" />
	    <meta name="revisit" content="3 days" />
	    <meta name="robots" content="index, follow" />
	    <meta name="url" content="" />
	    <meta name="copyright" content="" />
	    <meta name="author" content="" />

	    <meta property="og:image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta property="og:title" content="" />
	    <meta property="og:url" content="" />
	    <meta property="og:description" content="" />
 
	    <meta itemprop="image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta itemprop="name" content="" />
	    <meta itemprop="url" content="" />

		<link rel="canonical" href="<?php echo base_url(); ?>cadastro-1" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/content/css/application.css" media="all" />
	    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>/content/images/favicon.png" />

		<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>/content/images//html5.js"></script>
		<![endif]-->
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">Cadastro</a></li>
					<li>Cadastro Efetuado</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php include('includes/sidebar.php'); ?>

			<div class="content">

				<section class="main">

					<h3 class="title page-title">Agora você faz parte do Mercado Arte! :)</h3>
					<div class="message">
						<span class="hide-text centered sprite-logo">Mercado Arte</span>
						<h4>Seu cadastro foi efetuado com sucesso!</h4>
						<p>Você receberá em seu e-mail um manual em PDF, com ele você será capaz de operar <br />o painel de administração com facilidade.</p>
						<p>Para acessar agora seu painel de administração, <a href="#">clique aqui.</a></p>
						<p>Em caso de dúvidas, envie um e-mail para <a href="mailto:suporte@mercadoarte.com.br">suporte@mercadoarte.com.br</a> <br />ou ligue para: <strong>(11) 2594-5059</strong></p>
					</div>

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>