<?php require_once('_config.php'); ?>
<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8"> 

		<title>Mercado Arte | Um pouco sobre Patrícina Azoni</title>

 		<meta name="viewport" content="width=1080" />
		<meta name="description" content="" />
	    <meta name="keywords" content="" />
	    <meta name="revisit" content="3 days" />
	    <meta name="robots" content="index, follow" />
	    <meta name="url" content="" />
	    <meta name="copyright" content="" />
	    <meta name="author" content="" />

	    <meta property="og:image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta property="og:title" content="" />
	    <meta property="og:url" content="" />
	    <meta property="og:description" content="" />
 
	    <meta itemprop="image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta itemprop="name" content="" />
	    <meta itemprop="url" content="" />

		<link rel="canonical" href="<?php echo base_url(); ?>index" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/content/css/application.css" media="all" />
	    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>/content/images/favicon.png" />

		<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>/content/images//html5.js"></script>
		<![endif]-->
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">Artistas</a></li>
					<li>Patrícia Azoni</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php include('includes/sidebar.php'); ?>

			<div class="content">
				
				<div class="profile-banner">
					<img src="<?php echo base_url(); ?>/content/images/banner-profile.jpg" alt="" />
				</div> <!-- /profile-banner -->

				<section class="main">
					<div class="profile-about">
						<h2 class="title">Patrícia Azoni</h2>
						<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime, architecto, officiis porro commodi non aut maiores molestias atque. Odit, vel iure enim molestiae exercitationem nihil dignissimos debitis nostrum voluptate hic."</p>
					</div> <!-- /profile-about -->

					<h2 class="title">Obras mais vistas</h2>
					<section class="list js-filter-scroll">
							<article class="art art-work">
								<a href="#">
									<div class="thumbnail">
										<img src="<?php echo base_url(); ?>/content/images/work-1.jpg" alt="" />
									</div> <!-- /thumbnail -->
									
									<h4 class="art-title">Fim do dia</h4>
									<h5 class="description">Acrílica sobre tela - 50 x 70cm</h5>

									<div class="price">
										<div class="soldout">Vendido</div>
									</div> <!-- /price -->
								</a>

								<div class="rating">
									<ul class="star-rating">
										<li class="active">
											<a href="#"><i class="icon icon-star"></i>
											<span>1</span></a>
										</li>
										<li class="active">
											<i class="icon icon-star"></i>
											<span>2</span>
										</li>
										<li class="active">
											<i class="icon icon-star"></i>
											<span>3</span>
										</li>
										<li>
											<i class="icon icon-star"></i>
											<span>4</span>
										</li>
										<li>
											<i class="icon icon-star"></i>
											<span>5</span>
										</li>
									</ul>
								</div> <!-- /rating -->
							</article>

							<article class="art art-work">
								<a href="#">
									<div class="thumbnail">
										<img src="<?php echo base_url(); ?>/content/images/work-2.jpg" alt="" />
									</div> <!-- /thumbnail -->
									
									<h4 class="art-title">Bob</h4>
									<h5 class="description">Óleo sobre tela - 50 x 70cm</h5>

									<div class="price">
										<small class="value">De R$ 6.500</small>
										<p class="descount">R$ 5.000</p>
									</div> <!-- /price -->
								</a>

								<div class="rating">
									<ul class="star-rating">
										<li class="active">
											<a href="#"><i class="icon icon-star"></i>
											<span>1</span></a>
										</li>
										<li class="active">
											<i class="icon icon-star"></i>
											<span>2</span>
										</li>
										<li class="active">
											<i class="icon icon-star"></i>
											<span>3</span>
										</li>
										<li>
											<i class="icon icon-star"></i>
											<span>4</span>
										</li>
										<li>
											<i class="icon icon-star"></i>
											<span>5</span>
										</li>
									</ul>
								</div> <!-- /rating -->
							</article>

							<article class="art art-work last">
								<a href="#">
									<div class="thumbnail">
										<img src="<?php echo base_url(); ?>/content/images/work-3.jpg" alt="" />
									</div> <!-- /thumbnail -->
									
									<h4 class="art-title">Vaso de Flores</h4>
									<h5 class="description">Óleo sobre tela - 50 x 70cm</h5>

									<div class="price">
										<p class="descount">R$ 5.000</p>
									</div> <!-- /price -->
								</a>

								<div class="rating">
									<ul class="star-rating">
										<li class="active">
											<a href="#"><i class="icon icon-star"></i>
											<span>1</span></a>
										</li>
										<li class="active">
											<i class="icon icon-star"></i>
											<span>2</span>
										</li>
										<li class="active">
											<i class="icon icon-star"></i>
											<span>3</span>
										</li>
										<li>
											<i class="icon icon-star"></i>
											<span>4</span>
										</li>
										<li>
											<i class="icon icon-star"></i>
											<span>5</span>
										</li>
									</ul>
								</div> <!-- /rating -->
							</article>

					</section> <!-- /list -->

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>