<?php require_once('_config.php'); ?>
<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

		<title>Mercado Arte | Cadastro</title>

 		<meta name="viewport" content="width=1080" />
		<meta name="description" content="" />
	    <meta name="keywords" content="" />
	    <meta name="revisit" content="3 days" />
	    <meta name="robots" content="index, follow" />
	    <meta name="url" content="" />
	    <meta name="copyright" content="" />
	    <meta name="author" content="" />

	    <meta property="og:image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta property="og:title" content="" />
	    <meta property="og:url" content="" />
	    <meta property="og:description" content="" />
 
	    <meta itemprop="image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta itemprop="name" content="" />
	    <meta itemprop="url" content="" />

		<link rel="canonical" href="<?php echo base_url(); ?>cadastro-1" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/content/css/application.css" media="all" />
	    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>/content/images/favicon.png" />

		<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>/content/images//html5.js"></script>
		<![endif]-->
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="#">Home</a></li>
					<li>Cadastro</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php include('includes/sidebar.php'); ?>

			<div class="content">

				<section class="main">

					<form action="#" class="js-register-form">
						<h3 class="title page-title">Quero me cadastrar e fazer parte do Mercado Arte</h3>
					
						<div class="forms step-1 border">
							<span class="column-half">
								<legend>Bem-vindo ao Mercado Arte!</legend>

								<label for="pais">Selecione em qual pais você reside:</label>
								<select name="pais" id="pais" class="select-no-appearance validate[required]">
								    <option value="">Selecione um país...</option>
								    <option value="alemanha">Alemanha</option>
								    <option value="brasil">Brasil</option>
								</select>

								<label for="email">* Digite seu e-mail:</label>
								<input type="email" name="email" id="email" class="input rounded validate[required,custom[email],equals[email]]" />

								<label for="email-check">* Digite seu e-mail novamente:</label>
								<input type="email" name="email-check" id="email-check" class="input rounded validate[required,custom[email],equals[email]]" />

								<label for="password">* Crie uma senha:</label>
								<input type="password" name="password" id="password" class="input rounded validate[required]" />

								<label for="celular">* Telefone celular:</label>
								<input type="text" name="celular" id="celular" class="input rounded js-mask-cel validate[required]" />

								<label for="residencial">Telefone residencial:</label>
								<input type="text" name="residencial" id="residencial" class="input js-mask-phone rounded" />

								<button type="submit" class="button rounded js-submit-register">Cadastrar</button>
							</span>

							<span class="column-half border-left form-helper">
								<h4 class="hide-text sprite-logo">Mercado Arte</h4>
								<p>O <strong>Mercado Arte</strong> disponibiliza para os artistas a oportunidade de ter uma página na Web para exibir seus trabalhos e para o público em geral a chance de acessibilidade a um universo artístico criativo que vai muito além do que se apresenta em galerias, museus e sites atualmente.</p>
								<p>Preencha os campos ao lado e clique em <strong>cadastrar</strong>. Em caso de dúvidas, ligue para <strong>(11) 2594-5059</strong>.</p>
								<p>O número de telefone <strong>celular</strong> e <strong>residencial</strong>, não serão divulgados no site.</p>
								<p>Seu <strong>país</strong> será divulgado em sua página e visível para os visitantes.</p>
								<p><strong>* Preenchimento obrigatório</strong></p>
							</span>
						</div> <!-- /forms -->

					</form>

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>