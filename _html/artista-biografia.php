<?php require_once('_config.php'); ?>
<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8"> 

		<title>Mercado Arte | Um pouco sobre Patrícina Azoni</title>

 		<meta name="viewport" content="width=1080" />
		<meta name="description" content="" />
	    <meta name="keywords" content="" />
	    <meta name="revisit" content="3 days" />
	    <meta name="robots" content="index, follow" />
	    <meta name="url" content="" />
	    <meta name="copyright" content="" />
	    <meta name="author" content="" />

	    <meta property="og:image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta property="og:title" content="" />
	    <meta property="og:url" content="" />
	    <meta property="og:description" content="" />
 
	    <meta itemprop="image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta itemprop="name" content="" />
	    <meta itemprop="url" content="" />

		<link rel="canonical" href="<?php echo base_url(); ?>index" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/content/css/application.css" media="all" />
	    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>/content/images/favicon.png" />

		<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>/content/images//html5.js"></script>
		<![endif]-->
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">Artistas</a></li>
					<li><a href="#">Patrícia Azoni</a></li>
					<li>Biografia</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php include('includes/sidebar.php'); ?>

			<div class="content">

				<section class="main">
					<div class="profile-about">
						<h2 class="title">Patrícia Azoni</h2>
						<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime, architecto, officiis porro commodi non aut maiores molestias atque. Odit, vel iure enim molestiae exercitationem nihil dignissimos debitis nostrum voluptate hic."</p>
					</div> <!-- /profile-about -->

					<h2 class="title">Biografia</h2>
					<section class="artist-biography">
						
					</section> <!-- /artist-biography -->
					

					<h2 class="title">Contato</h2>
					<section class="artist-contact">
						<form action="#" class="js-artist-form">
							<div class="forms">
								<span class="column-half">
									<label for="nome">* Nome:</label>
									<input type="text" name="nome" id="nome" class="input rounded validate[required]" />
								</span>

								<span class="column-half">
									<label for="email">* E-mail:</label>
									<input type="email" name="email" id="email" class="input rounded validate[required,custom[email]]" />
								</span>

								<span class="column-half">
									<label for="assunto">Assunto:</label>
									<input type="text" name="assunto" id="assunto" class="input rounded" />
								</span>

								<span class="column-half">
									<label for="telefone">Telefone:</label>
									<input type="text" name="telefone" id="telefone" class="input rounded js-mask-phone" />
								</span>
								
								<label for="mensagem">Mensagem:</label>
								<textarea name="mensagem" id="mensagem" class="input textarea rounded"></textarea>

								<button type="submit" class="button rounded js-submit-artist">Enviar</button>
							</div> <!-- /forms -->
						</form>
					</section> <!-- /artist-contact -->

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>