<?php require_once('_config.php'); ?>
<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

		<title>Mercado Arte | Cadastro</title>
 
 		<meta name="viewport" content="width=1080" />
		<meta name="description" content="" />
	    <meta name="keywords" content="" />
	    <meta name="revisit" content="3 days" />
	    <meta name="robots" content="index, follow" />
	    <meta name="url" content="" />
	    <meta name="copyright" content="" />
	    <meta name="author" content="" />

	    <meta property="og:image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta property="og:title" content="" />
	    <meta property="og:url" content="" />
	    <meta property="og:description" content="" />
 
	    <meta itemprop="image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta itemprop="name" content="" />
	    <meta itemprop="url" content="" />

		<link rel="canonical" href="<?php echo base_url(); ?>cadastro-1" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/content/css/application.css" media="all" />
	    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>/content/images/favicon.png" />

		<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>/content/images//html5.js"></script>
		<![endif]-->
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="#">Home</a></li>
					<li>Cadastro</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php include('includes/sidebar.php'); ?>

			<div class="content">

				<section class="main">

					<form action="#" class="js-register-form">
						<h3 class="title page-title">Dados Pessoais</h3>
						
						<div class="forms step-2 border">
							<span class="column-half border-right">
								<label for="nome">* Seu nome completo:</label> 
								<input type="text" name="nome" id="nome" class="input rounded validate[required]" />

								<label for="cpf">* CPF (somente números):</label>
								<input type="text" name="cpf" id="cpf" class="input rounded js-mask-cpf validate[required]" />

								<label for="rg">* RG ou RNE:</label>
								<input type="text" name="rg" id="rg" class="input rounded" class="validate[required]" />
								
								<span class="label">* Sexo:</span>
								<span class="radios js-radios">
									<input type="radio" name="sexo" id="masculino" class="validate[required]" />
									<label for="masculino"><i class="icon icon-radio-unchecked"></i> Masculino</label>

									<input type="radio" name="sexo" id="feminino" class="validate[required]" />
									<label for="feminino"><i class="icon icon-radio-unchecked"></i> Feminino</label>
								</span>
							</span>

							<span class="column-half form-helper">
								<p>Seu número de <strong>CPF</strong> e <strong>RG/RNE</strong> não serão divulgados.</p>
								<p>O <strong>nome</strong> será divulgado em sua página e visível aos visitantes.</p>
							</span>
						</div> <!-- /forms -->

						<h3 class="title page-title">Telefone</h3>
						
						<div class="forms border">
							<span class="column-half border-right small-padding">
								<label for="comercial">Comercial:</label>
								<input type="text" name="comercial" id="comercial" class="input rounded js-mask-phone" />
							</span>

							<span class="column-half form-helper">
								<p>O número de telefone <strong>comercial</strong> será divulgado em sua página e visível aos visitantes.</p>
							</span>
						</div> <!-- /forms -->

						<h3 class="title page-title">Endereço</h3>
						
						<div class="forms border">
							<span class="column-half border-right">
								<label for="cep">* CEP:</label>
								<span class="block relative">
									<input type="text" name="cep" id="cep" class="input rounded js-mask-cep validate[required]" />
									<span class="js-loading-cep loading-form"><i class="icon icon-animated icon-loader-2"></i></span>
								</span>

								<label for="endereco">* Endereço:</label>
								<input type="text" name="endereco" id="endereco" class="input rounded js-autocomplete-cep validate[required]" />

								<label for="numero">* Número:</label>
								<input type="text" name="numero" id="numero" class="input rounded validate[required,custom[number]]" />

								<label for="complemento">Complemento:</label>
								<input type="text" name="complemento" id="complemento" class="input rounded" />

								<label for="bairro">* Bairro:</label>
								<input type="text" name="bairro" id="bairro" class="input rounded js-autocomplete-cep validate[required]" />

								<label for="cidade">* Cidade:</label>
								<input type="text" name="cidade" id="cidade" class="input rounded js-autocomplete-cep validate[required]" />

								<label for="estado">* Estado:</label>
								<input type="text" name="estado" id="estado" class="input rounded js-autocomplete-cep validate[required]" />
							</span>

							<span class="column-half form-helper">
								
								<p>Sua <strong>cidade</strong> e seu <strong>estado</strong> serão divulgados em sua página e visível aos visitantes.</p>
							</span>
						</div> <!-- /forms -->

						<h3 class="title page-title">Falta pouco, continuar com cadastro...</h3>
						
						<div class="forms border">
							<span class="column-half">
								<span class="label no-margin">Desejo receber publicações por e-mail.</span>
								<span class="radios js-radios">
									<input type="radio" name="publicacoes" id="receber" checked/>
									<label for="receber"><i class="icon icon-radio-checked"></i> Sim</label>

									<input type="radio" name="publicacoes" id="ignorar" />
									<label for="ignorar"><i class="icon icon-radio-unchecked"></i> Não</label>
								</span>
								
								<span class="label">Desejo receber notificações e alertas por SMS.</span>
								<span class="radios js-radios">
									<input type="radio" name="notificacoes" id="sim" checked/>
									<label for="sim"><i class="icon icon-radio-checked"></i> Sim</label>

									<input type="radio" name="notificacoes" id="nao" />
									<label for="nao"><i class="icon icon-radio-unchecked"></i> Não</label>
								</span>

								<button type="submit" class="button rounded continue js-submit-register">Continuar</button>
							</span>
						</div> <!-- /forms -->

					</form>

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>