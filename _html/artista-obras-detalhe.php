<?php require_once('_config.php'); ?>
<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

		<title>Mercado Arte | Patrícina Azoni</title>

 		<meta name="viewport" content="width=1080" />
		<meta name="description" content="" />
	    <meta name="keywords" content="" />
	    <meta name="revisit" content="3 days" />
	    <meta name="robots" content="index, follow" />
	    <meta name="url" content="" />
	    <meta name="copyright" content="" />
	    <meta name="author" content="" />

	    <meta property="og:image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta property="og:title" content="" />
	    <meta property="og:url" content="" />
	    <meta property="og:description" content="" />
 
	    <meta itemprop="image" content="<?php echo base_url(); ?>/content/images/share.png" />
	    <meta itemprop="name" content="" />
	    <meta itemprop="url" content="" />

		<link rel="canonical" href="<?php echo base_url(); ?>index" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/content/css/application.css" media="all" />
	    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>/content/images/favicon.png" />

		<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>/content/images//html5.js"></script>
		<![endif]-->
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">Artistas</a></li>
					<li><a href="#">Patrícia Azoni</a></li>
					<li><a href="#">Obras</a></li>
					<li>No sertão</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<section class="main">
				
				<article class="work">
					<div class="work-info">
						<div class="small-box bordered-content">
							<h2 class="work-title">
								<i class="icon icon-images"></i>
								<span>No sertão</span>
							</h2>
						</div> <!-- /small-box -->
						
						<div class="work-image bordered-content js-work-image">
							<!--
							<img
								id="img_01"
								src="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/image-2.jpg"
								data-zoom-image="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/zoom/image-2.jpg" 
								alt="" />
							-->
							<img 
							src="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/image-1.jpg" 
							data-zoom-image="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/zoom/image-1.jpg"/>
						</div> <!-- /work-image -->

						<ul id="thumbs" class="work-thumbs js-work-thumb">
							<li>
								<a 
									href="#" 
									data-image="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/image-1.jpg" 
									data-zoom-image="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/zoom/image-1.jpg">
										<img src="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/thumb/image-1.jpg" />
								</a>
							</li>
							<li>
								<a 
									href="#" 
									data-image="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/image-2.jpg" 
									data-zoom-image="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/zoom/image-2.jpg">
										<img src="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/thumb/image-2.jpg" />
								</a>
							</li>
							<li>
								<a 
									href="#" 
									data-image="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/image-3.jpg" 
									data-zoom-image="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/zoom/image-3.jpg">
										<img src="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/thumb/image-3.jpg" />
								</a>
							</li>
							<li class="placeholder"></li>
							<!--
							<li>
								<img src="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/thumb/image-1.jpg" alt="">
							</li>
							<li>
								<img src="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/thumb/image-2.jpg" alt="">
							</li>
							<li>
								<img src="<?php echo base_url(); ?>/content/images/uploads/patricia-azoni/no-sertao/thumb/image-3.jpg" alt="">
							</li>
							<li class="placeholder"></li>
							-->
						</ul> <!-- /work-thumbs -->

						<div class="work-details bordered-content">
							<h3 class="title">Detalhes</h3>
							<dl>
								<span>
									<dt>Artista:</dt>
									<dd><a href="#">Patrícia Azoni <span class="badge sprite-badge-gold"></span></a></dd>
								</span>
								<span>
									<dt>Título:</dt>
									<dd>No Sertão</dd>
								</span>
								<span>
									<dt>Técnica:</dt>
									<dd>Acrílica sobre tela</dd>
								</span>
								<span>
									<dt>Altura:</dt>
									<dd>50 cm</dd>
								</span>
								<span>
									<dt>Comprimento:</dt>
									<dd>70 cm</dd>
								</span>
								<span>
									<dt>Formato:</dt>
									<dd>Horizontal</dd>
								</span>
								<span>
									<dt>Código da Obra:</dt>
									<dd>PATAZO001</dd>
								</span>
								<span>
									<dt>Descrição:</dt>
									<dd>Esta obra participou de 3 expedições no Brasil e 2 no exterior.</dd>
								</span>
							</dl>
							<div class="extra-info">
								<br />
								<p>O frete para todas as regiões do Brasil já está incluso no preço.</p>
								<br />
								<p>Não acompanha moldura.</p>
							</div> <!-- /extra-info -->
						</div> <!-- /work-details -->

					</div> <!-- /work-info -->

					<div class="work-contacts">
						<div class="work-sharer small-box bordered-content">
							<div class="rating">
								<p>Gostou dessa obra? Avalie</p>
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
							<?php include('includes/social.php'); ?>
						</div> <!-- /work-sharer -->

						<div class="work-form bordered-content">
							<h3 class="title">Comprar</h3>
							<div class="price">
								<small class="value">De R$ 6.500,00</small>
								<p class="descount">
									<span>R$ 3.500,00</span>
									<small><strong>+</strong> frete a ser definido pelo artista.</small>
								</p>
							</div> <!-- /price -->

							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, debitis, unde aperiam atque dolor libero nobis vel et porro repellat reprehenderit optio dignissimos repellendus delectus praesentium aut similique odio iusto.</p>

							<form action="#" class="js-artist-form">
								<div class="forms">
									<label for="nome">* Nome:</label>
									<input type="text" name="nome" id="nome" class="input rounded validate[required]" />

									<label for="email">* E-mail:</label>
									<input type="email" name="email" id="email" class="input rounded validate[required,custom[email]]" />

									<label for="telefone">Telefone:</label>
									<input type="text" name="telefone" id="telefone" class="input rounded js-mask-phone" />

									<label for="mensagem">Mensagem:</label>
									<textarea name="mensagem" id="mensagem" class="input textarea rounded"></textarea>

									<button type="submit" class="button rounded js-submit-artist">Enviar Proposta</button>
								</div> <!-- /forms -->
							</form>

						</div> <!-- /work-form -->
						
					</div> <!-- /work-contacts -->

				</article> <!-- /work -->

				<br class="clear" />

				<h2 class="title">Obras mais vistas</h2>
				<section class="list js-filter-scroll">
						<article class="art art-work">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/work-1.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="art-title">Fim do dia</h4>
								<h5 class="description">Acrílica sobre tela - 50 x 70cm</h5>

								<div class="price">
									<div class="soldout">Vendido</div>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art art-work">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/work-2.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="art-title">Bob</h4>
								<h5 class="description">Óleo sobre tela - 50 x 70cm</h5>

								<div class="price">
									<small class="value">De R$ 6.500</small>
									<p class="descount">R$ 5.000</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

						<article class="art art-work last">
							<a href="#">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/work-3.jpg" alt="" />
								</div> <!-- /thumbnail -->
								
								<h4 class="art-title">Vaso de Flores</h4>
								<h5 class="description">Óleo sobre tela - 50 x 70cm</h5>

								<div class="price">
									<p class="descount">R$ 5.000</p>
								</div> <!-- /price -->
							</a>

							<div class="rating">
								<ul class="star-rating">
									<li class="active">
										<a href="#"><i class="icon icon-star"></i>
										<span>1</span></a>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>2</span>
									</li>
									<li class="active">
										<i class="icon icon-star"></i>
										<span>3</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>4</span>
									</li>
									<li>
										<i class="icon icon-star"></i>
										<span>5</span>
									</li>
								</ul>
							</div> <!-- /rating -->
						</article>

				</section> <!-- /list -->

			</section> <!-- /main -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>