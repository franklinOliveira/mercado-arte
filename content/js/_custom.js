/*!
 * Title: Custom JS
 * Author: Wallace Erick
 * Author URI: http://wallaceerick.com.br/
 * Version: 1.0
 */

// Function to get the URL Parameter
function getParameter(name) {
	var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
	return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
function captcha(field, rules, i, options){
	if (field.val() != '7') {
		return options.allrules.captcha.alertText;
	}
}


$(document).ready(function(){

    // Cached selectors to improve performance
    var loadedContent         = $('body'),
    	urlParameter          = getParameter('section'),
    	urlOrder              = getParameter('ordem'),
    	pullMenu			  = $('.js-pull'),
    	primaryMenu			  = $('.js-menu'),
    	fixedFilter			  = $('.js-filter-fixed'),
    	// Slider
        primaryBanner      	  = $('.js-primary-banner'),
        thumbsSlider      	  = $('.js-thumbs-slider'),
        // Validation
        contactButton         = $('.js-submit-contact'),
        contactForm           = $('.js-contact-form'),
        filterButton          = $('.js-submit-filter'),
        filterForm            = $('.js-filter-validation'),
        newsletterButton      = $('.js-submit-newsletter'),
        newsletterForm        = $('.js-newsletter-form'),
        registerButton        = $('.js-submit-register'),
        registerForm          = $('.js-register-form'),
        artistButton          = $('.js-submit-artist'),
        artistForm            = $('.js-artist-form'),
        loginButton           = $('.js-submit-login'),
        loginForm             = $('.js-login-form'),
        lostButton            = $('.js-submit-lost'),
        lostForm              = $('.js-lost-form'),
        // Masked Input's
        selectCountry         = $('.js-select-country'),
        maskCel               = $('.js-mask-cel'),
        maskPhone             = $('.js-mask-phone'),
        maskCpf               = $('.js-mask-cpf'),
        maskCep               = $('.js-mask-cep'),
        loadingCep            = $('.js-loading-cep'),
        // Filter
        selectStyled          = $('.js-custom-select'),
        avaliableColors       = $('.js-colors label'),
        orderSelect           = $('.js-change-order'),
        radioButtons          = $('.js-radios label'),
        checkboxItens 		  = $('.js-checkbox-filter li label'),
        radioItens 		  	  = $('.js-radio-filter li label'),
        typeItens 		  	  = $('.js-type-filter li label'),
        categoryItens 		  = $('.js-categories li label'),
        sizeItens 		  	  = $('.js-size-filter li label'),
        priceItens 		  	  = $('.js-price-filter li label'),
        colorItens 		  	  = $('.js-color-filter li label'),
        // Plans
        planOption            = $('.js-choose-plan label'),
	    planSelector          = $('.js-select-plan'),
	    periodSelector        = $('.js-select-period'),
	    // Works
	    workThumb             = $('.js-work-thumb img'),
	    workImage             = $('.js-work-image ul'),
	    // Esqueceu a Senha
	    lostShowButton         = $('.js-show-pass'),
	    lostHideButton         = $('.js-hide-pass'),
	    lostContent            = $('.js-lost-pass');

    // Preloader
    loadedContent.jpreLoader({
        showPercentage:       true,
        autoClose:            true,
        onetimeLoad:          true,
        splashFunction: function(){
            //console.log('Carregando...');
        }
    }, function(){
        //console.log('Carregado!');
    }); 

    // Select Customizado
    if(selectStyled[0]){
	    selectStyled.customSelect({
	    	mapStyle: true
	    });
    }

    if(pullMenu[0]){
	    pullMenu.click(function(e){  
	        e.preventDefault();
	        $(this).toggleClass('opened');  
	        $('.js-menu-pull').stop().slideToggle();
	    }); 
    }

    // Work's Image Switcher
    if(workThumb[0]){
    	/*
    	workThumb.elevateZoom({
	    	gallery:            'thumbs', 
	    	cursor:             'pointer', 
	    	galleryActiveClass: 'current',
	    	zoomWindowPosition: 3,
	    	zoomWindowOffetx:   13,
	    	zoomWindowFadeIn:   500,
	    	zoomWindowFadeOut:  500,
	    	lensFadeIn:         500,
	    	lensFadeOut:        500,
	    	imageCrossfade:     true,
	    	easing: 			true,
	    	zoomWindowHeight:   428,
	    	zoomWindowWidth:    428
    	}); 
		*/
		workImage.find('li').eq(0).fadeIn(300);
		workImage.lightGallery({
	        lang: {
	        	allPhotos: 'Todas as Fotos'
	        },
	        controls: 			true,
	        showThumbByDefault: true,
	        addClass:           'showThumbByDefault'
	    });
	    workThumb.click(function(e){
	    	e.preventDefault();
			var thumb  = $(this).parent().parent().attr('data-value'),
				image  = workImage.find('li');

			// ToggleClass
			workThumb.parent().removeClass('current');
			$(this).parent().addClass('current');

			// Toggle Image in Box
			image.hide();
			workImage.find('li[data-value="'+ thumb +'"]').fadeIn(600);
		});
	}

    // Banner: Home
    if (primaryBanner[0]){
        primaryBanner.owlCarousel({
        	autoPlay: 			5000,
            slideSpeed:         1000,
            pagination:         true,
            navigation:         false,
            itemsDesktop:       false,
            itemsDesktopSmall:  false,
            singleItem:       	true
        });
    }

    // Slider: Fotos dos Parceiros
    if (thumbsSlider[0]){
        thumbsSlider.owlCarousel({
            slideSpeed:         500,
            pagination:         false,
            navigation:         true,
            navigationText: 	false,
            itemsDesktop:       false,
            itemsDesktopSmall:  false,
            items:       		4
        });

    }

    // Masked Input's
    if(maskCel[0]){
    	maskCel.mask('(99) 9?9999-9999');
    }
    if(maskPhone[0]){
    	maskPhone.mask('(99) 9999-9999');
    }
    if(maskCpf[0]){
    	maskCpf.mask('999.999.999-99');
    }
    if(maskCep[0]){
    	maskCep.mask('99999-999');
    	// Auto Complete Address
    	/*
    	function getEndereco(cep){
    		if($.trim(cep) != ''){
    			loadingCep.fadeIn(150);
		        $.getScript('cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=' + cep, function(){  
                	//console.log(resultadoCEP);     
		            if (resultadoCEP['resultado'] != 0) {
		            	// $('.js-autocomplete-cep').attr('disabled','disabled');
		                $('#cidade').val(unescape(resultadoCEP['cidade']));
		                $('#estado').val(unescape(resultadoCEP['uf']));
		                $('#bairro').val(unescape(resultadoCEP['bairro']));
		                $('#endereco').val(unescape(resultadoCEP['tipo_logradouro']) + ' ' + unescape(resultadoCEP['logradouro']));
		            	// $('.js-autocomplete-cep').removeAttr('disabled');
		        		loadingCep.fadeOut(300);
		            }
		            else {
		                loadingCep.html(unescape(resultadoCEP['resultado_txt']));               
		            }            
		        });
		    }
		    else {
		        loadingCep.html('Você precisa informar o CEP.');
		    }
		}
		*/
    	maskCep.blur(function(){
    		getEndereco($('#cep').val());
    	});
    }

    // Apply Mask if is Brazil.
    selectCountry.change(function(){
		var country = $(this).val();
		console.log(country);
		if(country == 76){
			$('#celular').mask('(99) 9?9999-9999');
			$('#residencial').mask('(99) 9999-9999');
		}
		else {
			$('#celular').unmask();
			$('#residencial').unmask();
		}
	});

    // FormulÃ¡rio de Contato
    if(contactForm[0]){
    	contactForm.click(function(){
	        contactForm.validationEngine({
	            scroll: false
	        });
    	});
    }

    // Cadastro para Newsletter no RodapÃ©
    if(newsletterForm[0]){
	    newsletterButton.click(function(){
	        newsletterForm.validationEngine({
	        	promptPosition: 'bottomLeft',
	            scroll: false,
		        onValidationComplete: function(){
		        	// Envio 		
					var nome  = $('input[name=nome]').val(),
						email = $('input[name=email]').val(),
						data  = {
							nome: nome, 
							email: email
						},
						//host = window.location.host,
						//url  = host + '/cadastro/cadastro_news';
						url = 'https://www.mercadoarte.com.br/cadastro/cadastro_news';

					var request = $.ajax({
						url: url,
						type: 'POST',
						data: data,
						dataType: 'json'
					});		

					request.done(function(resp) {
					   if(resp['sucesso'] == 1){
					   		$('.form-news-content').fadeOut(300);
					   		$('#form-news-msg').html('');
					   		$('#form-news-msg').delay(400).fadeIn(300).html('<i class="icon icon-check"></i><p>' + resp['msg'] + '</p>');
					   }
					   else {
					   		$('#form-news-msg').html(resp['msg']);
					   }	  		  				  			 
					});		 
					request.fail(function(jqXHR, textStatus){
					  // alert('Desculpe, não foi possível cadastrar seu e-mail.');
					});	
	            }
	        });
	    });
    }  

    // FormulÃ¡rio para Contato com o Artista
    if(artistForm[0]){
    	artistForm.click(function(){
	        artistForm.validationEngine({
	            scroll: false,
	            promptPosition: 'bottomRight'
	        });
    	});
    } 

    // FormulÃ¡rio de Login e Esqueceu a Senha
    if(loginForm[0]){
    	// Login
	    loginButton.click(function(){
	        loginForm.validationEngine({
	            scroll: false,
	            promptPosition: 'bottomRight'
	        });
	    }); 
	    // Esqueceu a Senha
	    lostButton.click(function(){
	        lostForm.validationEngine({
	            scroll: false,
	            promptPosition: 'topRight'
	        });
	    });
	    // Modal de Esqueceu a Senha
		lostShowButton.click(function(e){
			e.preventDefault();
			lostContent.fadeIn(500); 
		});
		lostHideButton.click(function(e){
			e.preventDefault();
			lostContent.fadeOut(200); 
		});
    } 

    // FormulÃ¡rio de Cadastro
    if(registerForm[0]){
	    registerButton.click(function(){
	        registerForm.validationEngine({
	            scroll: true,
	            validationEventTrigger: 'blur',
	            promptPosition: 'bottomRight'
	            /*
	            onValidationComplete: function(form, status){
				    //alert(status);
				    if(status == true){
				    	$('.js-loading-register').fadeIn(300);
				    	registerForm.validationEngine('attach');
				    }
				}
				*/
	        });
	    });
	    radioButtons.click(function(){
			$(this).parent().find('i').removeClass('icon-radio-checked');
			$(this).find('i').addClass('icon-radio-checked');
		});
    } 

    // Filtro da Sidebar
    if(filterForm[0]){
	    filterButton.click(function(){
	        filterForm.validationEngine({
	            scroll: true
	        });
	    });
		// Radio Buttons
	    $.each([typeItens, sizeItens, priceItens, colorItens], function(i, variable){
	    	variable.click(function(){
				variable.removeClass('selected').find('i').removeClass('icon-radio-checked');
				$(this).addClass('selected').find('i').addClass('icon-radio-checked');
				
				// Show / Hide Color Pallete
				if($('label[for="preto-e-branco"]').hasClass('selected')){
					avaliableColors.parent().hide();
				}
				else {
					avaliableColors.parent().show();
				}
			});
	    });
		// Checkboxes
		checkboxItens.click(function(){
	    	$(this).toggleClass('selected');
			$(this).find('i').toggleClass('icon-checkbox-checked');
			$(this).next().attr('checked', !$(this).next().attr('checked'));
		});
		// Colors 

	    avaliableColors.click(function(){
	    	var span = $(this).parent(); 
	    	// span.toggleClass('selected');

	    	//$(this).next().toggleAttr('checked');
			//$(this).next().attr('checked', !$(this).next().attr('checked'));
			
			// Troca a classe selecionada dos blocos de cor
			if($(this).find('input').is(':checked')){
				span.addClass('selected');
			}
			else {
				span.removeClass('selected');
			}

	    	// Ativa o bloco de cores
	    	if(avaliableColors.parent().hasClass('selected')){
				$('label[for="colorida"]').click();
			}
	    });
    } 

    // Selecionar Plano: Cadastro
    if(planOption[0]){

    	$('.js-payment-box').hide(); //Alcides
	    
	    // Tabela: Planos
	    planOption.click(function(e){
	    	e.preventDefault();

	    	var planPrice     = $(this).next().attr('data-price'),
	    	    selectedPlan  = $(this).next().attr('id'); 

	    	// Ativa os Radios
			planOption.find('i').removeClass('icon-radio-checked');
			$(this).find('i').addClass('icon-radio-checked');
			$(this).next().prop('checked', !$(this).prop('checked'));

			// Populate Select
			planSelector.val(selectedPlan).attr('selected', true);
			// if(selectedPlan == 'bronze'){

			if(selectedPlan == 'super'){
				periodSelector.val('12').attr('selected', true); //Alcides
			}
			//planSelector.trigger('change');
			//planSelector.find('option').prop('selected', function() {
			//	return selectedPlan
			//});
			// Atualiza a funcão de cauculo
			calculatePlan();
			checkFree();
		});

	    // Select: Planos
		planSelector.change(function(){
			var plan = $(this).val();
			// Atualiza o plano na tabela
			$('label[for="'+ plan +'"]').trigger('click');
			// Atualiza a funcão de cálculo
			calculatePlan();
			checkFree();
		});

		// Select: Periodicidade
		periodSelector.change(function(){
			// Atualiza a funcão de cálculo
			calculatePlan();
			checkFree();
		});

		// FunÃ§Ã£o para converter a multiplicaÃ§Ã£o em real
		function formatReal(mixed) {
			var int = parseInt(mixed.toFixed(2).toString().replace(/[^\d]+/g, ''));
			var tmp = int + '';
			tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
			if (tmp.length > 6)
				tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
			return 'R$ ' + tmp;
		}

		function calculatePlan(){
			var parcels    = periodSelector.val(),
				//price      = planOption.parent().find('input:checked').val(),
				price      = $('.js-choose-plan input:checked').attr('data-price'),
				finalPrice = parcels * price;

				//console.log(price);
			// Verifica se exite algum plano selecionado
			if(planOption.next().is(':checked') && parcels != ''){
				$('.js-total').fadeIn(300);
				$('.js-result-plan').html(formatReal(finalPrice));
				$('.js-alert-message').fadeOut(150);
			}
			else {
				$('.js-alert-message').fadeIn(300).html('Selecione um plano para calcularmos o valor total.')
			} 
		}

    	function checkFree(){
    		var plano   = $('.js-choose-plan input:checked').attr('id'),
    			periodo = $('.js-select-period').children(':selected').attr('value');

			if(plano == 'super' && (periodo == '3' || periodo == '6' || periodo == '12')){ //Alcides
				$('.js-payment-box').hide();
				$('label[for="free"]').click();
				$('.js-payment-box').find('input').removeClass('validate[required]');
				$('.js-result-plan').html('Grátis');
				$('div.total.js-total > span').html('Por tempo ILIMITADO'); //Alcides
				$('select[name="periodicidade"]').empty(); //Alcides
				$('select[name="periodicidade"]').append('<option value="12">Anual</option>'); //Alcides
				$('.its-free').show();
				$('.its-super').hide();
				// $('.js-select-plan').children(':selected').text('Bronze - Grátis por 3 Meses');
			}
			else {
				$('.js-payment-box').show();
				$('label[for="pagseguro"]').click();
				$('.js-payment-box').find('input').addClass('validate[required]');
				//$('.its-free').hide(); Alcides
				$('select[name="periodicidade"]').empty(); //Alcides
				$('select[name="periodicidade"]').append('<option value="3">Trimestral</option>'); //Alcides
				$('select[name="periodicidade"]').append('<option value="6">Semestral</option>'); //Alcides
				$('select[name="periodicidade"]').append('<option value="12">Anual</option>'); //Alcides
				$('.its-super').show();
				// $('.js-select-plan').children(':selected').text('Bronze - R$ 55,00/mês');
			}
    	}

	}

    // Order By (Callback)
    if(orderSelect[0]){
		if(urlOrder != null){
			var artsContent = $('.js-filter-scroll').offset().top;
			$('html, body').stop().animate({
		        scrollTop: artsContent - 125
		    }, 1500);
		    // alert(urlOrder);
		} 
		/* Usada no select para ordernar as obras  */ 
	    orderSelect.change(function(){
	    	var orderBy       = $('#order-by').val(),
	    		orderPartner  = $('#order-partner').val(),
	    		tipo          = $(this).attr('id'),
	    	    tipoObra      =  location.pathname.split("/")[2];
	    	
	    	$('.loading-filter').show();
			if(tipo == 'order-by'){
				// Obras
				if(tipoObra == undefined){
					tipoObra = 'obras/pinturas';
				}
				$(location).attr('href', tipoObra + '?ordem=' + orderBy);
			}
			else {
				// Parceiros
				$(location).attr('href','?filtroP=' + orderPartner);
			} 
	    });  

    }

    // ForÃ§a o scroll da pÃ¡gina ao clicar no link de contato 
    if(urlParameter != null){
		var contentTop = $('#' + urlParameter).offset().top;
		$('html, body').stop().animate({
	       scrollTop: contentTop - 80
		}, 1000);
    }


    //Chama a funÃ§Ã£o para adicionar o artista aos favoritos  
    $(".add-favorite-click").on("click",function(e){
    	e.preventDefault();
    	var id = $(this).attr("id");    	
    	var idArtista = $(this).attr("data-idArtista");
		var request = $.ajax({
		  url: "favoritos/adicionarFavorito",
		  type: "POST",
		  data: { idArtista : idArtista },
		  dataType: "html"
		});
		 
		request.done(function( msg ) {
		  if(msg == "sucesso"){
		  	$("#"+id).addClass('favorite').html('');
		  	$("#"+id).html('Artista Favorito <i class="icon icon-heart"></i>');
		  }		  				  			  
		});
		 
		request.fail(function( jqXHR, textStatus ) {
		  alert( "Ops, não foi possível adicionar o artista aos favoritos" );
		});
    });
    

    //Chama a funÃ§Ã£o para aplicar o desconto 
    $("#aplicarDesconto").on("click",function(e){
    	e.preventDefault(); 
    	var cupom = $("#cupom").val();
    	var valorTotal = $(".js-result-plan").html();
    	 	
		var request = $.ajax({
		  url: "aplicarDesconto",
		  type: "GET",
		  data: { cupom : cupom },
		  dataType: "json"
		});
		 
		request.done(function( data ) {
			if(data.cupomValido == true){				
				/* calcula o valor total com a porcemtagem de desconto */
				valorTotal = valorTotal.replace( '.', '' );
				valorTotal = valorTotal.replace( 'R$ ', '' );
				valorTotal = valorTotal.replace( ',', '.' );
				valorTotal = number_format(valorTotal,2,'.','');							
				varlorDesconto = parseFloat(valorTotal*(data.porcentagemDesconto/100));
				valorTotal = parseFloat(valorTotal) - parseFloat(varlorDesconto);
				$('.js-result-plan').html(formatReal(valorTotal));
				$(".error").remove();
				$("#aplicarDesconto").attr("disabled", "disabled");
				$("#aplicarDesconto").html("Cupom Aplicado");

			}else {				
				$(".error-plan").html("<div class='error'><p>Cupom inválido</p></div>")
			}								  			
		});
		 
		request.fail(function( jqXHR, textStatus ) {
		  alert( "Ops, não foi possível aplicar o cupom" );
		});
    });

    function number_format(number, decimals, dec_point, thousands_sep) {
		  //   example 1: number_format(1234.56);
		  //   returns 1: '1,235'
		  //   example 2: number_format(1234.56, 2, ',', ' ');
		  //   returns 2: '1 234,56'
		  //   example 3: number_format(1234.5678, 2, '.', '');
		  //   returns 3: '1234.57'
		  //   example 4: number_format(67, 2, ',', '.');
		  //   returns 4: '67,00'
		  //   example 5: number_format(1000);
		  //   returns 5: '1,000'
		  //   example 6: number_format(67.311, 2);
		  //   returns 6: '67.31'
		  //   example 7: number_format(1000.55, 1);
		  //   returns 7: '1,000.6'
		  //   example 8: number_format(67000, 5, ',', '.');
		  //   returns 8: '67.000,00000'
		  //   example 9: number_format(0.9, 0);
		  //   returns 9: '1'
		  //  example 10: number_format('1.20', 2);
		  //  returns 10: '1.20'
		  //  example 11: number_format('1.20', 4);
		  //  returns 11: '1.2000'
		  //  example 12: number_format('1.2000', 3);
		  //  returns 12: '1.200'
		  //  example 13: number_format('1 000,50', 2, '.', ' ');
		  //  returns 13: '100 050.00'
		  //  example 14: number_format(1e-8, 8, '.', '');
		  //  returns 14: '0.00000001'

		  number = (number + '')
		    .replace(/[^0-9+\-Ee.]/g, '');
		  var n = !isFinite(+number) ? 0 : +number,
		    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		    s = '',
		    toFixedFix = function(n, prec) {
		      var k = Math.pow(10, prec);
		      return '' + (Math.round(n * k) / k)
		        .toFixed(prec);
		    };
		  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
		  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
		    .split('.');
		  if (s[0].length > 3) {
		    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		  }
		  if ((s[1] || '')
		    .length < prec) {
		    s[1] = s[1] || '';
		    s[1] += new Array(prec - s[1].length + 1)
		      .join('0');
		  }
		  return s.join(dec);
	} 


	// Rollover star rating to show numbers
	$('.icon-vote').mouseover(function(){
		var tip = $(this).find('span');
		$(this).parent().prevAll().addClass('hovered');
		$('.icon-vote span').hide();
		tip.fadeToggle(100);
	});
	$('.icon-vote').mouseout(function(){
		$('.icon-vote').parent().removeClass('hovered');
		$('.icon-vote span').hide();
	});

	/* Verifica qual a nota votada na obra */
	$('.icon-vote').click(function(e){
		e.preventDefault();		
		var valorParaNota = $(this).attr('data-icon'),
		    idObra        = $(this).attr('data-idObra'),
		    pai           = $(this).parent(),
			//host = window.location.host,
			//url  = host + '/home/somarNota';
		    url           = 'https://www.mercadoarte.com.br/home/somarNota';
		    // url         = "http://localhost:9090/home/somarNota";

		/* segurança para caso o valor data-icon seja alterado */ 
		switch (valorParaNota) {
		  case '0':
		    var nota = 1;
		    break;

		  case '1':
		    var nota = 2;
		    break;

		  case '2':
		    var nota = 3;
		    break;

		  case '3':
		    var nota = 4;
		    break;

		  case '4':
		   	var nota = 5;
		    break;
		  
		  default:
		     var nota = 1;
		    break;
		}


		var request = $.ajax({
		  url: url,
		  type: "GET",
		  data: {
		  	idObra: idObra,
		  	nota: nota
		  },
		  dataType: "html"
		});		 
		request.done(function(resp) {
		   if(resp == 1){
		   		// $('#star-rating-' + idObra).fadeOut('slow');
				// Ativa as estrelas
				pai.addClass('active');
				pai.prevAll().addClass('active');
				pai.nextAll().removeClass('active');
		   		$('.js-notification').addClass('alert-success').fadeIn(300).html('Seu voto foi inserido com sucesso!');
		   		$('.js-notification').delay(6000).fadeOut(500);
		   }
		   else{
               $('.js-notification').addClass('alert-info').fadeIn(300).html(resp);
               $('.js-notification').delay(6000).fadeOut(500);
		   }				  			  
		});		 
		request.fail(function(jqXHR, textStatus) {
		    // alert("Ops, nÃ£o foi possÃ­vel votar na obra.");
			$('.js-notification').addClass('alert-danger').fadeIn(300).html('Desculpe, ocorreu um erro ao inserir seu voto!');
			$('.js-notification').delay(6000).fadeOut(500);
		});	
	
	});

	/* Escolhe o tipo de obra no filtro */
	$(categoryItens).on('click',function(){
		var TipoObra = $(this).parent().find('span').html();
		var url =  $(location).attr('href');
		var queryString = '?filtroTipoObra=';

		if($(location).attr('search') != ""){		
			var arrUrl = url.split('?');
			var arrQuery = $(location).attr('search').split('?');			
			url = arrUrl[0];
			queryString = arrQuery[0] + '?filtroTipoObra=';
		}				

		$(location).attr('href', url + queryString + TipoObra);		
	});

	// Fixed Menu
	//primaryMenu

    $(window).scroll(function(){
        var primaryMenuTop      = primaryMenu.offset().top,
            windowTop           = $(window).scrollTop();
        if(fixedFilter[0]){
        	var fixedFilterTop = fixedFilter.offset().top;
        }

        // console.log('window: ' + windowTop + ' menu: ' + primaryMenuTop + ' filtro: ' + fixedFilterTop);

        if (windowTop >= 180) {
            primaryMenu.addClass('fixed'); 
        }
        else {
            primaryMenu.removeClass('fixed');
        }

        if (windowTop >= 570) {
        	$('.select-artist').fadeIn(100);
            fixedFilter.addClass('filter-fixed'); 
        }
        else {
        	$('.select-artist').hide();
            fixedFilter.removeClass('filter-fixed');
        }

        
    }); 

});

