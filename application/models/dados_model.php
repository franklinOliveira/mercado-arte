<?php

class Dados_model extends CI_Model {

    function __construct() {
        $this->load->database();
        //$this->output->enable_profiler(TRUE);
    }

    /*
	|--------------------------------------------------------------------------
	| Retorna todos os paises da tabela tblPaises
	|--------------------------------------------------------------------------	
	| Usada na pagina de Cadastro	
	*/
    function paises(){
        $this->db->where('statusRegistro',1);
		$this->db->order_by('nome'); //Inc. Alcides -> Adicionado order by
        $query = $this->db->get('tblgeralpais');
        return $query->result();
    }

    /*
	|--------------------------------------------------------------------------
	| Insere dados na tblUsuarios
	|--------------------------------------------------------------------------	
	| Usada na pagina de Cadastro	
	*/
    function insertCadastroInicial($dataUser){
        $this->db->insert('tblusuarios',$dataUser);
        $idUsuario = $this->db->insert_id();
        if($idUsuario == 0)
            throw new Exception("Ocorreu um erro ao inserir os dados no banco");
        return $idUsuario;
    }

    function insertPermissoesAcessoUsuario($idUsuario) {
        $dataPermissao = array(          
                'idUsuario'             => $idUsuario,               
                'acessaMeuPerfil'       => 1,
                'acessaMeuCadastro'     => 1,
                'acessaObras'           => 1,
                'acessaMeuPlano'        => 1
            );  

        $this->db->insert('tblusuariopermissoes', $dataPermissao);
    }

    /*
	|--------------------------------------------------------------------------
	| Retorna o ID pais do user
	|--------------------------------------------------------------------------	
	| Usada na pagina de Cadastro	
	*/
    function verifica_pais($idUsuario){
        $this->db->where('idUsuario',$idUsuario);
        $this->db->select('idPaisResidente');
        $query = $this->db->get('tblusuarios');
        return $query->result();
    }

    /*
	|--------------------------------------------------------------------------
	| Insere dados na tblUsuariosDados
	|--------------------------------------------------------------------------	
	| Usada na pagina de Cadastro completo	
	*/
    function insertCadastroDados($dataUser){
        $ret = $this->db->insert('tblusuariosdados',$dataUser);
        if($ret == false)
            throw new Exception("Ocorreu um erro ao inserir os dados no banco");
        return $ret;
    }


    /*
	|--------------------------------------------------------------------------
	| Retorna os dados do artista do mês
	|--------------------------------------------------------------------------	
	| Usada na pagina de Artistas	
	*/
    function artistaDoMes($idUsuario){
        $this->db->select('bio.txtBiografia'.LENG.' as txtBiografia, bio.imgBiografia, pg.imgBannerPg, pg.idUsuario, pg.nomeArtistico, pg.imgPerfil, user.cidade, user.estado, u.idPaisResidente,pg.slugpagina, p.nome as nomePais, p.iso,
        (SELECT SUM(qtdVotos) FROM tblobras  WHERE idPgArtista = pg.idPgArtista AND statusRegistro = 1 AND pg.statusRegistro = 1) as votosTotais',true);
        $this->db->where('user.idUsuario',$idUsuario);
        $this->db->join('tblpgartista pg','user.idUsuario = pg.idUsuario');
        $this->db->join('tblbiografiaartista bio','pg.idBiografia = bio.idBiografia');
        $this->db->join('tblusuarios u','u.idUsuario = user.idUsuario');
        $this->db->join('tblgeralpais p','p.idPais = u.idPaisResidente');
        $query = $this->db->get('tblusuariosdados user');
        return $query->result();
    }


    /*
	|--------------------------------------------------------------------------
	| Retorna os ARTISTAS
	|--------------------------------------------------------------------------	
	| Usada na pagina de Artistas
	| Retorna todos os artistas com a inicial escolhida pelo usuario
	*/
    function getArtistas($inicialNome, $limit_end, $limit_start){
        $this->db->select('pg.idUsuario, pg.idPgArtista, pg.nomeArtistico, pg.imgPerfil, user.cidade, user.estado, u.idPaisResidente, pg.slugpagina, p.nome as nomePais, p.iso,
         (SELECT SUM(qtdVotos) FROM tblobras  WHERE idPgArtista = pg.idPgArtista AND statusRegistro = 1 AND pg.statusRegistro = 1) as votosTotais',true);
        $this->db->like('pg.nomeArtistico',$inicialNome,'after');
        $this->db->join('tblpgartista pg','user.idUsuario = pg.idUsuario');
        $this->db->join('tblbiografiaartista bio','pg.idBiografia = bio.idBiografia');
        $this->db->join('tblusuarios u','u.idUsuario = user.idUsuario');
        $this->db->join('tblgeralpais p','p.idPais = u.idPaisResidente');
        $this->db->where('u.statusRegistro',1);
        $query = $this->db->get('tblusuariosdados user',$limit_end, $limit_start);
        return $query->result();
    }


    /*
	|--------------------------------------------------------------------------
	| Retorna o total de artitas
	|--------------------------------------------------------------------------	
	| Usada na pagina de Artistas e favoritos	
	*/
    function totalArtistas($inicialNome = null){
        $this->db->select('pg.idUsuario, pg.idPgArtista, pg.nomeArtistico, pg.imgPerfil,pg.slugpagina, user.cidade, user.estado, u.idPaisResidente');
        if($inicialNome != null)
            $this->db->like('pg.nomeArtistico',$inicialNome,'after');
        $this->db->join('tblpgartista pg','user.idUsuario = pg.idUsuario');
        $this->db->join('tblbiografiaartista bio','pg.idBiografia = bio.idBiografia');
        $this->db->join('tblusuarios u','u.idUsuario = user.idUsuario');
        $this->db->where('u.statusRegistro',1);
        $query = $this->db->get('tblusuariosdados user');
        $count = $query->num_rows();
        return $count;
    }


    /*
	|--------------------------------------------------------------------------
	| Retorna as obras passando idPgArtista
	|--------------------------------------------------------------------------	
	| Usada na pagina de Artistas e favoritos
	| Retorna as ultimas 4 obras cadastrdas, podendo ser  Escultura, Fotografia e Pintura
	*/
    function getUltimasObras($idPgArtista){
        $query = $this->db->query("SELECT imgFoto1,titulo".LENG." as titulo, idPgArtista, idObra, dtCriacao, statusRegistro, slugObra, idStatusObra
									FROM tblobras 
								    WHERE idPgArtista = " . $idPgArtista . " 
							        AND statusRegistro =1 ORDER BY dtCriacao DESC LIMIT 4");
        return $query->result();
    }


    /*
	|--------------------------------------------------------------------------
	| Retorna os ARTISTAS FAVORITOS
	|--------------------------------------------------------------------------	
	| Usada na pagina FAVORITOS
	| Retorna todos os artistas que o usuario adicionou como favorito
	*/
    function getArtistasFavoritos($idArtistas){
        $this->db->select('pg.idUsuario, pg.idPgArtista, pg.nomeArtistico, pg.imgPerfil, pg.slugpagina, user.cidade, user.estado, u.idPaisResidente,
        (SELECT SUM(qtdVotos) FROM tblobras  WHERE idPgArtista = pg.idPgArtista AND statusRegistro = 1 AND pg.statusRegistro = 1) as votosTotais', false);
        $this->db->where_in('pg.idUsuario',$idArtistas);
        $this->db->join('tblpgartista pg','user.idUsuario = pg.idUsuario');
        $this->db->join('tblbiografiaartista bio','pg.idBiografia = bio.idBiografia');
        $this->db->join('tblusuarios u','u.idUsuario = user.idUsuario');
        $query = $this->db->get('tblusuariosdados user');
        return $query->result();
    }


    /*
	|--------------------------------------------------------------------------
	| Retorna os ARTISTAS aleatórios
	|--------------------------------------------------------------------------	
	| Usada na pagina de favoritos
	*/
    function getArtistasRandon($limit){
        $this->db->select('pg.idUsuario, pg.idPgArtista, pg.nomeArtistico, pg.imgPerfil,pg.slugpagina , user.cidade, user.estado, u.idPaisResidente,
        (SELECT SUM(qtdVotos) FROM tblobras  WHERE idPgArtista = pg.idPgArtista AND statusRegistro = 1 AND pg.statusRegistro = 1) as votosTotais',false);
        $this->db->join('tblpgartista pg','user.idUsuario = pg.idUsuario');
        $this->db->join('tblbiografiaartista bio','pg.idBiografia = bio.idBiografia');
        $this->db->join('tblusuarios u','u.idUsuario = user.idUsuario');
        $this->db->where('pg.statusRegistro',1);
        $this->db->where('user.statusRegistro',1);
        $this->db->where('u.statusRegistro',1);
        $this->db->where('bio.statusRegistro',1);
        $this->db->order_by('RAND()');
        $query = $this->db->get('tblusuariosdados user',$limit);

        return $query->result();
    }


    /*
    |--------------------------------------------------------------------------
    | Retorna os planos agrupados
    |--------------------------------------------------------------------------
    | Usada na tela de planos
    */
    function getPlanos(){
        $query = $this->db->query("SELECT a.idTipoPlano, a.grupoPlano, a.planoPT, a.periodicidade, a.vlPlano, c.nome, b.idValorChave, c.idPropriedadeChave
									FROM tbltipoplano a
									INNER JOIN tblpropriedadeplano b ON (a.grupoPlano = b.grupoPlano)
									INNER JOIN tblpropriedadechave c ON (b.idPropriedadeChave = c.idPropriedadeChave)
									WHERE (a.statusRegistro = 1 AND b.statusRegistro = 1 AND c.statusRegistro = 1)
									GROUP BY nome, grupoPlano;");
        return $query->result();
    }


    /*
	|--------------------------------------------------------------------------
	| Retorna as propriedades de cada plano
	|--------------------------------------------------------------------------	
	| Usada na tela de planos
	*/
    function getPropriedadePlano(){
        $this->db->select("idPropriedadeChave, nome");
        $this->db->where("statusRegistro",1);
        $query = $this->db->get("tblpropriedadechave");
        return $query->result();

    }

    /*
	|--------------------------------------------------------------------------
	| Retorna o nome dos planos
	|--------------------------------------------------------------------------	
	| Foi passado a periodicidade de 360 para dividir o valor por 12 e obter o valor mensal.
	| O campo planoEN planoPT , foi selecionado para colocar nas classes.
	*/
    function getNomePlanos(){
        $this->db->select('idTipoPlano, plano'.LENG.' as planoNome, vlPlano, planoEN, grupoPlano, planoPT');
        $this->db->distinct();
        $this->db->where("statusRegistro",1);
        $this->db->where("periodicidade",360);
        $this->db->group_by('planoNome');
        $this->db->order_by('grupoPlano');
        $query = $this->db->get("tbltipoplano");
        return $query->result();

    }


    /*
	|--------------------------------------------------------------------------
	| Verifica se o cupom de desconto existe
	|--------------------------------------------------------------------------		
	*/
    function verificaCupom($cupom){
        $this->db->where("cupomAtivo",1);
        $this->db->where("codCupom",$cupom);
        $query = $this->db->get("tblgeralcupom");
        return $query->result();
    }


    /*
	|--------------------------------------------------------------------------
	| Retorna o plano escolhido pelo usuario
	|--------------------------------------------------------------------------		
	*/
    function verificaPlano($periodicidade,$plano){
        $this->db->select("idTipoPlano, grupoPlano, idFuncao, plano".LENG." as planoNome, periodicidade, vlPlano, dtCriacao, dtUltAlteracao, statusRegistro");
        $this->db->where("statusRegistro",1);
        $this->db->where("periodicidade",$periodicidade);
        $this->db->where("planoPT",$plano);

        $query = $this->db->get("tbltipoplano");
        return $query->result();
    }


    /*
	|--------------------------------------------------------------------------
	| Atualiza o cupom usado para 0
	|--------------------------------------------------------------------------		
	*/
    function invalidaCupom($cupom,$dateTime){
        $data = array(
            'cupomAtivo' => 0,
            'dtUltAlteracao' => $dateTime,
        );
        $this->db->where('codCupom', $cupom);
        $ret = $this->db->update('tblgeralcupom', $data);
        return $ret;
    }


    /*
	|--------------------------------------------------------------------------
	| Insere dados na tblcontratos
	|--------------------------------------------------------------------------	
	| Usada na tela de Cadastro - planos	
	*/
    function insertContrato($dataUser){
        $this->db->insert('tblcontratos',$dataUser);
        $idContrato = $this->db->insert_id();
        if($idContrato == 0)
            throw new Exception("Ocorreu um erro ao inserir os dados do contrato no banco");
        return $idContrato;
    }


    /*
	|--------------------------------------------------------------------------
	| Insere dados na tbltransacoes
	|--------------------------------------------------------------------------	
	| Usada na tela de Cadastro - planos	
	*/
    function insertTransact($dataUser){
        $this->db->insert('tbltransacoes',$dataUser);
        $idTransacao = $this->db->insert_id();
        if($idTransacao == 0)
            throw new Exception("Ocorreu um erro ao inserir os dados da transação no banco");
        return $idTransacao;
    }



    /*
	|--------------------------------------------------------------------------
	| Retorna as obras de acordo com tipo e ordem
	|--------------------------------------------------------------------------		
	*/
    function getGeralObras($ordem = null,$limit_end, $limit_start,$idTipoObra){
        $this->db->select(' o.idObra,
    						o.idPgArtista,
						    o.idTipoObra,						
						    o.titulo'.LENG.' as titulo,
						    o.idTecnica,
						    o.altura,
						    o.comprimento,
						    o.largura,
						    o.ladoMaior,						   
						    o.descricao'.LENG.' as descricao,
						    o.idFormato,
						    o.precoObra,
						    o.precoComDesconto,
						    o.idCorPred,
						    o.idCategoria1,
						    o.idCategoria2,
						    o.idCategoria3,
						    o.imgFoto1,
						    o.imgFoto2,
						    o.imgFoto3,
						    o.idStatusObra,
						    o.NumVisitas,
						    o.somatoriaNotas,
						    o.qtdVotos,
						    o.dtCriacao,
						    o.dtUltAlteracao,
						    o.slugObra,
						    pg.idPgArtista,
						    pg.nomeArtistico,
						    pg.slugpagina,
						    tec.nomeTecnica'.LENG.' as nomeTecnica,
						    tec.idTecnica,
						    ROUND(o.somatoriaNotas / o.qtdVotos,2) as nota,
						    (SELECT SUM(qtdVotos) FROM tblobras  WHERE idPgArtista = pg.idPgArtista AND statusRegistro = 1 AND pg.statusRegistro = 1) as votosTotais',false);

        $this->db->where("pg.statusRegistro",1);
        $this->db->where("o.statusRegistro",1);
        $this->db->where("o.idTipoObra",$idTipoObra);

        //Ordenação
        if($ordem != null || $ordem != 0){
            if($ordem == 1){
                $this->db->order_by('o.titulo'.LENG);
                //$this->db->order_by('pg.nomeArtistico');
            }
            if($ordem == 2){
                $this->db->order_by('nota','desc');
            }
            if($ordem == 3){
                $this->db->order_by('o.precoObra');
            }
            if($ordem == 4){
                $this->db->order_by('o.precoObra','desc');
            }

        }else{
            $this->db->order_by('o.dtCriacao' ,'desc'); //Inc. Alcides -> Adicionado order by
        }

        $this->db->join('tblpgartista pg','o.idPgArtista = pg.idPgArtista');
        $this->db->join('tbltecnicaobras tec','tec.idTecnica = o.idTecnica');
        $query = $this->db->get("tblobras o",$limit_end, $limit_start);

        return $query->result();
    }


    /*
	|--------------------------------------------------------------------------
	| Retorna as obras de acordo com tipo e ordem
	|--------------------------------------------------------------------------		
	*/
    function getTotalObras($ordem = null,$idTipoObra){
        $this->db->select("ROUND(o.somatoriaNotas / o.qtdVotos) as nota,2",false);
        $this->db->where("pg.statusRegistro",1);
        $this->db->where("o.statusRegistro",1);
        $this->db->where("o.idTipoObra",$idTipoObra);

        //Ordenação
        if($ordem != null || $ordem != 0){
            if($ordem == 1){
                $this->db->order_by('o.titulo'.LENG);
            }
            if($ordem == 2){
                $this->db->order_by('nota','desc');
            }
            if($ordem == 3){
                $this->db->order_by('o.precoComDesconto');
            }
            if($ordem == 4){
                $this->db->order_by('o.precoComDesconto','desc');
            }

        }else{
            $this->db->order_by('o.dtCriacao');
        }

        $this->db->join('tblpgartista pg','o.idPgArtista = pg.idPgArtista');
        $query = $this->db->get("tblobras o");
        return $query->num_rows();
    }


    /*
	|--------------------------------------------------------------------------
	| Salva as notas de cada obra
	|--------------------------------------------------------------------------		
	*/
    function salvaNota($nota, $idObra){
        $this->db->select('somatoriaNotas, qtdVotos');
        $this->db->where("statusRegistro",1);
        $this->db->where("idObra",$idObra);
        $query = $this->db->get("tblobras");
        $dadosObra = $query->result();


        if($dadosObra[0]->somatoriaNotas > 0){
            $novaNota = $dadosObra[0]->somatoriaNotas + $nota;
        }else{
            $novaNota = $nota;
        }

        if($dadosObra[0]->qtdVotos > 0){
            $qtdVotos = $dadosObra[0]->qtdVotos + 1;
        }else{
            $qtdVotos = 1;
        }

        $ret = $this->updateNotaObra($novaNota, $qtdVotos, $idObra);
        return $ret;

    }

    /* Atualiza a nosa da obra */
    function updateNotaObra($novaNota, $qtdVotos,$idObra){
        $data = array(
            'somatoriaNotas' => $novaNota,
            'qtdVotos'       => $qtdVotos,
        );

        $this->db->where("statusRegistro",1);
        $this->db->where("idObra",$idObra);
        $ret = $this->db->update('tblobras', $data);
        $this->salvaIpObra($idObra);
        return $ret;
    }

    /* Grava ip e obra  */
    function salvaIpObra($idObra){
        $dateTime       = date("Y-m-d H:i:s");
        $ipUser         = $this->input->ip_address();
        $dataUser       = array(
            'idObra'         => $idObra,
            'userIp'		 => $ipUser,
            'dataCriacao'	 => $dateTime,
            'statusRegistro' => 1
        );

        $ret =$this->db->insert('tblvotos',$dataUser);
        return $ret;
    }

    /* Verifica se a obra ja foi votada */
    function verificaVoto($idObra){
        $ipUser  = $this->input->ip_address();
        $this->db->where("statusRegistro",1);
        $this->db->where("idObra",$idObra);
        $this->db->where("userIp",$ipUser);
        $query = $this->db->get('tblvotos');
        return $query->num_rows();;
    }



    /*
    |--------------------------------------------------------------------------
    | Retorna as obras de acordo com o plano limite 25 (home)
    |--------------------------------------------------------------------------
    */
    function getObrasHome($ordem = null,$limite){
        $this->db->select(' o.idObra,
    						o.idPgArtista,
						    o.idTipoObra,						
						    o.titulo'.LENG.' as titulo,
						    o.idTecnica,
						    o.altura,
						    o.comprimento,
						    o.largura,
						    o.ladoMaior,						   
						    o.descricao'.LENG.' as descricao,
						    o.idFormato,
						    o.precoObra,
						    o.precoComDesconto,
						    o.idCorPred,
						    o.idCategoria1,
						    o.idCategoria2,
						    o.idCategoria3,
						    o.imgFoto1,
						    o.imgFoto2,
						    o.imgFoto3,
						    o.idStatusObra,
						    o.NumVisitas,
						    o.somatoriaNotas,
						    o.qtdVotos,
						    o.dtCriacao,
						    o.dtUltAlteracao,
						    o.slugObra,
						    pg.idPgArtista,
						    pg.nomeArtistico,
						    pg.slugpagina,
						    tec.nomeTecnica'.LENG.' as nomeTecnica,
						    tec.idTecnica,
						    ROUND(o.somatoriaNotas / o.qtdVotos,2) as nota,
						    (SELECT SUM(qtdVotos) FROM tblobras  WHERE idPgArtista = pg.idPgArtista AND statusRegistro = 1 AND pg.statusRegistro = 1) as votosTotais',false);

        $this->db->where("pg.statusRegistro",1);
        $this->db->where("o.statusRegistro",1);
        $this->db->where("con.statusRegistro",1);
        $this->db->where("o.forcaDestaqPgInicial",1);
        $this->db->distinct();

        /*//Ordenação
        if($ordem != null || $ordem != 0){
            if($ordem == 1){
                $this->db->order_by('o.titulo'.LENG);
            }
            if($ordem == 2){
                $this->db->order_by('nota','desc');
            }
            if($ordem == 3){
                $this->db->order_by('o.precoComDesconto');
            }
            if($ordem == 4){
                $this->db->order_by('o.precoComDesconto','desc');
            }

        }else{
            $this->db->order_by('o.dtCriacao');
        }*/

        $this->db->join('tblpgartista pg','o.idPgArtista = pg.idPgArtista');
        $this->db->join('tblcontratos con','con.idUsuario = pg.idUsuario');
        $this->db->join('tbltecnicaobras tec','tec.idTecnica = o.idTecnica');

        /*
        *  Primeira Query que retorna as obras com forcaDestaqPgInicial == 1.
        *  Se essa  query retornar menos de 25 registros, ela faz union com outra
        *  query dos planos prata e/ou bronze
        */
        $queryOuro = $this->db->get("tblobras o",$limite);


        if($queryOuro->num_rows() < $limite){

            $this->db->select(' o.idObra,
    						o.idPgArtista,
						    o.idTipoObra,						
						    o.titulo'.LENG.' as titulo,
						    o.idTecnica,
						    o.altura,
						    o.comprimento,
						    o.largura,
						    o.ladoMaior,						   
						    o.descricao'.LENG.' as descricao,
						    o.idFormato,
						    o.precoObra,
						    o.precoComDesconto,
						    o.idCorPred,
						    o.idCategoria1,
						    o.idCategoria2,
						    o.idCategoria3,
						    o.imgFoto1,
						    o.imgFoto2,
						    o.imgFoto3,
						    o.idStatusObra,
						    o.NumVisitas,
						    o.somatoriaNotas,
						    o.qtdVotos,
						    o.dtCriacao,
						    o.dtUltAlteracao,
						    o.slugObra,
						    pg.idPgArtista,
						    pg.nomeArtistico,
						    pg.slugpagina,
						    tec.nomeTecnica'.LENG.' as nomeTecnica,
						    tec.idTecnica,
						    ROUND(o.somatoriaNotas / o.qtdVotos,2) as nota,
						    (SELECT SUM(qtdVotos) FROM tblobras  WHERE idPgArtista = pg.idPgArtista AND statusRegistro = 1 AND pg.statusRegistro = 1) as votosTotais',false);

            $this->db->where("pg.statusRegistro",1);
            $this->db->where("o.statusRegistro",1);
            $this->db->where("con.statusRegistro",1);
            $this->db->where("tipoP.grupoPlano",1);
            $this->db->where("o.forcaDestaqPgInicial",0);
            $this->db->distinct();

            /* IDs Aleatórios */
            $this->db->order_by('RAND()');
            /*//Ordenação
            if($ordem != null || $ordem != 0){
                if($ordem == 1){
                    $this->db->order_by('o.titulo'.LENG);
                }
                if($ordem == 2){
                    $this->db->order_by('nota','desc');
                }
                if($ordem == 3){
                    $this->db->order_by('o.precoComDesconto');
                }
                if($ordem == 4){
                    $this->db->order_by('o.precoComDesconto','desc');
                }

            }else{
                $this->db->order_by('o.dtCriacao');
            }*/

            $this->db->join('tblpgartista pg','o.idPgArtista = pg.idPgArtista');
            $this->db->join('tblcontratos con','con.idUsuario = pg.idUsuario');
            $this->db->join('tbltipoplano tipoP','con.idTipoPlano = tipoP.idTipoPlano');
            $this->db->join('tbltecnicaobras tec','tec.idTecnica = o.idTecnica');


            /*
            *  Segunda Query que retorna as obras com plano prata.
            *  Se essa  query retornar menos de (25 - query anterior) de registros.
            *  Merge com os resultados das query dos planos ouro e/ou bronze
            */
            $limitePrata = ($limite - $queryOuro->num_rows());
            $queryPrata = $this->db->get("tblobras o",$limitePrata);


            if($queryPrata->num_rows() < $limitePrata){

                $this->db->select(' o.idObra,
    						o.idPgArtista,
						    o.idTipoObra,						
						    o.titulo'.LENG.' as titulo,
						    o.idTecnica,
						    o.altura,
						    o.comprimento,
						    o.largura,
						    o.ladoMaior,						   
						    o.descricao'.LENG.' as descricao,
						    o.idFormato,
						    o.precoObra,
						    o.precoComDesconto,
						    o.idCorPred,
						    o.idCategoria1,
						    o.idCategoria2,
						    o.idCategoria3,
						    o.imgFoto1,
						    o.imgFoto2,
						    o.imgFoto3,
						    o.idStatusObra,
						    o.NumVisitas,
						    o.somatoriaNotas,
						    o.qtdVotos,
						    o.dtCriacao,
						    o.dtUltAlteracao,
						    o.slugObra,
						    pg.idPgArtista,
						    pg.nomeArtistico,
						    pg.slugpagina,
						    tec.nomeTecnica'.LENG.' as nomeTecnica,
						    tec.idTecnica,
						    ROUND(o.somatoriaNotas / o.qtdVotos,2) as nota,
						    (SELECT SUM(qtdVotos) FROM tblobras  WHERE idPgArtista = pg.idPgArtista AND statusRegistro = 1 AND pg.statusRegistro = 1) as votosTotais',false);

                $this->db->where("pg.statusRegistro",1);
                $this->db->where("o.statusRegistro",1);
                $this->db->where("con.statusRegistro",1);
                $this->db->where("tipoP.grupoPlano",0);
                $this->db->where("o.forcaDestaqPgInicial",0);
                $this->db->distinct();

                /* IDs Aleatórios */
                $this->db->order_by('RAND()');
                /*//Ordenação
                if($ordem != null || $ordem != 0){
                    if($ordem == 1){
                        $this->db->order_by('o.titulo'.LENG);
                    }
                    if($ordem == 2){
                        $this->db->order_by('nota','desc');
                    }
                    if($ordem == 3){
                        $this->db->order_by('o.precoComDesconto');
                    }
                    if($ordem == 4){
                        $this->db->order_by('o.precoComDesconto','desc');
                    }

                }else{
                    $this->db->order_by('o.dtCriacao');
                }*/

                $this->db->join('tblpgartista pg','o.idPgArtista = pg.idPgArtista');
                $this->db->join('tblcontratos con','con.idUsuario = pg.idUsuario');
                $this->db->join('tbltipoplano tipoP','con.idTipoPlano = tipoP.idTipoPlano');
                $this->db->join('tbltecnicaobras tec','tec.idTecnica = o.idTecnica');

                /*
                *  Terceira Query que retorna as obras com plano Bronze.
                *  Se essa  query retornar menos de (25 - query anterior) de registros.
                *  Merge com os resultados das query dos planos ouro e prata
                */
                $limiteBronze = ($limite - $queryOuro->num_rows() - $queryPrata->num_rows());
                $queryBronze  = $this->db->get("tblobras o",$limiteBronze);


                $queryOuro   = $queryOuro->result();
                $queryPrata  = $queryPrata->result();
                $queryBronze = $queryBronze->result();
                $queryFinal  = array_merge($queryOuro,$queryPrata,$queryBronze);
                return $queryFinal;

            }else{
                $queryOuro   = $queryOuro->result();
                $queryPrata  = $queryPrata->result();
                $queryFinal  = array_merge($queryOuro,$queryPrata);
                return $queryFinal;
            }


        }else{
            return $queryOuro->result();
        }

    }



    /*
    |--------------------------------------------------------------------------
    | Métodos para listar categorias e filtrar obras
    |--------------------------------------------------------------------------
    */


    /* Retorna a tecnica de acordo com o tipo de obra */
    function getTecnica($filtroTipoObra){
        $this->db->select('idTecnica, nomeTecnica'.LENG.' as nomeTecnica');
        $this->db->where('idTipoObra',$filtroTipoObra);
        $this->db->where('statusRegistro',1);
        $query = $this->db->get('tbltecnicaobras');
        return $query->result();
    }

    /* Retorna o formato de acordo com o tipo de obra */
    function getFormato($filtroTipoObra){
        $this->db->select('idFormato, nomeFormato'.LENG.' as nomeFormato');
        $this->db->where('idTipoObra',$filtroTipoObra);
        $this->db->where('statusRegistro',1);
        $query = $this->db->get('tblformatoobras');
        return $query->result();
    }

    /* Retorna a categoria de acordo com o tipo de obra */
    function getCategoria($filtroTipoObra){
        $this->db->select('idCategoria, nomeCategoria'.LENG.' as nomeCategoria');
        $this->db->where('idTipoObra',$filtroTipoObra);
        $this->db->where('statusRegistro',1);
        $query = $this->db->get('tblcategoriasobras');
        return $query->result();
    }


    /* Retorna as obras de acordo com o filtro */
    function getObrasFiltro($ordem = null, $filtro){

        $this->db->select(' o.idObra,
    						o.idPgArtista,
						    o.idTipoObra,						
						    o.titulo'.LENG.' as titulo,
						    o.idTecnica,
						    o.altura,
						    o.comprimento,
						    o.largura,
						    o.ladoMaior,						   
						    o.descricao'.LENG.' as descricao,
						    o.idFormato,
						    o.precoObra,
						    o.precoComDesconto,
						    o.idCorPred,
						    o.idCategoria1,
						    o.idCategoria2,
						    o.idCategoria3,
						    o.imgFoto1,
						    o.imgFoto2,
						    o.imgFoto3,
						    o.idStatusObra,
						    o.NumVisitas,
						    o.somatoriaNotas,
						    o.qtdVotos,
						    o.dtCriacao,
						    o.dtUltAlteracao,
						    o.slugObra,
						    pg.idPgArtista,
						    pg.nomeArtistico,
						    pg.slugpagina,
                            tec.nomeTecnica'.LENG.' as nomeTecnica,
						    tec.idTecnica,
						    ROUND(o.somatoriaNotas / o.qtdVotos,2) as nota,
						    (SELECT SUM(qtdVotos) FROM tblobras  WHERE idPgArtista = pg.idPgArtista AND statusRegistro = 1 AND pg.statusRegistro = 1) as votosTotais',false);

        $this->db->where("pg.statusRegistro",1);
        $this->db->where("o.statusRegistro",1);

        /* Filtro */

        /* Campo de busca header */
        if(!empty($filtro['busca'])){

            $filtro['busca'] = strtolower($filtro['busca']);

            /* Verifica se o usuario busca por tipo da obra */
            if($filtro['busca'] == 'pintura' || $filtro['busca'] == 'pinturas'){
                $filtro['tipo'] = 1;

            }elseif($filtro['busca'] == 'escultura' || $filtro['busca'] == 'esculturas'){
                $filtro['tipo'] = 2;

            }elseif($filtro['busca'] == 'foto' || $filtro['busca'] == 'fotografia' || $filtro['busca'] == 'fotos' || $filtro['busca'] == 'fotografias'){
                $filtro['tipo'] = 3;

            }else{
                $where = "((titulo".LENG ." LIKE ('%". $filtro['busca'] ."%')) OR (descricao".LENG ." LIKE ('%". $filtro['busca'] ."%')))";
                $this->db->where($where);
            }

        }

        /* Tipo Obra */
        if(!empty($filtro['tipo'])){
            $this->db->where("o.idTipoObra",$filtro['tipo']);
        }

        /* Tamanho */
        if(!empty($filtro['tamanho'])){
            if($filtro['tamanho'] == 1){
                $where = "(o.ladoMaior <= 50.00)";
                $this->db->where($where);
            }elseif($filtro['tamanho'] == 2){
                $where = "(o.ladoMaior BETWEEN 51.00 AND 100.00)";
                $this->db->where($where);
            }elseif ($filtro['tamanho'] == 3) {
                $where = "(o.ladoMaior >= 101.00)";
                $this->db->where($where);
            }
        }

        /* Tecnica */
        if(!empty($filtro['technique'])){
            $this->db->where_in("o.idTecnica",$filtro['technique']);
        }

        /* Formato */
        if(!empty($filtro['format'])){
            $this->db->where_in("o.idFormato",$filtro['format']);
        }

        /* Preço */
        if(!empty($filtro['preco'])){
            //Primeiro verifica se a obra esta a venda
            $statusObrasAvenda = array(1, 4, 7);
            $this->db->where_in('o.idStatusObra',$statusObrasAvenda);

            if($filtro['preco'] == 1){
                $where = "(IF(o.precoComDesconto = 0.00, o.precoObra, o.precoComDesconto) <= 100.00)";
                $this->db->where($where);
            }elseif($filtro['preco'] == 2){
                $where = "(IF(o.precoComDesconto = 0.00, o.precoObra, o.precoComDesconto) BETWEEN 100.00 AND 500.00)";
                $this->db->where($where);
            }elseif ($filtro['preco'] == 3) {
                $where = "(IF(o.precoComDesconto = 0.00, o.precoObra, o.precoComDesconto) BETWEEN 500.00 AND 2500.00)";
                $this->db->where($where);
            }elseif ($filtro['preco'] == 4) {
                $where = "(IF(o.precoComDesconto = 0.00, o.precoObra, o.precoComDesconto) > 2500.00)";
                $this->db->where($where);
            }
        }

        /* Cores */
        if(!empty($filtro['cores'])){

            if($filtro['cores'][0] != 0){
                $where = "((idCorPred in(". implode(',', $filtro['cores']) .")) OR (idCorPred2 in(".  implode(',', $filtro['cores']) .")) OR (idCorPred3 in(".  implode(',', $filtro['cores']) .")))";
                $this->db->where($where);
            }else{
                $codPretoBranco = array(11,9);
                $where = "((idCorPred in(". implode(',', $codPretoBranco) .")) OR (idCorPred2 in(". implode(',', $codPretoBranco) .")) OR (idCorPred3 in(". implode(',', $codPretoBranco) .")))";
                $this->db->where($where);
            }


        }


        /* Categorias */
        if(!empty($filtro['categorias'])){
            $categorias = implode(",", $filtro['categorias']);
            $where = "((idCategoria1 in(". $categorias .")) OR (idCategoria2 in(". $categorias .")) OR (idCategoria3 in(". $categorias .")))";
            $this->db->where($where);
        }

        /* Por artista */
        if(!empty($filtro['porArtista'])){
            $this->db->where("pg.slugpagina",$filtro['porArtista']);
        }


        //Ordenação
        if($ordem != null || $ordem != 0){
            if($ordem == 1){
                $this->db->order_by('o.titulo'.LENG);
            }
            if($ordem == 2){
                $this->db->order_by('nota','desc');
            }
            if($ordem == 3){
                $this->db->order_by('o.precoComDesconto');
            }
            if($ordem == 4){
                $this->db->order_by('o.precoComDesconto','desc');
            }

        }else{
            $this->db->order_by('o.dtCriacao');
        }

        //$this->output->enable_profiler(TRUE);
        $this->db->join('tblpgartista pg','o.idPgArtista = pg.idPgArtista');
        $this->db->join('tbltecnicaobras tec','tec.idTecnica = o.idTecnica');
        $query = $this->db->get("tblobras o");

        return $query->result();
    }


    /*
    |--------------------------------------------------------------------------
    | FIM
    |--------------------------------------------------------------------------
    */




    /*
    |--------------------------------------------------------------------------
    | Método usado para recuperar  os dados da pg artista_home
    |--------------------------------------------------------------------------
    */

    public function getArtistasInfo($slug){
        $limitObra = 3;
        $this->db->select(' o.idObra,
    						o.idPgArtista,
						    o.idTipoObra,						
						    o.titulo'.LENG.' as titulo,
						    o.idTecnica,
						    o.altura,
						    o.comprimento,
						    o.largura,
						    o.ladoMaior,						   
						    o.descricao'.LENG.' as descricao,
						    o.idFormato,
						    o.precoObra,
						    o.precoComDesconto,
						    o.idCorPred,
						    o.idCategoria1,
						    o.idCategoria2,
						    o.idCategoria3,
						    o.imgFoto1,
						    o.imgFoto2,
						    o.imgFoto3,
						    o.idStatusObra,
						    o.NumVisitas,
						    o.somatoriaNotas,
						    o.qtdVotos,
						    o.dtCriacao,
						    o.dtUltAlteracao,
						    o.slugObra,
						    pg.idPgArtista,
						    pg.nomeArtistico,
						    pg.slugpagina,
						    pg.subtituloPg'.LENG.' as subtitulo,
						    pg.idBiografia,
						    pg.txtMsgVisitante'.LENG.' as txtMsgVisitante,
						    pg.imgPerfil,
						    pg.imgBannerPg,
						    pg.facebookID,
						    pg.twitterID,
						    pg.googlePlusID,
						    pg.dtCriacao,
						    bio.imgBiografia,
						    bio.txtBiografia'.LENG.' as txtBiografia,
						    u.telefoneCelular,
						    u.idUsuario,
						    ud.cidade,
						    ud.estado,
						    tec.nomeTecnica'.LENG.' as nomeTecnica,
						    tec.idTecnica,
						    ROUND(o.somatoriaNotas / o.qtdVotos,2) as nota',false);

        $this->db->where("pg.statusRegistro",1);
        $this->db->where("ud.statusRegistro",1);
        $this->db->where("u.statusRegistro",1);
        $this->db->where("o.statusRegistro",1);
        $this->db->where("pg.slugpagina",$slug);
        $this->db->order_by("o.NumVisitas", 'desc');
        $this->db->join('(SELECT * FROM tblobras o WHERE o.statusRegistro = 1) as o','o.idPgArtista = pg.idPgArtista','left');
        $this->db->join('tblusuarios u','u.idUsuario = pg.idUsuario');
        $this->db->join('tblusuariosdados ud','ud.idUsuario = u.idUsuario');
        $this->db->join('tblbiografiaartista bio','bio.idBiografia = pg.idBiografia');
        $this->db->join('tbltecnicaobras tec','tec.idTecnica = o.idTecnica');
        $query = $this->db->get("tblpgartista pg",$limitObra);

        return $query->result();
    }


    function getArtistaInfoSemObra($slug){
      $query = $this->db->query("SELECT  pg.idPgArtista,
                                pg.nomeArtistico,
                                pg.slugpagina,
                                pg.subtituloPgPT as subtitulo,
                                pg.idBiografia,
                                pg.txtMsgVisitantePT as txtMsgVisitante,
                                pg.imgPerfil,
                                pg.imgBannerPg,
                                pg.facebookID,
                                pg.twitterID,
                                pg.googlePlusID,
                                pg.dtCriacao,
                                bio.imgBiografia,
                                bio.txtBiografiaPT as txtBiografia,
                                u.telefoneCelular,
                                u.idUsuario,
                                ud.cidade,
                                ud.estado
                                FROM (tblpgartista pg)
                                JOIN tblusuarios u ON u.idUsuario = pg.idUsuario
                                JOIN tblusuariosdados ud ON ud.idUsuario = u.idUsuario
                                JOIN tblbiografiaartista bio ON bio.idBiografia = pg.idBiografia
                                WHERE pg.statusRegistro =  1
                                AND ud.statusRegistro =  1
                                AND u.statusRegistro =  1
                                AND pg.slugpagina = '$slug'
                                LIMIT 3;  "
      );
        return $query->result();
    }



    /*
    |--------------------------------------------------------------------------
    | Método usado para recuperar  os dados das obras dos artistas
    |--------------------------------------------------------------------------
    */

    public function getArtistasObras($slug, $limit_end, $limit_start){

        $this->db->select(' o.idObra,
    						o.idPgArtista,
						    o.idTipoObra,						
						    o.titulo'.LENG.' as titulo,
						    o.idTecnica,
						    o.altura,
						    o.comprimento,
						    o.largura,
						    o.ladoMaior,						   
						    o.descricao'.LENG.' as descricao,
						    o.idFormato,
						    o.precoObra,
						    o.precoComDesconto,
						    o.idCorPred,
						    o.idCategoria1,
						    o.idCategoria2,
						    o.idCategoria3,
						    o.imgFoto1,
						    o.imgFoto2,
						    o.imgFoto3,
						    o.idStatusObra,
						    o.NumVisitas,
						    o.somatoriaNotas,
						    o.qtdVotos,
						    o.dtCriacao,
						    o.dtUltAlteracao,
						    o.slugObra,
						    pg.idPgArtista,
						    pg.nomeArtistico,
						    pg.slugpagina,
						    pg.subtituloPg'.LENG.' as subtitulo,
						    pg.idBiografia,
						    pg.txtMsgVisitante'.LENG.' as txtMsgVisitante,
						    pg.imgPerfil,
						    pg.imgBannerPg,
						    pg.facebookID,
						    pg.twitterID,
						    pg.googlePlusID,
						    pg.dtCriacao,
						    bio.imgBiografia,
						    bio.txtBiografia'.LENG.' as txtBiografia,
						    u.telefoneCelular,
						    ud.cidade,
						    ud.estado,
						    tec.nomeTecnica'.LENG.' as nomeTecnica,
						    tec.idTecnica,
						    ROUND(o.somatoriaNotas / o.qtdVotos,2) as nota',false);

        $this->db->where("pg.statusRegistro",1);
        $this->db->where("ud.statusRegistro",1);
        $this->db->where("u.statusRegistro",1);
        $this->db->where("pg.slugpagina",$slug);
        $this->db->order_by("o.dtCriacao");

        $this->db->join('(SELECT * FROM tblobras o WHERE o.statusRegistro = 1) as o','o.idPgArtista = pg.idPgArtista','left');
        $this->db->join('tblusuarios u','u.idUsuario = pg.idUsuario');
        $this->db->join('tblusuariosdados ud','ud.idUsuario = u.idUsuario');
        $this->db->join('tblbiografiaartista bio','bio.idBiografia = pg.idBiografia');
        $this->db->join('tbltecnicaobras tec','tec.idTecnica = o.idTecnica');
        $query = $this->db->get("tblpgartista pg",$limit_end, $limit_start);


        return $query->result();
    }

    /*
|--------------------------------------------------------------------------
| Método usado para recuperar  o total das obras dos artistas
|--------------------------------------------------------------------------		
*/

    public function getTotalArtistasObras($slug){

        $this->db->select(' o.idObra',false);
        $this->db->where("pg.statusRegistro",1);
        $this->db->where("ud.statusRegistro",1);
        $this->db->where("u.statusRegistro",1);
        $this->db->where("pg.slugpagina",$slug);
        $this->db->order_by("o.dtCriacao");

        $this->db->join('(SELECT * FROM tblobras o WHERE o.statusRegistro = 1) as o','o.idPgArtista = pg.idPgArtista','left');
        $this->db->join('tblusuarios u','u.idUsuario = pg.idUsuario');
        $this->db->join('tblusuariosdados ud','ud.idUsuario = u.idUsuario');
        $this->db->join('tblbiografiaartista bio','bio.idBiografia = pg.idBiografia');
        $query = $this->db->get("tblpgartista pg");

        return $query->num_rows();
    }



    /*
    |--------------------------------------------------------------------------
    | Método usado para recuperar  os dados das obras dos artistas (FILTRO)
    |--------------------------------------------------------------------------
    */

    public function getObrasArtistaFiltro($filtro, $slug){

        $this->db->select(' o.idObra,
    						o.idPgArtista,
						    o.idTipoObra,						
						    o.titulo'.LENG.' as titulo,
						    o.idTecnica,
						    o.altura,
						    o.comprimento,
						    o.largura,
						    o.ladoMaior,						   
						    o.descricao'.LENG.' as descricao,
						    o.idFormato,
						    o.precoObra,
						    o.precoComDesconto,
						    o.idCorPred,
						    o.idCategoria1,
						    o.idCategoria2,
						    o.idCategoria3,
						    o.imgFoto1,
						    o.imgFoto2,
						    o.imgFoto3,
						    o.idStatusObra,
						    o.NumVisitas,
						    o.somatoriaNotas,
						    o.qtdVotos,
						    o.dtCriacao,
						    o.dtUltAlteracao,
						    o.slugObra,
						    pg.idPgArtista,
						    pg.nomeArtistico,
						    pg.slugpagina,
						    pg.subtituloPg'.LENG.' as subtitulo,
						    pg.idBiografia,
						    pg.txtMsgVisitante'.LENG.' as txtMsgVisitante,
						    pg.imgPerfil,
						    pg.imgBannerPg,
						    pg.facebookID,
						    pg.twitterID,
						    pg.googlePlusID,
						    pg.dtCriacao,
						    bio.imgBiografia,
						    bio.txtBiografia'.LENG.' as txtBiografia,
						    u.telefoneCelular,
						    ud.cidade,
						    ud.estado,						   
						    ROUND(o.somatoriaNotas / o.qtdVotos,2) as nota',false);

        $this->db->where("pg.statusRegistro",1);
        $this->db->where("ud.statusRegistro",1);
        $this->db->where("u.statusRegistro",1);
        $this->db->where("pg.slugpagina",$slug);
        $this->db->order_by("o.dtCriacao");


        /* filtros */

        //Tamanho
        if(!empty($filtro['tamanho'])){
            if($filtro['tamanho'] == 1){
                $this->db->where("o.altura <=",50.00);
            }elseif($filtro['tamanho'] == 2){
                $this->db->where('o.altura BETWEEN ' . 51.00 . ' AND ' . 100.00);
            }elseif ($filtro['tamanho'] == 3) {
                $this->db->where("o.altura >",101.00);
            }
        }
        //Preço
        if(!empty($filtro['preco'])){
            if($filtro['preco'] == 1){
                $this->db->where("o.precoComDesconto <=",100.00);
            }elseif($filtro['preco'] == 2){
                $this->db->where('o.precoComDesconto BETWEEN ' . 100.00 . ' AND ' . 500.00);
            }elseif ($filtro['preco'] == 3) {
                $this->db->where('o.precoComDesconto BETWEEN ' . 500.00 . ' AND ' . 2500.00);
            }elseif ($filtro['preco'] == 4) {
                $this->db->where("o.precoComDesconto >",2501.00);
            }
        }


        $this->db->join('(SELECT * FROM tblobras o WHERE o.statusRegistro = 1) as o','o.idPgArtista = pg.idPgArtista','left');
        $this->db->join('tblusuarios u','u.idUsuario = pg.idUsuario');
        $this->db->join('tblusuariosdados ud','ud.idUsuario = u.idUsuario');
        $this->db->join('tblbiografiaartista bio','bio.idBiografia = pg.idBiografia');
        $query = $this->db->get("tblpgartista pg");

        return $query->result();
    }


    /*
    |--------------------------------------------------------------------------
    | Método que retorna os detalhes de uma obra de acordo com o id ou slug
    |--------------------------------------------------------------------------
    */
    public function getDetalheObra($ident){
        $this->db->select(' o.idObra,
    						o.idPgArtista,
						    o.idTipoObra,						
						    o.titulo'.LENG.' as titulo,
						    o.idTecnica,
						    o.altura,
						    o.comprimento,
						    o.largura,
						    o.ladoMaior,						   
						    o.descricao'.LENG.' as descricao,
						    o.idFormato,
						    o.precoObra,
						    o.precoComDesconto,
						    o.idCorPred,
						    o.idCategoria1,
						    o.idCategoria2,
						    o.idCategoria3,
						    o.imgFoto1,
						    o.imgFoto2,
						    o.imgFoto3,
						    o.idStatusObra,
						    o.NumVisitas,
						    o.somatoriaNotas,
						    o.qtdVotos,
						    o.dtCriacao,
						    o.dtUltAlteracao,
						    o.slugObra,
						    pg.idPgArtista,
						    pg.nomeArtistico,
						    pg.slugpagina,
						    pg.subtituloPg'.LENG.' as subtitulo,
						    pg.idBiografia,
						    pg.txtMsgVisitante'.LENG.' as txtMsgVisitante,
						    pg.imgPerfil,
						    pg.imgBannerPg,
						    pg.facebookID,
						    pg.twitterID,
						    pg.googlePlusID,
						    pg.dtCriacao,						    
						    u.telefoneCelular,
						    ud.cidade,
						    ud.estado,
						    u.email,
						    u.idUsuario,
						    f.nomeFormato'.LENG.' as nomeFormato,
						    tec.nomeTecnica'.LENG.' as nomeTecnica,
						    tipo.nomeTipoObra,
						    ROUND(o.somatoriaNotas / o.qtdVotos,2) as nota,
						    (SELECT SUM(qtdVotos) FROM tblobras  WHERE idPgArtista = pg.idPgArtista AND statusRegistro = 1 AND pg.statusRegistro = 1) as votosTotais',false);

        $this->db->where("pg.statusRegistro",1);
        $this->db->where("ud.statusRegistro",1);
        $this->db->where("u.statusRegistro",1);
        $this->db->where("o.statusRegistro",1);

        //$this->db->where("o.idObra",$ident);
        $this->db->where("o.slugobra",$ident);
        $this->db->join('tblpgartista pg','pg.idPgArtista = o.idPgArtista');
        $this->db->join('tblusuarios u','u.idUsuario = pg.idUsuario');
        $this->db->join('tblusuariosdados ud','ud.idUsuario = u.idUsuario');
        $this->db->join('tbltecnicaobras tec','tec.idTecnica = o.idTecnica');
        $this->db->join('tblformatoobras f','f.idFormato = o.idFormato');
        $this->db->join('tbltipoobras tipo','tipo.idTipoObra = o.idTipoObra');
        $query = $this->db->get("tblobras o");

        return $query->result();
    }



    /*
    |--------------------------------------------------------------------------
    | Grava o contado do usuario com o artista
    |--------------------------------------------------------------------------
    */
    public function insertContato($data){
        $ret = $this->db->insert('tblcontato',$data);
        if($ret == false)
            throw new Exception("Ocorreu um erro ao inserir os dados no banco");
        return $ret;
    }

    /*
    |--------------------------------------------------------------------------
    | Grava o email e nome para receber newsletter
    |--------------------------------------------------------------------------
    */
    public function insertNewsLetter($data){
        $ret = $this->db->insert('tblnewsletter',$data);
        if($ret == false)
            throw new Exception("Ocorreu um erro ao inserir os dados no banco");
        return $ret;
    }



    /*
    |--------------------------------------------------------------------------
    | Recebe as categorias dos parceiros
    |--------------------------------------------------------------------------
    */
    function getCategoriaParceiro(){
        $this->db->select('idCategoriaParceiro, nomeCategoriaParc'.LENG.' as nomeCategoria');
        $this->db->where('statusRegistro',1);
        $query = $this->db->get('tblcategoriaparceiros');
        return $query->result();
    }


    /*
    |--------------------------------------------------------------------------
    | Recebe os parceiros
    |--------------------------------------------------------------------------
    */
    function getParceiro($filtro = null){
        $this->db->select('p.idParceiro,
							p.nome, 
							p.cidade, 
							p.estado, 
							p.urlSite, 
							p.email, 
							p.telefone, 
							p.idCategoriaParceiro, 
							p.imgLogo, 
							p.imgProduto1, 
							p.imgProduto2, 
							p.imgProduto3,
							p.imgProduto4, 
							p.imgProduto5, 
							p.dtCriacao, 
							p.dtUltAlteracao,
							catp.nomeCategoriaParc'.LENG.' as nomeCategoria,
							p.statusRegistro');

        if($filtro != null){
            $this->db->where('p.idCategoriaParceiro',$filtro);
        }
        $this->db->where('p.statusRegistro',1);
        $this->db->where('catp.statusRegistro',1);
        $this->db->join('tblcategoriaparceiros catp','catp.idCategoriaParceiro = p.idCategoriaParceiro');
        $query = $this->db->get('tblparceiros p');
        return $query->result();
    }


    /*
    |--------------------------------------------------------------------------
    | Recebe os a quantide de registros dos parceiros
    |--------------------------------------------------------------------------
    */
    function getTotalParceiro($filtro = null){
        $this->db->select('idParceiro, catp.nomeCategoriaParc'.LENG.' as nomeCategoria');
        if($filtro != null){
            $this->db->where('p.idCategoriaParceiro',$filtro);
        }
        $this->db->where('p.statusRegistro',1);
        $this->db->where('catp.statusRegistro',1);
        $this->db->join('tblcategoriaparceiros catp','catp.idCategoriaParceiro = p.idCategoriaParceiro');
        $query = $this->db->get('tblparceiros p');
        return $query->num_rows();
    }


    /*
	|--------------------------------------------------------------------------
	| Atualiza a quantide de visitas em cada obra
	|--------------------------------------------------------------------------
	*/
    function somaVisitas($idObra){
        $this->db->select('NumVisitas');
        $this->db->where('idObra',$idObra);
        $query = $this->db->get('tblobras');
        $num = $query->result();
        $numVisitas =  $num[0]->NumVisitas;
        $numVisitas ++ ;
        $data = array(
            'NumVisitas' => $numVisitas
        );

        $this->db->where('idObra',$idObra);
        $ret = $this->db->update('tblobras', $data);
        return $ret;
    }


    /*
	|--------------------------------------------------------------------------
	| Recebe os dados da pagina de biografia
	|--------------------------------------------------------------------------
	*/
    function getBiografia($slug){
        $this->db->select(' pg.idPgArtista,
                            pg.nomeArtistico,
                            pg.slugpagina,
                            pg.subtituloPg'.LENG.' as subtitulo,
						    pg.idBiografia,
						    pg.txtMsgVisitante'.LENG.' as txtMsgVisitante,
						    pg.imgPerfil,
						    pg.imgBannerPg,
						    pg.facebookID,
						    pg.twitterID,
						    pg.googlePlusID,
						    pg.dtCriacao,
						    u.telefoneCelular,
						    ud.cidade,
						    ud.estado,
						    u.email,
						    u.email,
						    bio.txtBiografia'.LENG.' as txtBiografia,
						    u.idUsuario,
						    pg.imgPerfil,
						    p.nome as nomePais,
						    p.iso,
						    (SELECT SUM(qtdVotos) FROM tblobras  WHERE idPgArtista = pg.idPgArtista AND statusRegistro = 1 AND pg.statusRegistro = 1) as votosTotais', true);

        $this->db->where('pg.statusRegistro',1);
        $this->db->where('u.statusRegistro',1);
        $this->db->where('ud.statusRegistro',1);
        $this->db->where('pg.statusRegistro',1);
        $this->db->where('pg.slugpagina',$slug);

        $this->db->join('tblusuariosdados ud','ud.idUsuario = pg.idUsuario');
        $this->db->join('tblbiografiaartista bio','bio.idBiografia = pg.idBiografia');
        $this->db->join('tblusuarios u','u.idUsuario = ud.idUsuario');
        $this->db->join('tblgeralpais p','p.idPais = u.idPaisResidente');
        $query = $this->db->get('tblpgartista pg');
        return $query->result();
    }



    /* Retorna o total de obras de um determinado artista */
    function totalObrasByArtista($idPgArtista){
        $this->db->where('statusRegistro',1);
        $this->db->where('idPgArtista',$idPgArtista);
        $query = $this->db->get('tblobras');
        return $query->num_rows();
    }


    /* Retorna o total de obras agrupado por tipos */
    function totalObrasByTipo($idPgArtista){
        $query = $this->db->query("SELECT count(*) as qtd, o.idTipoObra, t.nomeTipoObra FROM tblobras o
                                    JOIN tbltipoobras t ON o.idTipoObra = t.idTipoObra
                                    WHERE idPgArtista = $idPgArtista
                                    GROUP BY idTipoObra;");
        return $query->result();
    }

    /* Retorna o preço médio das obras dos artistas */
    function precoMedioObras($idPgArtista){
        $query = $this->db->query("SELECT avg(precoObra) as media FROM tblobras o WHERE idPgArtista = $idPgArtista;");
        return $query->result();
    }

    /**
     * Sets on the session the accessToken
     * @param string email
     * @param string '
     * @return bool, array
     */
    function login($email, $senha)
    {
        $hash = '!a8v7b1n3m8A9!0s#b';
        $this->db->where('email', $email);
        $query = $this->db->get('tblusuarios');

        if ($query->num_rows() > 0){
            $senhaHash = substr(md5($senha.$hash), 0,20);
            $senhaBanco = $query->result_array()[0]['senha'];

            if ($senhaBanco != $senhaHash)
                return false;

            session_start();
            $_SESSION['usuario_logado'] = $query->result_array()[0]['email'];

            $dadosUser['idUsuario'] = $query->result_array()[0]['idUsuario'];
            return $dadosUser;
        }

        return false;
    }

    /**
     * Verifica se o usuario terminou o cadastro
     * @param int idUsuario
     * @return bool, array
     */
    function verificaCadastro($idUsuario){
        $this->db->where('statusRegistro', 1);
        $this->db->where('idUsuario', $idUsuario);
        $queryUser = $this->db->get('tblusuariosdados');

        if($queryUser->num_rows() > 0){
            $this->db->where('idUsuario', $idUsuario);
            $this->db->where('statusRegistro', 1);
            $queryContrato = $this->db->get('tblcontratos');
            if($queryContrato->num_rows() > 0){
                //terminou o cadastro
                return true;
            }else{
                //Só foi até a segunda tela de cadastro
                $_SESSION['usuario_logado'] = false;
                $status['cadastro'] = 'segundoCadastro';
                return $status['cadastro'];
            }
        }else{
            //Só foi até a primeira tela de cadastro
            $_SESSION['usuario_logado'] = false;
            $status['cadastro'] = 'primeiroCadastro';
            return $status['cadastro'];
        }

    }

    /**
     * @param string email
     * @return bool
     */
    function emailExists($email)
    {
        $this->db->where('statusRegistro', 1);
        $this->db->where('email', $email);
        $query = $this->db->get('tblusuarios');
        return $query->num_rows() > 0;
    }

    /**
     * @param string email
     * @param datetime datetime
     * @return array
     */
    function getNewSenha($email, $dateTime)
    {
        $novaSenha = $this->geraSenha(6, true, true, true);
        $hash          = '!a8v7b1n3m8A9!0s#b';

        $data = array(
            'senha' => md5($novaSenha.$hash),
            'dtUltAlteracao' => $dateTime,
        );
        $this->db->where('email', $email);
        if($this->db->update('tblusuarios', $data)){
            /*Recupera o nome do usuario */
            $this->db->select('ud.nomeCompleto');
            $this->db->where('u.email', $email);
            $this->db->join('tblusuariosdados ud','ud.idUsuario = u.idUsuario');
            $query = $this->db->get('tblusuarios u');

            $dadosUser['novaSenha']   = $novaSenha;
            $dadosUser['dadosArtista'] = $query->result();
            return $dadosUser;
        }

        return false;
    }


    /**
     * Função para gerar senhas aleatórias
     *
     * @author    Thiago Belem <contato@thiagobelem.net>
     *
     * @param integer $tamanho Tamanho da senha a ser gerada
     * @param boolean $maiusculas Se terá letras maiúsculas
     * @param boolean $numeros Se terá números
     * @param boolean $simbolos Se terá símbolos
     *
     * @return string A senha gerada
     */
    function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
    {
        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num = '1234567890';
        $simb = '!@#$%*-';
        $retorno = '';
        $caracteres = '';

        $caracteres .= $lmin;
        if ($maiusculas) $caracteres .= $lmai;
        if ($numeros) $caracteres .= $num;
        if ($simbolos) $caracteres .= $simb;

        $len = strlen($caracteres);
        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand-1];
        }
        return $retorno;
    }


    /**
     * @param int idUsuario
     * @param string tabela
     * @return bool
     */
    function verificaDuplicidadeCadastro($idUsuario, $tabela)
    {
        $this->db->where('statusRegistro', 1);
        $this->db->where('idUsuario', $idUsuario);
        $query = $this->db->get($tabela);
        return $query->num_rows() > 0;
    }

    /**
     * @param int idUsuario
     * @param string tabela
     * @return bool
     */
    function apagaDuplicidade($idUsuario, $tabela)
    {
        $this->db->where('statusRegistro', 1);
        $this->db->where('idUsuario', $idUsuario);
        $this->db->delete($tabela);
    }

}

?>
