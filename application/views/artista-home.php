<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8"> 

		<?php include('includes/meta.php'); ?>
		
	</head>
	
	<body>

		
		<?php 				
				$data['artista'] = $artista;
 				$this->load->view('includes/header',$data); 
		?>
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php echo base_url(); ?>">Home</a></li>
					<li><a href="<?php base_url(); ?>/artistas">Artistas</a></li>
					<li><?php if(!empty($artista)){echo $artista[0]->nomeArtistico;}else{echo "Artista não encontrado";}  ?></li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php 				
				$data['artista']       = $artista;
 				$this->load->view('includes/sidebar',$data); 
			?>

			<div class="content">
				
				<div class="profile-banner">
					<?php if (isset($artista[0]->imgBiografia)) { ?>
						<img src="<?php echo base_url(); ?>content/upload/<?php echo SLUG . '/fp/fp-large/' . $artista[0]->imgBiografia; ?>" alt="Imagem destacada de <?php echo $artista[0]->nomeArtistico; ?>" />
					<?php } else { ?>
						<img src="<?php echo base_url(); ?>content/images/profile.png" alt="Imagem destacada de <?php echo $artista[0]->nomeArtistico; ?>" />
					<?php } ?> 
				</div> <!-- /profile-banner -->

				<section class="main">
					<div class="profile-about">
						<h2 class="title"><?php echo $artista[0]->nomeArtistico; ?></h2>
						<p>"<?php echo $artista[0]->txtMsgVisitante; ?>"</p>
					</div> <!-- /profile-about -->
					
					<?php if(empty($artista[0]->idObra)){ ?>
						<br />
						
						<h2 class="title">Obras mais vistas</h2>
						<div class="no-post">
							<span class="big-icon gray-icon">
								<i class="icon icon-images"></i>
							</span>
							<h3>Esse artista ainda não cadastrou <br />nenhuma obra no <strong>Mercado Arte</strong>.</h3>
							<p>Navegue pelas obras e encontre a obra perfeita.</p>
							<a href="/obras" class="button rounded big-button">Navegar pelas obras</a>
						</div> <!-- /no-post -->
					<?php } else { ?>


					<?php if(isset($artista[0]->idObra) && $artista[0]->idObra != null){ ?>
						<h2 class="title">Obras mais vistas</h2>
						
						<section class="list js-filter-scroll">
								<?php foreach ($artista as $a) { ?>
									
									<article class="art art-work">
										<a href="<?php echo base_url() . SLUG; ?>/obras/<?php echo $a->slugObra; ?>">
											<div class="thumbnail">
												<img src="<?php echo base_url(); ?>content/upload/<?php echo SLUG ?>/<?php echo $a->slugObra; ?>/art-medium/<?php echo $a->imgFoto1; ?>" alt="<?php echo $a->titulo; ?>" />
											</div> <!-- /thumbnail -->
											
											<h4 class="art-title"><?php echo $a->titulo; ?></h4>
											<h5 class="description"><?php echo $a->nomeTecnica; ?> - <?php echo number_format($a->altura, 0); ?> x <?php echo number_format($a->comprimento, 0); if(!empty($a->largura)){ echo ' x '. number_format($a->largura, 0);} ?> cm</h5>

											<div class="price">
                                                <?php if($a->idStatusObra == 2 || $a->idStatusObra == 5 || $a->idStatusObra == 8){ ?>
                                                    <p class="soldout">Vendido</p>
                                                <?php }else if($a->idStatusObra == 3 || $a->idStatusObra == 6 || $a->idStatusObra == 9){ ?>
                                                    <a href="<?php echo base_url(). $a->slugpagina; ?>/obras/<?php echo $a->slugObra; ?>?section=consultar" class="consult">Consulte</a>
                                                <?php }else{ ?>
                                                    <?php if($a->precoComDesconto == 0.00){ ?>
                                                        <p class="descount">R$ <?php echo number_format((int)str_ireplace(".",",",$a->precoObra), 2, ',', '.'); ?></p>
                                                    <?php }else{ ?>
                                                        <small class="value">De R$ <?php echo number_format((int)str_ireplace(".",",",$a->precoObra), 2, ',', '.'); ?></small>
                                                        <p class="descount">R$ <?php echo number_format((int)str_ireplace(".",",",$a->precoComDesconto), 2, ',', '.'); ?></p>
                                                    <?php } ?>
                                                <?php } ?>
											</div> <!-- /price -->
										</a>

										<div class="rating">
											<ul class="star-rating" id="star-rating-<?php echo $a->idObra; ?>">
											<?php 
												$notas = round($a->nota);

												for ($i=0; $i < 5; $i++) { ?>
													<li class="star-number-<?php echo $i + 1; ?><?php if($notas > 0){ echo " active"; } ?>">
														<a href="#" class="icon-vote" data-idObra="<?php echo $a->idObra; ?>" data-icon="<?php echo $i; ?>"><i class="icon icon-star"></i>
															<span><?php echo $i + 1; ?></span>
														</a>
													</li>
												
											<?php 
												$notas--;
												}

											?>		
											</ul>
										</div> <!-- /rating -->
									</article>

							<?php } } } ?>

					</section> <!-- /list -->

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>