<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
		
	</head>
	  
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>">Home</a></li>
					<li>Erro</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<section class="main search-result">
                <h3 class="title">404</h3>
                <div class="no-post">
                    <span class="big-icon gray-icon">
						<i class="icon icon-cross icon-cross-big"></i>
                    </span>
                    <h3>Desculpe, mas a página que você procura <br><strong>não foi encontrada</strong> em nosso sistema.</h3>
                    <p>Tente novamente ou navegue pelo site <br>e conheça todas nossas obras e artistas.</p>

                    <a href="/obras" class="button rounded big-button">Navegar pelas obras</a>

                </div> <!-- /no-post -->
			</section>

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>
