<!DOCTYPE HTML>
<html lang="pt-BR">
	<?php 
			/* Exibe o nome da pagina atual */
			$ocultaPaginacao = false;
			$url = $this->uri->segment("2");
			(!empty($url)? $nomePagina = $url : $nomePagina = "Pinturas");				
			$nomePagina = ucfirst($nomePagina);	
			if($nomePagina == "Filtrar"){
				$nomePagina = "Obras";
				$ocultaPaginacao = true;
			}
	?>

	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>				

		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>">Home</a></li>
					<li><?php echo $nomePagina; ?></li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php 
				
				$data['tecnicaObra']   = $tecnicaObra;
				$data['formatoObra']   = $formatoObra;
				$data['categoriaObra'] = $categoriaObra;
				$this->load->view('includes/sidebar',$data); 
			?>

			<div class="content">
				<section class="main">

					<h3 class="title page-title"><?php echo $nomePagina; ?></h3>

                    <?php 
                    	if(!empty($_GET['ordem'])){ 
                    		$filtro = $_GET['ordem'];
                    	}
                    	if($this->uri->segment(2) != 'filtrar'){ ?>
                        <div class="filter-order">
                            <label for="order-by">Ordenar por:</label>
                            <select name="order-by" id="order-by" class="select-no-appearance js-change-order">
                                <option class="option-order" value="1"<?php if(!empty($_GET['ordem']) && $filtro == 1){ echo 'selected'; } ?>>Nome</option>
                                <!-- <option class="option-order" value="2"<?php if(!empty($_GET['ordem']) && $filtro == 2){ echo 'selected'; } ?>>Avaliação</option> -->
                                <option class="option-order" value="3"<?php if(!empty($_GET['ordem']) && $filtro == 3){ echo 'selected'; } ?>>Menor Preço</option>
                                <option class="option-order" value="4"<?php if(!empty($_GET['ordem']) && $filtro == 4){ echo 'selected'; } ?>>Maior Preço</option>
                            </select>
                            <i class="loading-filter icon-animated icon icon-loader-2"></i>
                        </div>
                    <?php } ?>

					<section class="list js-filter-scroll">
						<?php if(!empty($obras)){ ?>
						<?php $num =0; ?>
						<?php foreach ($obras as $o) { ?>
							<?php $num++; ?>
							<article class="art<?php if($num % 5 == 0){echo " no-margin";} ?>">
								<a href="<?php echo base_url() . $o->slugpagina; ?>/obras/<?php echo $o->slugObra; ?>">
									<div class="thumbnail">
										<img src="<?php echo base_url(); ?>content/upload/<?php echo $o->slugpagina; ?>/<?php echo $o->slugObra; ?>/small/<?php echo $o->imgFoto1; ?>" alt="<?php echo $o->titulo; ?>" />
									</div> <!-- /thumbnail -->
									
									<h4 class="artist">
										<?php echo $o->nomeArtistico; ?>
										<span class="badge <?php if($o->votosTotais >= 0 && $o->votosTotais <= 99){ echo ''; }elseif($o->votosTotais >= 100 && $o->votosTotais <= 999 ){echo 'sprite-badge-bronze'; }elseif($o->votosTotais >= 1000 && $o->votosTotais <= 9999){echo 'sprite-badge-silver';}elseif($o->votosTotais >= 10000){echo 'sprite-badge-gold';} ?>"></span>
									</h4>
									<h5 class="description"><?php echo $o->titulo; ?> - <?php echo $o->nomeTecnica; ?> - <?php echo number_format($o->altura, 0); ?> x <?php echo number_format($o->comprimento, 0); if(!empty($o->largura)){ echo ' x '. number_format($o->largura, 0);} ?> cm</h5>

									<div class="price">
                                        <?php if($o->idStatusObra == 2 || $o->idStatusObra == 5 || $o->idStatusObra == 8){ ?>
                                            <p class="soldout">Vendido</p>
                                        <?php }else if($o->idStatusObra == 3 || $o->idStatusObra == 6 || $o->idStatusObra == 9){ ?>
                                            <a href="<?php echo base_url(). $o->slugpagina; ?>/obras/<?php echo $o->slugObra; ?>?section=consultar" class="consult">Consulte</a>
                                        <?php }else{ ?>
                                            <?php if($o->precoComDesconto == 0.00){ ?>
                                                <p class="descount">R$ <?php echo number_format((int)str_ireplace(".",",",$o->precoObra), 2, ',', '.'); ?></p>
                                            <?php }else{ ?>
                                                <small class="value">De R$ <?php echo number_format((int)str_ireplace(".",",",$o->precoObra), 2, ',', '.'); ?></small>
                                                <p class="descount">R$ <?php echo number_format((int)str_ireplace(".",",",$o->precoComDesconto), 2, ',', '.'); ?></p>
                                            <?php } ?>
                                        <?php } ?>
									</div> <!-- /price -->
								</a>

								<div class="rating">
									<ul class="star-rating" id="star-rating-<?php echo $o->idObra; ?>">
									<?php 
										$notas = round($o->nota);
										for ($i=0; $i < 5; $i++) { 
									?>
										<li class="star-number-<?php echo $i + 1; ?><?php if($notas > 0){ echo " active"; } ?>">
											<a href="#" class="icon-vote" data-idObra="<?php echo $o->idObra; ?>" data-icon="<?php echo $i; ?>"><i class="icon icon-star"></i>
												<span><?php echo $i + 1; ?></span>
											</a>
										</li>
									<?php 
										$notas--;
										}
									?>		
									</ul>
								</div> <!-- /rating -->
							</article>						
						
						<?php } } else { ?>	

						<div class="no-post">
							<span class="big-icon gray-icon">
								<i class="icon icon-images"></i>
							</span>
							<h3>Não existe nenhuma obra cadastrada no <br /><strong>Mercado Arte</strong> que se encaixe neste filtro.</h3>
							<p>Selecione uma categoria e tente novamente!</p>
						</div> <!-- /no-post -->

						<?php } ?>
						<br class="clear" />

						<ul class="pagination">							
							<?php if(!$ocultaPaginacao){ echo $this->pagination->create_links(); }   ?>													
						</ul>

					</section> <!-- /list -->

				</section>
				
			</div>

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>