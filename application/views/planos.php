<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>/">Home</a></li>
					<li><a href="<?php base_url(); ?>/cadastro">Cadastro</a></li>
					<li>Planos</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php
                $data['tecnicaObra']   = $tecnicaObra;
				$data['formatoObra']   = $formatoObra;
				$data['categoriaObra'] = $categoriaObra;
				$this->load->view('includes/sidebar',$data); 
			?>

			<div class="content">
				<?php
					echo '<div class="error">';
					echo validation_errors('<p>', '</p>');
					echo '</div>';

					if(!empty($msgErro)){
						echo '<div class="error">';
						echo '<p>'. $msgErro . '</p>';
						echo '</div>';
					} 
				?>
				<section class="main plans form-itens"> 

					<form action="<?php echo base_url(); ?>cadastro/realizarCompra" method="post" class="js-register-form">
						<h3 class="title page-title">Qual plano atende melhor suas necessidades?</h3>
						<div class="bordered-content choose-plan">
							<table class="table-plans">
							    <thead>
							        <tr>
							            <td class="table-title align-right">
							            	<h4>Selecione um plano:</h4>
							            </td>

							            <?php foreach($nomePlano as $nomeP){ ?>
								            <?php 
								            	/* Divide o valor do plano por 12 para obter o valor mensal */
								            	$op = 12;								            	
								            	$valor = str_ireplace(".","",$nomeP->vlPlano);
											    $valor = str_ireplace(",",".",$valor);
											    $valor  = $valor / $op;
											    $dataPrice  = $valor;
											    $valor  = number_format($valor, 2, ',', '.');

								             ?>
								            <td class="plan <?php echo strtolower($nomeP->planoEN) ?>">
								            	<div class="plan-title js-choose-plan">
									            	<label for="<?php echo strtolower($nomeP->planoPT) ?>">

									            		<?php if($nomeP->planoNome == 'Super' || $nomeP->planoNome == 'Bronze'){ ?>
									            			<i class="icon icon-radio-unchecked"></i>
									            		<?php } ?>
									            		<span><?php echo $nomeP->planoNome; ?></span>

									            		<?php
									            			if($nomeP->planoNome == 'Super' || $nomeP->planoNome == 'Bronze'){
									            		?>
											            	<span class="its-free">
											            		<p>Grátis</p>
											            		<small>para sempre</small>
										            		</span>
									            		<?php
									            			} else { 
									            		?>
									            		<p>Plano</p>
									            		<small>Indisponível</small>
										            	<!--
										            	<span class="its-not-free its-<?php echo strtolower($nomeP->planoPT) ?>">
										            		<p><?php echo 'R$ ' . $valor; ?></p>
										            		<small>por mês</small>
										            	</span>
										            	-->
										            	<?php } ?>
									            	</label>

													<?php if($nomeP->planoNome == 'Super' || $nomeP->planoNome == 'Bronze'){ ?>
								            			<input type="radio" name="plano" id="<?php echo strtolower($nomeP->planoPT) ?>" data-price="<?php echo $dataPrice; ?>" class="validate[required]" />
								            		<?php } ?>
								            	
								            	</div> <!-- /plan-title -->
								            </td>
								        <?php } ?>    								      
							        </tr>
							    </thead>
							    <tbody>
							        <!-- Tabela de Planos dinâmica --> 
							        <?php foreach($propriedadesPlanos as $p){ ?> 
								        <tr>
								            <td class="align-right"><?php echo $p->nome; ?></td>
								            <?php foreach($planos as $plano){ 
								            	 	if($plano->idPropriedadeChave == $p->idPropriedadeChave){ 
									            		if($plano->idValorChave == 1){ ?>
									            			<td>Sim</td>									           
								            			<?php }elseif($plano->idValorChave === '0' || $plano->idValorChave === 'Não'){ ?>
								            				<td><span class="red">Não</span></td>
								            			<?php }else{ ?>
								            			<td><?php echo $plano->idValorChave; ?></td>
								            <?php } } } ?>
								        </tr>
							        <?php } ?>							      
							    </tbody>
							</table>
							<p style="padding-left: 10px;">* Desconto de 50% na taxa de inscrição</p>
						</div> <!-- /choose-plan -->


						<h3 class="title page-title">Plano Escolhido</h3>
						<div class="selected-plan bordered-content">
						<div class="error-plan"></div>
							<span class="column-half">
								<label for="plano">Plano escolhido:</label>								
								<select name="plano" class="select-no-appearance js-select-plan validate[required]">
									<option selected>Selecione um plano...</option>  
								    <?php foreach($nomePlano as $nomeP){ ?>
								    	<?php
								    			/* Divide o valor do plano por 12 para obter o valor mensal */
								            	$op = 12;								            	
								            	$valor = str_ireplace(".","",$nomeP->vlPlano);
											    $valor = str_ireplace(",",".",$valor);
											    $valor  = $valor / $op;
											    $dataPrice  = $valor;
											    $valor  = number_format($valor, 2, ',', '.'); 
										?>

										<?php if($nomeP->planoNome == 'Super' || $nomeP->planoNome == 'Bronze'){ ?>	    									    	
								    		<option value="<?php echo strtolower($nomeP->planoPT) ?>">
								    			<?php echo $nomeP->planoNome . ' - Grátis'; ?>
								    		</option>
								    	<?php } ?>	

								    <?php } ?>	
								</select>

								<label for="periodicidade">Periodicidade:</label>
								<select name="periodicidade" class="select-no-appearance js-select-period validate[required]">
								    <!--
								    <option value="1" >Mensal</option>
								    -->
									<option value="3">Trimestral</option>
									<option value="6" selected>Semestral</option>
									<option value="12">Anual</option>

								    <!--
								    <option value="12">Anual</option>
								    <option value="24">Bienal</option>
								    <option value="36">Trienal</option>
									-->
								</select>

								<label for="desconto">Cupom de desconto:</label>
								<span class="cupon-code">
									<input id="cupom" name="cupom" type="text" class="input rounded small" />
									<button id="aplicarDesconto" class="button rounded continue">Aplicar</button>
								</span>
							</span> <!-- /column-half -->

							<span class="column-half">
								<span class="error-message js-alert-message"></span>
								<div class="total js-total">
									<h4>Total a pagar</h4>
									<p id="valorTotal" class="result js-result-plan"><small>Selecione um plano <br />e um período de assinatura.</small></p>
									<span>Parcele em até 12x com PagSeguro</span>
								</div>
							</span> <!-- /column-half -->
							
						</div> <!-- /selected-plan -->


						<h3 class="title page-title js-payment-box">Pagamento</h3>
						<div class="payment bordered-content js-payment-box">
								
							<span class="label"><strong>Atenção:</strong> Sua nota será emitida pelo nome cadastrado no CPF e será enviada por e-mail.</span>
							<span class="radios payment-radios js-radios">
								
								<input type="radio" name="pagamento" id="pagseguro" value="1" class="validate[required]" />
								<label for="pagseguro">
									<i class="icon icon-radio-unchecked"></i>
									<span class="hide-text sprite-pagseguro">PagSeguro</span>
								</label>

								<br class="clear" />
							
								<input type="radio" name="pagamento" id="paypal" value="2"  class="validate[required]" />
								<label for="paypal">
									<i class="icon icon-radio-unchecked"></i>
									<span class="hide-text sprite-paypal">PayPal</span>
								</label>

								<br class="clear" />
								
								<input type="radio" name="pagamento" id="free" value="5" />
								<label for="free" class="none">Grátis</label>
								
							</span>
						</div> <!-- /payment-->

						<h3 class="title page-title">Contrato</h3>
						<div class="contract bordered-content">
								
							<div class="contract-text bordered-content">
								<p>Prezado Cliente!</p>
								<p>Abaixo estão todos os termos e condições de uso do Mercado Arte, sua leitura é muto importante.

								<p class="red uppercase"><strong>TERMOS E CONDIÇÕES DO MERCADO ARTE LTDA</strong></p>

								<p><strong>DA ACEITAÇÃO DO PRESENTE DOCUMENTO</strong></p>
								<p>É condição essencial para que o USUÁRIO esteja apto a utilizar o sistema de divulgação de obras de arte de autoria própria, disponibilizado pelo MERCADO ARTE, que ACEITE eletronicamente estes “TERMOS E CONDIÇÕES” em sua totalidade, devendo ler e certifica-se de que o compreendeu e concordou com todas as suas cláusulas.</p>

								<p><strong>DO SERVIÇO OFERECIDO</strong></p>
								<p>O site www.mercadoarte.com.br é uma ferramenta de facilitação oferecida pela empresa MERCADO ARTE LTDA com a finalidade de oferecer espaço digital para que artistas possam anunciar e divulgar trabalhos exclusivamente de autoria própria, aproximando-os de seus possíveis consumidores.</p>
								<p>O MERCADO ARTE LTDA informa que o sistema eletrônico por ele disponibilizado é compatível apenas com máquinas que cumpram os requisitos abaixo:</p>
								<p>
									<strong>Sistema Operacional:</strong> Windows XP (ou superior)<br />
									<strong>Navegadores:</strong> Internet Explorer versão 8 (ou superior), Mozilla Firefox 3.5 (ou superior), Google Chrome 4.0.249 (ou superior)<br />
									<strong>Memória:</strong> 1 GB (ou superior)
								</p>

								<p><strong>DO USUÁRIO</strong></p>
								<p>Apenas pessoas prévia e devidamente cadastradas no sistema operante no site www.mercadoarte.com.br, que tenham optado pelo plano financeiro de sua preferência e aceitado estes termos e condições estarão aptas à utilização da ferramenta oferecida.</p>
								<p>Só poderão contratar os serviços do MERCADO ARTE LTDA pessoas capazes juridicamente de contratar e financeiramente de arcar com os valores cobrados pelo serviço.</p>
								<p>O USUÁRIO poderá anunciar apenas peças de autoria própria, podendo o MERCADO ARTE, LTDA em caso de violação deste dispositivo, bloquear o acesso do público à página do usuário até que a situação seja regularizada. Os dias de bloqueio da página do usuário serão contados, para todos os fins, como dias válidos na contagem do prazo contratado pelo USUÁRIO.</p>
								<p>Em caso de anúncio de obras de arte confeccionadas por incapazes, apenas seus representantes legais estarão aptos a contratar com o MERCADO ARTE LTDA, sendo estes também os responsáveis cíveis e criminais frente às obrigações advindas.</p>
								<p>Em casos como o citado no parágrafo acima, os representantes legais dos incapazes deverão entrar em contato com o Departamento Administrativo do MERCADO ARTE LTDA para informar a condição especial de contratação.</p>
								<p>O USUÁRIO será única e totalmente responsável pelas transações comerciais que realizar (venda de suas obras) com eventual cliente que tenha sido aproximado pelo sistema disponibilizado pelo MERCADO ARTE LTDA.</p>
								<p>O MERCADO ARTE NÃO INTERFERIRÁ DE QUALQUER MANEIRA NAS TRANSAÇÕES REALIZADAS PELOS USUÁRIOS COMO RESULTADO DOS ANÚNCIOS REALIZADOS, NÃO FARÁ QUALQUER TIPO DE CADASTRO DOS INTERESSADOS, NÃO PARTICIPARÁ DE QUALQUER NEGOCIAÇÃO, NÃO RECEBERÁ VALORES E NÃO COBRARÁ QUALQUER COMISSÃO POR VENDA REALIZADA PELO USUÁRIO, SENDO APENAS UM VEÍCULO DE DIVULGAÇÃO E ANÚNCIOS. ( )</p>
								<p>DESTA MANEIRA, O MERCADO ARTE NÃO SERÁ RESPONSÁVEL POR QUALQUER CONTENDA QUE VENHA A OCORRER ENTRE VENDEDOR E COMPRADOR, DEVENDO SER RESSARCIDO PELO USUÁRIO CASO VENHA A TER QUALQUER PREJUÍZO DECORRENTE DE TANTO. ( )</p>
								<p>O USUÁRIO se compromete a preencher os dados constantes da ficha cadastral apenas com dados verídicos e completos, comprometendo-se a atualizá-los sempre que houver qualquer modificação, não sendo o MERCADO ARTE LTDA responsável por qualquer problema que advenha do não cumprimento desta disposição.</p>
								<p>O USUÁRIO declara saber que os dados por ele indicados na ficha cadastral poderão ser verificados junto à órgãos governamentais ou de proteção ao crédito, o que poderá gerar solicitação de informações complementares, sendo de discricionariedade única e exclusiva do MERCADO ARTE LTDA a suspensão ou encerramento da contratação caso haja divergência entre as informações prestadas pelo USUÁRIO e aquelas resultantes das pesquisas realizadas pelo MERCADO ARTE LTDA, ou em caso destas apresentarem conteúdo considerado pelo MERCADO ARTE LTDA como prejudicial, danoso ou de risco à sua estrutura.</p>
								<p>O USUÁRIO será o único e exclusivo responsável pelo todo o conteúdo que gerar (veracidade, autenticidade, etc), podendo o MERCADO ARTE LTDA suprimir informações escritas ou visuais que entender representar risco ao público em geral, assim como aquelas contrárias à legislação vigente.</p>
								<p>O USUÁRIO declara estar ciente de que qualquer descumprimento a este regulamento ou utilização indevida do sistema o sujeitará ao encerramento permanente de sua conta pelo MERCADO ARTE LTDA, que devolverá os valores proporcionais ao plano escolhido e pagamento já realizado pelo USUÁRIO, quando houver.</p>
								<p>USUÁRIOS que utilizem o sistema oferecido pelo MERCADO ARTE LTDA sem atentar ao colocado nos parágrafos anteriores e demais conteúdo vinculante, serão consideradas de má-fé e poderão responder civil e criminalmente por tanto.</p>
								<p>Todas as informações prestadas pelo USUÁRIO para preenchimento da ficha cadastral serão mantidos em sigilo e a salvo de terceiros, podendo ser utilizados apenas para os fins de uso do sistema.</p>
								<p>O USUÁRIO AUTORIZA, DESDE JÁ, O MERCADO ARTE LTDA A UTILIZAR AS IMAGENS, TEXTOS E NOME ARTÍSTICO QUE VIER A PUBLICAR NO SITE, PARA OS FINS QUE ENTENDER NECESSÁRIOS PARA A DIVULGAÇÃO DO SITE, TAIS QUAIS, ANÚNCIOS EM JORNAIS, REVISTAS, OUTROS SITES, ETC. ( )</p>

								<p><strong>DO MERCADO ARTE LTDA</strong></p>
								<p>O MERCADO ARTE LTDA é a empresa prestadora de serviços, que disponibiliza o sistema contido no site www.mercadoarte.com.br, única e exclusivamente para anúncios de peças de arte por seus autores.</p>
								<p>O sistema disponibilizado pelo MERCADO ARTE LTDA é orginalmente desenvolvido e cumpre com as determinações legais, inclusive no que tange à propriedade intelectual de seu código fonte.</p>
								<p>É obrigação do MERCADO ARTE LTDA manter ativo o sistema oferecido aos USUÁRIOS, assim como cumprir as obrigações contidas nos planos financeiros apresentados aos USUÁRIOS.</p>
								<p>O MERCADO ARTE LTDA se compromete a manter o site www.mercadoarte.com.br no ar ininterruptamente, exceto em casos de falhas que recaiam sobre recursos que utiliza para tanto e que são oferecidos por terceiros, tais quais infra estrutura de TI, domínio e hospedagem, provedor de internet, etc.. Nos casos de interrupção das funcionalidades do site www.mercadoarte.com.br por motivos de terceiros, o MERCADO ARTE LTDA se compromete a fazer o que estiver ao seu alcance para que o problema seja resolvido o mais rápido possível.</p>
								<p>Em casos de interrupção dos serviços por conta de instabilidade das funcionalidades desenvolvidas pelo MERCADO ARTE LTDA, o mesmo se obriga a solver a questão em 12 (doze) horas úteis, sob pena de restituir ao USUÁRIO o número de dias necessários para que a manutenção do sistema seja realizada.</p>
								<p>É obrigação do MERCADO ARTE LTDA orientar os USUÁRIOS acerca da maneira de utilização do sistema, apresentando, para tanto, tutorial inicial com passo a passo, para que o usuário tenha liberdade na administração de sua página de anúncios.</p>
								<p>Havendo problemas ou dúvidas não solvidos pelo tutorial disponibilizado, o MERCADO ARTE LTDA disponibiliza e-mail de suporte técnico, que, após recebido, será respondido em até 48 (quarenta e oito) horas úteis, podendo tanto ser realizado como simples resposta ao e-mail do USUÁRIO ou por meio de ligação telefônica da equipe técnica especializada.</p>
								<p>O MERCADO ARTE poderá organizar eventos, diretamente ou por meio de empresa especializada, como uma outra maneira de promoção, anúncio e divulgação das obras dos USUÁRIOS que aderirem ao evento, inclusive no que se refere aos custos dos mesmos. O MERCADO ARTE LTDA poderá selecionar por si ou com consultoria de terceiros, as obras e artistas participantes dos eventos que intermediar.</p>
								<p>O MERCADO ARTE LTDA será apenas um facilitador/organizador desta publicidade extra, mas, assim como nos anúncios veiculados no site, não irá interferir de nenhuma maneira nas transações comerciais que eventualmente forem geradas pelo evento ou receberá qualquer comissão atrelada aos negócios nela gerados, envolvendo os USUÁRIOS e seus clientes.</p>

								<p><strong>DO PAGAMENTO</strong></p>
								<p>O USUÁRIO do sistema disponibilizado no site www.mercadoarte.com.br deverá quitar o valor correspondente ao “PACOTE DE SERVIÇOS” escolhidos pelo USUÁRIO virtualmente em momento anterior à assinatura deste instrumento, conforme ali especificado. Em caso do não pagamento dos valores devidos na data acordada, serão acrescidos multa de 10%, além de juros de mora de 2% ao mês.</p>
								<p>Sendo o atraso no pagamento superior a 15 (quinze) dias, a página do USUÁRIO em mora terá a visualização suspensa, assim como serão bloqueados todos os acessos do USUÁRIO à sua página de controle.</p>
								<p>O USUÁRIO que não efetuar os pagamentos conforme constante no “PACOTE DE SERVIÇOS” contratados estará sujeito:<br />
									<strong>a)</strong> a execução judicial do valor total devido com atualização, correção monetária e demais acessórios pertinentes;<br />
									<strong>b)</strong> ao saque de letras de câmbio à vista das referidas quantias.
								</p>
								<p>O USUÁRIO que contratar o sistema (preenchido a ficha cadastral, optado pelo “PLANO FINANCEIRO” e dado seu “de acordo” no presente instrumento) e não realizar o pagamento inicial até a data acordada, terá seu contrato automaticamente cancelado e eventual conteúdo já publicado deletado, devendo reiniciar o processo de contratação, caso haja interesse.</p>
								<p>Em caso de rescisão contratual solicitada pelo USUÁRIO, o mesmo deverá ao MERCADO ARTE LTDA, a título de multa, o valor correspondente à uma mensalidade, mesmo que o pagamento tenha sido acordado em periodicidade diferente, quando será utilizada fórmula aritmética simples: = valor total.1/12.</p>

								<p><strong>DISPOSIÇÕES FINAIS</strong></p>
								<p>O não exercício por qualquer das partes de qualquer direito deste decorrente, ainda que de forma reiterada, não representará renúncia ou mesmo novação ou alteração contratual.</p>
								<p>O MERCADO ARTE LTDA reserva seu direito de modificar o presente contrato sempre que for necessário, informando aos USUÁRIO por e-mail sempre que tanto ocorrer. Poderá o USUÁRIO que tiver alguma objeção ao novo texto cancelar seu plano a qualquer momento, sem que haja qualquer cobrança por parte do MARCADO ARTE LTDA, que, se for o caso, restituirá ao USUÁRIO o valor proporcional de sua assinatura, considerando os dias ou meses transcorridos do aceite a este contrato e a solicitação de seu rompimento.</p>
								<p>O presente contrato é assinado em caráter irretratável e irrevogável e obriga não só as partes contratantes, como a seus sucessores. <br />Se qualquer cláusula ou condição deste contrato for considerada nula ou inexequível por decisão judicial definitiva, isto não afetará as demais cláusulas, permanecendo o contrato em vigor e vinculativo das partes. Nesta hipótese, as partes se comprometem, de boa fé, a empregar os melhores esforços para acordar uma cláusula ou condição substituta, que seja válida e exequível e tenha os efeitos econômicos mais próximos possíveis dos da cláusula substituída.</p>
								<p>As partes elegem, com desistência de qualquer outro por mais privilegiado que seja, o Foro Central da Comarca de São Paulo para dirimir quaisquer questões decorrentes deste contrato, arcando a parte vencida com as custas e despesas do processo e com a verba honorária de sucumbência.</p>

							</div>

							<div class="contract-option">
								<a href="<?php base_url(); ?>/content/download/termos-de-uso.pdf" target="blank" class="print">
									<i class="icon icon-print"></i>
									Imprimir Contrato
								</a>
								<div class="accept-contract">
									<span class="label">Aceita este contrato?</span>
									<span class="radios js-radios">
										<input type="radio" name="aceitar" id="sim" value="sim" class="validate[required]" />
										<label for="sim"><i class="icon icon-radio-unchecked"></i> Sim</label>

										<input type="radio" name="aceitar" id="nao" value="nao" class="validate[required]" />
										<label for="nao"><i class="icon icon-radio-unchecked"></i> Não</label>
									</span>

									<button id="continuar" type="submit" class="button rounded continue js-submit-register">Continuar</button>
								</div>
							</div>

							<br class="clear" />
							
						</div> <!-- /contract -->

					</form>

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>