<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
			</div> <!-- /breadcrumb -->

			<?php 
				$data['tecnicaObra']   = $tecnicaObra;
				$data['formatoObra']   = $formatoObra;
				$data['categoriaObra'] = $categoriaObra;
				$this->load->view('includes/sidebar',$data); 
			?>

			<div class="content">
				<section class="primary-banner owl-carousel js-primary-banner">
					<div class="item">
						<a href="https://www.mercadoarte.com.br/cadastro">
							<img src="<?php echo base_url(); ?>/content/images/Banner_Slider_1_v2.jpg" alt="Procuram-se artistas, cadastre-se aqui" />
						</a>
					</div>
					<div class="item">
						<a href="https://www.mercadoarte.com.br/blog/sobre-nos/">
							<img src="<?php echo base_url(); ?>/content/images/Banner_Slider_2_v2.jpg" alt="Mercado Arte: Unindo artistas e colecionadores" />
						</a>
					</div>
				</section>

				<section class="right-column">
					<div class="box">
						<?php include('includes/social.php'); ?>

						<h2 class="title">Destaque</h2>
						<div class="box-content">
							<a href="https://www.mercadoarte.com.br/blog/avaliacao-de-obras-de-arte/">
								<img src="<?php echo base_url(); ?>/content/images/Banner_Small_2015-02-23.jpg" alt="Avalie sua obra" />
							</a>
						</div> <!-- /box-content -->
					</div> <!-- /box -->
				</section>

				<br class="clear" />
				<?php if(!empty($obras)){ ?>
					<section class="main border-top">

						<h3 class="title">Ei, separamos estas obras especialmente pra você!</h3>
						<!-- <div class="filter-order">
							<label for="order-by">Ordenar por:</label>
							<select name="order-by"id="order-by" class="js-custom-select js-change-order">
							    <option class="option-order" value="1">Nome</option>
							    <option class="option-order" value="2">Avaliação</option>
							    <option class="option-order" value="3">Menor Preço</option>
							    <option class="option-order" value="4">Maior Preço</option>
							</select>
						</div> -->

						<section class="list js-filter-scroll">
							<?php $num =0; ?>
							<?php foreach ($obras as $o) { ?>
								<?php $num++; ?>
								<article class="art <?php if($num % 5 == 0){echo "no-margin";} ?>">
									<a href="<?php echo base_url(). $o->slugpagina; ?>/obras/<?php echo $o->slugObra; ?>">
										<div class="thumbnail">
                                            <img src="<?php echo base_url(); ?>content/upload/<?php echo $o->slugpagina ?>/<?php echo $o->slugObra; ?>/small/<?php echo $o->imgFoto1; ?>" />
										</div> <!-- /thumbnail -->
										
										<h4 class="artist">
											<?php echo $o->nomeArtistico; ?>
											<span class="badge <?php if($o->votosTotais >= 0 && $o->votosTotais <= 99){ echo ''; }elseif($o->votosTotais >= 100 && $o->votosTotais <= 999 ){echo 'sprite-badge-bronze'; }elseif($o->votosTotais >= 1000 && $o->votosTotais <= 9999){echo 'sprite-badge-silver';}elseif($o->votosTotais >= 10000){echo 'sprite-badge-gold';} ?>"></span>
										</h4>
										<h5 class="description"><?php echo $o->titulo; ?> - <?php echo $o->nomeTecnica; ?> - <?php echo number_format($o->altura, 0); ?> x <?php echo number_format($o->comprimento, 0); if(!empty($o->largura)){ echo ' x '. number_format($o->largura, 0);} ?> cm</h5>

										<div class="price">
											<?php if($o->idStatusObra == 2 || $o->idStatusObra == 5 || $o->idStatusObra == 8){ ?>
                                                <p class="soldout">Vendido</p>
										    <?php }else if($o->idStatusObra == 3 || $o->idStatusObra == 6 || $o->idStatusObra == 9){ ?>
                                                <a href="<?php echo base_url(). $o->slugpagina; ?>/obras/<?php echo $o->slugObra; ?>?section=consultar" class="consult">Consulte</a>
                                            <?php }else{ ?>
                                                <?php if($o->precoComDesconto == 0.00){ ?>
                                                    <p class="descount">R$ <?php echo number_format((int)str_ireplace(".",",",$o->precoObra), 2, ',', '.'); ?></p>
                                                <?php }else{ ?>
                                                    <small class="value">De R$ <?php echo number_format((int)str_ireplace(".",",",$o->precoObra), 2, ',', '.'); ?></small>
                                                    <p class="descount">R$ <?php echo number_format((int)str_ireplace(".",",",$o->precoComDesconto), 2, ',', '.'); ?></p>
                                                <?php } ?>
                                            <?php } ?>
                                        </div> <!-- /price -->
									</a>

									<div class="rating">
										<ul class="star-rating" id="star-rating-<?php echo $o->idObra; ?>">
										<?php 
											$notas = round($o->nota);

											for ($i=0; $i < 5; $i++) { ?>
												<li class="star-number-<?php echo $i + 1; ?><?php if($notas > 0){ echo " active"; } ?>">
													<a href="#" class="icon-vote" data-idObra="<?php echo $o->idObra; ?>" data-icon="<?php echo $i; ?>"><i class="icon icon-star"></i>
														<span><?php echo $i + 1; ?></span>
													</a>
												</li> 
										<?php 
											$notas--;
											}

										?>		
										</ul>
									</div> <!-- /rating -->
								</article>						
							
							<?php }} ?>	
				
						<br class="clear" />

					</section> <!-- /list -->

				</section>
				
			</div>

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>