<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8"/>
    <title>Pagamento Paypal</title>
</head>
<body>
<?php @session_start();?>
<!-- Pagamento via redirecionamento para o paypal -->
<div id="esconde-btn-pagamento" style="display: none">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="formPaypal" target="_top">
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="business" value="alcidesrib.j@yahoo.com.br">
        <input type="hidden" name="lc" value="BR">
        <input type="hidden" name="item_name" value="Plano <?php if (!isset($_SESSION['nomePlano']) || empty($_SESSION['nomePlano'])) {echo $this->session->userdata('nomePlano');} else {echo $_SESSION['nomePlano'];}?>">
        <input type="hidden" name="amount" value="<?php if (!isset($_SESSION['valorAPagar']) || empty($_SESSION['valorAPagar'])) {echo $this->session->userdata('valorAPagar');} else {echo $_SESSION['valorAPagar'];}?>">
        <input type="hidden" name="currency_code" value="BRL">
        <input type="hidden" name="button_subtype" value="services">
        <input type="hidden" name="no_note" value="0">
        <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest">
        <input type="hidden" name="return" value="https://www.mercadoarte.com.br/sucesso" />
        <input type="image" src="https://www.paypalobjects.com/pt_BR/BR/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - A maneira fácil e segura de enviar pagamentos online!">
        <img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1">
    </form>
</div>


</body>
</html>

<script type="text/javascript">
    document.forms["formPaypal"].submit();
</script>






