<table cellspacing="0" cellpadding="0" border="0" width="750">

    <tr height="5" bgcolor="#e54e53">
        <td>
        </td>
    </tr>

    <tr height="40" bgcolor="#f2f2f2">
        <td>
            <table>
                <tr>
                    <td width="35">&nbsp;</td>
                    <td width="130" align="left">
						<span class="header-links">
							<font face="Arial" size="2" color="#727272">
                                <a href="#" style="color: #727272; text-decoration: none;">Faça seu login</a> &nbsp;&nbsp;&nbsp;&nbsp;
                            </font>
						</span>
                    </td>
                    <td width="550" align="right">
						<span class="header-links">
							<font face="Arial" size="2" color="#727272">
                                <a href="https://www.mercadoarte.com.br/blog/comunidade/" style="color: #727272; text-decoration: none;">Ajuda</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="https://www.mercadoarte.com.br/blog/" style="color: #727272; text-decoration: none;">Blog</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="https://www.mercadoarte.com.br/blog/comunidade/" style="color: #727272; text-decoration: none;">Fórum</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="https://www.mercadoarte.com.br/blog/avaliacao-de-obras-de-arte/" style="color: #727272; text-decoration: none;">Serviços</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="https://www.mercadoarte.com.br/blog/agenda-de-eventos-de-arte/" style="color: #727272; text-decoration: none;">Eventos</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="https://www.mercadoarte.com.br/blog/contato/" style="color: #727272; text-decoration: none;">Contato</a>
                            </font>
						</span>
                        <style type="text/css">.header-links a:hover {color: #e54e53!important;}</style>
                    </td>
                    <td width="35">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table style="border-right: 1px solid #f2f2f2; border-left: 1px solid #f2f2f2;">
                <tr>
                    <td width="35">&nbsp;</td>

                    <td width="680">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr height="120" vertical-align="middle">
                                <td colspan="3">
                                    <a href="https://www.mercadoarte.com.br/">
                                        <img src="https://www.mercadoarte.com.br/content/images/sprite/logo.png" alt="Mercado Arte" border="0" />
                                    </a>
                                </td>
                            </tr>
                            <tr height="25">
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <style type="text/css">.content {line-height: 21px;} .content a:hover {color: #e54e53!important;}</style>
									<span class="content">
										<font face="Arial" size="4" color="#000">
                                            Olá <b><?php echo  $nomeArtistico; ?></b>
                                        </font>

										<br /><br />

										<font face="Arial" size="3" color="#3c3c3b">
                                            Você recebeu um contato:

                                            <br /><br />

                                            <b>Nome: </b><?php echo $nome; ?><br />
                                            <b>E-mail: </b><?php echo $email; ?><br />

                                            <?php if(isset($assunto) && !empty($assunto)){?>
                                                <b>Assunto: </b><?php echo $assunto; ?><br />
                                            <?php } ?>

                                            <b>Telefone: </b><?php echo $telefone; ?><br />
                                            <b>Mensagem: </b><?php echo $mensagem; ?><br />

                                            <br />
                                            <br />
                                            Para acessar agora seu painel de administração, <b><a href="https://www.mercadoarte.com.br/login" style="color: #000; text-decoration: none;">clique aqui</a></b>.

                                            <br /><br />
                                            Obrigado por fazer parte de nossa rede.<br />

                                        </font>
									</span>
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td width="35">&nbsp;</td>
                </tr>

                <tr height="100">
                    <td colspan="3">&nbsp;</td>
                </tr>
            </table>

        </td>
    </tr>

    <tr height="85" bgcolor="#19232d">
        <td>
        </td>
    </tr>

    <tr height="45" bgcolor="#0c1116">
        <td>
            <table>
                <td width="35">&nbsp;</td>
                <td width="680">
                    <table>
                        <tr >
                            <td width="200" align="left">
                                <font face="Arial" size="2" color="#727272">
                                    Copyright &copy; 2015 Mercado Arte
                                </font>
                            </td>
                            <td width="480" align="right">
								<span class="footer-links">
									<font face="Arial" size="2" color="#727272">
                                        <a href="https://www.mercadoarte.com.br/blog/sobre-nos/" style="color: #727272; text-decoration: none;">Sobre Nós</a> &nbsp;
                                        <a href="https://www.mercadoarte.com.br/blog/politica-de-privacidade/" style="color: #727272; text-decoration: none;">Política de Privacidade</a> &nbsp;
                                        <a href="https://www.mercadoarte.com.br/blog/como-comprar/" style="color: #727272; text-decoration: none;">Como Comprar</a> &nbsp;
                                        <a href="https://www.mercadoarte.com.br/blog/como-vender/" style="color: #727272; text-decoration: none;">Como Vender</a> &nbsp;
                                        <a href="https://www.mercadoarte.com.br/blog/planos/" style="color: #727272; text-decoration: none;">Planos</a>
                                    </font>
								</span>
                                <style type="text/css">.footer-links a:hover {color: #fff!important;}</style>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="35">&nbsp;</td>
            </table>
        </td>
    </tr>

    <tr height="25" bgcolor="#19232d">
        <td>&nbsp;</td>
    </tr>

</table>