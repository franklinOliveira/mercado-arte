			
						<h2 class="title">Mercado Arte na WEB</h2>
						<div class="box-content">
							<ul class="social">
								<li>
									<a href="https://www.mercadoarte.com.br/blog/feed/" class="rss">
										<i class="icon icon-rss"></i>
										<span>RSS</span>
									</a>
								</li>
								<li>
									<a href="https://www.google.com/+MercadoarteBrSP" class="google-plus">
										<i class="icon icon-google-plus"></i>
										<span>Google Plus</span>
									</a>
								</li>
								<li>
									<a href="https://www.facebook.com/Mercado.Arte.BR" class="facebook">
										<i class="icon icon-facebook"></i>
										<span>Facebook</span>
									</a>
								</li>
								<li>
									<a href="https://twitter.com/mercadoarte_br" class="twitter">
										<i class="icon icon-twitter"></i>
										<span>Twitter</span>
									</a>
								</li>
								<li>
									<a href="https://www.youtube.com/channel/UC_Z9jW5sDMP9rzz00CR970A/" class="youtube">
										<i class="icon icon-youtube"></i>
										<span>Youtube</span>
									</a>
								</li>
							</ul>
						</div> <!-- /box-content -->
						