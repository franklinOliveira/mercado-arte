<?php
	
	$current = current_url();

    $pageTitle       = '';
    $pageDescription = '';
    $pageKeywords    = '';


    /* Define o nome da página */
    $pagina = $this->uri->segment(1);
    if($this->uri->segment(2) != ''){
        $pagina  = $pagina . '/' .$this->uri->segment(2);
    }
    if($this->uri->segment(3) != ''){
        $pagina  = $pagina . '/' .$this->uri->segment(3);
    }


    /* verifica o nome do artista */
    if(defined('SLUG')){
        $slug = SLUG;
    }
    else {
        $slug = null;
    }


    /* Define o identificador da obra */
    if($this->session->userdata('identObra') != ''){
        $identObra = $this->session->userdata('identObra');
    }
    else {
        $identObra = null;
    }


    /* Define a biografia do artista (150 caracteres) */
    if($this->session->userdata('biografia') != ''){
        $txtBiografia =  strtolower($this->session->userdata('biografia'));
    }
    else {
        $txtBiografia = null;
    }


    /*Define o nome do artista */
    if($this->session->userdata('nomeArtista') != '' ){
        $nomeArtista = $this->session->userdata('nomeArtista');
    }
    else {
        $nomeArtista = $this->session->userdata('nomeCompleto');
        // $nomeArtista = null;
    }


    /*Define os tipos de obras do artista */
    if($this->session->userdata('tipoObraArtista') != '' ){
        $tipoObraArtista = $this->session->userdata('tipoObraArtista');
        $tipoObraArtista = strtolower(implode(",", $tipoObraArtista));
    }
    else {
        $tipoObraArtista = null;
    }


    /*Define o tipo da obra, tamanho, titulo e tecnica*/
    if( $this->session->userdata('tipoObra') != '' &&
        $this->session->userdata('tituloObra') != '' &&
        $this->session->userdata('nomeTecnicaObra') != '' &&
        $this->session->userdata('tamanhoObra') != ''){

        $tipoObra        =  strtolower($this->session->userdata('tipoObra'));
        $tituloObra      =  $this->session->userdata('tituloObra');
        $nomeTecnicaObra =  strtolower($this->session->userdata('nomeTecnicaObra'));
        $tamanhoObra     =  strtolower($this->session->userdata('tamanhoObra'));

    }else {
        $tipoObra = null;
        $tituloObra = null;
        $nomeTecnicaObra = null;
        $tamanhoObra = null;
    }

    /* Recupera a palavra buscada */
    if($this->session->userdata('palavraBusca') != ''){
        $palavraBusca = $this->session->userdata('palavraBusca');
    }else{
        $palavraBusca = null;
    }

    switch ($pagina) {
        case '':
            $pageTitle       = 'Mercado Arte • Encontre a obra de arte perfeita!';
            $pageDescription = 'Encontre obras de arte EXCLUSIVAS: PINTURA, ESCULTURA E FOTOGRAFIA - Um novo e real conceito de democratização da arte no Brasil.';
            $pageKeywords    = 'comprar quadros, obras de arte, quadros decorativos, decoração de interiores, galeria de arte';
            break;

        case 'home':
            $pageTitle       = 'Mercado Arte • Encontre a obra de arte perfeita!';
            $pageDescription = 'Encontre obras de arte EXCLUSIVAS: PINTURA, ESCULTURA E FOTOGRAFIA - Um novo e real conceito de democratização da arte no Brasil.';
            $pageKeywords    = 'comprar quadros, obras de arte, quadros decorativos, decoração de interiores, galeria de arte';
            break;

        case 'obras':
            $pageTitle       = 'Obras de Arte • Mercado Arte';
            $pageDescription = 'Encontre a obra de arte perfeita, compre diretamente com o artista! Seja exclusivo, seja único!';
            $pageKeywords    = 'obras de arte, quadros modernos, arte online, galeria de fotos, gravuras';
            break;

        case 'obras/pinturas':
            $pageTitle       = 'Pinturas • Mercado Arte';
            $pageDescription = 'Pinturas exclusivas em diferentes técnicas e tamanhos. Veja abstratos, natureza morta, entre outros temas. Confira!';
            $pageKeywords    = 'pintura, pintores famosos, decoração de casas, pinturas famosas, quadros decorativos';
            break;

        case 'obras/esculturas':
            $pageTitle       = 'Esculturas • Mercado Arte';
            $pageDescription = 'Esculturas exclusivas em bronze, madeira, cerâmica entre outras. Diferentes tamanhos e técnicas. Confira!';
            $pageKeywords    = 'escultura, arte contemporânea, arte moderna, esculturas famosas, estatua';
            break;

        case 'obras/fotografias':
            $pageTitle       = 'Fotografias • Mercado Arte';
            $pageDescription = 'Fotografias artísticas, coloridas e preto e branco, diversos tamanhos e temas. Compre diretamente do artista';
            $pageKeywords    = 'fotografia digital, foto, foto preto e branco, fotografias artisticas';
            break;

        case 'obras/filtrar':
            $pageTitle       = 'Encontre a obra de arte perfeita • Mercado Arte';
            $pageDescription = 'Utilize o filtro e selecione entre diferentes tamanhos, técnicas, temas e preços. Tudo para te ajudar a encontrar a obra de arte perfeita!';
            $pageKeywords    = 'arte personalizada, decoração quarto, decoração quadros, galeria de arte';
            break;

        case 'obras/buscar':
            $pageTitle       = 'Resultado da busca por "'. $palavraBusca .'" • Mercado Arte';
            $pageDescription = 'Utilize a busca para encontrar obras de arte relacioandas a um texto em específico.';
            $pageKeywords    = 'buscar obra de arte, comprar pintura, comprar escultura, compra quadros';
            break;

        case 'parceiros':
            $pageTitle       = 'Parceiros • Mercado Arte';
            $pageDescription = 'Nossos parceiros oferecem oportunidades únicas para os visitantes do Mercado Arte, aproveite!';
            $pageKeywords    = 'galeria de arte, arquitetura, material de arte, escola de arte, molduraria';
            break;

        case 'favoritos':
            $pageTitle       = 'Meus Favoritos • Mercado Arte';
            $pageDescription = 'O que acha de fazer uma lista com seus artistas favoritos? não é necessário se cadastrar! Aproveite!';
            $pageKeywords    = 'artistas favoritos, artistas famosos, pintores famosos, artista plástico';
            break;

        case 'artistas':
            $pageTitle       = 'Artistas • Mercado Arte';
            $pageDescription = 'Todos os artistas afiliados ao Mercado Arte desfrutam de benefícios exclusivos, faça parte você também!';
            $pageKeywords    = 'artes plásticas, galeria de arte online, artista plástico, pintores, escultores, fotografos';
            break;

        case $slug:
            $pageTitle       =  $nomeArtista . ' • Obras, biografia e contato';
            $pageDescription =  $txtBiografia;
            $pageKeywords    =  $nomeArtista . ','.$tipoObraArtista. ' de '.$nomeArtista .', obras de arte de '. $nomeArtista;
            break;

        case $slug . '/obras':
            $pageTitle       = $nomeArtista . ' • Obras de arte';
            $pageDescription = 'Obras de arte exclusivas, compre diretamente com o artista: ' . $nomeArtista;
            $pageKeywords    =  $nomeArtista.' , site do artista, '. $tipoObraArtista .', obras de '. $nomeArtista;
            break;

        case $slug . '/biografia':
            $pageTitle       = $nomeArtista . ' • Biografia';
            $pageDescription =  $txtBiografia;
            $pageKeywords    = 'Biografia de '. $nomeArtista.', história de '. $nomeArtista.', tudo sobre '. $nomeArtista;
            break;

        case $slug . '/obras/'. $identObra:
            $pageTitle       =  $tituloObra.' • ' . $nomeArtista;
            $pageDescription = 'Obra de arte: '. $nomeArtista .', '. $tituloObra .', '.$nomeTecnicaObra.', '.$tamanhoObra.', compre agora mesmo!';
            $pageKeywords    =  $nomeArtista.', '.$tituloObra.','.$nomeTecnicaObra.', '.$tipoObra;
            break;

        case 'cadastro':
            $pageTitle       = 'Cadastro • Mercado Arte';
            $pageDescription = 'O Mercado Arte disponibiliza para os artistas a oportunidade de ter uma página na Web para exibir seus trabalhos, cadastre-se!';
            $pageKeywords    = 'galeria de arte online, cadastro mercado arte, vender arte online, comprar arte online';
            break;

        case 'cadastro/cadastro_completo':
            $pageTitle       = 'Cadastro Completo • Mercado Arte';
            $pageDescription = 'Bem-vindo ao Mercado Arte, desfrute dos benefícios e faça uma boa campanha online. Se precisar de ajuda não deixe de entrar em contato!';
            $pageKeywords    = 'cadastro, mercado, arte, completo, finalizado, sucesso';
            break;

        case 'cadastro/planos':
            $pageTitle       = 'Planos • Mercado Arte';
            $pageDescription = 'Todos os nossos planos tem como objetivo auxiliar o artista a exibir suas obras e fechar negócios, selecione um entre eles e faça parte você também!';
            $pageKeywords    = 'planos mercado arte, fazer parte mercado arte, mercado arte funciona';
            break;

        case 'cadastro/sucesso':
            $pageTitle       = 'Bem-vindo • Mercado Arte';
            $pageDescription = 'Seu cadastro foi efetuado com sucesso, bem-vindo!';
            $pageKeywords    = 'cadastro sucesso mercado arte, cadastro mercado arte, mercado arte na web';
            break;

        default:
            $pageTitle       = 'Mercado Arte • Sua casa merece!';
            $pageDescription = 'Sua casa merece exclusividade, encontre obras de arte lindas e exclusivas. Negocie diretamente com o artista.';
            $pageKeywords    = 'galeria de arte online, obras de arte online, comprar arte online';
        break;
	}

?>

		<title><?php echo $pageTitle; ?></title>

		<meta name="language" content="pt-BR"/>
		<meta name="viewport" content="width=1080" />
		<meta name="description" content="<?php echo $pageDescription; ?>" />
		<meta name="keywords" content="<?php echo $pageKeywords; ?>" />
		<meta name="revisit" content="3 days" />
		<meta name="robots" content="index, follow" />
		<meta name="url" content="<?php echo base_url(); ?>" />
		<meta name="copyright" content="<?php echo $pageTitle; ?>"/>
		<meta name="author" content="Mercado Arte" />
		<meta name="google-site-verification" content="UA-31891297-1"/>
		<meta name="msvalidate.01" content="58A9093F43D9B1F7FF56C2A9CA5A698A"/>
		<meta name="alexaVerifyID" content="anZDlc7RzdY_JdD8yHW7QEwpRJg"/>

		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:creator" content="@MERCADOARTE_BR"/>
		<meta name="twitter:site" content="@MERCADOARTE_BR"/>
		<meta name="twitter:title" content="<?php echo $pageTitle; ?>">
		<meta name="twitter:image:src" content="<?php
			if($slug != null){
				// Artistas
                if($pagina == $slug . '/obras' || $pagina == $slug . '/biografia' || $pagina == $slug){
                	// Imagem do Artista
                    if (isset($artista[0]->imgPerfil)) {
						echo base_url() . 'content/upload/' . SLUG . '/fp/fp-medium/' . $artista[0]->imgPerfil;
					} 
					else {
						echo base_url() . 'content/images/avatar.png';
					}
                }
                // Obras
                elseif($pagina == $slug . '/obras/'. $identObra){
                    echo base_url() . 'content/upload/' . SLUG . '/' . $obra[0]->slugObra . '/medium/' . $obra[0]->imgFoto1;

                }
            }
            else{
                echo base_url() . 'content/images/icons/twitter.png';
            }
		?>">

		<meta name="twitter:domain" content="<?php echo $pageTitle; ?>">

		<meta name="DC.Publisher" content="<?php echo $pageTitle; ?>" />
		<meta name="DC.Title" content="<?php echo $pageTitle; ?>" />
		<meta name="DC.Description" content="<?php echo $pageDescription; ?>" />
		<meta name="DC.Date" content="<?php echo date('Y-m-d'); ?>" />
		<meta name="DC.Date.Issued" content="<?php echo date('Y-m-d'); ?>" />

		<meta property="og:url" content="<?php echo current_url(); ?>" />
		<meta property="og:image" content="<?php
			if($slug != null){
				// Artistas
                if($pagina == $slug . '/obras' || $pagina == $slug . '/biografia' || $pagina == $slug){
                	// Imagem do Artista
                    if (isset($artista[0]->imgPerfil)) {
						echo base_url() . 'content/upload/' . SLUG . '/fp/fp-medium/' . $artista[0]->imgPerfil;
					} 
					else {
						echo base_url() . 'content/images/avatar.png';
					}
                }
                // Obras
                elseif($pagina == $slug . '/obras/'. $identObra){
                    echo base_url() . 'content/upload/' . SLUG . '/' . $obra[0]->slugObra . '/medium/' . $obra[0]->imgFoto1;

                }
            }
            else{
                echo base_url() . 'content/images/icons/facebook.png';
            }
		?>" />
		<meta property="og:title" content="<?php echo $pageTitle; ?>" />
		<meta property="og:description" content="<?php echo $pageDescription; ?>" />
		<meta property="og:site_name" content="<?php echo $pageTitle; ?>" />
		<meta property="og:type" content="website"/>
		<meta property="fb:admins" content="Mercado.Arte.BR"/>

		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>content/css/application.css" media="all" />

		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>content/images/icons/favicon.png" />
		<link rel="apple-touch-icon" href="<?php echo base_url(); ?>content/images/icons/57.png" />
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>content/images/icons/76.png" />
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>content/images/icons/120.png" />
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>content/images/icons/152.png" />
		<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>content/images/icons/144.png" />

		<link rel="dns-prefetch" href="//connect.facebook.net" />
		<link rel="author" href="https://plus.google.com/+MercadoarteBrSP" />
		<link rel="canonical" href="<?php echo current_url(); ?>" />
		
