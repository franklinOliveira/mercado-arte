<footer id="footer">
			<div class="wrapper">
				<span class="footer-column column-one">
					<p>O <strong>Mercado Arte</strong> disponibiliza para os artistas a oportunidade de ter uma página na Web para exibir seus trabalhos e para o público em geral a chance de acessibilidade a um universo artístico criativo que vai muito além do que se apresenta em galerias, museus e sites atualmente. <a href="<?php echo base_url(); ?>cadastro">Cadastre-se</a></p>
				</span>
				<span class="footer-column column-two">
					<span class="form-news-content">
						<h4>Cadastre-se e receba as novidades por e-mail:</h4>
						<form action="#"  id="form-news" method="post" class="js-newsletter-form">
							<input type="email" name="email" id="email" class="input email rounded validate[required,custom[email]]" placeholder="Seu e-mail" />
							<input type="text" name="nome" id="nome" class="input name rounded validate[required]" placeholder="Seu nome" />
							<button type="submit" id="btn-submit-news" class="button register rounded js-submit-newsletter">Cadastrar</button>
						</form>
					</span>
					<div id="form-news-msg"></div>
				</span>
				<span class="footer-column column-three">
					<h4>Central do relacionamento</h4>
					<a href="https://www.mercadoarte.com.br/blog/contato/" class="button rounded">Tire suas dúvidas</a>
					<p>Se preferir, ligue para: <br /> (11) 2528-4599</p>
				</span> 
			</div>

			<br class="clear" />

			<div class="bottom-links">
				<div class="wrapper">
					<p>Copyright &copy; 2015 Mercado Arte</p>
					<ul>
						<li><a href="https://www.mercadoarte.com.br/blog/sobre-nos/">Sobre Nós</a></li>
						<li><a href="https://www.mercadoarte.com.br/blog/politica-de-privacidade/">Política de Privacidade</a></li>
						<li><a href="https://www.mercadoarte.com.br/blog/como-comprar/">Como Comprar</a></li>
						<li><a href="https://www.mercadoarte.com.br/blog/como-vender/">Como Vender</a></li>
						<li><a href="https://www.mercadoarte.com.br/blog/planos/">Planos</a></li>
					</ul>
				</div>
			</div>

		</footer>
		<div class="global-notification alert js-notification"></div>

		<script src="<?php echo base_url(); ?>content/js/_jquery.js"></script>
		<script src="<?php echo base_url(); ?>content/js/_preloader.js"></script>
		<script src="<?php echo base_url(); ?>content/js/_carousel.js"></script>
		<script src="<?php echo base_url(); ?>content/js/_validate.js"></script>
		<script src="<?php echo base_url(); ?>content/js/_portuguese.js"></script>
		<script src="<?php echo base_url(); ?>content/js/_maskedinput.js"></script>
		<script src="<?php echo base_url(); ?>content/js/_select.js"></script>
		<script src="<?php echo base_url(); ?>content/js/_gallery.js"></script>
		<script src="<?php echo base_url(); ?>content/js/_custom.js"></script>

		<!--[if lt IE 9]>
			<script src="<?php echo base_url(); ?>content/js/html5.js"></script>
			<script src="<?php echo base_url(); ?>content/js/ie.js" charset="utf-8"></script>
			<link rel="stylesheet" href="<?php echo base_url(); ?>content/css/ie.css" />
		<![endif]-->

		<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-31891297-1']);
		_gaq.push(['_trackPageview']);
		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
		</script>
		
		<?php
			$url = basename($_SERVER['PHP_SELF']); 
			if($url == "https://www.mercadoarte.com.br"){
		?>
			<script>document.write('<script src="https://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
		<?php } ?>