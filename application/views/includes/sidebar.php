<?php 
	$url = basename($_SERVER['PHP_SELF']); 
	$current = current_url();
	if(!empty($artista)){
		$slug = $artista[0]->slugpagina; 
	}else{
		$slug = 'Artista_nao_existe';
	}
?>
<?php 
	if ($current == base_url() . $slug ||  $current == base_url() . 'artistas/biografia' || $current == base_url() . 'artistas/sobre' || $current == base_url() . $slug . "/obras" || $current == base_url() . 'artistas/obra_detalhe' || (empty($formatoObra) && empty($tecnicaObra))){ 
?>

			<aside id="sidebar">
				<div class="box">
					<h2 class="title align-left">Filtro</h2>
					<form action="<?php echo base_url(); ?>obras/filtrar" method="get" class="js-filter-validation">
                        <input type="hidden" name="filtro-artista" value="<?php echo $slug; ?>" />
                        <div class="box-content">
							<h3 class="subtitle">Tamanho</h3>
							<ul class="js-size-filter">
								<li> 
									<label for="ate-50-cm">
										<i class="icon icon-radio-unchecked"></i>
										<span>Até 50 cm</span>
									</label>
									<input type="radio" name="tamanho" value="1" id="ate-50-cm" />
								</li>
								<li>
									<label for="de-51-a-100">
										<i class="icon icon-radio-unchecked"></i> 
										<span>51 cm ~ 100 cm</span>
									</label>
									<input type="radio" name="tamanho" value="2" id="de-51-a-100" />
								</li>
								<li> 
									<label for="maior-que-100">
										<i class="icon icon-radio-unchecked"></i>
										<span>Maior que 101 cm</span>
									</label>
									<input type="radio" name="tamanho" value="3" id="maior-que-100" />
								</li>
								<li> 
									<label for="indiferente">
										<i class="icon icon-radio-unchecked"></i>
										<span>- Indiferente -</span>
									</label>
									<input type="radio" name="tamanho" value="4" id="indiferente" />
								</li>
							</ul>

							<h3 class="subtitle">Preço</h3>
							<ul class="js-price-filter">
								<li>
									<label for="ate-100">
										<i class="icon icon-radio-unchecked"></i>
										<span>Até R$ 100</span>
									</label>
									<input type="radio" name="preco" value="1" id="ate-100" />
								</li>
								<li>
									<label for="entre-100-e-500">
										<i class="icon icon-radio-unchecked"></i>
										<span>R$ 100 - R$ 500</span>
									</label>
									<input type="radio" name="preco" value="2" id="entre-100-e-500" />
								</li>
								<li>
									<label for="entre-500-e-2500">
										<i class="icon icon-radio-unchecked"></i>
										<span>R$ 500 - R$ 2.500</span>
									</label>
									<input type="radio" name="preco" value="3" id="entre-500-e-2500" />
								</li>
								<li>
									<label for="acima-de-2500">
										<i class="icon icon-radio-unchecked"></i>
										<span>Acima de R$ 2.500</span>
									</label>
									<input type="radio" name="preco" value="4" id="acima-de-2500" />
								</li>
								<li>
									<label for="indiferente1">
										<i class="icon icon-radio-unchecked"></i>
										<span>- Indiferente -</span>
									</label>
									<input type="radio" name="preco" value="5" id="indiferente1" />
								</li>
							</ul>

							<button type="submit" class="button rounded js-submit-filter">Filtrar</button>
						</div> <!-- /box-content -->
					</form>
				</div>
				
				<?php if(!empty($artista) && !empty($artista[0]->facebookID)){ ?>
				<div class="box facebook-box">
					<h2 class="title align-left">Facebook</h2>
					<div class="box-content">
                        <iframe src="//www.facebook.com/plugins/likebox.php?href=<?php echo  $artista[0]->facebookID; ?>&amp;width&amp;height=558&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=true&amp;show_border=false" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
					</div> <!-- /box-content -->
				</div> <!-- /box -->
				<?php } ?>
			</aside>

	<?php } else { ?>

			<aside id="sidebar">
				<div class="box">
					<h2 class="title">Encontre a obra perfeita</h2>
					<form action="<?php echo base_url(); ?>obras/filtrar" method="get" class="js-filter-validation">
                    <input type="hidden" name="filtroTipoObra" id="filtroTipoObra" value="<?php if(isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])){ echo $_GET['filtroTipoObra'];} ?>"  />
                        <div class="box-content">
							<h3 class="subtitle">O que você procura?</h3> 
							<!-- <ul class="categories js-type-filter"> -->
							<ul class="categories js-categories">
								<li <?php if(!empty($_GET['filtroTipoObra']) && $_GET['filtroTipoObra'] == 'Pintura'){ echo 'class="selected"'; } ?>>
									<label for="pintura">
										<i class="icon icon-arrow-right-medium"></i>
										<span>Pintura</span>
									</label>
									<input type="radio" name="tipo" id="pintura" value="1" <?php if(!empty($_GET['filtroTipoObra']) && $_GET['filtroTipoObra'] == 'Pintura'){ echo "checked='checked'";} ?> />
								</li>
								<li <?php if(!empty($_GET['filtroTipoObra']) && $_GET['filtroTipoObra'] == 'Escultura'){ echo 'class="selected"';} ?>>
									<label for="escultura">
										<i class="icon icon-arrow-right-medium"></i>
										<span>Escultura</span>
									</label>
									<input type="radio" name="tipo" id="escultura" value="2" <?php if(!empty($_GET['filtroTipoObra']) && $_GET['filtroTipoObra'] == 'Escultura'){ echo "checked='checked'";} ?> />
								</li> 
								<li <?php if(!empty($_GET['filtroTipoObra']) && $_GET['filtroTipoObra'] == 'Fotografia'){ echo 'class="selected"';} ?>>
									<label for="fotografia">
										<i class="icon icon-arrow-right-medium"></i>
										<span>Fotografia</span>
									</label>
									<input type="radio" name="tipo" id="fotografia" value="3" <?php if(!empty($_GET['filtroTipoObra']) && $_GET['filtroTipoObra'] == 'Fotografia'){ echo "checked='checked'";} ?> />
								</li>
							</ul>

							<h3 class="subtitle">Tamanho</h3>
							<ul class="js-size-filter">
								<li> 
									<label for="ate-50-cm">
										<i class="icon icon-radio-unchecked <?php if(isset($_GET['tamanho']) && !empty($_GET['tamanho']) && $_GET['tamanho'] == 1){echo 'icon-radio-checked'; } ?>"></i>
										<span>Até 50 cm</span>
									</label>
									<input type="radio" name="tamanho" value="1" id="ate-50-cm" <?php if(isset($_GET['tamanho']) && !empty($_GET['tamanho']) && $_GET['tamanho'] == 1){echo 'checked'; }else{echo '' ;} ?>/>
								</li>
								<li>
									<label for="de-51-a-100">
										<i class="icon icon-radio-unchecked <?php if(isset($_GET['tamanho']) && !empty($_GET['tamanho']) && $_GET['tamanho'] == 2){echo 'icon-radio-checked'; } ?>"></i>
										<span>51 cm ~ 100 cm</span>
									</label>
									<input type="radio" name="tamanho" value="2" id="de-51-a-100" <?php if(isset($_GET['tamanho']) && !empty($_GET['tamanho']) && $_GET['tamanho'] == 2){echo 'checked'; }else{echo '' ;} ?> />
								</li>
								<li> 
									<label for="maior-que-100">
										<i class="icon icon-radio-unchecked <?php if(isset($_GET['tamanho']) && !empty($_GET['tamanho']) && $_GET['tamanho'] == 3){echo 'icon-radio-checked'; } ?>"></i>
										<span>Maior que 101 cm</span>
									</label>
									<input type="radio" name="tamanho" value="3" id="maior-que-100" <?php if(isset($_GET['tamanho']) && !empty($_GET['tamanho']) && $_GET['tamanho'] == 3){echo 'checked'; }else{echo '' ;} ?> />
								</li>
								<li> 
									<label for="indiferente">
										<i class="icon icon-radio-unchecked <?php if(isset($_GET['tamanho']) && !empty($_GET['tamanho']) && $_GET['tamanho'] == 4){echo 'icon-radio-checked'; } ?>"></i>
										<span>- Indiferente -</span>
									</label>
									<input type="radio" name="tamanho" value="4" id="indiferente" <?php if(isset($_GET['tamanho']) && !empty($_GET['tamanho']) && $_GET['tamanho'] == 4){echo 'checked'; }else{echo '' ;} ?>/>
								</li>
							</ul>

							<h3 class="subtitle">Técnica</h3><ul class="js-checkbox-filter">
                                <?php $arrTecnica = (isset($_GET['technique'])) ? $_GET['technique'] : array(); ?>
								<?php foreach ($tecnicaObra as $tec) { ?>
									<li>
										<label for="acrilica">
											<i class="icon icon-checkbox-unchecked <?php if(in_array($tec->idTecnica, $arrTecnica)){echo 'icon-checkbox-checked';} ?>"></i>
											<span><?php echo $tec->nomeTecnica; ?></span>
										</label>
										<input type="checkbox" name="technique[]" value="<?php echo $tec->idTecnica; ?>" id="<?php echo $tec->idTecnica;?>" <?php if(in_array($tec->idTecnica, $arrTecnica)){echo 'checked';}else{ echo '';} ?> />
									</li>
								<?php }?>		
							</ul>

							<h3 class="subtitle">Formato</h3>
							<ul class="js-checkbox-filter">
                                <?php $arrFormat = (isset($_GET['format'])) ? $_GET['format'] : array(); ?>
                                <?php foreach ($formatoObra as $form) { ?>
									<li>
										<label for="">
											<i class="icon icon-checkbox-unchecked <?php if(in_array($form->idFormato, $arrFormat)){echo 'icon-checkbox-checked';} ?>"></i>
											<span><?php echo $form->nomeFormato; ?></span>
										</label>
										<input type="checkbox" value="<?php echo $form->idFormato; ?>" name="format[]" id="<?php echo $form->idFormato;?>"  <?php if(in_array($form->idFormato, $arrFormat)){echo 'checked';}else{ echo '';} ?> />
									</li>
								<?php }?>											
							</ul>

							<h3 class="subtitle">Preço</h3>
							<ul class="js-price-filter">
								<li>
									<label for="ate-100">
										<i class="icon icon-radio-unchecked <?php if(isset($_GET['preco']) && !empty($_GET['preco']) && $_GET['preco'] == 1){echo 'icon-radio-checked'; } ?>"></i>
										<span>Até R$ 100</span>
									</label>
									<input type="radio" name="preco" value="1" id="ate-100" <?php if(isset($_GET['preco']) && !empty($_GET['preco']) && $_GET['preco'] == 1){echo 'checked'; }else{echo '' ;} ?>/>
								</li>
								<li>
									<label for="entre-100-e-500">
										<i class="icon icon-radio-unchecked <?php if(isset($_GET['preco']) && !empty($_GET['preco']) && $_GET['preco'] == 2){echo 'icon-radio-checked'; } ?>"></i>
										<span>R$ 100 - R$ 500</span>
									</label>
									<input type="radio" name="preco" value="2" id="entre-100-e-500" <?php if(isset($_GET['preco']) && !empty($_GET['preco']) && $_GET['preco'] == 2){echo 'checked'; }else{echo '' ;} ?> />
								</li>
								<li>
									<label for="entre-500-e-2500">
										<i class="icon icon-radio-unchecked <?php if(isset($_GET['preco']) && !empty($_GET['preco']) && $_GET['preco'] == 3){echo 'icon-radio-checked'; } ?>"></i>
										<span>R$ 500 - R$ 2.500</span>
									</label>
									<input type="radio" name="preco" value="3" id="entre-500-e-2500" <?php if(isset($_GET['preco']) && !empty($_GET['preco']) && $_GET['preco'] == 3){echo 'checked'; }else{echo '' ;} ?>/>
								</li>
								<li>
									<label for="acima-de-2500">
										<i class="icon icon-radio-unchecked <?php if(isset($_GET['preco']) && !empty($_GET['preco']) && $_GET['preco'] == 4){echo 'icon-radio-checked'; } ?>"></i>
										<span>Acima de R$ 2.500</span>
									</label>
									<input type="radio" name="preco" value="4" id="acima-de-2500" <?php if(isset($_GET['preco']) && !empty($_GET['preco']) && $_GET['preco'] == 4){echo 'checked'; }else{echo '' ;} ?>/>
								</li>
								<li>
									<label for="indiferente1">
										<i class="icon icon-radio-unchecked <?php if(isset($_GET['preco']) && !empty($_GET['preco']) && $_GET['preco'] == 5){echo 'icon-radio-checked'; } ?>"></i>
										<span>- Indiferente -</span>
									</label>
									<input type="radio" name="preco" value="5" id="indiferente1" <?php if(isset($_GET['preco']) && !empty($_GET['preco']) && $_GET['preco'] == 5){echo 'checked'; }else{echo '' ;} ?>/>
								</li>
							</ul>

							<h3 class="subtitle">Cor</h3>
							<ul class="js-color-filter">
								<li>
									<label for="colorida">
										<i class="icon icon-radio-unchecked <?php if(isset($_GET['cor']) && !empty($_GET['cor']) && $_GET['cor'] == 1){echo 'icon-radio-checked'; } ?>"></i>
										<span>Colorida</span>
									</label>
									<input type="radio" name="cor" value="1" id="colorida" <?php if(isset($_GET['cor']) && !empty($_GET['cor']) && $_GET['cor'] == 1){echo 'checked'; }else{echo '' ;} ?> />
								</li>
								<li>
									<label for="preto-e-branco">
										<i class="icon icon-radio-unchecked <?php if(isset($_GET['cor']) && !empty($_GET['cor']) && $_GET['cor'] == 100){echo 'icon-radio-checked'; } ?>"></i>
										<span>Preto &amp; Branco</span>
									</label>
									<input type="radio" name="cor" value="100" id="preto-e-branco" <?php if(isset($_GET['cor']) && !empty($_GET['cor']) && $_GET['cor'] == 100){echo 'checked'; }else{echo '' ;} ?> />
								</li>
							</ul>

                            <?php $arrCores = (isset($_GET['cores'])) ? $_GET['cores'] : array(); ?>
							<span class="colors js-colors">

								<span class="color red <?php if(in_array(1, $arrCores) && $_GET['cor'] != 100){echo 'selected';}else{ echo '';} ?>">

									<label for="vermelho">
										<span>Vermelho</span>
										<input type="checkbox" id="vermelho"  value="1" name="cores[]" <?php if(in_array(1, $arrCores) && $_GET['cor'] != 100){echo 'checked';}else{ echo '';} ?> />
									</label>
								</span>
								
								<span class="color orange <?php if(in_array(2, $arrCores) && $_GET['cor'] != 100){echo 'selected';}else{ echo '';} ?>">
									<label for="laranja">
										<span>Laranja</span>
										<input type="checkbox" id="laranja" value="2" name="cores[]" <?php if(in_array(2, $arrCores) && $_GET['cor'] != 100){echo 'checked';}else{ echo '';} ?> />
									</label>
								</span>

								<span class="color yellow <?php if(in_array(3, $arrCores) && $_GET['cor'] != 100){echo 'selected';}else{ echo '';} ?>">
									<label for="amarelo">
										<span>Amarelo</span>
										<input type="checkbox" id="amarelo" value="3" name="cores[]" <?php if(in_array(3, $arrCores) && $_GET['cor'] != 100){echo 'checked';}else{ echo '';} ?> />
									</label>
								</span>

								<span class="color green <?php if(in_array(4, $arrCores) && $_GET['cor'] != 100){echo 'selected';}else{ echo '';} ?>">
									<label for="verde">
										<span>Verde</span>
										<input type="checkbox" id="verde" value="4" name="cores[]" <?php if(in_array(4, $arrCores) && $_GET['cor'] != 100){echo 'checked';}else{ echo '';} ?>/>
									</label>
								</span>

								<span class="color blue-light <?php if(in_array(5, $arrCores) && $_GET['cor'] != 100){echo 'selected';}else{ echo '';} ?>">
									<label for="azul-claro">
										<span>Azul Claro</span>
										<input type="checkbox" id="azul-claro" value="5" name="cores[]" <?php if(in_array(5, $arrCores) && $_GET['cor'] != 100){echo 'checked';}else{ echo '';} ?>/>
									</label>
								</span>

								<span class="color blue <?php if(in_array(6, $arrCores) && $_GET['cor'] != 100){echo 'selected';}else{ echo '';} ?>">
									<label for="azul">
										<span>Azul</span>
										<input type="checkbox" id="azul" value="6" name="cores[]" <?php if(in_array(6, $arrCores) && $_GET['cor'] != 100){echo 'checked';}else{ echo '';} ?> />
									</label>
								</span>

								<span class="color purple <?php if(in_array(7, $arrCores) && $_GET['cor'] != 100){echo 'selected';}else{ echo '';} ?>">
									<label for="roxo">
										<span>Roxo</span>
										<input type="checkbox" id="roxo" value="7" name="cores[]" <?php if(in_array(7, $arrCores) && $_GET['cor'] != 100){echo 'checked';}else{ echo '';} ?> />
									</label>
								</span>

								<span class="color pink <?php if(in_array(8, $arrCores) && $_GET['cor'] != 100){echo 'selected';}else{ echo '';} ?>">
									<label for="rosa">
										<span>Rosa</span>
										<input type="checkbox" id="rosa" value="8" name="cores[]" <?php if(in_array(8, $arrCores) && $_GET['cor'] != 100){echo 'checked';}else{ echo '';} ?> />
									</label>
								</span>

								<span class="color white <?php if(in_array(9, $arrCores) && $_GET['cor'] != 100){echo 'selected';}else{ echo '';} ?>">
									<label for="branco">
										<span>Branco</span>
										<input type="checkbox" id="branco" value="9" name="cores[]" <?php if(in_array(9, $arrCores) && $_GET['cor'] != 100){echo 'checked';}else{ echo '';} ?> />
									</label>
								</span>

								<span class="color gray <?php if(in_array(10, $arrCores) && $_GET['cor'] != 100){echo 'selected';}else{ echo '';} ?>">
									<label for="cinza">
										<span>Cinza</span>
										<input type="checkbox" id="cinza" value="10" name="cores[]" <?php if(in_array(10, $arrCores) && $_GET['cor'] != 100){echo 'checked';}else{ echo '';} ?> />
									</label>
								</span>

								<span class="color black <?php if(in_array(11, $arrCores) && $_GET['cor'] != 100){echo 'selected';}else{ echo '';} ?>">
									<label for="preto">
										<span>Preto</span>
										<input type="checkbox" id="preto" value="11" name="cores[]" <?php if(in_array(11, $arrCores) && $_GET['cor'] != 100){echo 'checked';}else{ echo '';} ?> />
									</label>
								</span>

								<span class="color brown <?php if(in_array(12, $arrCores) && $_GET['cor'] != 100){echo 'selected';}else{ echo '';} ?>">
									<label for="marrom">
										<span>Marrom</span>
										<input type="checkbox" id="marrom" value="12" name="cores[]" <?php if(in_array(12, $arrCores) && $_GET['cor'] != 100){echo 'checked';}else{ echo '';} ?> />
									</label>								
								</span>
														
							</span>
							
							<h3 class="subtitle">Categorias</h3>
							<ul class="js-checkbox-filter">
                                <?php $arrCategoria = (isset($_GET['categorias'])) ? $_GET['categorias'] : array(); ?>
                                <?php foreach ($categoriaObra as $cat) { ?>
									<li>
										<label for="abstratas">
											<i class="icon icon-checkbox-unchecked <?php if(in_array($cat->idCategoria, $arrCategoria)){echo 'icon-checkbox-checked';} ?>"></i>
											<span><?php echo $cat->nomeCategoria; ?></span>
										</label>
										<input type="checkbox" name="categorias[]" id="<?php echo $cat->idCategoria; ?>" value="<?php echo $cat->idCategoria; ?>" <?php if(in_array($cat->idCategoria, $arrCategoria)){echo 'checked';}else{ echo '';} ?>/>
									</li>
								<?php } ?>
							</ul>

							<button type="submit" class="button rounded js-submit-filter">Filtrar</button>
						</div> <!-- /box-content -->
					</form>
					<?php if ($url == 'parceiros.php'){ ?>
						<?php include('includes/social.php'); ?>
					<?php } ?>
				</div> <!-- /box -->
			</aside>

<?php } ?>

