<h2 class="title">Perguntas Frequentes</h2>
						<div class="box-content">
							<ul class="quickly-faq">
								<li><a href="https://www.mercadoarte.com.br/blog/como-faco-para-ter-minha-pagina-no-mercado-arte/" target="_blank">Como faço para ter minha página no Mercado Arte?</a></li>
								<li><a href="https://www.mercadoarte.com.br/blog/voces-aceitam-artistas-mercado-primario/" target="_blank">Vocês aceitam artistas do mercado primário?</a></li>
								<li><a href="https://www.mercadoarte.com.br/blog/quando-vender-terei-de-pagar-comissao/" target="_blank">Quando vender, terei de pagar comissão?</a></li>
							</ul>
						</div> <!-- /box-content -->