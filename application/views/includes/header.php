<?php
    $url = basename($_SERVER['PHP_SELF']);
	$current = current_url();
	
	if(!empty($identObra)){
		$identificador = $identObra;	
	}else{
		$identificador = '';	
	}    

	if(!empty($artista)){
		$slug = $artista[0]->slugpagina;

	}else{
		$slug = 'Artista_nao_existe';
	}



?>

		<header id="header">

			<div class="top-links">

				<div class="wrapper">
					<?php 
						if ($current == base_url() . $slug  ||  $current == base_url() .$slug.'/biografia' || $current == base_url() . 'artistas/sobre' || $current == base_url() . $slug . "/obras" || $current == base_url() . $slug . '/obras/' . $identificador){
					?>
					<a class="pull-button js-pull" href="#">
						<span>Menu</span>
					</a>
					<h1 class="back-home"><a href="<?php echo base_url(); ?>">Mercado Arte</a></h1>
					<?php
						} else {
								@session_start();
								if (!empty($_SESSION['usuario_logado'])) {
                                    $this->load->database();
                                    $this->db->select('pg.nomeArtistico, pg.slugPagina');
                                    $this->db->where('email',$_SESSION['usuario_logado']);
                                    $this->db->join('tblpgartista pg','u.idUsuario = pg.idUsuario');
                                    $query = $this->db->get('tblusuarios u');
                                    $infoLogado = $query->result(); 
                                    $usuario_logado = $_SESSION['usuario_logado'];
                    ?>
								<a href="/admin/" class="login">
								     <i class="icon icon-user"></i>
								     <span>Olá, 
								     <strong>
								     	<?php 
					                	if($usuario_logado == 'admin@mercadoarte.com.br'){
					                			echo 'Administrador';
					                	}
					                	elseif(!empty($infoLogado[0]->nomeArtistico)){
								     		echo $infoLogado[0]->nomeArtistico;
								     	} else {
								     		echo 'Usuário';
								     	}  ?>.
								     </strong>
								 	</span>
								</a>
							<?php } else { ?>
								<a href="<?php echo base_url(); ?>login" class="login">
									<i class="icon icon-user"></i>
									<span>Faça login</span>
								</a>
								<a href="<?php echo base_url(); ?>cadastro" class="login">
									<span>Cadastre-se grátis</span>
								</a>
							<?php } ?> 
					<?php }  ?>

					<ul>
						<li><a href="https://www.mercadoarte.com.br/blog/comunidade/">Ajuda</a></li>
						<li><a href="https://www.mercadoarte.com.br/blog/">Blog</a></li>
						<li><a href="https://www.mercadoarte.com.br/blog/comunidade/">Fórum</a></li>
						<li><a href="https://www.mercadoarte.com.br/blog/avaliacao-de-obras-de-arte/">Serviços</a></li>
						<li><a href="https://www.mercadoarte.com.br/blog/agenda-de-eventos-de-arte/">Eventos</a></li>
						<li><a href="https://www.mercadoarte.com.br/blog/contato/">Contato</a></li>
					</ul>
				</div>

				<div class="menu-pull js-menu-pull">
					<ul>
						<li><a href="<?php echo base_url(); ?>obras/pinturas?filtroTipoObra=Pintura">Pinturas</a></li>
						<li><a href="<?php echo base_url(); ?>obras/esculturas?filtroTipoObra=Escultura">Esculturas</a></li>
						<li><a href="<?php echo base_url(); ?>obras/fotografias?filtroTipoObra=Fotografia">Fotografias</a></li>
						<li><a href="<?php echo base_url(); ?>artistas">Artistas</a></li>
						<li><a href="<?php echo base_url(); ?>parceiros">Parceiros</a></li>
					</ul>
				</div>

			</div> <!-- /quick-links -->

			<div class="header">

				<div class="wrapper">
					<?php 
						if ($current == base_url() . $slug ||  $current == base_url() .$slug.'/biografia' || $current == base_url() . 'artistas/sobre' || $current == base_url() . $slug . "/obras" || $current == base_url() . $slug . '/obras/' . $identificador){
					?>

						<div class="profile-name">
							<div class="thumbnail">
								<?php if (isset($artista[0]->imgPerfil)) { ?>
									<img src="<?php echo base_url(); ?>content/upload/<?php echo SLUG . '/fp/fp-small/' . $artista[0]->imgPerfil; ?>"  width="95" alt="<?php echo $artista[0]->nomeArtistico; ?>" />
								<?php } else { ?>
									<img src="<?php echo base_url(); ?>content/images/avatar.png" width="95" alt="<?php echo $artista[0]->nomeArtistico; ?>" />
								<?php } ?>
							</div>

							<div class="name">
								<h2><?php echo $artista[0]->nomeArtistico; ?></h2>
								<p><?php echo $artista[0]->subtitulo; ?></p>
							</div>
						</div> <!-- /profile-name -->

						<div class="contact-info">  
							<p>
								<i class="icon hide-text sprite-phone">Telefone:</i> 
								<strong><?php echo $artista[0]->telefoneCelular; ?></strong>
								<span class="region">
									<span class="flag hide-text sprite-flag-br">Brasil</span>
									<span><?php echo $artista[0]->cidade; ?>/<?php echo $artista[0]->estado; ?></span>
								</span>
							</p>
						</div> <!-- /contact-info -->
					<?php } else { ?>

						<h1><a href="<?php echo base_url(); ?>" class="logo hide-text sprite-logo">Mercado Arte</a></h1>

						<div class="search-form">
							<p>O você está procurando?</p>
							<form action="<?php echo base_url(); ?>obras/buscar" method="post">
								<input type="text" class="input rounded-left search-input" name="busca" placeholder="quadro abstrato, escultura em bronze, foto" />
								<button type="submit" class="button rounded-right search-button">
									<i class="icon icon-search"></i>
									<span>Buscar</span>
								</button>
							</form>
						</div> <!-- /search-form -->

						<div class="contact-info">  
							<p>Dúvidas? Fale conosco</p>
							<p>
								<i class="icon hide-text sprite-phone">Telefone:</i> 
								<strong>(11) 2528-4599</strong>
							</p>
						</div> <!-- /contact-info -->
					<?php } ?>

				</div>

			</div> <!-- /header -->

			<div class="wrapper wrapper-menu">
				<nav class="menu js-menu">
					<ul>
					<?php
						if ($current == base_url() . $slug ||  $current == base_url() .$slug.'/biografia' || $current == base_url() . 'artistas/sobre' || $current == base_url() . $slug . "/obras" || $current == base_url() . $slug . '/obras/' . $identificador ){
					?>

						<li>
							<a href="<?php echo base_url() . $slug; ?>" <?php if ($this->uri->segment(1) == $slug && $this->uri->segment(2) == ''){ echo ' class="current"'; } ?>>
								<i class="icon icon-home"></i>
								<span>Home</span>
							</a>
						</li>
						<li><a href="<?php echo base_url() . $slug; ?>/obras"<?php if ($this->uri->segment(2) == 'obras'){ echo ' class="current"'; } ?>>Obras</a></li>
						<li><a href="<?php echo base_url() . $slug; ?>/biografia"<?php if (empty($_GET['section']) && $this->uri->segment(2) == 'biografia'){ echo ' class="current"'; } ?>>Artista</a></li>
						<li><a href="<?php echo base_url() . $slug; ?>/biografia?section=contato"<?php if(!empty($_GET['section']) && $_GET['section'] == 'contato'){ echo ' class="current"'; } ?>>Contato</a></li>
					<?php } else { ?>

						<li>
							<a href="<?php echo base_url(); ?>" <?php if ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'home'  ){ echo ' class="current"'; } ?>>
								<i class="icon icon-home"></i>
								<span>Home</span>
							</a>
						</li>
						<li><a href="<?php echo base_url(); ?>obras/pinturas?filtroTipoObra=Pintura"<?php if ($url == 'pinturas' || !empty($_GET['filtroTipoObra']) && $_GET['filtroTipoObra'] == 'Pintura'){ echo ' class="current"'; } ?>>Pintura</a></li>
						<li><a href="<?php echo base_url(); ?>obras/esculturas?filtroTipoObra=Escultura"<?php if ($url == 'esculturas'  || !empty($_GET['filtroTipoObra']) && $_GET['filtroTipoObra'] == 'Escultura'){ echo ' class="current"'; } ?>>Escultura</a></li>
						<li><a href="<?php echo base_url(); ?>obras/fotografias?filtroTipoObra=Fotografia"<?php if ($url == 'fotografias'  || !empty($_GET['filtroTipoObra']) && $_GET['filtroTipoObra'] == 'Fotografia'){ echo ' class="current"'; } ?>>Fotografia</a></li>
						<li><a href="<?php echo base_url(); ?>artistas"<?php if ($this->uri->segment(1) == 'artistas'){ echo ' class="current"'; } ?>>Artistas</a></li>
						<li><a href="<?php echo base_url(); ?>parceiros"<?php if ($this->uri->segment(1) == 'parceiros'){ echo ' class="current"'; } ?>>Parceiros</a></li>
					<?php } ?>

					</ul>
					<a href="<?php echo base_url(); ?>favoritos" class="liked<?php if ($url == 'favoritos'){ echo ' current'; } ?>">Favoritos <i class="icon icon-heart"></i></a>
				</nav>
			</div>
		</header>
		