<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>">Home</a></li>
					<li>Resultado da Busca</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<section class="main search-result">
            <?php if(empty($obras)){ ?>
                <h3 class="title">Resultado da busca</h3>
                    <div class="no-post">
                        <span class="big-icon gray-icon">
                            <i class="icon icon-search"></i>
                        </span>
                        <h3>Desculpe, mas não encontramos <br />nada relacionado a <strong>"<?php echo $busca; ?>"</strong>.</h3>
                        <p>Tente novamente ou navegue pelo site <br />e conheça todas nossas obras e artistas.</p>

                        <a href="<?php base_url(); ?>/obras" class="button rounded big-button">Navegar pelas obras</a>

                    </div> <!-- /no-post -->
				<?php }else{ ?>

				<h3 class="title margin-bottom">Encontramos estes resultados para a busca por "<?php echo $busca; ?>" que você realizou:</h3>
				<section class="list">

                    <?php foreach ($obras as $o) { ?>

                        <article class="art">
                            <a href="<?php echo base_url() . $o->slugpagina; ?>/obras/<?php echo $o->slugObra; ?>">
                                <div class="thumbnail">
                                    <img src="<?php echo base_url(); ?>content/upload/<?php echo $o->slugpagina; ?>/<?php echo $o->slugObra; ?>/small/<?php echo $o->imgFoto1; ?>" alt="<?php echo $o->titulo; ?>" />
                                </div> <!-- /thumbnail -->

                                <h4 class="artist">
                                	<?php echo $o->nomeArtistico; ?>
                                    <span class="badge <?php if($o->votosTotais >= 0 && $o->votosTotais <= 99){ echo ''; }elseif($o->votosTotais >= 100 && $o->votosTotais <= 999 ){echo 'sprite-badge-bronze'; }elseif($o->votosTotais >= 1000 && $o->votosTotais <= 9999){echo 'sprite-badge-silver';}elseif($o->votosTotais >= 10000){echo 'sprite-badge-gold';} ?>"></span>
                            	</h4>
                                <h5 class="description"><?php echo $o->titulo; ?> - <?php echo $o->nomeTecnica; ?> - <?php echo $o->descricao; ?> - <?php echo number_format($o->altura, 0); ?> x <?php echo number_format($o->comprimento, 0); if(!empty($o->largura)){ echo ' x '. number_format($o->largura, 0);} ?> cm</h5>

                                <div class="price">
                                    <?php if($o->idStatusObra == 2 || $o->idStatusObra == 5 || $o->idStatusObra == 8){ ?>
                                        <p class="soldout">Vendido</p>
                                    <?php }else if($o->idStatusObra == 3 || $o->idStatusObra == 6 || $o->idStatusObra == 9){ ?>
                                        <a href="<?php echo base_url(). $o->slugpagina; ?>/obras/<?php echo $o->slugObra; ?>?section=consultar" class="consult">Consulte</a>
                                    <?php }else{ ?>
                                        <?php if($o->precoComDesconto == 0.00){ ?>
                                            <p class="descount">R$ <?php echo number_format((int)str_ireplace(".",",",$o->precoObra), 2, ',', '.'); ?></p>
                                        <?php }else{ ?>
                                            <small class="value">De R$ <?php echo number_format((int)str_ireplace(".",",",$o->precoObra), 2, ',', '.'); ?></small>
                                            <p class="descount">R$ <?php echo number_format((int)str_ireplace(".",",",$o->precoComDesconto), 2, ',', '.'); ?></p>
                                        <?php } ?>
                                    <?php } ?>
                                </div> <!-- /price -->
                            </a>

                            <div class="rating">
                                <ul class="star-rating" id="star-rating-<?php echo $o->idObra; ?>">
                                    <?php
                                    $notas = round($o->nota);

                                    for ($i=0; $i < 5; $i++) { ?>
                                        <li class="star-number-<?php echo $i + 1; ?><?php if($notas > 0){ echo " active"; } ?>">
											<a href="#" class="icon-vote" data-idObra="<?php echo $o->idObra; ?>" data-icon="<?php echo $i; ?>"><i class="icon icon-star"></i>
												<span><?php echo $i + 1; ?></span>
											</a>
										</li>

                                        <?php
                                        $notas--;
                                    }

                                    ?>
                                </ul>
                            </div> <!-- /rating -->
                        </article>

                    <?php } ?>

                    <br class="clear" />

                   <!-- <ul class="pagination">
                        <?php /*// echo $this->pagination->create_links();   */?>
                    </ul>-->

				</section> <!-- /list -->

			</section>

			<br class="clear" />
        <?php } ?>

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>