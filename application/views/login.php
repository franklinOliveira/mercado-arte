<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
		
	</head>
	  
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>">Home</a></li>
					<li>Login</li>
				</ul>
			</div> <!-- /breadcrumb -->

				<section class="main login-page">

                    <?php if (isset($success)): ?>
                        <div class="alert alert-success">
                            <p><?=$success?></p>
                        </div>
                    <?php endif; ?>

                    <?php if (isset($erro)): ?>
                        <div class="alert alert-danger">
                            <p><?=$erro?></p>
                        </div>
                    <?php endif; ?>

					<span class="column-half">
						<form class="login-form js-login-form" method="post">
							<span class="big-icon">
								<i class="icon icon-lock-open"></i>
							</span>

							<legend class="login-title">Já possui cadastro? Entre com seus dados de acesso ao <strong>Mercado Arte.</strong></legend>

							<label for="email">E-mail:</label>
							<input type="email" name="email" class="input email rounded validate[required,custom[email]]">

							<label for="email">Senha:</label>
							<input type="password" name="pass" class="input name rounded validate[required]">

							<span class="keep">
								<a href="#" class="js-show-pass">Esqueceu a Senha?</a>
							</span> <!-- /keep -->

							<button type="submit" class="button register rounded js-submit-login">Entrar</button>
						</form>
					</span>
					<span class="column-half">
						<span class="register-step">
							<span class="big-icon">
								<i class="icon icon-users"></i>
							</span>
							<span class="login-title">
								<p>Ainda <strong>não se registrou?</strong></p>
								<p>Está esperando o que?</p>
								<br />
								<p><strong>Faça parte</strong> do Mercado Arte e <br />tenha acesso a um universo artístico criativo para encontrar a <strong>obra de arte perfeita</strong>.</p>
							</span>
							<a href="<?php base_url(); ?>/cadastro" class="button rounded">Criar Conta</a>
						</span>
					</span>
					
					<div class="white-overlay js-lost-pass">
						<span class="lost-pass">
							<a href="#" class="icon icon-close icon-close-big js-hide-pass"></a>
							<form class="login-form js-lost-form"  method="post">
								<span class="big-icon">
									<i class="icon icon-user"></i>
								</span>
								<legend class="login-title">Para <strong>redefinir sua senha</strong>, preecha o campo email e clique em <strong>enviar</strong>.</legend>

								<label for="email">E-mail:</label>
								<input type="email" name="recuperarSenha" class="input email rounded validate[required,custom[email]]">

								<button type="submit" class="button register rounded js-submit-lost">Enviar</button>
							</form>
						</span>
					</div>

				</section> <!-- /main -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>
