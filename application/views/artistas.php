<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
		
	</head>
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>">Home</a></li>
					<li>Artistas</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php include('includes/sidebar.php'); ?>

			<div class="content">
                <?php if(!empty($artistaDoMes)){ ?>
				<section class="primary-banner primary-banner-small">
					<div class="resume">
						<h2 class="winner">Artista do Mês</h2>
						<h3 class="name">
							<?php echo $artistaDoMes[0]->nomeArtistico; ?>
							<span class="badge <?php if($artistaDoMes[0]->votosTotais >= 0 && $artistaDoMes[0]->votosTotais <= 99){ echo ''; }elseif($artistaDoMes[0]->votosTotais >= 100 && $artistaDoMes[0]->votosTotais <= 999 ){echo 'sprite-badge-bronze-big'; }elseif($artistaDoMes[0]->votosTotais >= 1000 && $artistaDoMes[0]->votosTotais <= 9999){echo 'sprite-badge-silver-big';}elseif($artistaDoMes[0]->votosTotais >= 10000){echo 'sprite-badge-gold-big';} ?>"></span>
						</h3>
						<?php if (isset($artistaDoMes[0]->imgBannerPg)) { ?>
							<img src="<?php echo base_url(); ?>content/upload/<?php echo $artistaDoMes[0]->slugpagina . '/fp/fp-large/' . $artistaDoMes[0]->imgBannerPg; ?>" width="640" height="90" alt="Imagem de <?php echo $artistaDoMes[0]->nomeArtistico; ?>" />
						<?php } else { ?>
							<img src="<?php echo base_url(); ?>content/images/art.png" width="640" height="90" alt="Imagem de <?php echo $artistaDoMes[0]->nomeArtistico; ?>" />
						<?php } ?>
					</div> <!-- /resume -->

					<article class="artist artist-about">
						<div class="thumbnail">
							<a href="<?php echo base_url() . $artistaDoMes[0]->slugpagina; ?>">
								<img src="<?php echo base_url(); ?>content/upload/<?php echo $artistaDoMes[0]->slugpagina; ?>/fp/fp-medium/<?php echo $artistaDoMes[0]->imgPerfil; ?>" width="180" height="180" alt="<?php echo $artistaDoMes[0]->nomeArtistico; ?>" />
							</a>
						</div> <!-- /thumbnail -->

						<div class="description">
							<p class="city"><span class="flag hide-text sprite-flag-<?php echo strtolower($artistaDoMes[0]->iso); ?>"><?php echo $artistaDoMes[0]->nomePais; ?></span><?php echo $artistaDoMes[0]->cidade; ?>/<?php echo $artistaDoMes[0]->estado; ?></p>
							<p><?php echo substr_replace($artistaDoMes[0]->txtBiografia, (strlen($artistaDoMes[0]->txtBiografia) > 300 ? '...' : ''), 300); ?><a href="<?php echo base_url(); ?><?php echo $artistaDoMes[0]->slugpagina; ?>/biografia"><i class="icon icon-arrow-right-big"></i> Continue Lendo</a></p>
							<ul class="actions">
								<li class="first"><a href="<?php echo base_url(); ?><?php echo $artistaDoMes[0]->slugpagina; ?>/obras">Ver Obras <i class="icon icon-camera"></i></a></li>
                                <?php if(!in_array($artistaDoMes[0]->idUsuario, $favoritos)){ ?>
                                    <li><a  class="add-favorite-click" id="add-favorite-<?php echo $artistaDoMes[0]->idUsuario; ?>" data-idArtista="<?php echo $artistaDoMes[0]->idUsuario; ?>" href="<?php echo base_url(); ?>favoritos/adicionarFavorito">Adicionar aos Favoritos <i class="icon icon-add-favorite"></i></a></li>
                                <?php } else { ?>
                                    <li><a href="<?php echo base_url(); ?>favoritos" class="favorite">Artista Favorito <i class="icon icon-heart"><span></span></i></a></li>
                                <?php } ?>
								<li class="last"><a href="<?php echo base_url() . $artistaDoMes[0]->slugpagina; ?>/biografia?section=contato">Entrar em Contato <i class="icon icon-comment"></i></a></li>
							</ul>
						</div> <!-- /description -->
					</article>
				</section> <!-- /primary-banner -->
                <?php } ?>
				<section class="right-column">
					<div class="box">
						<?php include('includes/social.php'); ?>

						<?php include('includes/faq.php'); ?>
					</div> <!-- /box -->
				</section> <!-- /right-column -->

				<br class="clear" />

				<section class="main border-top">

					<h3 class="title page-title">Artistas</h3>
						<div class="select-artist">
							<h4>Selecione uma letra:</h4>
						</div>
						<ul class="filter-letter js-filter-fixed">
							<li <?php $action = ((!empty($_GET['i']) && $_GET['i'] == 'A') || (!isset($_GET['i']))) ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=A">A</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'B') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=B">B</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'C') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=C">C</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'D') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=D">D</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'E') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=E">E</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'F') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=F">F</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'G') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=G">G</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'H') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=H">H</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'I') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=I">I</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'J') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=J">J</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'K') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=K">K</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'L') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=L">L</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'M') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=M">M</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'N') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=N">N</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'O') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=O">O</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'P') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=P">P</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'Q') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=Q">Q</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'R') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=R">R</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'S') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=S">S</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'T') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=T">T</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'U') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=U">U</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'V') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=V">V</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'W') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=W">W</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'X') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=X">X</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'Y') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=Y">Y</a></li>
							<li <?php $action = (!empty($_GET['i']) && $_GET['i'] == 'Z') ? 'class="current"' : ""; echo $action; ?>><a href="<?php echo base_url(); ?>artistas?i=Z">Z</a></li>
						</ul>

					<section class="list js-filter-scroll">

						<?php if(!empty($artistasObras)){ ?> 	
							<?php foreach ($artistasObras as $artista) { ?>
								<article class="partner artist">
									<div class="thumbnail">
										<a href="<?php echo base_url() . $artista['dados']->slugpagina; ?>">
											<?php if (isset($artista['dados']->imgPerfil)) { ?>
												<img src="<?php echo base_url(); ?>content/upload/<?php echo $artista['dados']->slugpagina . '/fp/fp-large/' . $artista['dados']->imgPerfil; ?>" width="110" height="110" alt="<?php echo $artista['dados']->nomeArtistico; ?>" />
											<?php } else { ?>
												<img src="<?php echo base_url(); ?>content/images/avatar.png" width="110" height="110" alt="<?php echo $artista['dados']->nomeArtistico; ?>" />
											<?php } ?>
										</a>
									</div> <!-- /thumbnail -->

									<div class="description">
										<h5 class="name"><a href="<?php echo base_url() . $artista['dados']->slugpagina; ?>"><?php echo $artista['dados']->nomeArtistico; ?></a><span class="badge  <?php if($artista['dados']->votosTotais >= 0 && $artista['dados']->votosTotais <= 99){ echo ''; }elseif($artista['dados']->votosTotais >= 100 && $artista['dados']->votosTotais <= 999 ){echo 'sprite-badge-bronze'; }elseif($artista['dados']->votosTotais >= 1000 && $artista['dados']->votosTotais <= 9999){echo 'sprite-badge-silver';}elseif($artista['dados']->votosTotais >= 10000){echo 'sprite-badge-gold';} ?>"></span></h5>
										<p class="city"><span class="flag sprite-flag-<?php echo strtolower($artista['dados']->iso); ?>"><?php echo $artista['dados']->nomePais; ?></span><?php echo $artista['dados']->cidade; ?>/<?php echo $artista['dados']->estado; ?></p>
										<ul class="actions">
											<?php if(!in_array($artista['dados']->idUsuario, $favoritos)){ ?>
												<li><a  class="add-favorite-click" id="add-favorite-<?php echo $artista['dados']->idUsuario; ?>" data-idArtista="<?php echo $artista['dados']->idUsuario; ?>" href="<?php echo base_url(); ?>favoritos/adicionarFavorito">Adicionar aos Favoritos <i class="icon icon-add-favorite"></i></a></li>
											<?php } else { ?>
												<li><a href="<?php echo base_url(); ?>favoritos" class="favorite">Artista Favorito <i class="icon icon-heart"><span>Veja os seus favoritos!</span></i></a></li>
											<?php } ?>
											<li><a href="<?php echo base_url() . $artista['dados']->slugpagina; ?>/biografia?section=contato">Entrar em Contato <i class="icon icon-comment"></i></a></li>
										</ul>
									</div> <!-- /description -->

									<div class="thumbs">
										<?php foreach ($artista['obras'] as $obras) { ?>
											<div class="item">
												<a href="<?php echo base_url() . $artista['dados']->slugpagina; ?>/obras/<?php echo $obras->slugObra; ?>">
													<img src="<?php echo base_url(); ?>content/upload/<?php echo $artista['dados']->slugpagina; ?>/<?php echo $obras->slugObra; ?>/small/<?php echo $obras->imgFoto1; ?>" alt="<?php echo $obras->titulo; ?>">
												</a>
											</div>
										<?php } ?>								
										<div class="item view-more">
											<a href="<?php echo base_url(); ?><?php echo $artista['dados']->slugpagina;?>/obras">
												<span>Ver</span> 
												<i class="icon icon-plus"><span>Mais</span></i>
											</a>
										</div>
									</div> <!-- /thumbs -->
								</article>

							<?php } ?>
						<?php } else { ?>
							<div class="no-post">
		                        <span class="big-icon gray-icon">
		                            <i class="icon icon-users"></i>
		                        </span>
		                        <h3>Desculpe, não existe <strong>nenhum artista</strong> <br />que comece com a <strong>letra <?php echo (isset($_GET['i']) && !empty($_GET['i']))? $_GET['i'] : 'A'; ?></strong>.</h3>
		                        <a href="javascript:history.back();" class="button rounded big-button">Voltar</a> 
		                    </div> <!-- /no-post -->
						<?php } ?>					

						<br class="clear" />

						<ul class="pagination">
							<?php echo $this->pagination->create_links(); ?>							
						</ul>

					</section> <!-- /list -->

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>