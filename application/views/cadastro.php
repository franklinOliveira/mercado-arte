<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>

	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>/">Home</a></li>
					<li><a href="<?php base_url(); ?>/cadastro">Cadastro</a></li>
					<li>Passo 1</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php 
				$data['tecnicaObra']   = $tecnicaObra;
				$data['formatoObra']   = $formatoObra;
				$data['categoriaObra'] = $categoriaObra;
				$this->load->view('includes/sidebar',$data); 
			?>

			<div class="content">

				<section class="main">

					<form action="<?php echo base_url(); ?>cadastro/cadastrar" class="js-register-form" method="post">
						<h3 class="title page-title">Quero me cadastrar e fazer parte do Mercado Arte</h3>
						<div class="forms step-1 border">
							<span class="column-half">
								<legend>Bem-vindo ao Mercado Arte!</legend>
								<?php
									echo '<div class="error">';
									echo validation_errors('<p>', '</p>');
									echo '</div>'; 
								?>
								<label for="pais">Selecione em qual pais você reside:</label>
								<select name="pais" id="pais" class="select-no-appearance js-select-country validate[required]">
								    <option value="">Selecione um país...</option>
								    <?php foreach ($paises as $p) { ?>
								    	<option value="<?php echo $p->idPais; ?>"> <?php echo $p->nome;?></option>
								    <?php } ?>
								</select>

								<label for="email">* Digite seu e-mail:</label>
								<input type="email" name="email" id="email" value="<?php echo set_value('email'); ?>" class="input rounded validate[required,custom[email]]" />

								<label for="email-check">* Digite seu e-mail novamente:</label>
								<input type="email" name="email-check" id="email-check" value="<?php echo set_value('email-check'); ?>" class="input rounded validate[required,custom[email],equals[email]]" />

								<label for="password">* Crie uma senha:</label>
								<input type="password" name="senha" id="password" value="<?php echo set_value('senha'); ?>" class="input rounded validate[required,minSize[6],maxSize[12]]" />

								<label for="celular">* Telefone celular:</label>
								<input type="text" name="celular" id="celular" value="<?php echo set_value('celular'); ?>" class="input rounded validate[required]" />

								<label for="residencial">Telefone residencial:</label>
								<input type="text" name="telefoneResidencial" id="residencial" value="<?php echo set_value('telefoneResidencial'); ?>" class="input rounded" />

								<button type="submit" class="button rounded js-submit-register">Cadastrar</button>
								<span class="js-loading-register loading-register"><i class="icon icon-animated icon-loader-2"></i></span>
							</span>

							<span class="column-half border-left form-helper">
								<h4 class="hide-text sprite-logo">Mercado Arte</h4>
								<p>O <strong>Mercado Arte</strong> disponibiliza para os artistas a oportunidade de ter uma página na Web para exibir seus trabalhos e para o público em geral a chance de acessibilidade a um universo artístico criativo que vai muito além do que se apresenta em galerias, museus e sites atualmente.</p>
								<p>Preencha os campos ao lado e clique em <strong>cadastrar</strong>. Em caso de dúvidas, ligue para <strong>(11) 2528-4599</strong>.</p>
								<p>O número de telefone <strong>celular</strong> e <strong>residencial</strong>, não serão divulgados no site.</p>
								<p>Seu <strong>país</strong> será divulgado em sua página e visível para os visitantes.</p>
								<p><strong>* Preenchimento obrigatório</strong></p>
							</span>
						</div> <!-- /forms -->

					</form>

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>