<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>">Home</a></li>
					<li>Favoritos</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<section class="main favorites">
				<h3 class="title">Meus favoritos</h3>				
			<?php if($possuiFavoritos){ ?>	
				<section class="list js-filter-scroll">
						<?php foreach ($artistasObras as $artista) { ?>
							
							<article class="partner artist">
								<div class="thumbnail">
									<a href="<?php echo base_url(); ?><?php echo $artista['dados']->slugpagina;?>">
										<img src="<?php echo base_url(); ?>content/upload/<?php echo $artista['dados']->slugpagina . '/fp/fp-large/' . $artista['dados']->imgPerfil; ?>" width="110" height="110" alt="<?php echo $artista['dados']->nomeArtistico; ?>" />
									</a>
								</div> <!-- /thumbnail -->

								<div class="description">
									<h5 class="name">
										<a href="<?php base_url(); ?><?php echo $artista['dados']->slugpagina;?>">
											<?php echo $artista['dados']->nomeArtistico; ?>
										</a>
                                        <span class="badge <?php if($artista['dados']->votosTotais >= 0 || $artista['dados']->votosTotais <= 99){ echo ''; }elseif($artista['dados']->votosTotais >= 100 && $artista['dados']->votosTotais <= 999 ){echo 'sprite-badge-bronze'; }elseif($artista['dados']->votosTotais >= 1000 && $artista['dados']->votosTotais <= 9999){echo 'sprite-badge-silver';}elseif($artista['dados']->votosTotais >= 10000){echo 'sprite-badge-gold';} ?>"></span>
									</h5>
									<p class="city"><span class="flag sprite-flag-br">Brasil</span><?php echo $artista['dados']->cidade; ?>/<?php echo $artista['dados']->estado; ?></p>
									<ul class="actions">										
										<li><a href="<?php echo base_url(); ?><?php echo $artista['dados']->slugpagina;?>/biografia?section=contato">Entrar em Contato <i class="icon icon-comment"></i></a></li>
									</ul>
								</div> <!-- /description -->

								<div class="thumbs">
									<?php foreach ($artista['obras'] as $obras) { ?>
									
										<div class="item">
											<a href="<?php echo base_url(); ?><?php echo $artista['dados']->slugpagina;?>/obras/<?php echo $obras->slugObra; ?>">
												<img src="<?php echo base_url(); ?>content/upload/<?php echo $artista['dados']->slugpagina; ?>/<?php echo $obras->slugObra; ?>/small/<?php echo $obras->imgFoto1; ?>" alt="<?php echo $obras->titulo; ?>" />
											</a>
										</div>

									<?php } ?>								
									<div class="item view-more">
										<a href="<?php echo base_url(); ?><?php echo $artista['dados']->slugpagina;?>/obras">
											<span>Ver</span> 
											<i class="icon icon-plus"><span>Mais</span></i>
										</a>
									</div>
								</div> <!-- /thumbs -->
							</article>

					<?php } ?>						

						<br class="clear" />						
					</section> <!-- /list -->					
					<?php } ?>

				<?php if(!$possuiFavoritos){ ?>
					<div class="no-post">
						<span class="big-icon gray-icon">
							<i class="icon icon-profile"></i>
						</span>
						<h3>Você ainda não adicionou <br />nenhum artista em seus <strong>favoritos</strong>.</h3>
						<p>Está esperando o que pra começar a curtir?</p>
					</div>					
					<section class="list js-filter-scroll">
						<h3 class="title margin-bottom">Aqui estão algumas dicas pra você começar...</h3>

						<?php if(!empty($artistasRandon )){ ?>
                        <?php foreach ($artistasRandon as $artista) { ?>
							<article class="partner artist">
								<div class="thumbnail">
									<a href="<?php echo base_url(); ?><?php echo $artista['dados']->slugpagina;?>">
										<img src="<?php echo base_url(); ?>content/upload/<?php echo $artista['dados']->slugpagina; ?>/fp/fp-large/<?php echo $artista['dados']->imgPerfil;?>" width="110" height="110" alt="<?php echo $artista['dados']->nomeArtistico; ?>" />
									</a>
								</div> <!-- /thumbnail -->

								<div class="description">
									<h5 class="name"><a href="<?php base_url(); ?><?php echo $artista['dados']->slugpagina;?>"><?php echo $artista['dados']->nomeArtistico; ?></a><span class="badge <?php if($artista['dados']->votosTotais >= 0 && $artista['dados']->votosTotais <= 99){ echo ''; }elseif($artista['dados']->votosTotais >= 100 && $artista['dados']->votosTotais <= 999 ){echo 'sprite-badge-bronze'; }elseif($artista['dados']->votosTotais >= 1000 && $artista['dados']->votosTotais <= 9999){echo 'sprite-badge-silver';}elseif($artista['dados']->votosTotais >= 10000){echo 'sprite-badge-gold';} ?>"></span></h5>
									<p class="city"><span class="flag sprite-flag-br">Brasil</span><?php echo $artista['dados']->cidade; ?>/<?php echo $artista['dados']->estado; ?></p>
									<ul class="actions">										
										<li><a class="add-favorite-click" id="add-favorite-<?php echo $artista['dados']->idUsuario; ?>" data-idArtista="<?php echo $artista['dados']->idUsuario; ?>" href="<?php echo base_url(); ?>favoritos/adicionarFavorito">Adicionar aos Favoritos <i class="icon icon-add-favorite"></i></a></li>
										<li><a href="<?php echo base_url(); ?><?php echo $artista['dados']->slugpagina;?>/biografia?section=contato">Entrar em Contato <i class="icon icon-comment"></i></a></li>
									</ul>
								</div> <!-- /description -->

								<div class="thumbs">
									<?php foreach ($artista['obras'] as $obras) { ?>
									
										<div class="item">
											<a href="<?php echo base_url(); ?><?php echo $artista['dados']->slugpagina;?>/obras/<?php echo $obras->slugObra; ?>">
												<img src="<?php echo base_url(); ?>content/upload/<?php echo $artista['dados']->slugpagina;?>/<?php echo '/' . $obras->slugObra; ?>/small/<?php echo $obras->imgFoto1; ?>" alt="<?php echo $obras->titulo; ?>" />
											</a>
										</div>

									<?php } ?>								
									<div class="item view-more">
										<a href="<?php echo base_url(); ?><?php echo $artista['dados']->slugpagina;?>/obras">
											<span>Ver</span> 
											<i class="icon icon-plus"><span>Mais</span></i>
										</a>
									</div>
								</div> <!-- /thumbs -->
							</article>

					<?php } ?>
                    <?php } ?>

						<br class="clear" />
					</section>
				<?php } ?>	
				<a href="<?php base_url(); ?>/artistas" class="button rounded big-button">Veja mais artistas aqui</a>

			</section>

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>