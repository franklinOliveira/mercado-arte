<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>">Home</a></li>
					<li>Parceiros</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php 
				
				$data['tecnicaObra']   = $tecnicaObra;
				$data['formatoObra']   = $formatoObra;
				$data['categoriaObra'] = $categoriaObra;
				$this->load->view('includes/sidebar',$data); 
			?>

			<div class="content">

				<section class="primary-banner primary-banner-small">
					<a href="#">
						<img src="<?php echo base_url(); ?>/content/images/catalogo.jpg" alt="" />
					</a>
				</section>

				<section class="right-column">
					<div class="box">
						<?php include('includes/social.php'); ?>

						<?php include('includes/faq.php'); ?>
					</div> <!-- /box -->
				</section>

				<br class="clear" />

				<section class="main border-top">
					<?php 
                    	if(!empty($_GET['filtroP'])){ 
                    		$filtro = $_GET['filtroP'];
                    	}
                    ?>
					<h3 class="title page-title">Nossos Parceiros</h3>
					<div class="filter-order">
						<label for="order-by">Filtro:</label>
						<select name="order-by" id="order-partner" class="select-no-appearance js-change-order">
						    <option value="0">Todos os Parceiros</option>
						    <?php if(!empty($categoriaParceiros)){ ?>
						    <?php foreach ($categoriaParceiros as $c) { ?>
						    	<option value="<?php echo $c->idCategoriaParceiro; ?>"<?php if(!empty($_GET['filtroP']) && $filtro == $c->idCategoriaParceiro){ echo 'selected'; } ?>><?php echo $c->nomeCategoria; ?></option>
						    <?php } } ?> 
						</select>
						<i class="loading-filter small-loading icon-animated icon icon-loader-2"></i>
					</div>

					<section class="list js-filter-scroll">				
						<?php if(!empty($parceiros)){ ?>
						<?php foreach ($parceiros as $p) { ?>

							<article class="partner">
								<div class="thumbnail">
									<img src="<?php echo base_url(); ?>/content/images/<?php echo $p->imgLogo; ?>" width="110" height="110" alt="<?php echo $p->nome; ?>" />
									<h4 class="category"><?php echo $p->nomeCategoria; ?></h4>
								</div> <!-- /thumbnail -->

								<div class="description">
									<h5 class="name"><a href="http://<?php echo $p->urlSite; ?>" target="_blank"><?php echo $p->nome; ?></a></h5>
									<p class="city"><span class="flag sprite-flag-br">Brasil</span> <?php echo $p->cidade; ?>/<?php echo $p->estado; ?></p>
									<a href="http://<?php echo $p->urlSite; ?>" target="_blank"><?php echo $p->urlSite; ?></a>
									<p><?php echo $p->telefone; ?></p>
									<a href="mailto:<?php echo $p->email; ?>?Contato pelo Site [Mercado Arte]"><?php echo $p->email; ?></a>
								</div> <!-- /description -->

								<div class="thumbs <?php if(!empty($p->imgProduto1) && !empty($p->imgProduto2) && !empty($p->imgProduto3) && !empty($p->imgProduto4) && !empty($p->imgProduto5)){echo 'owl-carousel js-thumbs-slider'; } ?>  ">
									<?php if (!empty($p->imgProduto1)){?>
										<div class="item"><img src="<?php echo base_url(); ?>/content/images/<?php echo $p->imgProduto1; ?>"></div>								
									<?php } ?>
									<?php if (!empty($p->imgProduto2)){?>
										<div class="item"><img src="<?php echo base_url(); ?>/content/images/<?php echo $p->imgProduto2; ?>"></div>								
									<?php } ?>
									<?php if (!empty($p->imgProduto3)){?>
										<div class="item"><img src="<?php echo base_url(); ?>/content/images/<?php echo $p->imgProduto3; ?>"></div>								
									<?php } ?>
									<?php if (!empty($p->imgProduto4)){?>
										<div class="item"><img src="<?php echo base_url(); ?>/content/images/<?php echo $p->imgProduto4; ?>"></div>								
									<?php } ?>
									<?php if (!empty($p->imgProduto5)){?>
										<div class="item"><img src="<?php echo base_url(); ?>/content/images/<?php echo $p->imgProduto5; ?>"></div>								
									<?php } ?>
								</div> <!-- /thumbs -->
							</article>
						<?php } } ?>
						<br class="clear" />

						<ul class="pagination">							
							<?php  echo $this->pagination->create_links(); ?>													
						</ul>

					</section> <!-- /list -->

				</section>
				
			</div>

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>