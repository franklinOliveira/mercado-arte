<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		<?php $sitePagamento =  $this->session->userdata('sitePagamento'); ?>		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php echo base_url(); ?>/">Home</a></li>
					<li><a href="<?php echo base_url(); ?>/cadastro">Cadastro</a></li>
					<li>Cadastro Finalizado</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php 
				$data['tecnicaObra']   = $tecnicaObra;
				$data['formatoObra']   = $formatoObra;
				$data['categoriaObra'] = $categoriaObra;
				$this->load->view('includes/sidebar',$data); 
			?>

			<div class="content">

				<section class="main">

					<h3 class="title page-title">Agora você faz parte do Mercado Arte! :)</h3>
					<div class="message">
						<span class="hide-text centered sprite-logo">Mercado Arte</span>
						<h4>Seu cadastro foi efetuado com sucesso!</h4>
						<?php if(!empty($msgSucesso)){ ?>
							<p><?php echo $msgSucesso; ?></p>
						<?php } ?>
						<p>Você receberá em seu e-mail um manual em PDF, com ele você será capaz de operar <br />o painel de administração com facilidade.</p>
						<p>Para acessar agora seu painel de administração, <a href="<?php echo base_url(); ?>/login">clique aqui.</a></p>
						<p>Em caso de dúvidas, envie um e-mail para <a href="mailto:suporte@mercadoarte.com.br">suporte@mercadoarte.com.br</a> <br />ou ligue para: <strong>(11) 2528-4599</strong></p>
						<br />
                       <!-- <a href="<?php /*echo $sitePagamento; */?>" target="_blank" class="button rounded">Finalizar o Pagamento</a>-->

                       <!-- <?php /*if(!empty($sitePagamento) && $sitePagamento == 2){  */?>

                            <div id="esconde-btn-pagamento" style="display: none">
                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="form-paypal" target="_top">
                                    <input type="hidden" name="cmd" value="_xclick">
                                    <input type="hidden" name="business" value="frank.oliveira.07@hotmail.com">
                                    <input type="hidden" name="lc" value="BR">
                                    <input type="hidden" name="item_name" value="Plano <?php /*echo $this->session->userdata('nomePlano'); */?>">
                                    <input type="hidden" name="amount" value="<?php /*echo $this->session->userdata('valorAPagar'); */?>">
                                    <input type="hidden" name="currency_code" value="BRL">
                                    <input type="hidden" name="button_subtype" value="services">
                                    <input type="hidden" name="no_note" value="0">
                                    <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest">
                                    <input type="hidden" name="return" value="http://www.mercadoarte.com.br/sucesso" />
                                    <input type="image" src="https://www.paypalobjects.com/pt_BR/BR/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - A maneira fácil e segura de enviar pagamentos online!">
                                    <img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1">
                                </form>
                            </div>

                        <?php /*}elseif(!empty($sitePagamento) && $sitePagamento == 1){  */?>
                            <a href="<?php /*echo base_url(); */?>cadastro/pagamento/<?php /*echo $sitePagamento; */?>" target="_blank" class=""><input type="image" src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/184x42-pagar-preto-assina.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!" /></a>

                        <?php /*}elseif(!empty($sitePagamento) && $sitePagamento == 999){  */?>
                            <a href="<?php /*echo base_url(); */?>home"  class="button rounded">Continuar</a>

                        --><?php /*}*/?>

                        <a href="<?php echo base_url(); ?>login"  class="button rounded">Continuar</a>

                    </div>

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>