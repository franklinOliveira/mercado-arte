<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8"> 

 		<?php include('includes/meta.php'); ?>
 		
	</head>
	
	<body>

    <?php
        $data['artista']       = $biografia;
        $this->load->view('includes/header',$data);
    ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php echo  base_url(); ?>/">Home</a></li>
					<li><a href="<?php echo  base_url(); ?>/artistas">Artistas</a></li>
                    <li><a href="<?php echo base_url() . SLUG; ?>"><?php echo $biografia[0]->nomeArtistico; ?></a></li>
                    <li>Biografia</li>
				</ul>
			</div> <!-- /breadcrumb -->
			

            <?php
            $data['artista']       = $biografia;
            $this->load->view('includes/sidebar',$data);
            ?>

			<div class="content">

				<section class="main">
					<div class="profile-about">
						<h2 class="title"><?php echo $biografia[0]->nomeArtistico; ?></h2>
						<p>"<?php echo $biografia[0]->txtMsgVisitante; ?>"</p>
					</div> <!-- /profile-about -->

					<h2 class="title">Biografia</h2>
					<section class="artist-biography">
						
						<?php if (isset($biografia[0]->imgPerfil)) { ?>
							<img src="<?php echo base_url(); ?>content/upload/<?php echo SLUG . '/fp/fp-medium/' . $biografia[0]->imgPerfil; ?>"  width="250" alt="<?php echo $biografia[0]->nomeArtistico; ?>" />
						<?php } else { ?>
							<img src="<?php echo base_url(); ?>content/images/avatar.png" width="250" alt="<?php echo $artista[0]->nomeArtistico; ?>" />
						<?php } ?>

						<ul>
							<li>
								<strong>Nome: </strong>
								<span class="name"><?php echo $biografia[0]->nomeArtistico; ?> <span class="badge <?php if($biografia[0]->votosTotais >= 0 && $biografia[0]->votosTotais <= 99){ echo ''; }elseif($biografia[0]->votosTotais >= 100 && $biografia[0]->votosTotais <= 999 ){echo 'sprite-badge-bronze'; }elseif($biografia[0]->votosTotais >= 1000 && $biografia[0]->votosTotais <= 9999){echo 'sprite-badge-silver';}elseif($biografia[0]->votosTotais >= 10000){echo 'sprite-badge-gold';} ?>"></span></span>
							</li>
							<li><strong>Origem:</strong>
								<span class="flag sprite-flag-<?php echo strtolower($biografia[0]->iso); ?>"><?php echo strtolower($biografia[0]->nomePais); ?></span>
								<?php echo $biografia[0]->cidade; ?>/<?php echo $biografia[0]->estado; ?>
							</li>
                            <li><strong>Categoria:</strong>
                                <?php 
                                	if(!empty($obraPorTipo)){
                                        foreach($obraPorTipo as $ot){
                                            $arr[] = $ot->nomeTipoObra . " (" . $ot->qtd .") ";
                                        }
                                        echo implode(",",$arr);
                                    }
                                    else {
                                    	echo '-';
                                    }
                                ?>
                            </li>
                            <li>
                            	<strong>Preço Médio:</strong>
                            	<?php
                            		if(empty($mediaPreco)){
                            			echo '-';
                            		}
                            		else {
                            			echo 'R$ ' . $mediaPreco;
                            		} 
                            	?>
                            </li>
							<li>
								<strong><?php if($totalObras > 0){ echo $totalObras; } ?> Obras:</strong>
								<?php if($totalObras > 0){ ?>
									<a href="<?php echo base_url() . SLUG; ?>/obras">Ver todas as obras <i class="icon icon-arrow-right-medium"></i></a>
								<?php } else { ?>
									Nenhuma
								<?php } ?>
							</li>
						</ul>
						<p><?php echo $biografia[0]->txtBiografia; ?></p>
					</section> <!-- /artist-biography -->

                    <?php if(!empty($contato) && $contato == true){ ?>
                        <div class="alert alert-success">
                            <p><?php echo $msg; ?></p>
                        </div>
                    <?php } ?>
                    <?php if(!empty($contato) && $contato == false){ ?>
                        <div class="alert alert-danger">
                            <p><?php echo $msg; ?></p>
                        </div>
                    <?php } ?>

                    <br class="clear" />
                    
					<h2 class="title">Contato</h2>
					<section id="contato" class="artist-contact">
						<form method="POST" action="<?php echo base_url() . SLUG ; ?>/biografia" class="js-artist-form">
							<div class="forms">
								<span class="column-half">
									<label for="nome">* Nome:</label>
									<input type="text" name="nome" id="nome" class="input rounded validate[required]" />
								</span>

								<span class="column-half">
									<label for="email">* E-mail:</label>
									<input type="email" name="email" id="email" class="input rounded validate[required,custom[email]]" />
								</span>

								<span class="column-half">
									<label for="assunto">Assunto:</label>
									<input type="text" name="assunto" id="assunto" class="input rounded" />
								</span>

								<span class="column-half">
									<label for="telefone">Telefone:</label>
									<input type="text" name="telefone" id="telefone" class="input rounded" />
								</span>
								
								<label for="mensagem">Mensagem:</label>
								<textarea name="mensagem" id="mensagem" class="input textarea rounded"></textarea>
								
								<br class="clear" />

								<span class="column-half">
									<label for="captcha" class="captcha">* Qual o valor da soma entre os números 4 + 3?</label>
								</span>

								<span class="column-half">
									<input type="text" name="captcha" id="captcha" class="input rounded validate[required,funcCall[captcha]]" />
								</span>

								<button type="submit" class="button rounded js-submit-artist">Enviar</button>
							</div> <!-- /forms -->
						</form>
					</section> <!-- /artist-contact -->
				</section> <!-- /main -->
				<script>
				</script>
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>