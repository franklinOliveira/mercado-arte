<!DOCTYPE HTML>
<html lang="pt-BR">

	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>

	</head>

	<body>

		<?php
				if(!empty($obras)){
                    $data['artista'] = $obras;
                }else{
                    $data['artista'] = $dados_artista_sem_obra;
                }

 				$this->load->view('includes/header',$data);
		?>

		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>/">Home</a></li>
					<li><a href="<?php base_url(); ?>/artistas/">Artistas</a></li>
					<li><a href="<?php base_url(); ?>/<?php echo SLUG ?>"><?php if(!empty($obras)){echo $obras[0]->nomeArtistico;}else{echo SLUG;}  ?></a></li>
					<li>Obras</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php
				$data['artista']       = $obras;
 				$this->load->view('includes/sidebar',$data);
			?>

			<div class="content">

				<section class="main">
					<h2 class="title">Obras</h2>

					<?php if(empty($obras[0]->idObra)){ ?>
						<div class="no-post">
							<span class="big-icon gray-icon">
								<i class="icon icon-images"></i>
							</span>
							<h3>Esse artista ainda não cadastrou <br />nenhuma obra no <strong>Mercado Arte</strong>.</h3>
							<p>Navegue pelos artistas e encontre obras de arte.</p>
							<a href="/artistas" class="button rounded big-button">Conheça alguns artistas</a>
						</div> <!-- /no-post -->
					<?php }else{ ?>

					<section class="list js-filter-scroll">
						<?php $num = 0; ?>
						<?php foreach ($obras as $a) { ?>
								<?php $num++; ?>
								<article class="art art-work <?php if($num % 3 == 0){echo "no-margin";} ?>">
									<a href="<?php echo base_url() . SLUG; ?>/obras/<?php echo $a->slugObra; ?>">
										<div class="thumbnail">
											<img src="<?php echo base_url(); ?>content/upload/<?php echo SLUG ?>/<?php echo $a->slugObra; ?>/art-medium/<?php echo $a->imgFoto1; ?>" alt="<?php echo $a->titulo; ?>" />
											
										</div> <!-- /thumbnail -->

										<h4 class="art-title"><?php echo $a->titulo; ?></h4>
										<h5 class="description"><?php echo $a->nomeTecnica; ?> - <?php echo number_format($a->altura, 0); ?> x <?php echo number_format($a->comprimento, 0); if(!empty($a->largura)){ echo ' x '. number_format($a->largura, 0);} ?> cm</h5>

										<div class="price">
                                            <?php if($a->idStatusObra == 2 || $a->idStatusObra == 5 || $a->idStatusObra == 8){ ?>
                                                <p class="soldout">Vendido</p>
                                            <?php }else if($a->idStatusObra == 3 || $a->idStatusObra == 6 || $a->idStatusObra == 9){ ?>
                                                <a href="<?php echo base_url(). $a->slugpagina; ?>/obras/<?php echo $a->slugObra; ?>?section=consultar" class="consult">Consulte</a>
                                            <?php }else{ ?>
                                                <?php if($a->precoComDesconto == 0.00){ ?>
                                                    <p class="descount">R$ <?php echo number_format((int)str_ireplace(".",",",$a->precoObra), 2, ',', '.'); ?></p>
                                                <?php }else{ ?>
                                                    <small class="value">De R$ <?php echo number_format((int)str_ireplace(".",",",$a->precoObra), 2, ',', '.'); ?></small>
                                                    <p class="descount">R$ <?php echo number_format((int)str_ireplace(".",",",$a->precoComDesconto), 2, ',', '.'); ?></p>
                                                <?php } ?>
                                            <?php } ?>
										</div> <!-- /price -->
									</a>

									<div class="rating">
										<ul class="star-rating" id="star-rating-<?php echo $a->idObra; ?>">
										<?php
											$notas = round($a->nota);

											for ($i=0; $i < 5; $i++) { ?>
												<li class="star-number-<?php echo $i + 1; ?><?php if($notas > 0){ echo " active"; } ?>">
													<a href="#" class="icon-vote" data-idObra="<?php echo $a->idObra; ?>" data-icon="<?php echo $i; ?>"><i class="icon icon-star"></i>
														<span><?php echo $i + 1; ?></span>
													</a>
												</li>

										<?php
											$notas--;
											}

										?>
										</ul>
									</div> <!-- /rating -->
								</article>

							<?php } } ?>


						<br class="clear" />

						<ul class="pagination">
							<?php if($ocultaPaginacao != true){echo $this->pagination->create_links();} ?>
						</ul>

					</section> <!-- /list -->

				</section> <!-- /main -->

			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>

	</body>
</html>