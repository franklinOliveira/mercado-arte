<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
 		
	</head>
	
	<body>

		<?php 				
				$data['artista'] = $obra;
				$data['identObra'] = $identObra;
 				$this->load->view('includes/header',$data); 
		?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>/">Home</a></li>					
					<li><a href="<?php base_url(); ?>/artistas">Artistas</a></li>
					<li><a href="<?php echo base_url() . SLUG; ?>"><?php echo $obra[0]->nomeArtistico; ?></a></li>
					<li><a href="<?php echo base_url() . SLUG; ?>/obras/">Obras</a></li>
					<li><?php echo $obra[0]->titulo; ?></li>
				</ul>
			</div> <!-- /breadcrumb -->

			<section class="main">
				
				<article class="work">
					<div class="work-info">
						<div class="small-box bordered-content">
							<h2 class="work-title">
								<i class="icon icon-images"></i>
								<span><?php echo $obra[0]->titulo; ?></span>
							</h2>
						</div> <!-- /small-box -->
						
						<div class="work-image bordered-content js-work-image">
							<ul>
								<?php if($obra[0]->imgFoto1 != null){ ?>
									<li
										data-value="1"
										data-src="<?php echo base_url(); ?>content/upload/<?php echo SLUG ?>/<?php echo $obra[0]->slugObra; ?>/large/<?php echo $obra[0]->imgFoto1; ?>">
										<a href="#">
											<img src="<?php echo base_url(); ?>content/upload/<?php echo SLUG ?>/<?php echo $obra[0]->slugObra; ?>/medium/<?php echo $obra[0]->imgFoto1; ?>" />
										</a>
									</li>
                                <?php } ?>
								
								<?php if($obra[0]->imgFoto2 != null){ ?>
									<li 
										data-value="2"
										data-src="<?php echo base_url(); ?>content/upload/<?php echo SLUG ?>/<?php echo $obra[0]->slugObra; ?>/large/<?php echo $obra[0]->imgFoto2; ?>">
										<a href="#">
											<img src="<?php echo base_url(); ?>content/upload/<?php echo SLUG ?>/<?php echo $obra[0]->slugObra; ?>/medium/<?php echo $obra[0]->imgFoto2; ?>" />
										</a> 
									</li>
                                <?php } ?>
								
								<?php if($obra[0]->imgFoto3 != null){ ?>
									<li 
										data-value="3"
										data-src="<?php echo base_url(); ?>content/upload/<?php echo SLUG ?>/<?php echo $obra[0]->slugObra; ?>/large/<?php echo $obra[0]->imgFoto3; ?>">
										<a href="#">
											<img src="<?php echo base_url(); ?>content/upload/<?php echo SLUG ?>/<?php echo $obra[0]->slugObra; ?>/medium/<?php echo $obra[0]->imgFoto3; ?>" />
										</a> 
									</li>
                                <?php } ?>
							</ul>
						</div> <!-- /work-image -->

						<ul id="thumbs" class="work-thumbs js-work-thumb">
							<?php if($obra[0]->imgFoto1 != null){ ?>
								<li data-value="1">
									<a href="#">
										<img src="<?php echo base_url(); ?>content/upload/<?php echo SLUG ?>/<?php echo $obra[0]->slugObra; ?>/small/<?php echo $obra[0]->imgFoto1; ?>" /> <!-- thumb -->
									</a>
								</li>
							<?php }else{ ?>
                                <li class="placeholder"></li>
                            <?php } ?>

							<?php if($obra[0]->imgFoto2 != null){ ?>
								<li data-value="2">
									<a href="#">
										<img src="<?php echo base_url(); ?>content/upload/<?php echo SLUG ?>/<?php echo $obra[0]->slugObra; ?>/small/<?php echo $obra[0]->imgFoto2; ?>" />
									</a>
								</li>
                            <?php }else{ ?>
                                <li class="placeholder"></li>
                            <?php } ?>

							<?php if($obra[0]->imgFoto3 != null){ ?>
								<li data-value="3">
									<a href="#">
										<img src="<?php echo base_url(); ?>content/upload/<?php echo SLUG ?>/<?php echo $obra[0]->slugObra; ?>/small/<?php echo $obra[0]->imgFoto3; ?>" />
									</a>
								</li>
                            <?php }else{ ?>
                                <li class="placeholder"></li>
                            <?php } ?>
							<li class="placeholder"></li>
						</ul> <!-- /work-thumbs -->

						<div class="work-details bordered-content">
							<h3 class="title">Detalhes</h3>
							<dl>
								<span>
									<dt>Artista:</dt>
									<dd><a href="<?php echo base_url() . SLUG ?>"><?php echo $obra[0]->nomeArtistico; ?><span class="badge <?php if($obra[0]->votosTotais >= 0 && $obra[0]->votosTotais <= 99){ echo ''; }elseif($obra[0]->votosTotais >= 100 && $obra[0]->votosTotais <= 999 ){echo 'sprite-badge-bronze'; }elseif($obra[0]->votosTotais >= 1000 && $obra[0]->votosTotais <= 9999){echo 'sprite-badge-silver';}elseif($obra[0]->votosTotais >= 10000){echo 'sprite-badge-gold';} ?>"></span></a></dd>
								</span>
								<span>
									<dt>Título:</dt>
									<dd><?php echo $obra[0]->titulo; ?></dd>
								</span>
								<span>
									<dt>Técnica:</dt>
									<dd><?php echo $obra[0]->nomeTecnica; ?></dd>
								</span>
								<?php if(!empty($obra[0]->altura)){ ?>
								<span>
									<dt>Altura:</dt>
									<dd><?php echo number_format($obra[0]->altura, 0); ?> cm</dd>
								</span>
								<?php } ?>
								<?php if(!empty($obra[0]->comprimento)){ ?>
								<span>
									<dt>Comprimento:</dt>
									<dd><?php echo number_format($obra[0]->comprimento, 0); ?> cm</dd>
								</span>
								<?php } ?>
								<?php if(!empty($obra[0]->largura)){ ?>
									<span>
										<dt>Largura:</dt>
										<dd><?php echo number_format($obra[0]->largura, 0); ?> cm</dd>
									</span>
								<?php } ?>
								<span>
									<dt>Formato:</dt>
									<dd><?php echo $obra[0]->nomeFormato; ?></dd>
								</span>
								<span>
									<dt>Código da Obra:</dt>
									<dd><?php echo $obra[0]->idObra; ?></dd>
								</span>
								<span>
									<dt>Descrição:</dt>
									<dd><?php echo $obra[0]->descricao; ?></dd>
								</span>
							</dl>
							<div class="extra-info">
							</div> <!-- /extra-info -->
						</div> <!-- /work-details -->

					</div> <!-- /work-info -->

					<div class="work-contacts">
						<div class="work-sharer small-box bordered-content">
							<div class="rating">
							<ul class="star-rating" id="star-rating-<?php echo $obra[0]->idObra; ?>">
							<?php 
								$notas = round($obra[0]->nota);
								for ($i=0; $i < 5; $i++) { ?>
									<li class="star-number-<?php echo $i + 1; ?><?php if($notas > 0){ echo " active"; } ?>">
										<a href="#" class="icon-vote" data-idObra="<?php echo $obra[0]->idObra; ?>" data-icon="<?php echo $i; ?>"><i class="icon icon-star"></i>
											<span><?php echo $i + 1; ?></span>
										</a>
									</li>										
								<?php 
									$notas--;
								}
								?>		
							</ul>
						</div> <!-- /rating -->
							<h2 class="title">Mercado Arte na WEB</h2>
							<div class="box-content">
								<ul class="social">
									<!--
									<li>
										<a href="#" class="rss">
											<i class="icon icon-rss"></i>
											<span>RSS</span>
										</a>
									</li>
									-->
									<li>
										<a href="https://plus.google.com/u/1/share?url=<?php echo current_url(); ?>" class="google-plus">
											<i class="icon icon-google-plus"></i>
											<span>Google Plus</span>
										</a>
									</li>
									<li>
										<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url(); ?>" class="facebook">
											<i class="icon icon-facebook"></i>
											<span>Facebook</span>
										</a>
									</li>
									<li>
										<a href="https://twitter.com/intent/tweet?text=Visite a obra <?php echo $obra[0]->titulo; ?> no Mercado Arte. <?php echo current_url(); ?>" class="twitter">
											<i class="icon icon-twitter"></i>
											<span>Twitter</span>
										</a>
									</li>
									<!-- <li>
										<a href="https://www.youtube.com/<?php echo $obra[0]->idObra; ?>" class="youtube">
											<i class="icon icon-youtube"></i>
											<span>Youtube</span>
										</a>
									</li> -->
								</ul>
							</div> <!-- /box-content -->
						</div> <!-- /work-sharer -->

						<div class="work-form bordered-content">
							<h3 class="title">Comprar</h3>
							<div class="price">
                                <?php if($obra[0]->idStatusObra == 2 || $obra[0]->idStatusObra == 5 || $obra[0]->idStatusObra == 8){ ?>
                                    <p class="soldout">Vendido</p>
                                <?php }else if($obra[0]->idStatusObra == 3 || $obra[0]->idStatusObra == 6 || $obra[0]->idStatusObra == 9){ ?>
                                    <a href="<?php echo base_url(). $obra[0]->slugpagina; ?>/obras/<?php echo $obra[0]->slugObra; ?>?section=consultar" class="consult">Consulte</a>
                                <?php }else{ ?>
                                    <?php if($obra[0]->precoComDesconto == 0.00){ ?>
                                        <p class="descount">
                                        	<span>R$ <?php echo number_format((int)str_ireplace(".",",",$obra[0]->precoObra), 2, ',', '.'); ?></span>
                                        	<small><strong>+</strong> frete a ser definido pelo artista.</small>
                                        </p> 
                                    <?php }else{ ?>
                                        <small class="value">De R$ <?php echo number_format((int)str_ireplace(".",",",$obra[0]->precoObra), 2, ',', '.'); ?></small>
                                        <p class="descount">
                                        	<span>R$ <?php echo number_format((int)str_ireplace(".",",",$obra[0]->precoComDesconto), 2, ',', '.'); ?></span>
                                        	<small><strong>+</strong> frete a ser definido pelo artista.</small>
                                        </p>
                                    <?php } ?>
                                <?php } ?>
							</div> <!-- /price -->

							<p>Para adquirir esta obra, preencha o formulário abaixo e entre em contato direto com o artista para receber instruções de como prosseguir com a compra, se optar por contato telefônico não se esqueça de perguntar o valor do frete, prazo de entrega e formas de pagamento.</p>

							<form method="POST" action="<?php echo base_url() . SLUG ?>/obras/<?php echo $obra[0]->slugObra; ?>" class="js-artist-form">
								<div class="forms">
									
									<?php if(!empty($msgSucesso)){ ?>
		                                <div class="alert alert-success">
		                                    <p><?php echo $msgSucesso; ?></p>
		                                </div>
		                            <?php } ?>
		                            <?php if(!empty($msgErro)){ ?>
										<div class="alert alert-danger">
											<p><?php echo $msgErro; ?></p>
										</div>
		                            <?php } ?>

									<label for="nome">* Nome:</label>
									<input type="text" name="nome" id="nome" class="input rounded validate[required]" />

									<label for="email">* E-mail:</label>
									<input type="email" name="email" id="email" class="input rounded validate[required,custom[email]]" />

									<label for="telefone">Telefone:</label>
									<input type="text" name="telefone" id="telefone" class="input rounded" />

									<label for="mensagem">Mensagem:</label>
									<textarea name="mensagem" id="mensagem" class="input textarea rounded"></textarea>

									<label for="captcha" class="captcha">* Qual o valor da soma entre os números 4 + 3?</label>
									<input type="text" name="captcha" id="captcha" class="input rounded validate[required,funcCall[captcha]]" />

									<button type="submit" class="button rounded js-submit-artist">Enviar Proposta</button>
								</div> <!-- /forms -->

							</form>

						</div> <!-- /work-form -->
						
					</div> <!-- /work-contacts -->

				</article> <!-- /work -->

				<br class="clear" />

				<?php if($artista[0]->idObra != null){ ?>
						<h2 class="title">Obras mais vistas</h2>
						<section class="list js-filter-scroll">

								<?php foreach ($artista as $a) { ?>
									
									<article class="art art-work">
										<a href="<?php echo base_url() . SLUG ?>/obras/<?php echo $a->slugObra; ?>">
											<div class="thumbnail">
												<img src="<?php echo base_url(); ?>content/upload/<?php echo SLUG ?>/<?php echo $a->slugObra; ?>/art-medium/<?php echo $a->imgFoto1; ?>" alt="<?php echo $a->titulo; ?>" /> 
											</div> <!-- /thumbnail -->
											
											<h4 class="art-title"><?php echo $a->titulo; ?></h4>
											<h5 class="description"><?php echo $a->nomeTecnica; ?> - <?php echo number_format($a->altura, 0); ?> x <?php echo number_format($a->comprimento, 0); if(!empty($a->largura)){ echo ' x '. number_format($a->largura, 0);} ?> cm</h5>

											<div class="price">
				                                <?php if($a->idStatusObra == 2 || $a->idStatusObra == 5 || $a->idStatusObra == 8){ ?>
				                                    <p class="soldout">Vendido</p>
				                                <?php }else if($a->idStatusObra == 3 || $a->idStatusObra == 6 || $a->idStatusObra == 9){ ?>
				                                    <a href="<?php echo base_url(). $a->slugpagina; ?>/obras/<?php echo $a->slugObra; ?>?section=consultar" class="consult">Consulte</a>
				                                <?php }else{ ?>
				                                    <?php if($a->precoComDesconto == 0.00){ ?>
				                                        <p class="descount">
				                                        	<span>R$ <?php echo number_format((int)str_ireplace(".",",",$a->precoObra), 2, ',', '.'); ?></span>
				                                        </p> 
				                                    <?php }else{ ?>
				                                        <small class="value">De R$ <?php echo number_format((int)str_ireplace(".",",",$a->precoObra), 2, ',', '.'); ?></small>
														<p class="descount">R$ <?php echo number_format((int)str_ireplace(".",",",$a->precoComDesconto), 2, ',', '.'); ?></p>
				                                    <?php } ?>
				                                <?php } ?>
											</div> <!-- /price -->
										</a>

										<div class="rating">
											<ul class="star-rating" id="star-rating-<?php echo $a->idObra; ?>">
											<?php 
												$notas = round($a->nota);

												for ($i=0; $i < 5; $i++) { ?>
													<li <?php if($notas > 0){ echo "class='active'"; } ?> >
														<a href="#" class="icon-vote" data-idObra="<?php echo $a->idObra; ?>" data-icon="<?php echo $i; ?>">
															<i class="icon icon-star"></i>
															<span><?php echo $i + 1; ?></span>
														</a>
													</li>
												
											<?php 
												$notas--;
												}

											?>		
											</ul>
										</div> <!-- /rating -->
									</article>

							<?php } }?>

				</section> <!-- /list -->

			</section> <!-- /main -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>