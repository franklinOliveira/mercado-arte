<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
		
	</head>
	
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>/">Home</a></li>
					<li><a href="<?php base_url(); ?>/cadastro">Cadastro</a></li>
					<li>Passo 2</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<?php 
				$data['tecnicaObra']   = $tecnicaObra;
				$data['formatoObra']   = $formatoObra;
				$data['categoriaObra'] = $categoriaObra;
				$this->load->view('includes/sidebar',$data); 
			?>

			<div class="content">

				<section class="main">

					<form action="<?php echo base_url(); ?>cadastro/cadastro_completo/<?php echo  $this->session->userdata('idUsuario'); ?>" class="js-register-form" method="post">
						<h3 class="title page-title">Dados Pessoais</h3>
						<?php
							echo '<div class="error">';
							echo validation_errors('<p>', '</p>');
							echo '</div>'; 
						?>
						
						<div class="forms step-2 border">
							<span class="column-half border-right">
								<label for="nome">* Seu nome completo:</label> 
								<input type="text" name="nomeCompleto" id="nome" value="<?php echo set_value('nomeCompleto'); ?>" class="input rounded validate[required]" />

								<label for="cpf">* CPF (somente números):</label>
								<input type="text" name="cpf" id="cpf" value="<?php echo set_value('cpf'); ?>" class="input rounded <?php if($idPais == 76){echo  'js-mask-cpf';}  ?>  validate[required]" />
								<label for="rg">* RG ou RNE:</label>
								<input type="text" name="rg" id="rg" value="<?php echo set_value('rg'); ?>" class="input rounded" class="validate[required]" />
								
								<span class="label">* Sexo:</span>
								<span class="radios js-radios">
									<input type="radio" name="sexo" id="masculino"  value="M" class="validate[required]" />
									<label for="masculino"><i class="icon icon-radio-unchecked"></i> Masculino</label>

									<input type="radio" name="sexo" id="feminino" value="F" class="validate[required]" />
									<label for="feminino"><i class="icon icon-radio-unchecked"></i> Feminino</label>
								</span>
							</span>

							<span class="column-half form-helper">
								<p>Seu número de <strong>CPF</strong> e <strong>RG/RNE</strong> não serão divulgados.</p>
								<p>O <strong>nome</strong> será divulgado em sua página e visível aos visitantes.</p>
							</span>
						</div> <!-- /forms -->

						<h3 class="title page-title">Telefone</h3>
						
						<div class="forms border">
							<span class="column-half border-right small-padding">
								<label for="comercial">Comercial:</label>
								<input type="text" name="telefoneComercial" id="comercial" value="<?php echo set_value('telefoneComercial'); ?>" class="input rounded<?php if($idPais == 76){ echo ''; } ?>" />
							</span>

							<span class="column-half form-helper">
								<p>O número de telefone <strong>comercial</strong> será divulgado em sua página e visível aos visitantes.</p>
							</span>
						</div> <!-- /forms -->

						<h3 class="title page-title">Endereço</h3>
						
						<div class="forms border">
							<span class="column-half border-right">
								<label for="cep">* CEP:</label>
								<span class="block relative">
									<input type="text" name="cep" id="cep" value="<?php echo set_value('cep'); ?>" class="input rounded <?php if($idPais == 76){ echo 'js-mask-cep ';} ?>validate[required]" />
									<span class="js-loading-cep loading-form"><i class="icon icon-animated icon-loader-2"></i></span>
								</span>

								<label for="endereco">* Endereço:</label>
								<input type="text" name="endereco" id="endereco" value="<?php echo set_value('endereco'); ?>" class="input rounded js-autocomplete-cep validate[required]" />

								<label for="numero">* Número:</label>
								<input type="text" name="numero" id="numero" value="<?php echo set_value('numero'); ?>" class="input rounded validate[required,custom[number]]" />

								<label for="complemento">Complemento:</label>
								<input type="text" name="complemento" id="complemento" value="<?php echo set_value('complemento'); ?>" class="input rounded" />

								<label for="bairro">* Bairro:</label>
								<input type="text" name="bairro" id="bairro" value="<?php echo set_value('bairro'); ?>" class="input rounded js-autocomplete-cep validate[required]" />

								<label for="cidade">* Cidade:</label>
								<input type="text" name="cidade" id="cidade" value="<?php echo set_value('cidade'); ?>" class="input rounded js-autocomplete-cep validate[required]" />

								<label for="estado">* Estado:</label>
								<input type="text" name="estado" id="estado" value="<?php echo set_value('estado'); ?>" class="input rounded js-autocomplete-cep validate[required,custom[onlyLetterSp],maxSize[2]]" />
							</span>

							<span class="column-half form-helper">
								
								<p>Sua <strong>cidade</strong> e seu <strong>estado</strong> serão divulgados em sua página e visível aos visitantes.</p>
							</span>
						</div> <!-- /forms -->

						<h3 class="title page-title">Falta pouco, continuar com cadastro...</h3>
						
						<div class="forms border">
							<span class="column-half">
								<span class="label no-margin">Desejo receber publicações por e-mail.</span>
								<span class="radios js-radios">
									<input type="radio" name="publicacoesEmail" id="receber" value="1" checked/>
									<label for="receber"><i class="icon icon-radio-unchecked icon-radio-checked"></i> Sim</label>

									<input type="radio" name="publicacoesEmail" id="ignorar"  value="0"/>
									<label for="ignorar"><i class="icon icon-radio-unchecked"></i> Não</label>
								</span>
								
								<span class="label">Desejo receber notificações e alertas por SMS.</span>
								<span class="radios js-radios">
									<input type="radio" name="notificacaoAlertaSMS" value="1" id="sim" checked/>
									<label for="sim"><i class="icon icon-radio-unchecked icon-radio-checked"></i> Sim</label>

									<input type="radio" name="notificacaoAlertaSMS" value="0" id="nao" />
									<label for="nao"><i class="icon icon-radio-unchecked"></i> Não</label>
								</span>

								<button type="submit" class="button rounded continue js-submit-register">Continuar</button>
							</span>
						</div> <!-- /forms -->

					</form>

				</section> <!-- /main -->
				
			</div> <!-- /content -->

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>