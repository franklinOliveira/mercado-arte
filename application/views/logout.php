<?php
	session_start();
	unset($_SESSION['usuario_logado']);

?>
<!DOCTYPE HTML>
<html lang="pt-BR">
		
	<head prefix="og: http://ogp.me/ns#">
		<meta charset="UTF-8">

 		<?php include('includes/meta.php'); ?>
		
	</head>
	  
	<body>

		<?php include('includes/header.php'); ?>
		
		<div class="wrapper">
			<div class="breadcrumb">
				<ul>
					<li><a href="<?php base_url(); ?>">Home</a></li>
					<li>Logout</li>
				</ul>
			</div> <!-- /breadcrumb -->

			<section class="main search-result">
                <h3 class="title">Você saiu...</h3>
                <div class="no-post">
                    <span class="big-icon gray-icon">
						<i class="icon icon-lock "></i>
                    </span>
                    <h3>Obrigado por fazer parte <br />da equipe <strong>Mercado Arte.</strong></h3>
					<p>Esperamos que você tenha gostado e <br />aproveitado ao máximo este projeto.</p>
					<br />
					<br />
					<br />
                </div> <!-- /no-post -->
			</section>

			<br class="clear" />

		</div> <!-- /wrapper -->

		<?php include('includes/footer.php'); ?>
		
	</body>
</html>
