<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Classe das obras
|--------------------------------------------------------------------------	
| Exibe as obras de todos os artistas
*/ 

class Obras extends CI_Controller {
/*
|--------------------------------------------------------------------------
| Método que exibe a página de todas os tipos de obras
|--------------------------------------------------------------------------		
*/ 

	public function index()
	{  
	   
	}

/*
|--------------------------------------------------------------------------
| Método que exibe a página de pinturas
|--------------------------------------------------------------------------		
*/ 

	public function pinturas()
	{  		
		$this->load->model('Dados_model', 'Dados');	    
	    $tipoObra   = 'pinturas';
	    $idTipoObra = 1; 

	    if(isset($_GET['ordem']) && !empty($_GET['ordem'])){
	    	$ordem = $_GET['ordem'];
	    }else{
	    	$ordem = 0;
	    }	  


	    /* Paginação */
		$this->load->library('pagination');
		$limit_end = 25;
		$limit_start = (empty($_GET['per_page'])) ? 0 : $_GET['per_page'];  
		$config['base_url']   =   base_url() .'obras/'.$tipoObra.'?ordem='.$ordem;
		$config['total_rows'] =   $this->Dados->getTotalObras($ordem,$idTipoObra);
		$config['per_page']   =   $limit_end;
		$config["uri_segment"]=   2;
		$config['page_query_string'] = true;			

		$config['first_link'] = '&laquo; Primeira';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] = '</li>';

	    $config['last_link'] = 'Última &raquo;';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] = '</li>';		
	    	    	   
	    $config['next_link'] = '<i class="icon icon-arrow-right-small"></i><span>Próxima</span>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';

	    $config['prev_link'] = '<i class="icon icon-arrow-left-small"></i><span>Anterior</span>';
	    $config['prev_tag_open'] = '<li class="prev page">';
	    $config['prev_tag_close'] = '</li>';

	    $config['cur_tag_open'] = '<li class="active"><a href="" class="current">';
	    $config['cur_tag_close'] = '</a></li>';

	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		/* Fim paginação */

		/* FILTRO */
		if(isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])){
	    	$filtroTipoObra = $_GET['filtroTipoObra'];
	    }else{
	    	$filtroTipoObra = 1;
	    }

	    switch ($filtroTipoObra) {
	    	case 'Pintura':
	    		$filtroTipoObra = 1;
	    		break;
	    	case 'Escultura':
	    		$filtroTipoObra = 2;
	    		break;
	    	case 'Fotografia':
	    		$filtroTipoObra = 3;
	    		break;	    	
	    	default:
	    		$filtroTipoObra = 1;
	    		break;
	    }


		$data['tecnicaObra'] = $this->Dados->getTecnica($filtroTipoObra);
		$data['formatoObra'] = $this->Dados->getFormato($filtroTipoObra);
		$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
		/* Fim filtro*/
				
		$data['obras'] = $this->Dados->getGeralObras($ordem, $limit_end, $limit_start,$idTipoObra);	    
		$this->load->view('obras',$data);		

	}


/*
|--------------------------------------------------------------------------
| Método que exibe a página de esculturas
|--------------------------------------------------------------------------		
*/ 

	public function esculturas()
	{  
		$this->load->model('Dados_model', 'Dados');	    
	    $tipoObra   = 'esculturas';
	    $idTipoObra = 2; 

	    if(isset($_GET['ordem']) && !empty($_GET['ordem'])){
	    	$ordem = $_GET['ordem'];
	    }else{
	    	$ordem = 0;
	    }	 

	    /* Paginação */
		$this->load->library('pagination');
		$limit_end = 25;
		$limit_start = (empty($_GET['per_page'])) ? 0 : $_GET['per_page'];  
		$config['base_url']   =   base_url() .'obras/'.$tipoObra.'?ordem='.$ordem;
		$config['total_rows'] =   $this->Dados->getTotalObras($ordem,$idTipoObra);
		$config['per_page']   =   $limit_end;
		$config["uri_segment"]=   2;
		$config['page_query_string'] = true;			

		$config['first_link'] = '&laquo; Primeira';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] = '</li>';

	    $config['last_link'] = 'Última &raquo;';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] = '</li>';		
	    	    	   
	    $config['next_link'] = '<i class="icon icon-arrow-right-small"></i><span>Próxima</span>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';

	    $config['prev_link'] = '<i class="icon icon-arrow-left-small"></i><span>Anterior</span>';
	    $config['prev_tag_open'] = '<li class="prev page">';
	    $config['prev_tag_close'] = '</li>';

	    $config['cur_tag_open'] = '<li class="active"><a href="" class="current">';
	    $config['cur_tag_close'] = '</a></li>';

	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		/* Fim paginação */

		
		/* FILTRO */
		if(isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])){
	    	$filtroTipoObra = $_GET['filtroTipoObra'];
	    }else{
	    	$filtroTipoObra = 1;
	    }

	    switch ($filtroTipoObra) {
	    	case 'Pintura':
	    		$filtroTipoObra = 1;
	    		break;
	    	case 'Escultura':
	    		$filtroTipoObra = 2;
	    		break;
	    	case 'Fotografia':
	    		$filtroTipoObra = 3;
	    		break;	    	
	    	default:
	    		$filtroTipoObra = 1;
	    		break;
	    }


		$data['tecnicaObra'] = $this->Dados->getTecnica($filtroTipoObra);
		$data['formatoObra'] = $this->Dados->getFormato($filtroTipoObra);
		$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
		/* Fim filtro*/


		$data['obras'] = $this->Dados->getGeralObras($ordem, $limit_end, $limit_start,$idTipoObra);	    
		$this->load->view('obras',$data);
	}



/*
|--------------------------------------------------------------------------
| Método que exibe a página de fotografias
|--------------------------------------------------------------------------		
*/ 
	public function fotografias()
	{  
		$this->load->model('Dados_model', 'Dados');	    
	    $tipoObra   = 'fotografias';
	    $idTipoObra = 3; 

	    if(isset($_GET['ordem']) && !empty($_GET['ordem'])){
	    	$ordem = $_GET['ordem'];
	    }else{
	    	$ordem = 0;
	    }

	    /* Paginação */
		$this->load->library('pagination');
		$limit_end = 25;
		$limit_start = (empty($_GET['per_page'])) ? 0 : $_GET['per_page'];  
		$config['base_url']   =   base_url() .'obras/'.$tipoObra.'?ordem='.$ordem;
		$config['total_rows'] =   $this->Dados->getTotalObras($ordem,$idTipoObra);
		$config['per_page']   =   $limit_end;
		$config["uri_segment"]=   2;
		$config['page_query_string'] = true;			

		$config['first_link'] = '&laquo; Primeira';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] = '</li>';

	    $config['last_link'] = 'Última &raquo;';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] = '</li>';		
	    	    	   
	    $config['next_link'] = '<i class="icon icon-arrow-right-small"></i><span>Próxima</span>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';

	    $config['prev_link'] = '<i class="icon icon-arrow-left-small"></i><span>Anterior</span>';
	    $config['prev_tag_open'] = '<li class="prev page">';
	    $config['prev_tag_close'] = '</li>';

	    $config['cur_tag_open'] = '<li class="active"><a href="" class="current">';
	    $config['cur_tag_close'] = '</a></li>';

	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		/* Fim paginação */

		/* FILTRO */
		if(isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])){
	    	$filtroTipoObra = $_GET['filtroTipoObra'];
	    }else{
	    	$filtroTipoObra = 1;
	    }

	    switch ($filtroTipoObra) {
	    	case 'Pintura':
	    		$filtroTipoObra = 1;
	    		break;
	    	case 'Escultura':
	    		$filtroTipoObra = 2;
	    		break;
	    	case 'Fotografia':
	    		$filtroTipoObra = 3;
	    		break;	    	
	    	default:
	    		$filtroTipoObra = 1;
	    		break;
	    }


		$data['tecnicaObra'] = $this->Dados->getTecnica($filtroTipoObra);
		$data['formatoObra'] = $this->Dados->getFormato($filtroTipoObra);
		$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
		/* Fim filtro*/

		
		$data['obras'] = $this->Dados->getGeralObras($ordem, $limit_end, $limit_start,$idTipoObra);	    
		$this->load->view('obras',$data);
	}


/*
|--------------------------------------------------------------------------
| Filtra as obras de arte
|--------------------------------------------------------------------------		
*/ 
	public function filtrar(){
		
		if($_POST != false || $_GET != false ){				
			$this->load->model('Dados_model', 'Dados');	
			$cor=null;
			$cores = $this->input->get('cor');
			if(isset($cores) && !empty($cores)){
			 	if($cores == '1'){
			 		$cor = $this->input->get('cores');
			 	}else{
			 		$cor[] = 0;
			 	}
			}
				

			$filtro = array('tipo'      => $this->input->get('tipo'),
							'tamanho'   => $this->input->get('tamanho'),
							'technique' => $this->input->get('technique'),
							'format'    => $this->input->get('format'),
							'preco'     => $this->input->get('preco'),
							'categorias'=> $this->input->get('categorias'),
							'cores'     => $cor,
							'busca'     => $this->input->post('busca'),
                            'porArtista'=> $this->input->get('filtro-artista')
						);



			/* FILTRO */
			if(isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])){
		    	$filtroTipoObra = $_GET['filtroTipoObra'];
		    }else{
		    	$filtroTipoObra = 1;
		    }

		    switch ($filtroTipoObra) {
		    	case 'Pintura':
		    		$filtroTipoObra = 1;
		    		break;
		    	case 'Escultura':
		    		$filtroTipoObra = 2;
		    		break;
		    	case 'Fotografia':
		    		$filtroTipoObra = 3;
		    		break;	    	
		    	default:
		    		$filtroTipoObra = 1;
		    		break;
		    }


			$data['tecnicaObra'] = $this->Dados->getTecnica($filtroTipoObra);
			$data['formatoObra'] = $this->Dados->getFormato($filtroTipoObra);
			$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
			/* Fim filtro*/	

			$data['obras'] = $this->Dados->getObrasFiltro($ordem = null, $filtro);
			$this->load->view('obras',$data);

		}else{
			redirect('home');
		}

	}


    public function buscar(){

        if($_POST != false || $_GET != false ){
            $this->load->model('Dados_model', 'Dados');
            $filtro = array('busca'     => $this->input->post('busca'));

            $data['obras'] = $this->Dados->getObrasFiltro($ordem = null, $filtro);
            $data['busca'] = $this->input->post('busca');

            /* Usada no meta.php */
            $this->session->set_userdata('palavraBusca',$data['busca']);

            $this->load->view('busca',$data);
        }else{
            redirect('home');
        }


    }
}