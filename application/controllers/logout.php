<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Classe do Logout
|--------------------------------------------------------------------------	
| Exibe uma página para o usuário se logar ou ser redirecionado para o cadastro
*/ 

class Logout extends CI_Controller {
/*
|--------------------------------------------------------------------------
| Método que exibe a página de logout
|--------------------------------------------------------------------------     
*/ 

    public function index()
    {  
       $this->load->view('logout');
    }
}
