<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

/*
|--------------------------------------------------------------------------
| Classe de cadastro
|--------------------------------------------------------------------------
| Realiza o cadastro dos clientes do mercado arte
 */

class Cadastro extends CI_Controller {

	public function index() {
		$this->load->model('Dados_model', 'Dados');
		/* FILTRO */
		if (isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])) {
			$filtroTipoObra = $_GET['filtroTipoObra'];
		} else {
			$filtroTipoObra = 1;
		}

		switch ($filtroTipoObra) {
			case 'Pintura':
				$filtroTipoObra = 1;
				break;
			case 'Escultura':
				$filtroTipoObra = 2;
				break;
			case 'Fotografia':
				$filtroTipoObra = 3;
				break;
			default:
				$filtroTipoObra = 1;
				break;
		}

		$data['tecnicaObra']   = $this->Dados->getTecnica($filtroTipoObra);
		$data['formatoObra']   = $this->Dados->getFormato($filtroTipoObra);
		$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
		/* Fim filtro*/

		$data['paises'] = $this->Dados->paises();
		$this->load->view('cadastro', $data);
	}

	/*
	|--------------------------------------------------------------------------
	| Método do cadastro inicial
	|--------------------------------------------------------------------------
	 */
	public function cadastrar() {
		$this->load->model('Dados_model', 'Dados');
		/* FILTRO */
		if (isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])) {
			$filtroTipoObra = $_GET['filtroTipoObra'];
		} else {
			$filtroTipoObra = 1;
		}

		switch ($filtroTipoObra) {
			case 'Pintura':
				$filtroTipoObra = 1;
				break;
			case 'Escultura':
				$filtroTipoObra = 2;
				break;
			case 'Fotografia':
				$filtroTipoObra = 3;
				break;
			default:
				$filtroTipoObra = 1;
				break;
		}

		$data['tecnicaObra']   = $this->Dados->getTecnica($filtroTipoObra);
		$data['formatoObra']   = $this->Dados->getFormato($filtroTipoObra);
		$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
		/* Fim filtro*/

		$data['paises'] = $this->Dados->paises();

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('pais', "Pais", 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tblusuarios.email]|max_length[45]');
		$this->form_validation->set_rules('email-check', 'Digite novamente seu e-mail', 'required|valid_email |matches[email]');
		$this->form_validation->set_rules('senha', 'Senha', 'required|min_length[6]|max_length[20]');
		$this->form_validation->set_rules('celular', "Celular", 'required |min_length[6]|max_length[20]');
		$this->form_validation->set_rules('telefoneResidencial', "Telefone Residencial", ' min_length[6]|max_length[20]');
		$this->form_validation->set_message('is_unique', 'O email que você tentou se cadastrar já encontra-se <br>em nosso sistema. Tente um e-mail diferente!');

		if ($this->form_validation->run() == TRUE) {
			$hash         = '!a8v7b1n3m8A9!0s#b';
			$pais         = $this->input->post('pais');
			$email        = $this->input->post('email');
			$emailConfirm = $this->input->post('email-check');
			$senha        = md5($this->input->post('senha').$hash);
			$senhaSemCrip = $this->input->post('senha');
			$celular      = $this->input->post('celular');
			$telefone     = $this->input->post('telefoneResidencial');
			$dateTime     = date("Y-m-d H:i:s");

			$dataUser = array(
				'email'               => $email,
				'idPaisResidente'     => $pais,
				'senha'               => $senha,
				'telefoneCelular'     => $celular,
				'telefoneResidencial' => $telefone,
				'idFuncao'            => 2,
				'dtCriacao'           => $dateTime,
				'dtUltAlteracao'      => '',
				'statusRegistro'      => 1,
			);

			/* Grava a senha e o email para enviar para o cliente no proximo cadastro */
			$this->session->set_userdata('usuario_logado', $email);
			$this->session->set_userdata('email', $email);
			$this->session->set_userdata('senhaSemCrip', $senhaSemCrip);

			try {
				$idUsuario = $this->Dados->insertCadastroInicial($dataUser);
				$this->Dados->insertPermissoesAcessoUsuario($idUsuario);

				$this->session->set_userdata('idUsuario', $idUsuario);
				redirect('/cadastro/cadastro_completo/');
			} catch (Exception $e) {
				echo $e->getMessage();
			}

		} else {
			$this->load->view('cadastro', $data);
		}

	}

	/*
	|--------------------------------------------------------------------------
	| Método que chama a view para a tela de cadastro dos dados do user
	|--------------------------------------------------------------------------
	 */
	function cadastro_completo() {
		$idUsuario = $this->session->userdata('idUsuario');
		if (empty($idUsuario)) {
			redirect('cadastro');
		}

		$this->load->model('Dados_model', 'Dados');

		/* FILTRO */
		if (isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])) {
			$filtroTipoObra = $_GET['filtroTipoObra'];
		} else {
			$filtroTipoObra = 1;
		}

		switch ($filtroTipoObra) {
			case 'Pintura':
				$filtroTipoObra = 1;
				break;
			case 'Escultura':
				$filtroTipoObra = 2;
				break;
			case 'Fotografia':
				$filtroTipoObra = 3;
				break;
			default:
				$filtroTipoObra = 1;
				break;
		}

		$data['tecnicaObra']   = $this->Dados->getTecnica($filtroTipoObra);
		$data['formatoObra']   = $this->Dados->getFormato($filtroTipoObra);
		$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
		/* Fim filtro*/

		$idPais         = $this->Dados->verifica_pais($idUsuario);
		$data['idPais'] = $idPais[0]->idPaisResidente;

		/* Verifica se o pais é Brasil e aplica as validações */
		if ($idPais[0]->idPaisResidente == 76) {
			$requiredCPF = 'required|min_length[11]|max_length[15]';
			$requiredRG  = 'max_length[15]';
			$requiredCEP = 'required|max_length[9]';
		} else {
			$requiredCPF = 'min_length[11]|max_length[15]';
			$requiredRG  = 'required|max_length[15]';
			$requiredCEP = 'max_length[9]';
		}

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('nomeCompleto', "Nome Completo", 'required|max_length[70]');
		$this->form_validation->set_rules('cpf', "CPF", $requiredCPF);
		$this->form_validation->set_rules('rg', 'RG ou RNE', $requiredRG);
		$this->form_validation->set_rules('sexo', "Sexo", 'required');
		$this->form_validation->set_rules('telefoneComercial', "Telefone Comercial", 'max_length[20]');
		$this->form_validation->set_rules('cep', "CEP", $requiredCEP);
		$this->form_validation->set_rules('endereco', "Endereço", 'required|max_length[60]');
		$this->form_validation->set_rules('numero', "Número", 'required|max_length[12]');
		$this->form_validation->set_rules('complemento', "Complemento", 'max_length[45]');
		$this->form_validation->set_rules('bairro', "Bairro", 'required|max_length[60]');
		$this->form_validation->set_rules('estado', "Estado", 'required|max_length[45]');
		$this->form_validation->set_rules('cidade', "Cidade", 'required|max_length[45]');

		if ($this->form_validation->run() == TRUE) {
			$rg       = '';
			$rne      = '';
			$dateTime = date("Y-m-d H:i:s");
			if ($idPais[0]->idPaisResidente == 76) {
				$rg = $this->input->post('rg');
			} else {
				$rne = $this->input->post('rg');
			}

			$dataUser = array(
				'idUsuario'            => $idUsuario,
				'nomeCompleto'         => $this->input->post('nomeCompleto'),
				'CPF'                  => $this->input->post('cpf'),
				'RG'                   => $rg,
				'rne'                  => $rne,
				'sexo'                 => $this->input->post('sexo'),
				'telefoneComercial'    => $this->input->post('telefoneComercial'),
				'enderecoCEP'          => $this->input->post('cep'),
				'enderecologradouro'   => $this->input->post('endereco'),
				'enderecoNumero'       => $this->input->post('numero'),
				'enderecoComplemento'  => $this->input->post('complemento'),
				'enderecoBairro'       => $this->input->post('bairro'),
				'cidade'               => $this->input->post('cidade'),
				'estado'               => $this->input->post('estado'),
				'publicacoesEmail'     => $this->input->post('publicacoesEmail'),
				'notificacaoAlertaSMS' => $this->input->post('notificacaoAlertaSMS'),
				'dtCriacao'            => $dateTime,
				'dtUltAlteracao'       => null,
				'statusRegistro'       => 1,
			);

			try {
				$this->session->set_userdata('nomeCompleto', $this->input->post('nomeCompleto'));
				$duplicidade = $this->Dados->verificaDuplicidadeCadastro($idUsuario, 'tblusuariosdados');

				//Verifica se ja existe um registro com mesmo id no banco.
				if ($duplicidade) {
					$this->Dados->apagaDuplicidade($idUsuario, 'tblusuariosdados');
				}
				$return = $this->Dados->insertCadastroDados($dataUser);
				redirect('/cadastro/planos');
			} catch (Exception $e) {
				echo $e->getMessage();
			}

		} else {
			$this->load->view('cadastro_completo', $data);
		}

	}

	/*
	|--------------------------------------------------------------------------
	| Método para enviar e-mail
	|--------------------------------------------------------------------------
	 */
	function enviarEmail($email, $htmlMensagem) {
		$this->load->library('email');
		$this->email->from('sistema@mercadoarte.com.br', 'Mercado Arte');
		$this->email->to($email);
		$this->email->subject('Confirmação de Cadastro – Mercado Arte');
		$this->email->message($htmlMensagem);
		$this->email->send();
	}

	/*
	|--------------------------------------------------------------------------
	| View planos
	|--------------------------------------------------------------------------
	 */
	function planos() {
		$idUsuario = $this->session->userdata('idUsuario');
		if (empty($idUsuario)) {
			redirect('cadastro');
		}

		$this->load->model('Dados_model', 'Dados');

		/* FILTRO */
		if (isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])) {
			$filtroTipoObra = $_GET['filtroTipoObra'];
		} else {
			$filtroTipoObra = 1;
		}

		switch ($filtroTipoObra) {
			case 'Pintura':
				$filtroTipoObra = 1;
				break;
			case 'Escultura':
				$filtroTipoObra = 2;
				break;
			case 'Fotografia':
				$filtroTipoObra = 3;
				break;
			default:
				$filtroTipoObra = 1;
				break;
		}

		$data['tecnicaObra']   = $this->Dados->getTecnica($filtroTipoObra);
		$data['formatoObra']   = $this->Dados->getFormato($filtroTipoObra);
		$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
		/* Fim filtro*/

		$propriedadesPlanos         = $this->Dados->getPropriedadePlano();
		$planos                     = $this->Dados->getPlanos();
		$data['propriedadesPlanos'] = $propriedadesPlanos;
		$data['planos']             = $planos;
		$data['nomePlano']          = $this->Dados->getNomePlanos();

		$this->load->view('planos', $data);
	}

	/*
	|--------------------------------------------------------------------------
	| Verifica o cupom de desconto
	|--------------------------------------------------------------------------
	 */
	function aplicarDesconto($param = null, $cupom = null) {
		$this->load->model('Dados_model', 'Dados');

		if ($param != null && $param == true) {
			$respCupom = $this->Dados->verificaCupom($cupom);

			if (count($respCupom) > 0) {
				$data['porcentagemDesconto'] = $respCupom[0]->porcentagemDesconto;
				$data['cupomValido']         = 1;
			} else {
				$data['porcentagemDesconto'] = 0;
				$data['cupomValido']         = 0;
			}

			return $data;
		} else {
			$cupom     = $this->input->get('cupom');
			$respCupom = $this->Dados->verificaCupom($cupom);

			if (count($respCupom) > 0) {
				$data['porcentagemDesconto'] = $respCupom[0]->porcentagemDesconto;
				$data['cupomValido']         = 1;
			} else {
				$data['porcentagemDesconto'] = 0;
				$data['cupomValido']         = 0;
			}
			echo json_encode($data);
		}
	}

	/*
	|--------------------------------------------------------------------------
	| Realiza a compra do cliente
	|--------------------------------------------------------------------------
	 */
	function realizarCompra() {
		$this->load->model('Dados_model', 'Dados');
		/* FILTRO */
		if (isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])) {
			$filtroTipoObra = $_GET['filtroTipoObra'];
		} else {
			$filtroTipoObra = 1;
		}

		switch ($filtroTipoObra) {
			case 'Pintura':
				$filtroTipoObra = 1;
				break;
			case 'Escultura':
				$filtroTipoObra = 2;
				break;
			case 'Fotografia':
				$filtroTipoObra = 3;
				break;
			default:
				$filtroTipoObra = 1;
				break;
		}

		$data['tecnicaObra']   = $this->Dados->getTecnica($filtroTipoObra);
		$data['formatoObra']   = $this->Dados->getFormato($filtroTipoObra);
		$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
		/* Fim filtro*/

		$idUsuario = $this->session->userdata('idUsuario');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('plano', "Plano Escolhido", 'required');
		$this->form_validation->set_rules('periodicidade', 'Periodicidade', 'required');
		$this->form_validation->set_rules('pagamento', 'Pagamento', 'required');
		$this->form_validation->set_rules('aceitar', 'Aceitar termo', 'required|callback_verificaTermo');

		$propriedadesPlanos         = $this->Dados->getPropriedadePlano();
		$planos                     = $this->Dados->getPlanos();
		$data['propriedadesPlanos'] = $propriedadesPlanos;
		$data['planos']             = $planos;
		$data['nomePlano']          = $this->Dados->getNomePlanos();

		if ($this->form_validation->run() == TRUE) {
			$plano         = $this->input->post('plano');
			$periodicidade = $this->input->post('periodicidade');
			$pagamento     = $this->input->post('pagamento');
			$aceitaTermo   = $this->input->post('aceitar');
			$cupom         = $this->input->post('cupom');
			$dateTime      = date("Y-m-d H:i:s");

			switch ($periodicidade) {
				case '1':
					$periodicidade = 30;
					break;
				case '3':
					$periodicidade = 90;
					break;
				case '6':
					$periodicidade = 180;
					break;
				case '12':
					$periodicidade = 360;
					break;
				case '24':
					$periodicidade = 720;
					break;
				case '48':
					$periodicidade = 1440;
					break;
			}

			$infoPlano = $this->Dados->verificaPlano($periodicidade, $plano);
			if (!empty($infoPlano)) {
				$retCupom         = $this->aplicarDesconto(true, $cupom);
				$valorSemDesconto = $infoPlano[0]->vlPlano;
				$valorSemDesconto = str_ireplace(".", "", $valorSemDesconto);
				$valorSemDesconto = str_ireplace(",", ".", $valorSemDesconto);

				//Verifica se tem cupom
				if (!empty($cupom)) {

					if ($retCupom['cupomValido'] != 0) {
						$valorTotal       = $valorSemDesconto*($retCupom['porcentagemDesconto']/100);
						$valorTotal       = $valorSemDesconto-$valorTotal;
						$valorTotal       = number_format($valorTotal, 2, '.', '');
						$valorSemDesconto = number_format($valorSemDesconto, 2, '.', '');

						$this->session->set_userdata('valorTotal', $valorSemDesconto);
						$this->session->set_userdata('valorAPagar', $valorTotal);
						/* deixa o cupom como invalido */
						$cupomAlterado = $this->Dados->invalidaCupom($cupom, $dateTime);
						if ($cupomAlterado) {
							$fazerCadastro = true;
						} else {
							$fazerCadastro   = false;
							$data['msgErro'] = "Não foi possível fazer o cadastro, tente novamente !";
							$this->load->view('planos', $data);
						}
					} else {
						$fazerCadastro   = false;
						$data['msgErro'] = "Seu cupom não é válido";
						$this->load->view('planos', $data);
					}

				} else {
					$cupom         = null;
					$fazerCadastro = true;

					$valorTotal = number_format($valorSemDesconto, 2, '.', '');
					$this->session->set_userdata('valorAPagar', $valorTotal);
					//$this->session->set_userdata('valorTotal',$valorTotal);
				}
				//fim verifica cupom

				if ($fazerCadastro) {
					$dtFimContrato = date("Y-m-d H:i:s", strtotime("+".$periodicidade." days"));

					/* Dados para tabela de contratos */
					$dataContrato = array(
						'idUsuario'        => $idUsuario,
						'idtipoPlano'      => $infoPlano[0]->idTipoPlano,
						'idStatusContrato' => 1, //verificar
						'dtInicioContrato' => $dateTime, //verificar
						'dtFimContrato'    => $dtFimContrato,
						'observacao'       => null,
						'dtCriacao'        => $dateTime,
						'dtUltAlteracao'   => null,
						'statusRegistro'   => 1,
					);

					$dataVencimento = date("Y-m-d H:i:s", strtotime("+7 days"));
					try {
						$idContrato = $this->Dados->insertContrato($dataContrato);

						//Verifica se o valor a pagar for 0.00 e troca o status da transação
						if ($this->session->userdata('valorAPagar') == 0.00) {
							$statusRegistro     = 1;
							$data['msgSucesso'] = "Esperamos que você aproveite todos os recursos de nosso site.";
						} else {
							$statusRegistro = 0;
						}

						$dataTransacao = array(
							'idContrato'            => $idContrato,
							'codCupom'              => $cupom,
							'idTipoPagamento'       => $pagamento,
							'valorTotal'            => $this->session->userdata('valorTotal'),
							'valorAPagar'           => $this->session->userdata('valorAPagar'),
							'txtidentPagamento'     => null, //verificar
							'valorPago'             => 0, //verificar
							'dtVencimentoTransacao' => $dataVencimento,
							'dtPagamentoTransacao'  => '', //verificar
							'observacao'            => null,
							'dtCriacao'             => $dateTime,
							'dtUltAlteracao'        => null,
							'statusRegistro'        => $statusRegistro,
						);

						$idTransact = $this->Dados->insertTransact($dataTransacao);
						//$this->session->unset_userdata('valorTotal');
					} catch (Exception $e) {
						echo $e->getMessage();
					}

					if (!empty($idTransact)) {

						$htmlMensagem = $this->load->view('partial-view/email-confirma-cadastro', '', true);

						$this->enviarEmail($this->session->userdata('email'), $htmlMensagem);

						/* Salva os dados em uma session para ser usada nas APIs de pagamento */
						$this->session->set_userdata('sitePagamento', $pagamento);
						$this->session->set_userdata('nomePlano', $infoPlano[0]->planoNome);
						$this->session->set_userdata('idTransact', $idTransact);

						/* Redirecionamentos para os pagamentos */
						if ($this->session->userdata('valorAPagar') == 0.00 || $retCupom['porcentagemDesconto'] == 100) {
							/* Caso o Cliente estiver com algum cupom de 100% de desconto */
							$this->session->set_userdata('sitePagamento', 999);
							$this->load->view('sucesso', $data);
						} else if ($this->session->userdata('sitePagamento') == 2) {
							//Paypal
							//TODO: Usar o serviço de API do paypal.
							redirect('cadastro/pagamento/2');
						} else if ($this->session->userdata('sitePagamento') == 1) {
							//PagSeguro
							redirect('cadastro/pagamento/1');
						} else {
							$data['msgErro'] = "Não foi possível fazer o cadastro, tente novamente.";
							$this->load->view('planos', $data);
						}

					} else {

						$data['msgErro'] = "Não foi possível fazer o cadastro, tente novamente.";
						$this->load->view('planos', $data);
					}

				}

			} else {

				$data['msgErro'] = "Não foi possível fazer o cadastro, tente novamente";
				$this->load->view('planos', $data);
			}

		} else {

			$this->load->view('planos', $data);
		}

	}

	/* Função de pagamento */
	function pagamento($tipoPagamento) {
		if (!empty($tipoPagamento)) {

			//Pagseguro = 1
			if ($tipoPagamento == 1) {
				//$this->load->library('PagSeguroLibrary');
				require_once (APPPATH.'libraries/PagSeguroLibrary/PagSeguroLibrary.php');
				session_start();
				if (!isset($_SESSION['nomePlano']) || empty($_SESSION['nomePlano']) || !isset($_SESSION['valorAPagar']) || empty($_SESSION['valorAPagar']) || !isset($_SESSION['idTransact']) || empty($_SESSION['idTransact'])) {
					$nomePlano  = "Plano ".$this->session->userdata('nomePlano');
					$valorPlano = $this->session->userdata('valorAPagar');
					//$valorPlano = 1.00;
					$idTransact = $this->session->userdata('idTransact');
				} else {
					$nomePlano  = "Plano ".$_SESSION['nomePlano'];
					$valorPlano = $_SESSION['valorAPagar'];
					//$valorPlano = 1.00;
					$idTransact = $_SESSION['idTransact'];
				}

				$paymentRequest = new PagSeguroPaymentRequest();
				$paymentRequest->addItem($idTransact, $nomePlano, 1, $valorPlano);

				$paymentRequest->setCurrency("BRL");
				$paymentRequest->setRedirectUrl("https://www.mercadoarte.com.br/sucesso");

				try {
					$credentials = PagSeguroConfig::getAccountCredentials();
					$checkoutUrl = $paymentRequest->register($credentials);
					if (!empty($checkoutUrl)) {
						$this->session->unset_userdata('valorTotal');
						$this->session->unset_userdata('valorAPagar');
						$this->session->unset_userdata('nomePlano');
						$this->session->unset_userdata('senhaSemCrip');
						redirect($checkoutUrl);
					} else {
						redirect('home');
					}

				} catch (PagSeguroServiceException $e) {
					die($e->getMessage());
				}

			} else if ($tipoPagamento == 2) {

				/* Pagamento Paypal */
				redirect('cadastro/pagamentoPaypal');
			}
		} else {
			/* FILTRO */
			if (isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])) {
				$filtroTipoObra = $_GET['filtroTipoObra'];
			} else {
				$filtroTipoObra = 1;
			}

			switch ($filtroTipoObra) {
				case 'Pintura':
					$filtroTipoObra = 1;
					break;
				case 'Escultura':
					$filtroTipoObra = 2;
					break;
				case 'Fotografia':
					$filtroTipoObra = 3;
					break;
				default:
					$filtroTipoObra = 1;
					break;
			}

			$data['tecnicaObra']   = $this->Dados->getTecnica($filtroTipoObra);
			$data['formatoObra']   = $this->Dados->getFormato($filtroTipoObra);
			$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
			/* Fim filtro*/

			$data['sucesso'] = false;
			$data['msgErro'] = 'Erro ao efetuar o pagamento, por favor escolha outro tipo de pagamento.';
			$this->load->view('planos', $data);
		}
	}

	/* verifica se o cliente aceitou o termo */
	function verificaTermo() {
		$this->form_validation->set_message('verificaTermo', 'Você precisa aceitar o contrato');
		if ($this->input->post('aceitar') != 'sim') {
			return false;
		} else {
			return true;
		}
	}

	/*
	|--------------------------------------------------------------------------
	| View Sucesso após cadastro
	|--------------------------------------------------------------------------
	 */
	function sucesso() {
		$idUsuario = $this->session->userdata('idUsuario');
		$this->load->model('Dados_model', 'Dados');
		/* FILTRO */
		if (isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])) {
			$filtroTipoObra = $_GET['filtroTipoObra'];
		} else {
			$filtroTipoObra = 1;
		}

		switch ($filtroTipoObra) {
			case 'Pintura':
				$filtroTipoObra = 1;
				break;
			case 'Escultura':
				$filtroTipoObra = 2;
				break;
			case 'Fotografia':
				$filtroTipoObra = 3;
				break;
			default:
				$filtroTipoObra = 1;
				break;
		}

		$data['tecnicaObra']   = $this->Dados->getTecnica($filtroTipoObra);
		$data['formatoObra']   = $this->Dados->getFormato($filtroTipoObra);
		$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
		/* Fim filtro*/

		$this->load->view('sucesso', $data);
	}

	/* Cadastro de newsletter */
	function cadastro_news() {
		$email = $this->input->post('email');
		$nome  = $this->input->post('nome');

		if (!empty($email) && !empty($email)) {
			$this->load->model('Dados_model', 'Dados');

			$data = array('nomeNewsletter' => $email,
				'emailNewsletter'             => $nome,
				'dtCriacao'                   => date("Y-m-d H:i:s"),
				'dtAlteracao'                 => null,
				'statusRegistro'              => 1,
			);

			$ret = $this->Dados->insertNewsLetter($data);

			if ($ret) {
				$resposta = array('msg' => 'Cadastro efetuado com sucesso!',
					'sucesso'              => 1);

				echo json_encode($resposta);
			}

		} else {
			$resposta = array('msg' => 'Preencha os campos corretamente!',
				'sucesso'              => 0);
			echo json_encode($resposta);
		}

	}

	/* Carrega a view que tem o form do paypal e um javascript que da um submit nele */
	function pagamentoPaypal() {
		session_start();

		if (isset($_SESSION['idTransact']) || !empty($_SESSION['idTransact'])) {
			$transact = $_SESSION['idTransact'];
		} else {
			$transact = $this->session->userdata('idTransact');
		}

		if (!empty($transact)) {
			$this->load->view('pagamento');
		} else {
			redirect('home');
		}
	}

}
