<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Classe do Login
|--------------------------------------------------------------------------	
| Exibe uma página para o usuário se logar ou ser redirecionado para o cadastro
*/ 

class Login extends CI_Controller {
/*
|--------------------------------------------------------------------------
| Método que exibe a página de login
|--------------------------------------------------------------------------     
*/ 
    public function index()
    {   
        $msg = [];
        $this->load->model('Dados_model', 'Dados');
        $this->load->helper('url');

        if (isset($_POST['email']) && isset($_POST['pass'])){
            $statusUser = $this->Dados->login($_POST['email'], $_POST['pass']);
            if ($statusUser != false){

                //Verifica se o usuario terminou o cadastro
                $statusCadastro = $this->Dados->verificaCadastro($statusUser['idUsuario']);

                if($statusCadastro === true){
                    redirect('/admin/');

                }else if($statusCadastro == 'primeiroCadastro'){
                    $this->session->set_userdata('idUsuario', $statusUser['idUsuario']);
                    redirect('/cadastro/cadastro_completo/');

                }else if($statusCadastro == 'segundoCadastro'){
                    $this->session->set_userdata('idUsuario', $statusUser['idUsuario']);
                    redirect('/cadastro/planos/');

                }else{
                    $msg['erro'] = '<strong>Desculpe</strong>, mas houve um erro ao verificar seus dados.';
                    $this->load->view('login', $msg);
                }

            } else {
                $msg['erro'] = '<strong>Desculpe</strong>, mas o endereço de e-mail ou senha estão incorretos.';
            }
        }
        
        if (isset($_POST['recuperarSenha'])){
            $status = $this->Dados->emailExists($_POST['recuperarSenha']);

            if ($status){
                /* Gera uma nova senha */
                $dadosUser = $this->Dados->getNewSenha($_POST['recuperarSenha'],date('Y-m-d H:i:s'));


                if($dadosUser == false){
                    $msg['erro'] = 'Não foi possível gerar a senha para o e-mail "'. $_POST['recuperarSenha'] .'".';
                }else{
                    $data['novaSenha']    = $dadosUser['novaSenha'];
                    $data['nome']         = $dadosUser['dadosArtista'][0]->nomeCompleto;
                    $this->load->library('email');
                    $this->email->from('sistema@mercadoarte.com.br', 'Sistema Mercado Arte');
                    $this->email->to($_POST['recuperarSenha']);
                    $this->email->subject('Recuperar Senha - Mercado Arte');
                    //$this->email->message($this->load->view('../_mail/reenvio-de-senha.html', TRUE));
                    $this->email->message($this->load->view('partial-view/email-reenvio-de-senha',$data , true));
                    $this->email->send();

                    $msg['success'] = 'Uma nova senha foi gerada e enviada para o e-mail "'.$_POST['recuperarSenha'].'"';
                }

            } else {
                $msg['erro'] = 'Não foi possível gerar uma nova senha para o e-mail "'. $_POST['recuperarSenha'] .'".';
            }
        }

        $this->load->view('login', $msg);
    }


/*
|--------------------------------------------------------------------------
| Método que exibe a página de logout
|--------------------------------------------------------------------------     
*/ 
    public function logout()
    {   
        $this->load->view('logout');
    }	
}
