<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Classe do Login
|--------------------------------------------------------------------------	
| Exibe uma página para o usuário se logar ou ser redirecionado para o cadastro
*/ 

class Busca extends CI_Controller {
/*
|--------------------------------------------------------------------------
| Método que exibe a página de login
|--------------------------------------------------------------------------		
*/ 
	public function index()
	{    
		$this->load->view('busca');
	}	


}