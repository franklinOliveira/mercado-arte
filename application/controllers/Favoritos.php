<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Favoritos extends CI_Controller {
/*
|--------------------------------------------------------------------------
| Método que exibe a página dos favoritos
|--------------------------------------------------------------------------		
*/ 
	public function index()
	{ 
		$this->load->model('Dados_model', 'Dados');

		/* Retorna os ids dos artistas favoritos */
		if(isset($_COOKIE['ma_artistas']) && !empty($_COOKIE['ma_artistas'])){
			$value = $this->input->cookie('ma_artistas');
	    	$value = unserialize($value);
	    	$artistasFavoritos = $value['artistas'];	    	
	    	
	    	$artistas  = $this->Dados->getArtistasFavoritos($artistasFavoritos);
	    	foreach ($artistas as $a) {
		    	$obras    = $this->Dados->getUltimasObras($a->idPgArtista);
		    	$artistasObras[$a->idUsuario]['obras']    = $obras;
		    	$artistasObras[$a->idUsuario]['dados']    = $a; 		    	    	    	   
	    	}
						
	    	$data['artistasObras']   = $artistasObras;	    	
	    	$data['possuiFavoritos'] = true;	       
		}else{
			/* Escolhe 5 artistas aleatórios */
	    	$limit = 5;	 
	    	$artistasRandon = $this->Dados->getArtistasRandon($limit);	    	

	    	foreach ($artistasRandon as $b) {
		    	$obras    = $this->Dados->getUltimasObras($b->idPgArtista);
		    	$artistasRandonObras[$b->idUsuario]['obras']    = $obras;
		    	$artistasRandonObras[$b->idUsuario]['dados']    = $b; 
		       	    	    
	    	}	    	
	    	   	
	    	$data['artistasRandon']  = $artistasRandonObras;
			$data['possuiFavoritos'] = false;
		}	

		$this->load->view('favoritos',$data);
	}


/*
|--------------------------------------------------------------------------
| Adiciona o artista aos cookies do navegador
|--------------------------------------------------------------------------		
*/
	public function adicionarFavorito(){				
		$idArtista = $this->input->post('idArtista');
		if(isset($_COOKIE['ma_artistas']) && !empty($_COOKIE['ma_artistas'])){
			$value = $this->input->cookie('ma_artistas');
	    	$value = unserialize($value);
	    	$i = count($value['artistas']);
	    	$i++;
	    	$value["artistas"][$i] = $idArtista;
	    	$value = serialize($value);
	    	$this->input->set_cookie("ma_artistas",$value,time() + (10 * 365 * 24 * 60 * 60));
	    	echo "sucesso";	    		    
		}else{
			$m_array["artistas"][1] = $idArtista; 
		    $m_array = serialize($m_array);	    
		    $this->input->set_cookie("ma_artistas",$m_array,time() + (10 * 365 * 24 * 60 * 60));
		    echo "sucesso";		   
		}			    	    

	}
}