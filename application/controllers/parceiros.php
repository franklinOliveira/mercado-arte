<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Classe dos parceiros do site mercado arte
|--------------------------------------------------------------------------	
| Exibe todos os parceiros do site
*/ 

class Parceiros extends CI_Controller {

	public function index()
	{				
	    $this->load->model('Dados_model', 'Dados');

	    /* FILTRO DOS PARCEIROS*/
		if(isset($_GET['filtroP']) && !empty($_GET['filtroP'])){
	    	$filtroParceiro = $_GET['filtroP'];
	    }else{
	    	$filtroParceiro = null;
	    }

	    /* Paginação */
		$this->load->library('pagination');
		$limit_end = 20;
		$limit_start = (empty($_GET['per_page'])) ? 0 : $_GET['per_page'];  
		$config['base_url']   =   base_url() .'parceiros/?filtroP='. $filtroParceiro;
		$config['total_rows'] =   $this->Dados->getTotalParceiro($filtroParceiro);
		$config['per_page']   =   $limit_end;
		$config["uri_segment"]=   2;
		$config['page_query_string'] = true;			

		$config['first_link'] = '&laquo; Primeira';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] = '</li>';

	    $config['last_link'] = 'Última &raquo;';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] = '</li>';		
	    	    	   
	    $config['next_link'] = '<i class="icon icon-arrow-right-small"></i><span>Próxima</span>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';

	    $config['prev_link'] = '<i class="icon icon-arrow-left-small"></i><span>Anterior</span>';
	    $config['prev_tag_open'] = '<li class="prev page">';
	    $config['prev_tag_close'] = '</li>';

	    $config['cur_tag_open'] = '<li class="active"><a href="" class="current">';
	    $config['cur_tag_close'] = '</a></li>';

	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		/* Fim paginação */

		
		/* FILTRO */
		if(isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])){
	    	$filtroTipoObra = $_GET['filtroTipoObra'];
	    }else{
	    	$filtroTipoObra = 1;
	    }

	    switch ($filtroTipoObra) {
	    	case 'Pintura':
	    		$filtroTipoObra = 1;
	    		break;
	    	case 'Escultura':
	    		$filtroTipoObra = 2;
	    		break;
	    	case 'Fotografia':
	    		$filtroTipoObra = 3;
	    		break;	    	
	    	default:
	    		$filtroTipoObra = 1;
	    		break;
	    }	    

		$data['tecnicaObra'] = $this->Dados->getTecnica($filtroTipoObra);
		$data['formatoObra'] = $this->Dados->getFormato($filtroTipoObra);
		$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
		/* Fim filtro*/
	

	    $data["categoriaParceiros"]  = $this->Dados->getCategoriaParceiro();
	    $data["parceiros"]           = $this->Dados->getParceiro($filtroParceiro);

		$this->load->view('parceiros',$data);
	}	
}	