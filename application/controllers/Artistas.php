<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Classe de Artistas
|--------------------------------------------------------------------------	
| Exibe as obras e paginas dos artistas
*/ 

class Artistas extends CI_Controller {

	public function index()
	{				
	    if(isset($_COOKIE['ma_artistas']) && !empty($_COOKIE['ma_artistas'])){
			$favoritos = unserialize($_COOKIE['ma_artistas']);
			$data['favoritos'] =  $favoritos['artistas'];
	    }else{
	    	$data['favoritos'] = array('msg' => 'Não existe favoritos' );;
	    }	

	    
	    $this->load->model('Dados_model', 'Dados');
	    
	    //TODO: Verificar como escolher o artista do mês
        //O artista do mês é escolhido manualmente passando seu id na função artistaDoMes
        $data['artistaDoMes'] = $this->Dados->artistaDoMes(56);
	    
	    /* Verifica a inicial do artista */
	    if(isset($_GET['i']) && !empty($_GET['i'])){
	    	$inicialNome = $_GET['i'];
	    }else{
	    	$inicialNome = "A";
	    }	

	    /* Paginação */
		$this->load->library('pagination');
		$limit_end = 10;
		$limit_start = (empty($_GET['per_page'])) ? 0 : $_GET['per_page']; 
		$config['base_url']   =   base_url() .'artistas/?i='.$inicialNome;
		$config['total_rows'] =   $this->Dados->totalArtistas($inicialNome);
		$config['per_page']   =   $limit_end;
		$config["uri_segment"]=   3;
		$config['page_query_string'] = TRUE;			

		$config['first_link'] = '&laquo; Primeira';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] = '</li>';

	    $config['last_link'] = 'Última &raquo;';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] = '</li>';		
	    	    	   
	    $config['next_link'] = '<i class="icon icon-arrow-right-small"></i><span>Próxima</span>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';

	    $config['prev_link'] = '<i class="icon icon-arrow-left-small"></i><span>Anterior</span>';
	    $config['prev_tag_open'] = '<li class="prev page">';
	    $config['prev_tag_close'] = '</li>';

	    $config['cur_tag_open'] = '<li class="active"><a href="" class="current">';
	    $config['cur_tag_close'] = '</a></li>';

	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		/* Fim paginação */

	    $artistas = $this->Dados->getArtistas($inicialNome,$limit_end, $limit_start);	   
	    
	    foreach ($artistas as $a) {
	    	$obras    = $this->Dados->getUltimasObras($a->idPgArtista);
	    	$artistasObras[$a->idUsuario]['obras']    = $obras;
	    	$artistasObras[$a->idUsuario]['dados']    = $a;  	    	    	    
	    }
	    if(!empty($artistasObras)){
			$data['artistasObras'] = $artistasObras; 
	    }else{
	    	$data['artistasObras'] = null;
	    }
	    

	    /* FILTRO */
		if(isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])){
	    	$filtroTipoObra = $_GET['filtroTipoObra'];
	    }else{
	    	$filtroTipoObra = 1;
	    }

	    switch ($filtroTipoObra) {
	    	case 'Pintura':
	    		$filtroTipoObra = 1;
	    		break;
	    	case 'Escultura':
	    		$filtroTipoObra = 2;
	    		break;
	    	case 'Fotografia':
	    		$filtroTipoObra = 3;
	    		break;	    	
	    	default:
	    		$filtroTipoObra = 1;
	    		break;
	    }


		$data['tecnicaObra'] = $this->Dados->getTecnica($filtroTipoObra);
		$data['formatoObra'] = $this->Dados->getFormato($filtroTipoObra);
		$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
		/* Fim filtro*/

	     	    

		$this->load->view('artistas',$data);
	}


/*
|--------------------------------------------------------------------------
| Método que exibe a pagina inicial do artista
|--------------------------------------------------------------------------		
*/ 
	public function home(){
		$this->load->model('Dados_model', 'Dados');				
		$data['artista'] = $this->Dados->getArtistasInfo(SLUG);


        if(!empty($data['artista'])){
            //Salva nome do artista e a categoria de suas obras em sessao para ser usada no meta.php
            $totalObrasByCategoria = $this->Dados->totalObrasByTipo($data['artista'][0]->idPgArtista);
            $this->session->set_userdata('nomeArtista',$data['artista'][0]->nomeArtistico);
            foreach($totalObrasByCategoria as $cat){
                $tipoDeObras[] = $cat->nomeTipoObra;
            }
            $this->session->set_userdata('tipoObraArtista',$tipoDeObras);
        }else{
            $data['artista'] = $this->Dados->getArtistaInfoSemObra(SLUG);
        }



		$this->load->view('artista-home',$data);
	}

/*
|--------------------------------------------------------------------------
| Método que exibe a biografia do artista
|--------------------------------------------------------------------------		
*/ 
	public function biografia(){
        $this->load->model('Dados_model', 'Dados');
        $data['biografia']   = $this->Dados->getBiografia(SLUG);
        $data['totalObras']  = $this->Dados->totalObrasByArtista($data['biografia'][0]->idPgArtista);
        $data['obraPorTipo'] = $this->Dados->totalObrasByTipo($data['biografia'][0]->idPgArtista);
        $media = $this->Dados->precoMedioObras($data['biografia'][0]->idPgArtista);
        $media = number_format((int)str_ireplace(".",",",$media[0]->media), 2, ',', '.');
        $data["mediaPreco"] = $media;

        //Salva nome do artista e a categoria de suas obras em sessao para ser usada no meta.php
        $totalObrasByCategoria = $this->Dados->totalObrasByTipo($data['biografia'][0]->idPgArtista);
        if(!empty($totalObrasByCategoria)){
            $this->session->set_userdata('nomeArtista',$data['biografia'][0]->nomeArtistico);
            foreach($totalObrasByCategoria as $cat){
                $tipoDeObras[] = $cat->nomeTipoObra;
            }
            $this->session->set_userdata('tipoObraArtista',$tipoDeObras);
        }



        //Salva a biografia para o descript das meta tags
        $this->session->set_userdata('biografia',substr_replace($data['biografia'][0]->txtBiografia, (strlen($data['biografia'][0]->txtBiografia) > 150 ? '...' : ''), 150));

        if($_POST != false || $_GET != false){
            $nome     = $this->input->post('nome');
            $email    = $this->input->post('email');
            $assunto  = $this->input->post('assunto');
            $telefone = $this->input->post('telefone');
            $mensagem = $this->input->post('mensagem');

            if(empty($nome) || empty($email)){
                $data['msg'] = "Preencha seu nome e e-mail";
                $data["contato"] = false;
            }else{
                $data["contato"] = true;

                $dataPartial['nomeArtistico'] = $data['biografia'][0]->nomeArtistico;
                $dataPartial['nome'] = $nome;
                $dataPartial['email'] = $email;
                $dataPartial['assunto'] = $assunto;
                $dataPartial['telefone'] = $telefone;
                $dataPartial['mensagem'] = $mensagem;

                $htmlMensagem    = $this->load->view('partial-view/email-contato', $dataPartial, true);

                $this->enviarEmail($data['biografia'][0]->email, $htmlMensagem);
                $data['msg'] = "E-mail enviado com sucesso.";
            }
        }




        $this->load->view('artista-biografia',$data);
	}


/*
|--------------------------------------------------------------------------
| Método que exibe as obras
|--------------------------------------------------------------------------		
*/ 
	public function obras(){
		// A URL deve ser: artistas/nome-do-artista/obras
		$this->load->model('Dados_model', 'Dados');

		if($_POST != false || $_GET != false ){																
			$filtro = array('tamanho'   => $this->input->post('tamanho'),
							'preco'     => $this->input->post('preco'));
									
			$data["ocultaPaginacao"]  = true;
			$data['obras'] = $this->Dados->getObrasArtistaFiltro($filtro,SLUG);					

		}else{
			/* Paginação */
			$this->load->library('pagination');
			$limit_end = 20;
			$limit_start = (empty($_GET['per_page'])) ? 0 : $_GET['per_page']; 
			$config['base_url']   =   base_url() . SLUG .'/obras?ordem=';
			$config['total_rows'] =   $this->Dados->getTotalArtistasObras(SLUG);
			$config['per_page']   =   $limit_end;
			$config["uri_segment"]=   3;
			$config['page_query_string'] = TRUE;			

			$config['first_link'] = '&laquo; Primeira';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';

		    $config['last_link'] = 'Última &raquo;';
		    $config['last_tag_open'] = '<li>';
		    $config['last_tag_close'] = '</li>';		
		    	    	   
		    $config['next_link'] = '<i class="icon icon-arrow-right-small"></i><span>Próxima</span>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';

		    $config['prev_link'] = '<i class="icon icon-arrow-left-small"></i><span>Anterior</span>';
		    $config['prev_tag_open'] = '<li class="prev page">';
		    $config['prev_tag_close'] = '</li>';

		    $config['cur_tag_open'] = '<li class="active"><a href="" class="current">';
		    $config['cur_tag_close'] = '</a></li>';

		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';

			$this->pagination->initialize($config);
			/* Fim paginação */		
			$data["ocultaPaginacao"]  = false;
			$data['obras'] = $this->Dados->getArtistasObras(SLUG, $limit_end, $limit_start);
            $data['dados_artista_sem_obra'] = $this->Dados->getArtistaInfoSemObra(SLUG);

		}


        if(!empty($data['obras'])){
            //Salva nome do artista e a categoria de suas obras em sessao para ser usada no meta.php
            $totalObrasByCategoria = $this->Dados->totalObrasByTipo($data['obras'][0]->idPgArtista);
            $this->session->set_userdata('nomeArtista',$data['obras'][0]->nomeArtistico);
            foreach($totalObrasByCategoria as $cat){
                $tipoDeObras[] = $cat->nomeTipoObra;
            }
            $this->session->set_userdata('tipoObraArtista',$tipoDeObras);
        }else{
            $this->session->set_userdata('nomeArtista',$data['dados_artista_sem_obra'][0]->nomeArtistico);
        }


		$this->load->view('artista-obras',$data);
	}

/*
|--------------------------------------------------------------------------
| Método que exibe detalhes das obras
|--------------------------------------------------------------------------		
*/ 
	public function obra_detalhe(){
		// A URL deve ser: artistas/nome-do-artista/obras/nome-da-obra
		$this->load->model('Dados_model', 'Dados');
        $dateTime = date("Y-m-d H:i:s");

        $data["identObra"] = $this->uri->segment(3);
        // Salva o id da obra para ser usado na página de meta tags
        $this->session->set_userdata('identObra', $this->uri->segment(3));

		$obra = $this->Dados->getDetalheObra($this->uri->segment(3));
		$data['obra'] = $obra;


        $this->Dados->somaVisitas($data['obra'][0]->idObra); //Soma uma visita na obra

		$data['artista'] = $this->Dados->getArtistasInfo(SLUG); //Somente é usado para receber as obras mais vistas.

        //Salva nome do artista e a categoria de suas obras em sessao para ser usada no meta.php
        $this->session->set_userdata('nomeArtista',$data['artista'][0]->nomeArtistico);
        $this->session->set_userdata('tipoObra',$data['obra'][0]->nomeTipoObra);
        $this->session->set_userdata('tituloObra',$data['obra'][0]->titulo);
        $this->session->set_userdata('nomeTecnicaObra',$data['obra'][0]->nomeTecnica);
        $tamanhoObra = number_format($data['obra'][0]->altura, 0) . ' x '. number_format($data['obra'][0]->comprimento, 0) . 'cm';
        $this->session->set_userdata('tamanhoObra',$tamanhoObra);
        //Fim


        //$idPais = $this->Dados->verifica_pais($data['artista'][0]->idUsuario);
        //$data['idPais'] = $idPais[0]->idPaisResidente;

		if($_POST != false){
			$contato  = array('idUsuario'      => $obra[0]->idUsuario,
							 'nomeContato'     => $this->input->post('nome'),
							 'emailContato'    => $this->input->post('email'),	
							 'telefoneContato' => $this->input->post('telefone'),	
							 'mensagemContato' => $this->input->post('mensagem'),
							 'dtCriacao' 	   => $dateTime,
							 'dtAlteracao'     => null,
							 'statusRegistro'  => 1
							 );

            $dataPartial['nomeArtistico'] = $obra[0]->nomeArtistico;
            $dataPartial['nome'] = $this->input->post('nome');
            $dataPartial['email'] = $this->input->post('email');
            $dataPartial['telefone'] = $this->input->post('telefone');
            $dataPartial['mensagem'] = $this->input->post('mensagem');


            $htmlMensagem    = $this->load->view('partial-view/email-contato', $dataPartial, true);

			$retEmail = $this->enviarEmail($obra[0]->email, $htmlMensagem);
			
			if($retEmail == true){
				$this->Dados->insertContato($contato);
				$data['msgSucesso'] = "<strong>Parabéns!</strong> Sua mensagem foi enviada com sucesso. :)";
			}else{
				$data['msgErro'] = "<strong>Desculpe!</strong>	Não foi possível enviar sua mensagem. :/";
			}
						
		}
		
		$this->load->view('artista-obras-detalhe',$data);
	}


    /*
    |--------------------------------------------------------------------------
    | Método para enviar e-mail
    |--------------------------------------------------------------------------
    */
    function enviarEmail($email,$htmlMensagem){		 
        $this->load->library('email');
        $this->email->from('sistema@mercadoarte.com.br', 'Mercado Arte');
        $this->email->to($email);
        $this->email->subject('Possível Comprador - Mercado Arte');
        $this->email->message($htmlMensagem);
        $ret = $this->email->send();
        return $ret;
	}





	
}

