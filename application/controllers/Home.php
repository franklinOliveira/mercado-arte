<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{		
		
		$limit = 25;
		$ordem = null;

		/*if(isset($_GET['ordem']) && !empty($_GET['ordem'])){
	    	$ordem = $_GET['ordem'];
	    }else{
	    	$ordem = 0;
	    }*/

		$this->load->model('Dados_model', 'Dados');

		/* FILTRO */
		if(isset($_GET['filtroTipoObra']) && !empty($_GET['filtroTipoObra'])){
	    	$filtroTipoObra = $_GET['filtroTipoObra'];
	    }else{
	    	$filtroTipoObra = 1;
	    }

	    switch ($filtroTipoObra) {
	    	case 'Pintura':
	    		$filtroTipoObra = 1;
	    		break;
	    	case 'Escultura':
	    		$filtroTipoObra = 2;
	    		break;
	    	case 'Fotografia':
	    		$filtroTipoObra = 3;
	    		break;	    	
	    	default:
	    		$filtroTipoObra = 1;
	    		break;
	    }


		$data['tecnicaObra'] = $this->Dados->getTecnica($filtroTipoObra);
		$data['formatoObra'] = $this->Dados->getFormato($filtroTipoObra);
		$data['categoriaObra'] = $this->Dados->getCategoria($filtroTipoObra);
		/* Fim filtro*/

			
		$data['obras'] = $this->Dados->getObrasHome($ordem, $limit);			

		$this->load->view('home',$data);
	}

/*
|--------------------------------------------------------------------------
| Soma a nota das obras
|--------------------------------------------------------------------------		
*/ 
	public function somarNota(){
		$this->load->model('Dados_model', 'Dados');
		$votoValido = $this->Dados->verificaVoto($this->input->get('idObra'));
		if($votoValido > 0){
			echo "Você já votou nessa obra!";
		}else{
			$ret = $this->Dados->salvaNota($this->input->get('nota'), $this->input->get('idObra'));
			echo $ret;
		}

		
	}
}
