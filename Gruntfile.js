module.exports = function(grunt){
    'use strict';

    // Configuration
    grunt.initConfig({

        // Package
        pkg: grunt.file.readJSON('package.json'),

        // Sprite Generator
        sprite: {
            images: {
                padding:   10,
                algorithm: 'binary-tree',
                cssFormat: 'css',
                src:       'content/images/sprite/*.png',
                destImg:   'content/images/sprite.png',
                destCSS:   'content/css/sprites/_sprite.scss',
                imgPath:   '../images/sprite.png',
                cssOpts: {
                    cssClass: function (item) {
                        return '.sprite-' + item.name;
                    }
                }
            },

            flags: {
                padding:   10,
                algorithm: 'binary-tree',
                cssFormat: 'css',
                src:       'content/images/flags/*.png',
                destImg:   'content/images/flags.png',
                destCSS:   'content/css/sprites/_flags.scss',
                imgPath:   '../images/flags.png',
                cssOpts: {
                    cssClass: function (item) {
                        return '.sprite-flag-' + item.name;
                    }
                }
            }
        }

    });
 
    // Plugins
    grunt.loadNpmTasks('grunt-spritesmith');

    // Tasks
    grunt.registerTask('default', ['sprite']);
    
};

