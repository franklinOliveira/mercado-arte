# Configuration
css_dir 		= "content/css"
sass_dir 		= "content/css"
images_dir 		= "content/images"
fonts_dir 		= "content/fonts"
javascripts_dir = "content/js"
http_path       = 'http://localhost/mercado-arte'

# Output
relative_assets = true
output_style 	= :compressed #nested