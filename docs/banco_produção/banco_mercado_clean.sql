/*
Navicat MySQL Data Transfer

Source Server         : LocalHost
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : walla148_sysma

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2015-01-18 21:42:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('09402e256d6c2a0d3395a48699bcb4c9', '191.255.201.54', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '1421621394', 'a:9:{s:14:\"usuario_logado\";s:7:\"a@e.com\";s:5:\"email\";s:7:\"a@e.com\";s:12:\"senhaSemCrip\";s:6:\"871389\";s:9:\"idUsuario\";i:85;s:12:\"nomeCompleto\";s:25:\"Franklin Oliveira Andrade\";s:11:\"valorAPagar\";s:6:\"360.00\";s:13:\"sitePagamento\";s:1:\"1\";s:9:\"nomePlano\";s:4:\"Ouro\";s:10:\"idTransact\";i:17;}');
INSERT INTO `ci_sessions` VALUES ('d8e0f9a84519eba285a92b42b9bc808d', '191.255.201.54', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '1421619651', '');
INSERT INTO `ci_sessions` VALUES ('f76e606fa106637b694805d8092414dc', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '1421623297', 'a:6:{s:14:\"usuario_logado\";s:7:\"b@b.com\";s:5:\"email\";s:7:\"b@b.com\";s:9:\"idUsuario\";i:87;s:12:\"nomeCompleto\";s:25:\"Franklin Oliveira Andrade\";s:13:\"sitePagamento\";s:1:\"1\";s:10:\"idTransact\";i:20;}');
INSERT INTO `ci_sessions` VALUES ('3c5be125db660167d559a007e7b2523d', '177.68.51.94', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.77 Safari/537.36', '1421622371', 'a:16:{s:9:\"user_data\";s:0:\"\";s:14:\"usuario_logado\";s:26:\"wallaceerick.dev@gmail.com\";s:5:\"email\";s:26:\"wallaceerick.dev@gmail.com\";s:12:\"senhaSemCrip\";s:6:\"123456\";s:9:\"idUsuario\";i:86;s:12:\"nomeCompleto\";s:13:\"Wallace Erick\";s:11:\"valorAPagar\";s:4:\"0.00\";s:13:\"sitePagamento\";i:999;s:9:\"nomePlano\";s:6:\"Bronze\";s:10:\"idTransact\";i:18;s:9:\"identObra\";s:4:\"kaka\";s:11:\"nomeArtista\";s:13:\"Wallace Erick\";s:8:\"tipoObra\";s:7:\"Pintura\";s:10:\"tituloObra\";s:5:\"Kaká\";s:15:\"nomeTecnicaObra\";s:9:\"Acrílica\";s:11:\"tamanhoObra\";s:11:\"200 x 200cm\";}');
INSERT INTO `ci_sessions` VALUES ('bc0fc21820edbd08550b9615d9edba1c', '177.41.213.4', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.99 Safari/537.36', '1421620774', 'a:7:{s:9:\"user_data\";s:0:\"\";s:14:\"usuario_logado\";s:19:\"fernando3@teste.com\";s:5:\"email\";s:19:\"fernando3@teste.com\";s:9:\"idUsuario\";i:84;s:12:\"nomeCompleto\";s:16:\"Fernando Godoy 3\";s:13:\"sitePagamento\";i:999;s:10:\"idTransact\";i:16;}');
INSERT INTO `ci_sessions` VALUES ('44b90108950ec1c624e57abbbd27b02f', '191.255.201.54', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '1421619653', '');
INSERT INTO `ci_sessions` VALUES ('b5f29e6c93f7b4b62c4880506b02cb81', '191.255.201.54', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '1421619653', '');
INSERT INTO `ci_sessions` VALUES ('ea8431b851beb351431826a4f315d0de', '191.255.201.54', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '1421619654', '');
INSERT INTO `ci_sessions` VALUES ('9f182af31b588626f64e6a63856ce700', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '1421624425', 'a:2:{s:9:\"user_data\";s:0:\"\";s:12:\"palavraBusca\";s:5:\"busca\";}');

-- ----------------------------
-- Table structure for tblbiografiaartista
-- ----------------------------
DROP TABLE IF EXISTS `tblbiografiaartista`;
CREATE TABLE `tblbiografiaartista` (
  `idBiografia` int(11) NOT NULL AUTO_INCREMENT,
  `txtBiografiaPT` text,
  `txtBiografiaEN` text,
  `txtBiografiaES` text,
  `imgBiografia` varchar(200) DEFAULT NULL,
  `dtCriacao` datetime DEFAULT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idBiografia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblbiografiaartista
-- ----------------------------

-- ----------------------------
-- Table structure for tblcategoriaobras
-- ----------------------------
DROP TABLE IF EXISTS `tblcategoriaobras`;
CREATE TABLE `tblcategoriaobras` (
  `idCategoriaObras` int(11) NOT NULL AUTO_INCREMENT,
  `nomeCategoriaObras` varchar(45) NOT NULL COMMENT 'Pintura, Escultura ou Fotografia',
  `descricaoCategoriaObras` varchar(45) DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idCategoriaObras`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblcategoriaobras
-- ----------------------------

-- ----------------------------
-- Table structure for tblcategoriaparceiros
-- ----------------------------
DROP TABLE IF EXISTS `tblcategoriaparceiros`;
CREATE TABLE `tblcategoriaparceiros` (
  `idCategoriaParceiro` int(11) NOT NULL AUTO_INCREMENT,
  `nomeCategoriaParcPT` varchar(45) NOT NULL,
  `nomeCategoriaParcEN` varchar(45) DEFAULT NULL,
  `nomeCategoriaParcES` varchar(45) DEFAULT NULL,
  `descricaoCategoriaParcPT` varchar(45) DEFAULT NULL,
  `descricaoCategoriaParcEN` varchar(45) DEFAULT NULL,
  `descricaoCategoriaParcES` varchar(45) DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idCategoriaParceiro`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblcategoriaparceiros
-- ----------------------------
INSERT INTO `tblcategoriaparceiros` VALUES ('1', 'Arquitetura', 'Arquitetura', 'Arquitetura', 'Arquitetura', 'Arquitetura', 'Arquitetura', '2014-12-13 17:59:36', null, '1');
INSERT INTO `tblcategoriaparceiros` VALUES ('2', 'Galeria de Arte', 'Galeria de Arte', 'Galeria de Arte', 'Galeria de Arte', 'Galeria de Arte', 'Galeria de Arte', '2014-12-13 18:00:40', null, '1');
INSERT INTO `tblcategoriaparceiros` VALUES ('3', 'Molduraria', 'Molduraria', 'Molduraria', 'Molduraria', 'Molduraria', 'Molduraria', '2014-12-13 18:00:42', null, '1');

-- ----------------------------
-- Table structure for tblcategoriasobras
-- ----------------------------
DROP TABLE IF EXISTS `tblcategoriasobras`;
CREATE TABLE `tblcategoriasobras` (
  `idCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `idTipoObra` int(11) NOT NULL,
  `nomeCategoriaPT` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `nomeCategoriaEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomeCategoriaES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoCategoriaPT` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoCategoriaEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoCategoriaES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tblcategoriasobras
-- ----------------------------
INSERT INTO `tblcategoriasobras` VALUES ('1', '1', 'CategoriaPintura', 'CategoriaPintura', 'CategoriaPintura', 'CategoriaPintura Teste', 'CategoriaPintura Teste', 'CategoriaPintura Teste', '2014-12-03 20:15:50', null, '0');
INSERT INTO `tblcategoriasobras` VALUES ('2', '1', 'CategoriaEscultura', 'CategoriaEscultura', 'CategoriaEscultura', 'CategoriaEscultura Teste', 'CategoriaEscultura Teste', 'CategoriaEscultura Teste', '2014-12-03 20:15:50', null, '0');
INSERT INTO `tblcategoriasobras` VALUES ('3', '3', 'CategoriaFotografia', 'CategoriaFotografia', 'CategoriaFotografia', 'CategoriaFotografia Teste', 'CategoriaFotografia Teste', 'CategoriaFotografia Teste', '2014-12-03 20:15:50', null, '0');
INSERT INTO `tblcategoriasobras` VALUES ('13', '1', 'Abstratas', null, null, 'Abstratas', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('14', '1', 'Animais', null, null, 'Animais', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('15', '1', 'Arquitetura & Edifícios', null, null, 'Arquitetura', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('16', '1', 'Beleza & Moda', null, null, 'Beleza & Moda', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('17', '1', 'Celebridade', null, null, 'Celebridade', null, null, '2014-12-30 20:13:00', '0000-00-00 00:00:00', '1');
INSERT INTO `tblcategoriasobras` VALUES ('18', '1', 'Cidades', null, null, 'Cidades', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('19', '1', 'Comida e Bebida', null, null, 'Comida e Bebida', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('20', '1', 'Ensino', null, null, 'Ensino', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('21', '1', 'Esporte', null, null, 'Esporte', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('22', '1', 'Fauna & Flora', null, null, 'Fauna & Flora', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('23', '1', 'Férias & Eventos', null, null, 'Férias & Eventos', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('24', '1', 'Industrial', null, null, 'Industrial', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('25', '1', 'Jóias & Gemas', null, null, 'Jóias & Gemas', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('26', '1', 'Medicina & Saúde', null, null, 'Medicina & Saúde', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('27', '1', 'Natureza', null, null, 'Natureza', null, null, '2014-12-30 20:13:00', '0000-00-00 00:00:00', '1');
INSERT INTO `tblcategoriasobras` VALUES ('28', '1', 'Negócios & Finanças', null, null, 'Negócios & Finanças', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('29', '1', 'Objetos & Ferramentas', null, null, 'Objetos & Ferramentas', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('30', '1', 'Pessoas', null, null, 'Pessoas', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('31', '1', 'Profissões', null, null, 'Profissões', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('32', '1', 'Religião', null, null, 'Religião', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('33', '1', 'Tecnologia', null, null, 'Tecnologia', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('34', '1', 'Transporte & Automóveis', null, null, 'Transporte & Automóveis', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('35', '1', 'Viagens', null, null, 'Viagens', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('36', '1', 'Vintage & Retro', null, null, 'Vintage & Retro', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('37', '1', 'Outras', null, null, 'Outras', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('42', '2', 'Abstratas', null, null, 'Abstratas', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('43', '2', 'Animais', null, null, 'Animais', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('44', '2', 'Arquitetura & Edifícios', null, null, 'Arquitetura', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('45', '2', 'Beleza & Moda', null, null, 'Beleza & Moda', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('46', '2', 'Celebridade', null, null, 'Celebridade', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('47', '2', 'Cidades', null, null, 'Cidades', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('48', '2', 'Comida e Bebida', null, null, 'Comida e Bebida', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('49', '2', 'Ensino', null, null, 'Ensino', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('50', '2', 'Esporte', null, null, 'Esporte', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('51', '2', 'Fauna & Flora', null, null, 'Fauna & Flora', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('52', '2', 'Férias & Eventos', null, null, 'Férias & Eventos', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('53', '2', 'Industrial', null, null, 'Industrial', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('54', '2', 'Jóias & Gemas', null, null, 'Jóias & Gemas', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('55', '2', 'Medicina & Saúde', null, null, 'Medicina & Saúde', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('56', '2', 'Natureza', null, null, 'Natureza', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('57', '2', 'Negócios & Finanças', null, null, 'Negócios & Finanças', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('58', '2', 'Objetos & Ferramentas', null, null, 'Objetos & Ferramentas', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('59', '2', 'Pessoas', null, null, 'Pessoas', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('60', '2', 'Profissões', null, null, 'Profissões', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('61', '2', 'Religião', null, null, 'Religião', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('62', '2', 'Tecnologia', null, null, 'Tecnologia', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('63', '2', 'Transporte & Automóveis', null, null, 'Transporte & Automóveis', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('64', '2', 'Viagens', null, null, 'Viagens', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('65', '2', 'Vintage & Retro', null, null, 'Vintage & Retro', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('66', '2', 'Outras', null, null, 'Outras', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('67', '3', 'Abstratas', null, null, 'Abstratas', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('68', '3', 'Animais', null, null, 'Animais', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('69', '3', 'Arquitetura & Edifícios', null, null, 'Arquitetura', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('70', '3', 'Beleza & Moda', null, null, 'Beleza & Moda', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('71', '3', 'Celebridade', null, null, 'Celebridade', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('72', '3', 'Cidades', null, null, 'Cidades', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('73', '3', 'Comida e Bebida', null, null, 'Comida e Bebida', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('74', '3', 'Ensino', null, null, 'Ensino', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('75', '3', 'Esporte', null, null, 'Esporte', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('76', '3', 'Fauna & Flora', null, null, 'Fauna & Flora', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('77', '3', 'Férias & Eventos', null, null, 'Férias & Eventos', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('78', '3', 'Industrial', null, null, 'Industrial', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('79', '3', 'Jóias & Gemas', null, null, 'Jóias & Gemas', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('80', '3', 'Medicina & Saúde', null, null, 'Medicina & Saúde', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('81', '3', 'Natureza', null, null, 'Natureza', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('82', '3', 'Negócios & Finanças', null, null, 'Negócios & Finanças', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('83', '3', 'Objetos & Ferramentas', null, null, 'Objetos & Ferramentas', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('84', '3', 'Pessoas', null, null, 'Pessoas', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('85', '3', 'Profissões', null, null, 'Profissões', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('86', '3', 'Religião', null, null, 'Religião', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('87', '3', 'Tecnologia', null, null, 'Tecnologia', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('88', '3', 'Transporte & Automóveis', null, null, 'Transporte & Automóveis', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('89', '3', 'Viagens', null, null, 'Viagens', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('90', '3', 'Vintage & Retro', null, null, 'Vintage & Retro', null, null, '2014-12-30 20:13:00', null, '1');
INSERT INTO `tblcategoriasobras` VALUES ('91', '3', 'Outras', null, null, 'Outras', null, null, '2014-12-30 20:13:00', null, '1');

-- ----------------------------
-- Table structure for tblcontato
-- ----------------------------
DROP TABLE IF EXISTS `tblcontato`;
CREATE TABLE `tblcontato` (
  `idContato` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) NOT NULL,
  `nomeContato` varchar(45) NOT NULL,
  `emailContato` varchar(45) NOT NULL,
  `telefoneContato` varchar(45) DEFAULT NULL,
  `mensagemContato` text,
  `dtCriacao` datetime NOT NULL,
  `dtAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(5) NOT NULL,
  PRIMARY KEY (`idContato`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblcontato
-- ----------------------------

-- ----------------------------
-- Table structure for tblcontratos
-- ----------------------------
DROP TABLE IF EXISTS `tblcontratos`;
CREATE TABLE `tblcontratos` (
  `idContrato` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Número do contrato',
  `idUsuario` int(11) NOT NULL COMMENT 'Código do usuário',
  `idTipoPlano` int(11) NOT NULL COMMENT 'Tipo de plano (1 = Bronze, 2 = Prata, 3 = Ouro, etc.)',
  `idStatusContrato` int(11) NOT NULL COMMENT 'Código do status do contrato (1 = Aguardando pagamento, 2 = Ativo, 3 = Inativo, etc.)',
  `dtInicioContrato` datetime NOT NULL COMMENT 'Data do aceite do contrato.',
  `dtFimContrato` datetime NOT NULL COMMENT 'Data do fim do contrato de acordo com a periodicidade e o plano escolhido.',
  `observacao` varchar(45) DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idContrato`),
  KEY `fkContratosxUsuarios_idx` (`idUsuario`) USING BTREE,
  KEY `fkContratosxTipoPlanos_idx` (`idTipoPlano`) USING BTREE,
  KEY `fkContratosxStatusContrato_idx` (`idStatusContrato`) USING BTREE,
  CONSTRAINT `tblcontratos_ibfk_1` FOREIGN KEY (`idStatusContrato`) REFERENCES `tblstatuscontrato` (`idStatusContrato`) ON UPDATE NO ACTION,
  CONSTRAINT `tblcontratos_ibfk_2` FOREIGN KEY (`idTipoPlano`) REFERENCES `tbltipoplano` (`idTipoPlano`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tblcontratos_ibfk_3` FOREIGN KEY (`idUsuario`) REFERENCES `tblusuarios` (`idUsuario`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblcontratos
-- ----------------------------

-- ----------------------------
-- Table structure for tblcorpredobras
-- ----------------------------
DROP TABLE IF EXISTS `tblcorpredobras`;
CREATE TABLE `tblcorpredobras` (
  `idCorPred` int(11) NOT NULL AUTO_INCREMENT,
  `idTipoObra` int(11) NOT NULL,
  `nomeCorPredPT` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `nomeCorPredEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomeCorPredES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoCorPredPT` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoCorPredEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoCorPredES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  `cssClass` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idCorPred`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tblcorpredobras
-- ----------------------------
INSERT INTO `tblcorpredobras` VALUES ('1', '1', 'Vermelho', null, null, 'Vermelho', null, null, '2015-01-05 15:08:35', '2015-01-05 15:09:06', '1', 'red');
INSERT INTO `tblcorpredobras` VALUES ('2', '1', 'Laranja', null, null, 'Laranja', null, null, '2015-01-05 15:08:42', '2015-01-05 15:09:08', '1', 'orange');
INSERT INTO `tblcorpredobras` VALUES ('3', '1', 'Amarelo', null, null, 'Amarelo', null, null, '2015-01-05 15:08:45', '2015-01-05 15:09:09', '1', 'yellow');
INSERT INTO `tblcorpredobras` VALUES ('4', '1', 'Verde', null, null, 'Verde', null, null, '2015-01-05 15:08:47', '2015-01-05 15:09:11', '1', 'green');
INSERT INTO `tblcorpredobras` VALUES ('5', '1', 'Ciano', null, null, 'Ciano', null, null, '2015-01-05 15:08:50', '2015-01-05 15:09:12', '1', 'blue-light');
INSERT INTO `tblcorpredobras` VALUES ('6', '1', 'Azul', null, null, 'Azul', null, null, '2015-01-05 15:08:52', '2015-01-05 15:09:14', '1', 'blue');
INSERT INTO `tblcorpredobras` VALUES ('7', '1', 'Roxo', null, null, 'Roxo', null, null, '2015-01-05 15:08:55', '2015-01-05 15:10:28', '1', 'purple');
INSERT INTO `tblcorpredobras` VALUES ('8', '1', 'Rosa', null, null, 'Rosa', null, null, '2015-01-05 15:08:57', '2015-01-05 15:09:17', '1', 'pink');
INSERT INTO `tblcorpredobras` VALUES ('9', '1', 'Branco', null, null, 'Branco', null, null, '2015-01-05 15:08:58', '2015-01-05 15:09:19', '1', 'white');
INSERT INTO `tblcorpredobras` VALUES ('10', '1', 'Cinza', null, null, 'Cinza', null, null, '2015-01-05 15:09:00', '2015-01-05 15:10:24', '1', 'gray');
INSERT INTO `tblcorpredobras` VALUES ('11', '1', 'Preto', null, null, 'Preto', null, null, '2015-01-05 15:09:02', '2015-01-05 15:09:22', '1', 'black');
INSERT INTO `tblcorpredobras` VALUES ('12', '1', 'Marrom', null, null, 'Marrom', null, null, '2015-01-05 15:09:03', '2015-01-05 15:09:24', '1', 'brown');
INSERT INTO `tblcorpredobras` VALUES ('13', '2', 'Vermelho', null, null, 'Vermelho', null, null, '2015-01-05 15:08:35', '2015-01-05 15:09:06', '1', 'red');
INSERT INTO `tblcorpredobras` VALUES ('14', '2', 'Laranja', null, null, 'Laranja', null, null, '2015-01-05 15:08:42', '2015-01-05 15:09:08', '1', 'orange');
INSERT INTO `tblcorpredobras` VALUES ('15', '2', 'Amarelo', null, null, 'Amarelo', null, null, '2015-01-05 15:08:45', '2015-01-05 15:09:09', '1', 'yellow');
INSERT INTO `tblcorpredobras` VALUES ('16', '2', 'Verde', null, null, 'Verde', null, null, '2015-01-05 15:08:47', '2015-01-05 15:09:11', '1', 'green');
INSERT INTO `tblcorpredobras` VALUES ('17', '2', 'Ciano', null, null, 'Ciano', null, null, '2015-01-05 15:08:50', '2015-01-05 15:09:12', '1', 'blue-light');
INSERT INTO `tblcorpredobras` VALUES ('18', '2', 'Azul', null, null, 'Azul', null, null, '2015-01-05 15:08:52', '2015-01-05 15:09:14', '1', 'blue');
INSERT INTO `tblcorpredobras` VALUES ('19', '2', 'Roxo', null, null, 'Roxo', null, null, '2015-01-05 15:08:55', '2015-01-05 15:10:28', '1', 'purple');
INSERT INTO `tblcorpredobras` VALUES ('20', '2', 'Rosa', null, null, 'Rosa', null, null, '2015-01-05 15:08:57', '2015-01-05 15:09:17', '1', 'pink');
INSERT INTO `tblcorpredobras` VALUES ('21', '2', 'Branco', null, null, 'Branco', null, null, '2015-01-05 15:08:58', '2015-01-05 15:09:19', '1', 'white');
INSERT INTO `tblcorpredobras` VALUES ('22', '2', 'Cinza', null, null, 'Cinza', null, null, '2015-01-05 15:09:00', '2015-01-05 15:10:24', '1', 'gray');
INSERT INTO `tblcorpredobras` VALUES ('23', '2', 'Preto', null, null, 'Preto', null, null, '2015-01-05 15:09:02', '2015-01-05 15:09:22', '1', 'black');
INSERT INTO `tblcorpredobras` VALUES ('24', '2', 'Marrom', null, null, 'Marrom', null, null, '2015-01-05 15:09:03', '2015-01-05 15:09:24', '1', 'brown');
INSERT INTO `tblcorpredobras` VALUES ('25', '3', 'Vermelho', null, null, 'Vermelho', null, null, '2015-01-05 15:08:35', '2015-01-05 15:09:06', '1', 'red');
INSERT INTO `tblcorpredobras` VALUES ('26', '3', 'Laranja', null, null, 'Laranja', null, null, '2015-01-05 15:08:42', '2015-01-05 15:09:08', '1', 'orange');
INSERT INTO `tblcorpredobras` VALUES ('27', '3', 'Amarelo', null, null, 'Amarelo', null, null, '2015-01-05 15:08:45', '2015-01-05 15:09:09', '1', 'yellow');
INSERT INTO `tblcorpredobras` VALUES ('28', '3', 'Verde', null, null, 'Verde', null, null, '2015-01-05 15:08:47', '2015-01-05 15:09:11', '1', 'green');
INSERT INTO `tblcorpredobras` VALUES ('29', '3', 'Ciano', null, null, 'Ciano', null, null, '2015-01-05 15:08:50', '2015-01-05 15:09:12', '1', 'blue-light');
INSERT INTO `tblcorpredobras` VALUES ('30', '3', 'Azul', null, null, 'Azul', null, null, '2015-01-05 15:08:52', '2015-01-05 15:09:14', '1', 'blue');
INSERT INTO `tblcorpredobras` VALUES ('31', '3', 'Roxo', null, null, 'Roxo', null, null, '2015-01-05 15:08:55', '2015-01-05 15:10:28', '1', 'purple');
INSERT INTO `tblcorpredobras` VALUES ('32', '3', 'Rosa', null, null, 'Rosa', null, null, '2015-01-05 15:08:57', '2015-01-05 15:09:17', '1', 'pink');
INSERT INTO `tblcorpredobras` VALUES ('33', '3', 'Branco', null, null, 'Branco', null, null, '2015-01-05 15:08:58', '2015-01-05 15:09:19', '1', 'white');
INSERT INTO `tblcorpredobras` VALUES ('34', '3', 'Cinza', null, null, 'Cinza', null, null, '2015-01-05 15:09:00', '2015-01-05 15:10:24', '1', 'gray');
INSERT INTO `tblcorpredobras` VALUES ('35', '3', 'Preto', null, null, 'Preto', null, null, '2015-01-05 15:09:02', '2015-01-05 15:09:22', '1', 'black');
INSERT INTO `tblcorpredobras` VALUES ('36', '3', 'Marrom', null, null, 'Marrom', null, null, '2015-01-05 15:09:03', '2015-01-05 15:09:24', '1', 'brown');

-- ----------------------------
-- Table structure for tblformatoobras
-- ----------------------------
DROP TABLE IF EXISTS `tblformatoobras`;
CREATE TABLE `tblformatoobras` (
  `idFormato` int(11) NOT NULL AUTO_INCREMENT,
  `idTipoObra` int(11) NOT NULL,
  `nomeFormatoPT` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `nomeFormatoEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomeFormatoES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoFormatoPT` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoFormatoEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoFormatoES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idFormato`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tblformatoobras
-- ----------------------------
INSERT INTO `tblformatoobras` VALUES ('1', '1', 'Horizontal', null, null, 'Horizontal', null, null, '2015-01-05 14:54:20', '2015-01-05 14:54:24', '1');
INSERT INTO `tblformatoobras` VALUES ('2', '1', 'Vertical', null, null, 'Vertical', null, null, '2015-01-05 14:54:41', '2015-01-05 14:54:43', '1');
INSERT INTO `tblformatoobras` VALUES ('3', '1', 'Circular', null, null, 'Circular', null, null, '2015-01-05 14:55:00', '2015-01-05 14:55:02', '1');
INSERT INTO `tblformatoobras` VALUES ('4', '1', 'Quadrado', null, null, 'Quadrado', null, null, '2015-01-05 14:55:21', '2015-01-05 14:55:24', '1');
INSERT INTO `tblformatoobras` VALUES ('5', '2', 'Horizontal', null, null, 'Horizontal', null, null, '2015-01-05 14:59:12', '2015-01-05 14:59:15', '1');
INSERT INTO `tblformatoobras` VALUES ('6', '2', 'Vertical', null, null, 'Vertical', null, null, '2015-01-05 14:59:50', '2015-01-05 14:59:54', '1');
INSERT INTO `tblformatoobras` VALUES ('7', '2', 'Circular', null, null, 'Circular', null, null, '2015-01-05 15:00:06', '2015-01-05 15:00:08', '1');
INSERT INTO `tblformatoobras` VALUES ('8', '2', 'Quadrado', null, null, 'Quadrado', null, null, '2015-01-05 15:00:22', '2015-01-05 15:00:24', '1');
INSERT INTO `tblformatoobras` VALUES ('9', '3', 'Horizontal', null, null, 'Horizontal', null, null, '2015-01-05 15:00:43', '2015-01-05 15:00:46', '1');
INSERT INTO `tblformatoobras` VALUES ('10', '3', 'Vertical', null, null, 'Vertical', null, null, '2015-01-05 15:01:00', '2015-01-05 15:01:03', '1');
INSERT INTO `tblformatoobras` VALUES ('11', '3', 'Circular', null, null, 'Circular', null, null, '2015-01-05 15:01:16', '2015-01-05 15:01:18', '1');
INSERT INTO `tblformatoobras` VALUES ('12', '3', 'Quadrado', null, null, 'Quadrado', null, null, '2015-01-05 15:01:30', '2015-01-05 15:01:33', '1');

-- ----------------------------
-- Table structure for tblformulariocontato
-- ----------------------------
DROP TABLE IF EXISTS `tblformulariocontato`;
CREATE TABLE `tblformulariocontato` (
  `idFormContato` int(11) NOT NULL AUTO_INCREMENT,
  `idCategoriaObras` int(11) NOT NULL COMMENT 'id da categoria da obra: Pintura, Escultura ou Fotografia',
  `idObra` int(11) NOT NULL COMMENT 'id da obra, seja pintura, escultura ou fotografia',
  `nomeRemetente` varchar(45) NOT NULL,
  `emailRemetente` varchar(45) NOT NULL,
  `telefoneRemetente` varchar(45) DEFAULT NULL,
  `mensagemRemetente` varchar(45) NOT NULL,
  `valorObra` varchar(45) NOT NULL,
  `dataEnvio` varchar(45) NOT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idFormContato`),
  KEY `fkFormContato_idx` (`idCategoriaObras`) USING BTREE,
  CONSTRAINT `tblformulariocontato_ibfk_1` FOREIGN KEY (`idCategoriaObras`) REFERENCES `tblcategoriaobras` (`idCategoriaObras`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblformulariocontato
-- ----------------------------

-- ----------------------------
-- Table structure for tblgeralcupom
-- ----------------------------
DROP TABLE IF EXISTS `tblgeralcupom`;
CREATE TABLE `tblgeralcupom` (
  `idCupom` int(11) NOT NULL AUTO_INCREMENT,
  `codCupom` varchar(8) NOT NULL COMMENT 'Código do cupom, randômico e alfanumérico com oito caracteres.',
  `porcentagemDesconto` tinyint(4) NOT NULL COMMENT 'Porcentagem de desconto que o cupom aplicará no valor total, de 1% até 100%.',
  `cupomAtivo` tinyint(4) NOT NULL COMMENT 'Cupom ativo? 0 = Não, 1 = Sim.',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idCupom`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblgeralcupom
-- ----------------------------
INSERT INTO `tblgeralcupom` VALUES ('1', 'G99XAE94', '50', '0', '2014-11-24 18:21:40', '2014-11-24 18:21:44', '1');
INSERT INTO `tblgeralcupom` VALUES ('2', 'AHFPXV7W', '50', '0', '2014-11-24 18:22:23', '2014-11-24 18:22:25', '1');
INSERT INTO `tblgeralcupom` VALUES ('3', 'WW3QFFTK', '50', '0', '2014-11-24 18:22:43', '2014-12-03 23:32:42', '1');
INSERT INTO `tblgeralcupom` VALUES ('4', 'P57YLD9K', '100', '1', '2014-11-24 18:23:02', '2014-11-24 18:23:05', '1');
INSERT INTO `tblgeralcupom` VALUES ('5', '5UDYYBSE', '100', '1', '2014-11-24 18:23:18', '2014-11-24 18:23:21', '1');
INSERT INTO `tblgeralcupom` VALUES ('6', 'PC47XTRL', '25', '0', '2014-11-24 18:23:36', '2015-01-06 10:46:01', '1');

-- ----------------------------
-- Table structure for tblgeralpais
-- ----------------------------
DROP TABLE IF EXISTS `tblgeralpais`;
CREATE TABLE `tblgeralpais` (
  `idPais` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) DEFAULT NULL,
  `iso3` char(3) DEFAULT NULL,
  `nome` varchar(45) NOT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPais`)
) ENGINE=InnoDB AUTO_INCREMENT=895 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblgeralpais
-- ----------------------------
INSERT INTO `tblgeralpais` VALUES ('1', null, null, 'teste', '0000-00-00 00:00:00', null, null, '0');
INSERT INTO `tblgeralpais` VALUES ('4', 'AF', 'AFG', 'Afeganistão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('8', 'AL', 'ALB', 'Albânia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('10', 'AQ', 'ATA', 'Antárctida', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('12', 'DZ', 'DZA', 'Argélia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('16', 'AS', 'ASM', 'Samoa Americana', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('20', 'AD', 'AND', 'Andorra', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('24', 'AO', 'AGO', 'Angola', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('28', 'AG', 'ATG', 'Antigua e Barbuda', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('31', 'AZ', 'AZE', 'Azerbeijão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('32', 'AR', 'ARG', 'Argentina', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('36', 'AU', 'AUS', 'Austrália', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('40', 'AT', 'AUT', 'Áustria', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('44', 'BS', 'BHS', 'Bahamas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('48', 'BH', 'BHR', 'Bahrain', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('50', 'BD', 'BGD', 'Bangladesh', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('51', 'AM', 'ARM', 'Arménia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('52', 'BB', 'BRB', 'Barbados', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('56', 'BE', 'BEL', 'Bélgica', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('60', 'BM', 'BMU', 'Bermuda', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('64', 'BT', 'BTN', 'Butão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('68', 'BO', 'BOL', 'Bolívia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('70', 'BA', 'BIH', 'Bósnia-Herzegovina', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('72', 'BW', 'BWA', 'Botswana', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('74', 'BV', 'BVT', 'Bouvet, Ilha', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('76', 'BR', 'BRA', 'Brasil', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('84', 'BZ', 'bel', 'Belize', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('86', 'IO', 'IOT', 'Território Britânico do Oceano Índico', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('90', 'SB', 'SLB', 'Salomão, Ilhas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('92', 'VG', 'VGB', 'Virgens Britânicas, Ilhas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('96', 'BN', 'BRN', 'Brunei', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('100', 'BG', 'BGR', 'Bulgária', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('104', 'MM', 'MMR', 'Myanmar (antiga Birmânia)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('108', 'BI', 'BDI', 'Burundi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('112', 'BY', 'BLR', 'Bielo-Rússia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('116', 'KH', 'KHM', 'Cambodja', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('120', 'CM', 'CMR', 'Camarões', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('124', 'CA', 'CAN', 'Canadá', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('132', 'CV', 'CPV', 'Cabo Verde', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('136', 'KY', 'CYM', 'Cayman, Ilhas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('140', 'CF', 'CAF', 'Centro-africana, República', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('144', 'LK', 'LKA', 'Sri Lanka', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('148', 'TD', 'TCD', 'Chade', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('152', 'CL', 'CHL', 'Chile', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('156', 'CN', 'CHN', 'China', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('158', 'TW', 'TWN', 'Taiwan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('162', 'CX', 'CXR', 'Christmas, Ilha', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('166', 'CC', 'CCK', 'Cocos, Ilhas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('170', 'CO', 'COL', 'Colômbia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('174', 'KM', 'COM', 'Comores', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('175', 'YT', 'MYT', 'Mayotte', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('178', 'CG', 'COG', 'Congo, República do', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('180', 'CD', 'COD', 'Congo, República Democrática do (antigo Zaire', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('184', 'CK', 'COK', 'Cook, Ilhas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('188', 'CR', 'CRI', 'Costa Rica', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('191', 'HR', 'HRV', 'Croácia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('192', 'c*', 'CUB', 'Cuba', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('196', 'CY', 'CYP', 'Chipre', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('203', 'CZ', 'CZE', 'Checa, República', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('204', 'BJ', 'BEN', 'Benin', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('208', 'DK', 'DNK', 'Dinamarca', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('212', 'DM', 'DMA', 'Dominica', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('214', 'DO', 'DOM', 'Dominicana, República', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('218', 'EC', 'ECU', 'Equador', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('222', 'SV', 'SLV', 'El Salvador', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('226', 'GQ', 'GNQ', 'Guiné Equatorial', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('231', 'ET', 'ETH', 'Etiópia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('232', 'ER', 'ERI', 'Eritreia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('233', 'EE', 'EST', 'Estónia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('234', 'FO', 'FRO', 'Faroe, Ilhas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('238', 'FK', 'FLK', 'Malvinas, Ilhas (Falkland)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('239', 'GS', 'SGS', 'Geórgia do Sul e Sandwich do Sul, Ilhas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('242', 'FJ', 'FJI', 'Fiji', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('246', 'FI', 'FIN', 'Finlândia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('248', 'AX', 'ALA', 'Åland, Ilhas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('250', 'FR', 'FRA', 'França', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('254', 'GF', 'GUF', 'Guiana Francesa', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('258', 'PF', 'PYF', 'Polinésia Francesa', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('260', 'TF', 'ATF', 'Terras Austrais e Antárticas Francesas (TAAF)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('262', 'DJ', 'DJI', 'Djibouti', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('266', 'GA', 'GAB', 'Gabão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('268', 'GE', 'GEO', 'Geórgia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('270', 'GM', 'GMB', 'Gâmbia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('275', 'PS', 'PSE', 'Palestina', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('276', 'DE', 'DEU', 'Alemanha', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('288', 'GH', 'GHA', 'Gana', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('292', 'GI', 'GIB', 'Gibraltar', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('296', 'KI', 'KIR', 'Kiribati', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('300', 'GR', 'GRC', 'Grécia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('304', 'GL', 'GRL', 'Gronelândia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('308', 'GD', 'GRD', 'Grenada', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('312', 'GP', 'GLP', 'Guadeloupe', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('316', 'GU', 'GUM', 'Guam', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('320', 'GT', 'GTM', 'Guatemala', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('324', 'GN', 'GIN', 'Guiné-Conacri', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('328', 'GY', 'GUY', 'Guiana', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('332', 'HT', 'HTI', 'Haiti', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('334', 'HM', 'HMD', 'Heard e Ilhas McDonald, Ilha', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('336', 'VA', 'VAT', 'Vaticano', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('340', 'HN', 'HND', 'Honduras', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('344', 'HK', 'HKG', 'Hong Kong', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('348', 'HU', 'HUN', 'Hungria', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('352', 'IS', 'ISL', 'Islândia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('356', 'IN', 'IND', 'Índia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('360', 'ID', 'IDN', 'Indonésia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('364', 'IR', 'IRN', 'Irão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('368', 'IQ', 'IRQ', 'Iraque', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('372', 'IE', 'IRL', 'Irlanda', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('376', 'IL', 'ISR', 'Israel', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('380', 'IT', 'ITA', 'Itália', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('384', 'CI', 'CIV', 'Costa do Marfim', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('388', 'JM', 'JAM', 'Jamaica', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('392', 'JP', 'JPN', 'Japão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('398', 'KZ', 'KAZ', 'Cazaquistão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('400', 'JO', 'JOR', 'Jordânia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('404', 'KE', 'KEN', 'Quénia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('408', 'KP', 'PRK', 'Coreia, República Democrática da (Coreia do N', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('410', 'KR', 'KOR', 'Coreia do Sul', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('414', 'KW', 'KWT', 'Kuwait', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('417', 'KG', 'KGZ', 'Quirguistão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('418', 'LA', 'LAO', 'Laos', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('422', 'LB', 'LBN', 'Líbano', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('426', 'LS', 'LSO', 'Lesoto', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('428', 'LV', 'LVA', 'Letónia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('430', 'LR', 'LBR', 'Libéria', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('434', 'LY', 'LBY', 'Líbia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('438', 'LI', 'LIE', 'Liechtenstein', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('440', 'LT', 'LTU', 'Lituânia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('442', 'LU', 'LUX', 'Luxemburgo', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('446', 'MO', 'MAC', 'Macau', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('450', 'MG', 'MDG', 'Madagáscar', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('454', 'MW', 'MWI', 'Malawi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('458', 'MY', 'MYS', 'Malásia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('462', 'MV', 'MDV', 'Maldivas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('466', 'ML', 'MLI', 'Mali', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('470', 'MT', 'MLT', 'Malta', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('474', 'MQ', 'MTQ', 'Martinica', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('478', 'MR', 'MRT', 'Mauritânia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('480', 'MU', 'MUS', 'Maurícia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('484', 'MX', 'MEX', 'México', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('492', 'MC', 'MCO', 'Mónaco', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('496', 'MN', 'MNG', 'Mongólia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('498', 'MD', 'MDA', 'Moldávia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('499', 'ME', 'MNE', 'Montenegro', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('500', 'MS', 'MSR', 'Montserrat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('504', 'MA', 'MAR', 'Marrocos', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('508', 'MZ', 'MOZ', 'Moçambique', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('512', 'OM', 'OMN', 'Oman', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('516', 'NA', 'NAM', 'Namíbia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('520', 'NR', 'NRU', 'Nauru', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('524', 'NP', 'NPL', 'Nepal', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('528', 'NL', 'NLD', 'Países Baixos (Holanda)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('530', 'AN', 'ANT', 'Antilhas Holandesas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('533', 'AW', 'ABW', 'Aruba', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('540', 'NC', 'NCL', 'Nova Caledónia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('548', 'VU', 'VUT', 'Vanuatu', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('554', 'NZ', 'NZL', 'Nova Zelândia (Aotearoa)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('558', 'NI', 'NIC', 'Nicarágua', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('562', 'NE', 'NER', 'Níger', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('566', 'NG', 'NGA', 'Nigéria', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('570', 'NU', 'NIU', 'Niue', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('574', 'NF', 'NFK', 'Norfolk, Ilha', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('578', 'NO', 'NOR', 'Noruega', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('580', 'MP', 'MNP', 'Marianas Setentrionais', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('581', 'UM', 'UMI', 'Menores Distantes dos Estados Unidos, Ilhas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('583', 'FM', 'FSM', 'Micronésia, Estados Federados da', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('584', 'MH', 'MHL', 'Marshall, Ilhas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('585', 'PW', 'PLW', 'Palau', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('586', 'PK', 'PAK', 'Paquistão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('591', 'PA', 'PAN', 'Panamá', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('598', 'PG', 'PNG', 'Papua-Nova Guiné', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('600', 'PY', 'PRY', 'Paraguai', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('604', 'PE', 'PER', 'Peru', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('608', 'PH', 'PHL', 'Filipinas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('612', 'PN', 'PCN', 'Pitcairn', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('616', 'PL', 'POL', 'Polónia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('620', 'PT', 'PRT', 'Portugal', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('624', 'GW', 'GNB', 'Guiné-Bissau', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('626', 'TL', 'TLS', 'Timor-Leste', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('630', 'PR', 'PRI', 'Porto Rico', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('634', 'QA', 'QAT', 'Qatar', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('638', 'RE', 'REU', 'Reunião', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('642', 'RO', 'ROU', 'Roménia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('643', 'RU', 'RUS', 'Rússia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('646', 'RW', 'RWA', 'Ruanda', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('654', 'SH', 'SHN', 'Santa Helena', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('659', 'KN', 'KNA', 'São Cristóvão e Névis (Saint Kitts e Nevis)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('660', 'AI', 'AIA', 'Anguilla', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('662', 'LC', 'LCA', 'Santa Lúcia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('666', 'PM', 'SPM', 'Saint Pierre et Miquelon', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('670', 'vo', 'VCT', 'São Vicente e Granadinas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('674', 'SM', 'SMR', 'San Marino', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('678', 'ST', 'STP', 'São Tomé e Príncipe', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('682', 'SA', 'SAU', 'Arábia Saudita', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('686', 'SN', 'SEN', 'Senegal', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('688', 'RS', 'SRB', 'Sérvia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('690', 'SC', 'SYC', 'Seychelles', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('694', 'SL', 'SLE', 'Serra Leoa', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('702', 'SG', 'SGP', 'Singapura', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('703', 'SK', 'SVK', 'Eslováquia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('704', 'VN', 'VNM', 'Vietname', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('705', 'SI', 'SVN', 'Eslovénia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('706', 'SO', 'SOM', 'Somália', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('710', 'ZA', 'ZAF', 'África do Sul', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('716', 'ZW', 'ZWE', 'Zimbabwe', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('724', 'ES', 'ESP', 'Espanha', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('732', 'EH', 'ESH', 'Saara Ocidental', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('736', 'SD', 'SDN', 'Sudão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('740', 'SR', 'SUR', 'Suriname', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('744', 'SJ', 'SJM', 'Svalbard e Jan Mayen', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('748', 'SZ', 'SWZ', 'Suazilândia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('752', 'SE', 'SWE', 'Suécia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('756', 'CH', 'CHE', 'Suíça', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('760', 'SY', 'SYR', 'Síria', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('762', 'TJ', 'TJK', 'Tajiquistão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('764', 'TH', 'THA', 'Tailândia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('768', 'TG', 'TGO', 'Togo', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('772', 'TK', 'TKL', 'Toquelau', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('776', 'TO', 'TON', 'Tonga', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('780', 'TT', 'TTO', 'Trindade e Tobago', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('784', 'AE', 'ARE', 'Emiratos Árabes Unidos', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('788', 'TN', 'TUN', 'Tunísia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('792', 'TR', 'TUR', 'Turquia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('795', 'TM', 'TKM', 'Turquemenistão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('796', 'TC', 'TCA', 'Turks e Caicos', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('798', 'TV', 'TUV', 'Tuvalu', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('800', 'UG', 'UGA', 'Uganda', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('804', 'UA', 'UKR', 'Ucrânia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('807', 'MK', 'MKD', 'Macedónia, República da', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('818', 'EG', 'EGY', 'Egipto', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('826', 'GB', 'GBR', 'Reino Unido da Grã-Bretanha e Irlanda do Nort', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('831', 'GG', 'GGY', 'Guernsey', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('832', 'JE', 'JEY', 'Jersey', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('833', 'IM', 'IMN', 'Man, Ilha de', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('834', 'TZ', 'TZA', 'Tanzânia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('840', 'US', 'USA', 'Estados Unidos da América', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('850', 'VI', 'VIR', 'Virgens Americanas, Ilhas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('854', 'BF', 'BFA', 'Burkina Faso', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('858', 'UY', 'URY', 'Uruguai', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('860', 'UZ', 'UZB', 'Usbequistão', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('862', 'VE', 'VEN', 'Venezuela', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('876', 'WF', 'WLF', 'Wallis e Futuna', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('882', 'WS', 'WSM', 'Samoa (Samoa Ocidental)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('887', 'YE', 'YEM', 'Iémen', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');
INSERT INTO `tblgeralpais` VALUES ('894', 'ZM', 'ZMB', 'Zâmbia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NULL', '1');

-- ----------------------------
-- Table structure for tblnewsletter
-- ----------------------------
DROP TABLE IF EXISTS `tblnewsletter`;
CREATE TABLE `tblnewsletter` (
  `idNewsletter` int(11) NOT NULL AUTO_INCREMENT,
  `nomeNewsletter` varchar(45) NOT NULL,
  `emailNewsletter` varchar(45) NOT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(5) NOT NULL,
  PRIMARY KEY (`idNewsletter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblnewsletter
-- ----------------------------

-- ----------------------------
-- Table structure for tblobras
-- ----------------------------
DROP TABLE IF EXISTS `tblobras`;
CREATE TABLE `tblobras` (
  `idObra` int(11) NOT NULL AUTO_INCREMENT,
  `idPgArtista` int(11) NOT NULL,
  `idTipoObra` int(11) NOT NULL COMMENT 'Tipo de obra: Obra, Escultura ou Fotografia',
  `tituloPT` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Titulo da Obra, algo semelhante há:\n\n"Mulher com crianças", "Figura abstrata", "Jogo de futebol".',
  `tituloEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tituloES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idTecnica` int(11) NOT NULL COMMENT 'Técnica da Obra (Óleo, acrílico, desenho).',
  `altura` decimal(15,2) NOT NULL COMMENT 'Altura da obra',
  `comprimento` decimal(15,2) NOT NULL COMMENT 'Comprimento da obra',
  `largura` decimal(15,2) DEFAULT NULL COMMENT 'Largura da obra',
  `ladoMaior` int(11) DEFAULT NULL COMMENT 'Esta coluna deverá ser preenchida pelo sistema no momento do insert/update das obras, ela armazenará o lado maior da obra. Por exemplo, uma Obra que foi cadastrada co',
  `dataCriacao` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Data de criação da obra',
  `descricaoPT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Descrição da obra',
  `descricaoEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idFormato` int(11) NOT NULL COMMENT 'Formato da obra (Horizontal, Vertical, etc.)',
  `precoObra` decimal(15,2) NOT NULL COMMENT 'Preço da Obra',
  `precoComDesconto` decimal(15,2) DEFAULT NULL COMMENT 'Preço com desconto aplicado',
  `idCorPred` int(11) NOT NULL COMMENT 'Id da cor predominante',
  `idCorPred2` int(11) DEFAULT NULL,
  `idCorPred3` int(11) DEFAULT NULL,
  `idCategoria1` int(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria2` int(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria3` int(11) DEFAULT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `imgFoto1` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Caminho da foto da Obra 1',
  `imgFoto2` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Caminho da foto da Obra 2',
  `imgFoto3` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Caminho da foto da Obra 3',
  `idStatusObra` int(11) NOT NULL COMMENT 'Status da Obra, exemplo:\n\n1 = À venda\n2 = Vendido\n',
  `NumVisitas` int(11) DEFAULT NULL,
  `somatoriaNotas` int(11) DEFAULT NULL COMMENT 'Somatória das notas atribuidas pelos visitantes .. por exempl',
  `qtdVotos` int(11) DEFAULT NULL COMMENT 'Armazena quantidade de votos.',
  `forcaDestaqPgInicial` tinyint(4) NOT NULL DEFAULT '0',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  `imgFoto4` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slugObra` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idObra`),
  KEY `fkObraxTecnica_idx` (`idTecnica`) USING BTREE,
  KEY `fkObraxFormato_idx` (`idFormato`) USING BTREE,
  KEY `fkObraxCorPredom_idx` (`idCorPred`) USING BTREE,
  KEY `fkObraxCategoria1_idx` (`idCategoria1`) USING BTREE,
  KEY `fkObraxCategoria2_idx` (`idCategoria2`) USING BTREE,
  KEY `fkObraxCategoria3_idx` (`idCategoria3`) USING BTREE,
  KEY `fkObraxStatusObra_idx` (`idStatusObra`) USING BTREE,
  KEY `fkObraxPgArtista_idx` (`idPgArtista`) USING BTREE,
  KEY `fkObraxCategoriaObra_idx` (`idTipoObra`) USING BTREE,
  CONSTRAINT `tblobras_ibfk_1` FOREIGN KEY (`idCategoria1`) REFERENCES `tblcategoriasobras` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tblobras_ibfk_2` FOREIGN KEY (`idCategoria2`) REFERENCES `tblcategoriasobras` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tblobras_ibfk_3` FOREIGN KEY (`idCategoria3`) REFERENCES `tblcategoriasobras` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tblobras_ibfk_4` FOREIGN KEY (`idTipoObra`) REFERENCES `tbltipoobras` (`idTipoObra`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tblobras_ibfk_5` FOREIGN KEY (`idCorPred`) REFERENCES `tblcorpredobras` (`idCorPred`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tblobras_ibfk_6` FOREIGN KEY (`idFormato`) REFERENCES `tblformatoobras` (`idFormato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tblobras_ibfk_7` FOREIGN KEY (`idPgArtista`) REFERENCES `tblpgartista` (`idPgArtista`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tblobras_ibfk_8` FOREIGN KEY (`idStatusObra`) REFERENCES `tblstatusobras` (`idStatusObras`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tblobras_ibfk_9` FOREIGN KEY (`idTecnica`) REFERENCES `tbltecnicaobras` (`idTecnica`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tblobras
-- ----------------------------

-- ----------------------------
-- Table structure for tblparceiros
-- ----------------------------
DROP TABLE IF EXISTS `tblparceiros`;
CREATE TABLE `tblparceiros` (
  `idParceiro` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(70) NOT NULL,
  `cidade` varchar(45) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `urlSite` varchar(200) NOT NULL,
  `email` varchar(45) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `idCategoriaParceiro` int(11) NOT NULL,
  `imgLogo` varchar(200) DEFAULT NULL,
  `imgProduto1` varchar(200) DEFAULT NULL,
  `imgProduto2` varchar(200) DEFAULT NULL,
  `imgProduto3` varchar(200) DEFAULT NULL,
  `imgProduto4` varchar(200) DEFAULT NULL,
  `imgProduto5` varchar(200) DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idParceiro`),
  KEY `fkParceirosxCategoriaParceiros_idx` (`idCategoriaParceiro`) USING BTREE,
  CONSTRAINT `tblparceiros_ibfk_1` FOREIGN KEY (`idCategoriaParceiro`) REFERENCES `tblcategoriaparceiros` (`idCategoriaParceiro`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblparceiros
-- ----------------------------

-- ----------------------------
-- Table structure for tblpgartista
-- ----------------------------
DROP TABLE IF EXISTS `tblpgartista`;
CREATE TABLE `tblpgartista` (
  `idPgArtista` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) NOT NULL,
  `slugPagina` varchar(200) NOT NULL,
  `nomeArtistico` varchar(45) NOT NULL,
  `subtituloPgPT` varchar(45) NOT NULL,
  `subtituloPgEN` varchar(45) DEFAULT NULL,
  `subtituloPgES` varchar(45) DEFAULT NULL,
  `idBiografia` int(11) NOT NULL,
  `txtMsgVisitantePT` varchar(250) NOT NULL,
  `txtMsgVisitanteEN` varchar(250) DEFAULT NULL,
  `txtMsgVisitanteES` varchar(250) DEFAULT NULL,
  `imgPerfil` varchar(200) DEFAULT NULL COMMENT 'Caminho da imagem do perfil do artista (foto)',
  `imgBannerPg` varchar(200) DEFAULT NULL COMMENT 'Caminho da imagem que representa o banner na página do artista.',
  `facebookID` varchar(45) DEFAULT NULL,
  `twitterID` varchar(45) DEFAULT NULL,
  `googlePlusID` varchar(45) DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idPgArtista`,`idUsuario`),
  KEY `fkPgArtistaxBiografia_idx` (`idBiografia`) USING BTREE,
  CONSTRAINT `tblpgartista_ibfk_1` FOREIGN KEY (`idBiografia`) REFERENCES `tblbiografiaartista` (`idBiografia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblpgartista
-- ----------------------------

-- ----------------------------
-- Table structure for tblpropriedadechave
-- ----------------------------
DROP TABLE IF EXISTS `tblpropriedadechave`;
CREATE TABLE `tblpropriedadechave` (
  `idPropriedadeChave` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idPropriedadeChave`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tblpropriedadechave
-- ----------------------------
INSERT INTO `tblpropriedadechave` VALUES ('1', 'Número de obras publicadas', 'Qtd. de obras que o artista pode incluir.', '2014-11-24 15:48:29', '2014-11-24 15:48:33', '1');
INSERT INTO `tblpropriedadechave` VALUES ('2', 'Espaço para biografia', 'Espaço para biografia', '2014-11-24 15:49:04', '2014-11-24 15:49:07', '1');
INSERT INTO `tblpropriedadechave` VALUES ('3', 'Espaço para textos críticos', 'Espaço para textos críticos', '2014-11-24 15:49:34', '2014-11-24 15:49:04', '0');
INSERT INTO `tblpropriedadechave` VALUES ('4', 'Subdominio', 'Subdomínio (Ex.: nome.mercadoarte.com.br)', '2014-11-24 15:50:24', '2014-11-24 15:50:27', '1');
INSERT INTO `tblpropriedadechave` VALUES ('5', 'Cartão de visita', 'Arte para cartão de visita personalizado', '2014-11-24 15:50:57', '2014-11-24 15:51:00', '1');
INSERT INTO `tblpropriedadechave` VALUES ('6', 'Destaque Pg Inicial', 'Obras em destaque na página inicial', '2014-11-24 15:51:57', '2014-11-24 15:52:00', '1');
INSERT INTO `tblpropriedadechave` VALUES ('7', 'E-mail personalizado', 'Alias para e-mail personalizado (Ex.: nome@mercadoarte.com.br)', '2014-11-24 15:52:54', '2014-11-24 15:52:58', '1');
INSERT INTO `tblpropriedadechave` VALUES ('8', 'Exposições coletivas', 'Participação em exposições coletivas do Mercado Arte (*sujeito a taxa de inscrição)', '2014-11-24 15:54:14', '2014-11-24 15:54:17', '1');
INSERT INTO `tblpropriedadechave` VALUES ('9', 'Eventuais publicidades', 'Participação em eventuais publicidades em veículos de terceiros (*sujeito a taxa de inscrição)', '2014-11-24 15:55:02', '2014-11-24 15:55:05', '1');

-- ----------------------------
-- Table structure for tblpropriedadeplano
-- ----------------------------
DROP TABLE IF EXISTS `tblpropriedadeplano`;
CREATE TABLE `tblpropriedadeplano` (
  `idPropPlano` int(11) NOT NULL AUTO_INCREMENT,
  `grupoPlano` int(11) NOT NULL,
  `idPropriedadeChave` int(11) NOT NULL COMMENT 'ID da propriedade do plano, como:\n- Número de obras permitidas\n- Espaço para biografia\n- Espaço para texto crítico\n- Sub-dominio',
  `idValorChave` varchar(100) NOT NULL COMMENT 'Valor para as propriedades de acordo com o plano, por exemplo:\n\nPlano: Bronze\nPropriedade: Número de obras permitidas\nVALOR: 10',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idPropPlano`),
  KEY `FKPropriedadePlanoxTipoPlano` (`grupoPlano`) USING BTREE,
  KEY `idPropriedadeChave` (`idPropriedadeChave`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblpropriedadeplano
-- ----------------------------
INSERT INTO `tblpropriedadeplano` VALUES ('3', '0', '1', '10', '2014-11-24 15:56:58', '2014-11-24 15:57:02', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('4', '0', '2', '1', '2014-11-24 15:58:21', '2014-11-24 15:58:21', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('5', '0', '3', '0', '2014-11-24 15:58:21', '2014-11-24 15:58:21', '0');
INSERT INTO `tblpropriedadeplano` VALUES ('6', '0', '4', '0', '2014-11-24 15:58:21', '2014-11-24 15:58:21', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('7', '0', '5', '0', '2014-11-24 15:58:21', '2014-11-24 15:58:21', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('8', '0', '6', '0', '2014-11-24 15:58:21', '2014-11-24 15:58:21', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('9', '0', '7', '0', '2014-11-24 15:58:21', '2014-11-24 15:58:21', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('10', '0', '8', 'Não', '2014-11-24 15:58:21', '2014-11-24 15:58:21', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('11', '0', '9', 'Não', '2014-11-24 15:58:21', '2014-11-24 15:58:21', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('12', '1', '1', '50', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('13', '1', '2', '1', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('14', '1', '3', '0', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '0');
INSERT INTO `tblpropriedadeplano` VALUES ('15', '1', '4', '0', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('16', '1', '5', '1', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('17', '1', '6', '0', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('18', '1', '7', '0', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('19', '1', '8', 'Sim*', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('20', '1', '9', 'Sim*', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('21', '2', '1', '9999', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('22', '2', '2', '1', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('23', '2', '3', '0', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '0');
INSERT INTO `tblpropriedadeplano` VALUES ('24', '2', '4', '1', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('25', '2', '5', '1', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('26', '2', '6', '1', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('27', '2', '7', '1', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('28', '2', '8', 'Sim**', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');
INSERT INTO `tblpropriedadeplano` VALUES ('29', '2', '9', 'Sim**', '2014-11-24 16:03:37', '2014-11-24 16:03:37', '1');

-- ----------------------------
-- Table structure for tblstatuscontrato
-- ----------------------------
DROP TABLE IF EXISTS `tblstatuscontrato`;
CREATE TABLE `tblstatuscontrato` (
  `idStatusContrato` int(11) NOT NULL AUTO_INCREMENT,
  `nomeStatusContrato` varchar(45) NOT NULL COMMENT 'Status do contrato (Aguardando pagamento, Ativo,  Inativo, etc.)',
  `descricaoStatusContrato` varchar(45) DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idStatusContrato`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblstatuscontrato
-- ----------------------------
INSERT INTO `tblstatuscontrato` VALUES ('1', 'Aguardando Pagamento', 'Aguardando Pagamento', '2015-01-05 14:46:20', '2015-01-05 14:46:22', '1');
INSERT INTO `tblstatuscontrato` VALUES ('2', 'Ativo', 'Ativo', '2015-01-05 14:46:36', '2015-01-05 14:46:39', '1');
INSERT INTO `tblstatuscontrato` VALUES ('3', 'Inativo', 'Inativo', '2015-01-05 14:46:59', '2015-01-05 14:47:01', '1');
INSERT INTO `tblstatuscontrato` VALUES ('4', 'Banido', 'Banido', '2015-01-05 14:48:00', '2015-01-05 14:48:02', '1');

-- ----------------------------
-- Table structure for tblstatusobras
-- ----------------------------
DROP TABLE IF EXISTS `tblstatusobras`;
CREATE TABLE `tblstatusobras` (
  `idStatusObras` int(11) NOT NULL AUTO_INCREMENT,
  `idTipoObra` int(11) NOT NULL,
  `nomeStatusObrasPT` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `nomeStatusObrasEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomeStatusObrasES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoStatusObrasPT` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoStatusObrasEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoStatusObrasES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idStatusObras`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tblstatusobras
-- ----------------------------
INSERT INTO `tblstatusobras` VALUES ('1', '1', 'À venda', null, null, 'À venda', null, null, '2015-01-05 14:37:35', '2015-01-05 14:37:39', '1');
INSERT INTO `tblstatusobras` VALUES ('2', '1', 'Vendido', null, null, 'Vendido', null, null, '2015-01-05 14:38:57', '2015-01-05 14:39:01', '1');
INSERT INTO `tblstatusobras` VALUES ('3', '1', 'Consulte', null, null, 'Consulte', null, null, '2015-01-05 14:39:50', '2015-01-05 14:39:53', '1');
INSERT INTO `tblstatusobras` VALUES ('4', '2', 'À venda', null, null, 'À venda', null, null, '2015-01-05 14:40:31', '2015-01-05 14:40:35', '1');
INSERT INTO `tblstatusobras` VALUES ('5', '2', 'Vendido', null, null, 'Vendido', null, null, '2015-01-05 14:40:59', '2015-01-05 14:41:02', '1');
INSERT INTO `tblstatusobras` VALUES ('6', '2', 'Consulte', null, null, 'Consulte', null, null, '2015-01-05 14:41:16', '2015-01-05 14:41:19', '1');
INSERT INTO `tblstatusobras` VALUES ('7', '3', 'À venda', null, null, 'À venda', null, null, '2015-01-05 14:41:36', '2015-01-05 14:41:39', '1');
INSERT INTO `tblstatusobras` VALUES ('8', '3', 'Vendido', null, null, 'Vendido', null, null, '2015-01-05 14:41:53', '2015-01-05 14:41:56', '1');
INSERT INTO `tblstatusobras` VALUES ('9', '3', 'Consulte', null, null, 'Consulte', null, null, '2015-01-05 14:42:10', '2015-01-05 14:42:16', '1');

-- ----------------------------
-- Table structure for tbltecnicaobras
-- ----------------------------
DROP TABLE IF EXISTS `tbltecnicaobras`;
CREATE TABLE `tbltecnicaobras` (
  `idTecnica` int(11) NOT NULL AUTO_INCREMENT,
  `idTipoObra` int(11) NOT NULL,
  `nomeTecnicaPT` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Óleo sobre tela, Acrílica',
  `nomeTecnicaEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomeTecnicaES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoTecnicaPT` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoTecnicaEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoTecnicaES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idTecnica`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbltecnicaobras
-- ----------------------------
INSERT INTO `tbltecnicaobras` VALUES ('1', '1', 'Óleo', null, null, 'Óleo sobre tela', null, null, '2014-12-19 16:53:51', '2014-12-19 16:53:51', '1');
INSERT INTO `tbltecnicaobras` VALUES ('2', '1', 'Acrílica', null, null, 'Acrílico sobre tela', null, null, '2014-12-19 16:57:38', '2014-12-19 16:57:41', '1');
INSERT INTO `tbltecnicaobras` VALUES ('3', '1', 'Colagem', null, null, 'Colagem', null, null, '2014-12-19 16:58:17', '2014-12-19 16:58:20', '1');
INSERT INTO `tbltecnicaobras` VALUES ('4', '1', 'Guache', null, null, 'Guache', null, null, '2014-12-19 16:58:56', '2014-12-19 16:58:59', '1');
INSERT INTO `tbltecnicaobras` VALUES ('5', '1', 'Pastel', null, null, 'Pastel (pintura)', null, null, '2014-12-19 16:59:31', '2014-12-19 16:59:34', '1');
INSERT INTO `tbltecnicaobras` VALUES ('6', '1', 'Sanguínea', null, null, 'Sanguínea', null, null, '2014-12-19 16:59:59', '2014-12-19 17:00:02', '1');
INSERT INTO `tbltecnicaobras` VALUES ('7', '1', 'Têmpera', null, null, 'Têmpera (pintura)', null, null, '2014-12-19 17:00:30', '2014-12-19 17:00:34', '1');
INSERT INTO `tbltecnicaobras` VALUES ('8', '1', 'Nanquim', null, null, 'Nanquim', null, null, '2014-12-19 17:01:58', '2014-12-19 17:02:01', '1');
INSERT INTO `tbltecnicaobras` VALUES ('9', '1', 'Técnica Mista', null, null, 'Técnica Mista', null, null, '2014-12-19 17:02:44', '2014-12-19 17:02:48', '1');
INSERT INTO `tbltecnicaobras` VALUES ('10', '1', 'Grafite', null, null, 'Grafite', null, null, '2014-12-26 12:32:25', '2014-12-26 12:32:29', '1');
INSERT INTO `tbltecnicaobras` VALUES ('11', '1', 'Desenho', null, null, 'Desenho', null, null, '2014-12-26 12:33:35', '2014-12-26 12:33:38', '1');
INSERT INTO `tbltecnicaobras` VALUES ('12', '1', 'Xilogravura', null, null, 'Xilogravura', null, null, '2014-12-26 12:35:22', '2014-12-26 12:35:25', '1');
INSERT INTO `tbltecnicaobras` VALUES ('13', '1', 'Litogravura', null, null, 'Litogravura', null, null, '2014-12-26 12:38:23', '2014-12-26 12:38:26', '1');
INSERT INTO `tbltecnicaobras` VALUES ('14', '1', 'Gravura', null, null, 'Gravura', null, null, '2014-12-26 12:38:42', '2014-12-26 12:38:45', '1');
INSERT INTO `tbltecnicaobras` VALUES ('15', '1', 'Serigrafia', null, null, 'Serigrafia', null, null, '2014-12-26 12:39:15', '2014-12-26 12:39:19', '1');
INSERT INTO `tbltecnicaobras` VALUES ('16', '1', 'Monotipia', null, null, 'Monotipia', null, null, '2014-12-26 12:39:37', '2014-12-26 12:39:40', '1');
INSERT INTO `tbltecnicaobras` VALUES ('17', '1', 'Arte Digital', null, null, 'Arte Digital', null, null, '2014-12-26 12:41:59', '2014-12-26 12:42:02', '1');
INSERT INTO `tbltecnicaobras` VALUES ('18', '2', 'Mármore', null, null, 'Mármore', null, null, '2014-12-26 12:44:03', '2014-12-26 12:44:07', '1');
INSERT INTO `tbltecnicaobras` VALUES ('19', '2', 'Pedra Calcária', null, null, 'Pedra Calcária', null, null, '2014-12-26 12:44:33', '2014-12-26 12:44:36', '1');
INSERT INTO `tbltecnicaobras` VALUES ('20', '2', 'Granito', null, null, 'Granito', null, null, '2014-12-26 12:44:58', '2014-12-26 12:45:02', '1');
INSERT INTO `tbltecnicaobras` VALUES ('21', '2', 'Bronze', null, null, 'Bronze', null, null, '2014-12-26 12:45:22', '2014-12-26 12:45:29', '1');
INSERT INTO `tbltecnicaobras` VALUES ('22', '2', 'Ouro', null, null, 'Ouro', null, null, '2014-12-26 12:45:44', '2014-12-26 12:45:47', '1');
INSERT INTO `tbltecnicaobras` VALUES ('23', '2', 'Prata', null, null, 'Prata', null, null, '2014-12-26 12:46:01', '2014-12-26 12:46:04', '1');
INSERT INTO `tbltecnicaobras` VALUES ('24', '2', 'Argila', null, null, 'Argila', null, null, '2014-12-26 12:47:10', '2014-12-26 12:47:13', '1');
INSERT INTO `tbltecnicaobras` VALUES ('25', '2', 'Terracota', null, null, 'Terracota', null, null, '2014-12-26 13:01:35', '2014-12-26 13:01:38', '1');
INSERT INTO `tbltecnicaobras` VALUES ('26', '2', 'Ébano', null, null, 'Ébano', null, null, '2015-01-05 14:11:50', '2015-01-05 14:11:55', '0');
INSERT INTO `tbltecnicaobras` VALUES ('27', '2', 'Madeira', null, null, 'Madeira', null, null, '2015-01-05 14:12:12', '2015-01-05 14:12:16', '1');
INSERT INTO `tbltecnicaobras` VALUES ('28', '2', 'Marfim', null, null, 'Marfim', null, null, '2015-01-05 14:13:10', '2015-01-05 14:13:13', '1');
INSERT INTO `tbltecnicaobras` VALUES ('29', '2', 'Âmbar', null, null, 'Âmbar', null, null, '2015-01-05 14:13:42', '2015-01-05 14:13:45', '0');
INSERT INTO `tbltecnicaobras` VALUES ('30', '2', 'Outras pedras', null, null, 'Outras pedras', null, null, '2015-01-05 14:14:09', '2015-01-05 14:14:12', '1');
INSERT INTO `tbltecnicaobras` VALUES ('31', '2', 'Outros metais', null, null, 'Outros metais', null, null, '2015-01-05 14:14:30', '2015-01-05 14:14:34', '1');
INSERT INTO `tbltecnicaobras` VALUES ('33', '2', 'Outros materiais', null, null, 'Outros materiais', null, null, '2015-01-05 14:20:12', '2015-01-05 14:20:16', '1');
INSERT INTO `tbltecnicaobras` VALUES ('34', '3', 'Impressão Fine Art', null, null, 'Impressão Fine Art', null, null, '2015-01-05 14:23:41', '2015-01-05 14:23:45', '1');
INSERT INTO `tbltecnicaobras` VALUES ('35', '3', 'Digital', null, null, 'Digital', null, null, '2015-01-05 14:24:02', '2015-01-05 14:24:06', '1');
INSERT INTO `tbltecnicaobras` VALUES ('36', '1', 'Aquarela', null, null, 'Aquarela', null, null, '2015-01-06 11:02:16', '2015-01-06 11:02:30', '1');

-- ----------------------------
-- Table structure for tbltipoobras
-- ----------------------------
DROP TABLE IF EXISTS `tbltipoobras`;
CREATE TABLE `tbltipoobras` (
  `idTipoObra` int(11) NOT NULL AUTO_INCREMENT,
  `nomeTipoObra` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Pintura, Escultura ou Fotografia',
  `descricaoTipoObra` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idTipoObra`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbltipoobras
-- ----------------------------
INSERT INTO `tbltipoobras` VALUES ('1', 'Pintura', 'Pintura', '2014-12-19 16:49:36', '2014-12-19 16:49:36', '1');
INSERT INTO `tbltipoobras` VALUES ('2', 'Escultura', 'Escultura', '2014-12-19 16:49:36', '2014-12-19 16:49:36', '1');
INSERT INTO `tbltipoobras` VALUES ('3', 'Fotografia', 'Fotografia', '2014-12-19 16:49:36', '2014-12-19 16:49:36', '1');

-- ----------------------------
-- Table structure for tbltipopagamento
-- ----------------------------
DROP TABLE IF EXISTS `tbltipopagamento`;
CREATE TABLE `tbltipopagamento` (
  `idTipoPagamento` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idTipoPagamento`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbltipopagamento
-- ----------------------------
INSERT INTO `tbltipopagamento` VALUES ('1', 'PagSeguro', 'PagSeguro', '2014-12-17 14:34:05', '2015-01-12 13:00:07', '1');
INSERT INTO `tbltipopagamento` VALUES ('2', 'PayPal', 'Paypal', '2014-12-17 14:33:41', '2015-01-12 13:00:07', '1');
INSERT INTO `tbltipopagamento` VALUES ('3', 'Boleto', 'Boleto Bancário', '2014-12-17 14:34:54', '2015-01-12 13:00:07', '1');
INSERT INTO `tbltipopagamento` VALUES ('4', 'Depósito em CC', 'Depósito em Conta Corrente', '2014-12-17 14:35:21', '2015-01-12 13:00:07', '1');
INSERT INTO `tbltipopagamento` VALUES ('5', 'Gratis', 'Promoção de plano Gratis', '2015-01-14 23:20:33', '0000-00-00 00:00:00', '1');

-- ----------------------------
-- Table structure for tbltipoplano
-- ----------------------------
DROP TABLE IF EXISTS `tbltipoplano`;
CREATE TABLE `tbltipoplano` (
  `idTipoPlano` int(11) NOT NULL AUTO_INCREMENT,
  `grupoPlano` int(11) NOT NULL,
  `idFuncao` int(11) NOT NULL COMMENT 'Plano destinado a função. (1 = Artista, 2 = Parceiro, 3 = TI, 4 = Comercial, 5 = Financeiro, 6 = RH, 7 = Marketing, etc.)',
  `planoPT` varchar(45) NOT NULL COMMENT 'Nome do plano: \nBronze - Trimestral, Bronze - Semestral, Bronze - Anual',
  `planoEN` varchar(45) DEFAULT NULL,
  `planoES` varchar(45) DEFAULT NULL,
  `periodicidade` int(11) NOT NULL COMMENT 'Periodicidades dos planos: \n3, 6 ou 12 meses.',
  `vlPlano` varchar(45) NOT NULL COMMENT 'Valor do plano em reais (sem desconto).',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  KEY `fkTipoPlanoxUsuariosFuncao_idx` (`idFuncao`) USING BTREE,
  KEY `fkTipoPlanoxPropriedadePlano` (`idTipoPlano`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbltipoplano
-- ----------------------------
INSERT INTO `tbltipoplano` VALUES ('2', '0', '2', 'Bronze', 'Bronze', 'Bronce', '90', '0.00', '2014-11-24 15:16:23', '2014-11-24 15:16:27', '1');
INSERT INTO `tbltipoplano` VALUES ('3', '0', '2', 'Bronze', 'Bronze', 'Bronce', '180', '0.00', '2014-11-24 15:21:09', '2014-11-24 15:21:12', '1');
INSERT INTO `tbltipoplano` VALUES ('4', '0', '2', 'Bronze', 'Bronze', 'Bronce', '360', '660,00', '2014-11-24 15:21:39', '2014-11-24 15:21:43', '1');
INSERT INTO `tbltipoplano` VALUES ('5', '1', '2', 'Prata', 'Silver', 'Plata', '180', '525,00', '2014-11-24 15:22:42', '2014-11-24 15:22:47', '1');
INSERT INTO `tbltipoplano` VALUES ('6', '1', '2', 'Prata', 'Silver', 'Plata', '360', '1.050,00', '2014-11-24 15:23:14', '2014-11-24 15:23:18', '1');
INSERT INTO `tbltipoplano` VALUES ('7', '2', '2', 'Ouro', 'Gold', 'Ouro', '180', '720,00', '2014-11-24 15:24:33', '2014-11-24 15:24:36', '1');
INSERT INTO `tbltipoplano` VALUES ('8', '2', '2', 'Ouro', 'Gold', 'Ouro', '360', '1.440,00', '2014-11-24 15:24:58', '2014-11-24 15:25:01', '1');
INSERT INTO `tbltipoplano` VALUES ('9', '1', '2', 'Prata', 'Silver', 'Plata', '90', '262,50', '2014-11-24 15:25:48', '2014-11-24 15:25:51', '1');
INSERT INTO `tbltipoplano` VALUES ('10', '2', '2', 'Ouro', 'Gold', 'Ouro', '90', '360,00', '2014-11-24 15:26:33', '2014-11-24 15:26:36', '1');

-- ----------------------------
-- Table structure for tbltransacoes
-- ----------------------------
DROP TABLE IF EXISTS `tbltransacoes`;
CREATE TABLE `tbltransacoes` (
  `idTransacao` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Número da transação',
  `idContrato` int(11) NOT NULL COMMENT 'Número do contrato',
  `codCupom` varchar(8) DEFAULT NULL,
  `idTipoPagamento` int(11) NOT NULL COMMENT 'Forma de pagamento escolhida pelo cliente (1 = Deposito em conta, 2 = PagSeguro, 3 = Paypal, 4 = Boleto, etc..)',
  `valorTotal` decimal(15,2) NOT NULL COMMENT 'Valor total do plano escolhido (sem aplicação do cupom de desconto).',
  `valorAPagar` decimal(15,2) NOT NULL COMMENT 'Valor total a pagar pelo gerador da transação (com cupom de desconto aplicado).',
  `txtidentPagamento` varchar(255) DEFAULT NULL COMMENT 'Explicação de como foi feito a identificação do pagamento, algo semelhante a:\n"Pagamento reconhecido por Alcides, no dia 09/09 às 16h28 através de consulta ao sistema do PagSeguro."',
  `valorPago` decimal(15,2) DEFAULT NULL COMMENT 'Valor pago pelo gerador da transação.',
  `dtVencimentoTransacao` datetime NOT NULL,
  `dtPagamentoTransacao` datetime NOT NULL,
  `observacao` varchar(255) DEFAULT NULL COMMENT 'Observação geral.',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` int(11) NOT NULL,
  PRIMARY KEY (`idTransacao`),
  KEY `fkTransacoesxContratos_idx` (`idContrato`) USING BTREE,
  KEY `fkTransacoesxTipoPagamento_idx` (`idTipoPagamento`) USING BTREE,
  CONSTRAINT `tbltransacoes_ibfk_1` FOREIGN KEY (`idContrato`) REFERENCES `tblcontratos` (`idContrato`) ON UPDATE NO ACTION,
  CONSTRAINT `tbltransacoes_ibfk_2` FOREIGN KEY (`idTipoPagamento`) REFERENCES `tbltipopagamento` (`idTipoPagamento`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbltransacoes
-- ----------------------------

-- ----------------------------
-- Table structure for tblusuariopermissoes
-- ----------------------------
DROP TABLE IF EXISTS `tblusuariopermissoes`;
CREATE TABLE `tblusuariopermissoes` (
  `idUsuarioPermissao` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) NOT NULL,
  `acessaMeuPerfil` int(1) NOT NULL DEFAULT '0',
  `acessaMeuCadastro` int(1) NOT NULL DEFAULT '0',
  `acessaObras` int(1) NOT NULL DEFAULT '0',
  `acessaMeuPlano` int(1) NOT NULL DEFAULT '0',
  `acessaCadastros` int(1) NOT NULL DEFAULT '0',
  `acessaFinanceiro` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idUsuarioPermissao`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tblusuariopermissoes
-- ----------------------------
INSERT INTO `tblusuariopermissoes` VALUES ('1', '38', '0', '0', '0', '0', '1', '1');
INSERT INTO `tblusuariopermissoes` VALUES ('3', '10', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('4', '11', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('5', '12', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('6', '13', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('7', '14', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('8', '15', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('9', '16', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('10', '17', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('11', '18', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('12', '19', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('13', '20', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('14', '21', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('15', '22', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('16', '23', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('17', '24', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('18', '25', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('19', '26', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('20', '27', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('21', '28', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('22', '29', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('23', '30', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('24', '31', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('25', '32', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('26', '33', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('27', '34', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('28', '35', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('29', '36', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('30', '37', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('31', '42', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('32', '43', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('33', '44', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('34', '45', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('35', '46', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('36', '47', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('37', '48', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('38', '49', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('39', '50', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('40', '51', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('41', '52', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('42', '53', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('43', '54', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('44', '55', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('45', '56', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('46', '57', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('47', '58', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('48', '59', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('49', '60', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('50', '61', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('51', '62', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('52', '63', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('53', '64', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('54', '65', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('55', '66', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('56', '67', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('57', '68', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('58', '69', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('59', '70', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('60', '71', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('61', '72', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('62', '73', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('63', '74', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('64', '75', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('65', '76', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('66', '77', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('67', '78', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('68', '79', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('69', '80', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('70', '81', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('71', '82', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('72', '83', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('73', '84', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('74', '85', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('75', '86', '1', '1', '1', '1', '0', '0');
INSERT INTO `tblusuariopermissoes` VALUES ('76', '87', '1', '1', '1', '1', '0', '0');

-- ----------------------------
-- Table structure for tblusuarios
-- ----------------------------
DROP TABLE IF EXISTS `tblusuarios`;
CREATE TABLE `tblusuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `idPaisResidente` int(11) NOT NULL COMMENT 'Código do país residente (1 = Brasil, 2 = Portugal, etc.)',
  `telefoneCelular` varchar(20) DEFAULT NULL,
  `telefoneResidencial` varchar(20) DEFAULT NULL,
  `idFuncao` int(11) NOT NULL COMMENT 'Função do usuário no site (1 = Artista, 2 = Parceiro, 3 = TI, 4 = Comercial, 5 = Financeiro, 6 = RH, 7 = Marketing, etc.)',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `email_UNIQUE` (`email`) USING BTREE,
  KEY `fkUsuariosFuncao_idx` (`idFuncao`) USING BTREE,
  KEY `fkGeralPais_idx` (`idPaisResidente`) USING BTREE,
  CONSTRAINT `tblusuarios_ibfk_1` FOREIGN KEY (`idPaisResidente`) REFERENCES `tblgeralpais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tblusuarios_ibfk_2` FOREIGN KEY (`idFuncao`) REFERENCES `tblusuariosfuncao` (`idFuncao`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblusuarios
-- ----------------------------

-- ----------------------------
-- Table structure for tblusuariosdados
-- ----------------------------
DROP TABLE IF EXISTS `tblusuariosdados`;
CREATE TABLE `tblusuariosdados` (
  `idUsuario` int(11) NOT NULL,
  `nomeCompleto` varchar(70) NOT NULL,
  `CPF` varchar(15) DEFAULT NULL COMMENT 'Somente números na coluna CPF',
  `RG` varchar(15) DEFAULT NULL,
  `RNE` varchar(15) DEFAULT NULL,
  `sexo` char(1) NOT NULL,
  `telefoneComercial` varchar(20) DEFAULT NULL,
  `enderecoCEP` varchar(9) DEFAULT NULL,
  `enderecologradouro` varchar(60) NOT NULL,
  `enderecoNumero` varchar(12) NOT NULL,
  `enderecoComplemento` varchar(45) DEFAULT NULL,
  `enderecoBairro` varchar(60) DEFAULT NULL,
  `cidade` varchar(45) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `publicacoesEmail` tinyint(4) NOT NULL COMMENT 'Deseja receber publicações por e-mail? (0=Não/1=Sim)',
  `notificacaoAlertaSMS` tinyint(4) NOT NULL COMMENT 'Receber notificação e alerta por SMS?  (0=Não/1=Sim)',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  KEY `fkUsuariosDados_idx` (`idUsuario`) USING BTREE,
  CONSTRAINT `tblusuariosdados_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `tblusuarios` (`idUsuario`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblusuariosdados
-- ----------------------------

-- ----------------------------
-- Table structure for tblusuariosfuncao
-- ----------------------------
DROP TABLE IF EXISTS `tblusuariosfuncao`;
CREATE TABLE `tblusuariosfuncao` (
  `idFuncao` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idFuncao`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tblusuariosfuncao
-- ----------------------------
INSERT INTO `tblusuariosfuncao` VALUES ('2', 'Artista', 'Pintores, escultores e fotógrafos.', '2014-11-24 14:59:35', '2014-11-24 14:59:41', '1');
INSERT INTO `tblusuariosfuncao` VALUES ('3', 'Parceiro', 'Galeria, Museu, etc.', '2014-11-24 15:00:07', '2014-11-24 15:00:13', '1');
INSERT INTO `tblusuariosfuncao` VALUES ('4', 'TI', 'Depart. Tecnologia da Informação', '2014-11-24 15:00:44', '2014-11-24 15:00:47', '1');
INSERT INTO `tblusuariosfuncao` VALUES ('5', 'Comercial', 'Depart. comercial', '2014-11-24 15:01:10', '2014-11-24 15:01:13', '1');
INSERT INTO `tblusuariosfuncao` VALUES ('6', ' Financeiro', 'Depart. Financeiro', '2014-11-24 15:01:31', '2014-11-24 15:01:34', '1');
INSERT INTO `tblusuariosfuncao` VALUES ('7', 'RH', 'Depart. Recursos Humanos', '2014-11-24 15:02:01', '2014-11-24 15:02:05', '1');
INSERT INTO `tblusuariosfuncao` VALUES ('8', 'Marketing', 'Depart. Marketing', '2014-11-24 15:02:24', '2014-11-24 15:02:38', '1');

-- ----------------------------
-- Table structure for tblvotos
-- ----------------------------
DROP TABLE IF EXISTS `tblvotos`;
CREATE TABLE `tblvotos` (
  `idVoto` int(11) NOT NULL AUTO_INCREMENT,
  `idObra` int(11) NOT NULL,
  `userIp` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `dataCriacao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idVoto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tblvotos
-- ----------------------------
