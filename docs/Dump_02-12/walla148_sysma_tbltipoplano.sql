CREATE DATABASE  IF NOT EXISTS `walla148_sysma` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `walla148_sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: wallaceerick.com.br    Database: walla148_sysma
-- ------------------------------------------------------
-- Server version	5.5.38-35.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbltipoplano`
--

DROP TABLE IF EXISTS `tbltipoplano`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltipoplano` (
  `idTipoPlano` int(11) NOT NULL AUTO_INCREMENT,
  `grupoPlano` int(11) NOT NULL,
  `idFuncao` int(11) NOT NULL COMMENT 'Plano destinado a função. (1 = Artista, 2 = Parceiro, 3 = TI, 4 = Comercial, 5 = Financeiro, 6 = RH, 7 = Marketing, etc.)',
  `planoPT` varchar(45) NOT NULL COMMENT 'Nome do plano: \nBronze - Trimestral, Bronze - Semestral, Bronze - Anual',
  `planoEN` varchar(45) DEFAULT NULL,
  `planoES` varchar(45) DEFAULT NULL,
  `periodicidade` int(11) NOT NULL COMMENT 'Periodicidades dos planos: \n3, 6 ou 12 meses.',
  `vlPlano` varchar(45) NOT NULL COMMENT 'Valor do plano em reais (sem desconto).',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  KEY `fkTipoPlanoxUsuariosFuncao_idx` (`idFuncao`),
  KEY `fkTipoPlanoxPropriedadePlano` (`idTipoPlano`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltipoplano`
--

LOCK TABLES `tbltipoplano` WRITE;
/*!40000 ALTER TABLE `tbltipoplano` DISABLE KEYS */;
INSERT INTO `tbltipoplano` VALUES (2,0,2,'Bronze','Bronze','Bronce',90,'165,00','2014-11-24 15:16:23','2014-11-24 15:16:27',0),(3,0,2,'Bronze','Bronze','Bronce',180,'330,00','2014-11-24 15:21:09','2014-11-24 15:21:12',1),(4,0,2,'Bronze','Bronze','Bronce',360,'660,00','2014-11-24 15:21:39','2014-11-24 15:21:43',1),(5,1,2,'Prata','Silver','Plata',180,'525,00','2014-11-24 15:22:42','2014-11-24 15:22:47',1),(6,1,2,'Prata','Silver','Plata',360,'1.050,00','2014-11-24 15:23:14','2014-11-24 15:23:18',1),(7,2,2,'Ouro','Gold','Ouro',180,'720,00','2014-11-24 15:24:33','2014-11-24 15:24:36',1),(8,2,2,'Ouro','Gold','Ouro',360,'1.440,00','2014-11-24 15:24:58','2014-11-24 15:25:01',1),(9,1,2,'Prata','Silver','Plata',90,'262,50','2014-11-24 15:25:48','2014-11-24 15:25:51',1),(10,2,2,'Ouro','Gold','Ouro',90,'360,00','2014-11-24 15:26:33','2014-11-24 15:26:36',1);
/*!40000 ALTER TABLE `tbltipoplano` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-02 23:04:25
