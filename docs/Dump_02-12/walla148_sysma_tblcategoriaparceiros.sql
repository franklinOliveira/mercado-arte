CREATE DATABASE  IF NOT EXISTS `walla148_sysma` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `walla148_sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: wallaceerick.com.br    Database: walla148_sysma
-- ------------------------------------------------------
-- Server version	5.5.38-35.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblcategoriaparceiros`
--

DROP TABLE IF EXISTS `tblcategoriaparceiros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblcategoriaparceiros` (
  `idCategoriaParceiro` int(11) NOT NULL AUTO_INCREMENT,
  `nomeCategoriaParcPT` varchar(45) NOT NULL,
  `nomeCategoriaParcEN` varchar(45) DEFAULT NULL,
  `nomeCategoriaParcES` varchar(45) DEFAULT NULL,
  `descricaoCategoriaParcPT` varchar(45) DEFAULT NULL,
  `descricaoCategoriaParcEN` varchar(45) DEFAULT NULL,
  `descricaoCategoriaParcES` varchar(45) DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idCategoriaParceiro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblcategoriaparceiros`
--

LOCK TABLES `tblcategoriaparceiros` WRITE;
/*!40000 ALTER TABLE `tblcategoriaparceiros` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblcategoriaparceiros` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-02 23:05:56
