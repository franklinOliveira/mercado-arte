CREATE DATABASE  IF NOT EXISTS `walla148_sysma` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `walla148_sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: wallaceerick.com.br    Database: walla148_sysma
-- ------------------------------------------------------
-- Server version	5.5.38-35.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblpropriedadechave`
--

DROP TABLE IF EXISTS `tblpropriedadechave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpropriedadechave` (
  `idPropriedadeChave` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idPropriedadeChave`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpropriedadechave`
--

LOCK TABLES `tblpropriedadechave` WRITE;
/*!40000 ALTER TABLE `tblpropriedadechave` DISABLE KEYS */;
INSERT INTO `tblpropriedadechave` VALUES (1,'Número de obras publicadas','Qtd. de obras que o artista pode incluir.','2014-11-24 15:48:29','2014-11-24 15:48:33',1),(2,'Espaço para biografia','Espaço para biografia','2014-11-24 15:49:04','2014-11-24 15:49:07',1),(3,'Espaço para textos críticos','Espaço para textos críticos','2014-11-24 15:49:34','2014-11-24 15:49:04',0),(4,'Subdominio','Subdomínio (Ex.: nome.mercadoarte.com.br)','2014-11-24 15:50:24','2014-11-24 15:50:27',1),(5,'Cartão de visita','Arte para cartão de visita personalizado','2014-11-24 15:50:57','2014-11-24 15:51:00',1),(6,'Destaque Pg Inicial','Obras em destaque na página inicial','2014-11-24 15:51:57','2014-11-24 15:52:00',1),(7,'E-mail personalizado','Alias para e-mail personalizado (Ex.: nome@mercadoarte.com.br)','2014-11-24 15:52:54','2014-11-24 15:52:58',1),(8,'Exposições coletivas','Participação em exposições coletivas do Mercado Arte (*sujeito a taxa de inscrição)','2014-11-24 15:54:14','2014-11-24 15:54:17',1),(9,'Eventuais publicidades','Participação em eventuais publicidades em veículos de terceiros (*sujeito a taxa de inscrição)','2014-11-24 15:55:02','2014-11-24 15:55:05',1);
/*!40000 ALTER TABLE `tblpropriedadechave` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-02 23:04:45
