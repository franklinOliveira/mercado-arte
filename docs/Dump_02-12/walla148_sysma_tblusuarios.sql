CREATE DATABASE  IF NOT EXISTS `walla148_sysma` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `walla148_sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: wallaceerick.com.br    Database: walla148_sysma
-- ------------------------------------------------------
-- Server version	5.5.38-35.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblusuarios`
--

DROP TABLE IF EXISTS `tblusuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblusuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `idPaisResidente` int(11) NOT NULL COMMENT 'Código do país residente (1 = Brasil, 2 = Portugal, etc.)',
  `telefoneCelular` varchar(20) DEFAULT NULL,
  `telefoneResidencial` varchar(20) DEFAULT NULL,
  `idFuncao` int(11) NOT NULL COMMENT 'Função do usuário no site (1 = Artista, 2 = Parceiro, 3 = TI, 4 = Comercial, 5 = Financeiro, 6 = RH, 7 = Marketing, etc.)',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fkUsuariosFuncao_idx` (`idFuncao`),
  KEY `fkGeralPais_idx` (`idPaisResidente`),
  CONSTRAINT `fkUsuariosxGeralPais` FOREIGN KEY (`idPaisResidente`) REFERENCES `tblgeralpais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkUsuariosxUsuariosFuncao` FOREIGN KEY (`idFuncao`) REFERENCES `tblusuariosfuncao` (`idFuncao`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusuarios`
--

LOCK TABLES `tblusuarios` WRITE;
/*!40000 ALTER TABLE `tblusuarios` DISABLE KEYS */;
INSERT INTO `tblusuarios` VALUES (10,'alcidesrib_j@yahoo.com.br','12345678',76,'(11) 9 8201-7998','(11) 3213-7389',2,'2014-11-24 18:11:29','2014-11-24 18:11:34',1),(11,'administrador@mercadoarte.com.br','87654321',76,'(11) 2 2222-2222','(11) 1111-1111',4,'2014-11-24 18:13:16','2014-11-24 18:13:19',1),(12,'estrangeiro@hotmail.com','00011111',20,'+12 2 234-56','+12 2 345-678',2,'2014-11-24 18:14:02','2014-11-24 18:14:04',1);
/*!40000 ALTER TABLE `tblusuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-02 23:07:07
