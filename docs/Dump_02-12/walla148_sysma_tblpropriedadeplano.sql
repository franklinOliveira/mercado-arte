CREATE DATABASE  IF NOT EXISTS `walla148_sysma` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `walla148_sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: wallaceerick.com.br    Database: walla148_sysma
-- ------------------------------------------------------
-- Server version	5.5.38-35.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblpropriedadeplano`
--

DROP TABLE IF EXISTS `tblpropriedadeplano`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpropriedadeplano` (
  `idPropPlano` int(11) NOT NULL AUTO_INCREMENT,
  `grupoPlano` int(11) NOT NULL,
  `idPropriedadeChave` int(11) NOT NULL COMMENT 'ID da propriedade do plano, como:\n- Número de obras permitidas\n- Espaço para biografia\n- Espaço para texto crítico\n- Sub-dominio',
  `idValorChave` varchar(100) NOT NULL COMMENT 'Valor para as propriedades de acordo com o plano, por exemplo:\n\nPlano: Bronze\nPropriedade: Número de obras permitidas\nVALOR: 10',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idPropPlano`),
  KEY `FKPropriedadePlanoxTipoPlano` (`grupoPlano`),
  KEY `idPropriedadeChave` (`idPropriedadeChave`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpropriedadeplano`
--

LOCK TABLES `tblpropriedadeplano` WRITE;
/*!40000 ALTER TABLE `tblpropriedadeplano` DISABLE KEYS */;
INSERT INTO `tblpropriedadeplano` VALUES (3,0,1,'10','2014-11-24 15:56:58','2014-11-24 15:57:02',1),(4,0,2,'1','2014-11-24 15:58:21','2014-11-24 15:58:21',1),(5,0,3,'0','2014-11-24 15:58:21','2014-11-24 15:58:21',0),(6,0,4,'0','2014-11-24 15:58:21','2014-11-24 15:58:21',1),(7,0,5,'0','2014-11-24 15:58:21','2014-11-24 15:58:21',1),(8,0,6,'0','2014-11-24 15:58:21','2014-11-24 15:58:21',1),(9,0,7,'0','2014-11-24 15:58:21','2014-11-24 15:58:21',1),(10,0,8,'Não','2014-11-24 15:58:21','2014-11-24 15:58:21',1),(11,0,9,'Não','2014-11-24 15:58:21','2014-11-24 15:58:21',1),(12,1,1,'50','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(13,1,2,'1','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(14,1,3,'0','2014-11-24 16:03:37','2014-11-24 16:03:37',0),(15,1,4,'0','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(16,1,5,'1','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(17,1,6,'0','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(18,1,7,'0','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(19,1,8,'Sim*','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(20,1,9,'Sim*','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(21,2,1,'9999','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(22,2,2,'1','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(23,2,3,'0','2014-11-24 16:03:37','2014-11-24 16:03:37',0),(24,2,4,'1','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(25,2,5,'1','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(26,2,6,'1','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(27,2,7,'1','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(28,2,8,'Sim**','2014-11-24 16:03:37','2014-11-24 16:03:37',1),(29,2,9,'Sim**','2014-11-24 16:03:37','2014-11-24 16:03:37',1);
/*!40000 ALTER TABLE `tblpropriedadeplano` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-02 23:05:40
