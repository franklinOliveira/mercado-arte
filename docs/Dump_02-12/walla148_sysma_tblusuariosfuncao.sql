CREATE DATABASE  IF NOT EXISTS `walla148_sysma` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `walla148_sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: wallaceerick.com.br    Database: walla148_sysma
-- ------------------------------------------------------
-- Server version	5.5.38-35.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblusuariosfuncao`
--

DROP TABLE IF EXISTS `tblusuariosfuncao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblusuariosfuncao` (
  `idFuncao` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idFuncao`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusuariosfuncao`
--

LOCK TABLES `tblusuariosfuncao` WRITE;
/*!40000 ALTER TABLE `tblusuariosfuncao` DISABLE KEYS */;
INSERT INTO `tblusuariosfuncao` VALUES (2,'Artista','Pintores, escultores e fotógrafos.','2014-11-24 14:59:35','2014-11-24 14:59:41',1),(3,'Parceiro','Galeria, Museu, etc.','2014-11-24 15:00:07','2014-11-24 15:00:13',1),(4,'TI','Depart. Tecnologia da Informação','2014-11-24 15:00:44','2014-11-24 15:00:47',1),(5,'Comercial','Depart. comercial','2014-11-24 15:01:10','2014-11-24 15:01:13',1),(6,' Financeiro','Depart. Financeiro','2014-11-24 15:01:31','2014-11-24 15:01:34',1),(7,'RH','Depart. Recursos Humanos','2014-11-24 15:02:01','2014-11-24 15:02:05',1),(8,'Marketing','Depart. Marketing','2014-11-24 15:02:24','2014-11-24 15:02:38',1);
/*!40000 ALTER TABLE `tblusuariosfuncao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-02 23:06:30
