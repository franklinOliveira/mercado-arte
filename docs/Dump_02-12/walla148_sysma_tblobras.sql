CREATE DATABASE  IF NOT EXISTS `walla148_sysma` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `walla148_sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: wallaceerick.com.br    Database: walla148_sysma
-- ------------------------------------------------------
-- Server version	5.5.38-35.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblobras`
--

DROP TABLE IF EXISTS `tblobras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblobras` (
  `idObra` int(11) NOT NULL AUTO_INCREMENT,
  `idPgArtista` int(11) NOT NULL,
  `idTipoObra` int(11) NOT NULL COMMENT 'Tipo de obra: Obra, Escultura ou Fotografia',
  `tituloPT` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Titulo da Obra, algo semelhante há:\n\n"Mulher com crianças", "Figura abstrata", "Jogo de futebol".',
  `tituloEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tituloES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idTecnica` int(11) NOT NULL COMMENT 'Técnica da Obra (Óleo, acrílico, desenho).',
  `altura` decimal(15,2) NOT NULL COMMENT 'Altura da obra',
  `comprimento` decimal(15,2) NOT NULL COMMENT 'Comprimento da obra',
  `largura` decimal(15,2) DEFAULT NULL COMMENT 'Largura da obra',
  `ladoMaior` int(11) DEFAULT NULL COMMENT 'Esta coluna deverá ser preenchida pelo sistema no momento do insert/update das obras, ela armazenará o lado maior da obra. Por exemplo, uma Obra que foi cadastrada co',
  `dataCriacao` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Data de criação da obra',
  `descricaoPT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Descrição da obra',
  `descricaoEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricaoES` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idFormato` int(11) NOT NULL COMMENT 'Formato da obra (Horizontal, Vertical, etc.)',
  `precoObra` decimal(15,2) NOT NULL COMMENT 'Preço da Obra',
  `precoComDesconto` decimal(15,2) DEFAULT NULL COMMENT 'Preço com desconto aplicado',
  `idCorPred` int(11) NOT NULL COMMENT 'Id da cor predominante',
  `idCategoria1` int(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria2` int(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria3` int(11) DEFAULT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `imgFoto1` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Caminho da foto da Obra 1',
  `imgFoto2` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Caminho da foto da Obra 2',
  `imgFoto3` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Caminho da foto da Obra 3',
  `idStatusObra` int(11) NOT NULL COMMENT 'Status da Obra, exemplo:\n\n1 = À venda\n2 = Vendido\n',
  `NumVisitas` int(11) DEFAULT NULL,
  `somatoriaNotas` int(11) DEFAULT NULL COMMENT 'Somatória das notas atribuidas pelos visitantes .. por exempl',
  `qtdVotos` int(11) DEFAULT NULL COMMENT 'Armazena quantidade de votos.',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idObra`),
  KEY `fkObraxTecnica_idx` (`idTecnica`),
  KEY `fkObraxFormato_idx` (`idFormato`),
  KEY `fkObraxCorPredom_idx` (`idCorPred`),
  KEY `fkObraxCategoria1_idx` (`idCategoria1`),
  KEY `fkObraxCategoria2_idx` (`idCategoria2`),
  KEY `fkObraxCategoria3_idx` (`idCategoria3`),
  KEY `fkObraxStatusObra_idx` (`idStatusObra`),
  KEY `fkObraxPgArtista_idx` (`idPgArtista`),
  KEY `fkObraxCategoriaObra_idx` (`idTipoObra`),
  CONSTRAINT `fkObraxTecnica` FOREIGN KEY (`idTecnica`) REFERENCES `tblTecnicaObras` (`idTecnica`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkObraxFormato` FOREIGN KEY (`idFormato`) REFERENCES `tblFormatoObras` (`idFormato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkObraxCorPredom` FOREIGN KEY (`idCorPred`) REFERENCES `tblCorPredObras` (`idCorPred`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkObraxCategoria1` FOREIGN KEY (`idCategoria1`) REFERENCES `tblCategoriasObras` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkObraxCategoria2` FOREIGN KEY (`idCategoria2`) REFERENCES `tblCategoriasObras` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkObraxCategoria3` FOREIGN KEY (`idCategoria3`) REFERENCES `tblCategoriasObras` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkObraxStatusObra` FOREIGN KEY (`idStatusObra`) REFERENCES `tblStatusObras` (`idStatusObras`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkObraxPgArtista` FOREIGN KEY (`idPgArtista`) REFERENCES `tblpgartista` (`idPgArtista`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkObraxCategoriaObra` FOREIGN KEY (`idTipoObra`) REFERENCES `tblTipoObras` (`idTipoObra`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblobras`
--

LOCK TABLES `tblobras` WRITE;
/*!40000 ALTER TABLE `tblobras` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblobras` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-02 23:05:03
