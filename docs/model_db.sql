-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema sysma
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema sysma
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sysma` DEFAULT CHARACTER SET utf8 ;
USE `sysma` ;

-- -----------------------------------------------------
-- Table `sysma`.`tblbiografiaartista`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblbiografiaartista` (
  `idBiografia` INT(11) NOT NULL AUTO_INCREMENT,
  `txtBiografiaPT` TEXT NULL DEFAULT NULL,
  `txtBiografiaEN` TEXT NULL DEFAULT NULL,
  `txtBiografiaES` TEXT NULL DEFAULT NULL,
  `imgBiografia` VARCHAR(200) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`idBiografia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblcategoriaobras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblcategoriaobras` (
  `idCategoriaObras` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeCategoriaObras` VARCHAR(45) NOT NULL COMMENT 'Pintura, Escultura ou Fotografia',
  `descricaoCategoriaObras` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idCategoriaObras`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblcategoriaparceiros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblcategoriaparceiros` (
  `idCategoriaParceiro` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeCategoriaParcPT` VARCHAR(45) NOT NULL,
  `nomeCategoriaParcEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeCategoriaParcES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCategoriaParcPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCategoriaParcEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCategoriaParcES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idCategoriaParceiro`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblcategoriasescultura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblcategoriasescultura` (
  `idCategoria` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeCategoriaPT` VARCHAR(45) NOT NULL,
  `nomeCategoriaEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeCategoriaES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCategoriaPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCategoriaEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCategoriaES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idCategoria`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblcategoriasfotografia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblcategoriasfotografia` (
  `idCategoria` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeCategoriaPT` VARCHAR(45) NOT NULL,
  `nomeCategoriaEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeCategoriaES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCategoriaPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCategoriaEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCategoriaES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idCategoria`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblcategoriaspintura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblcategoriaspintura` (
  `idCategoria` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeCategoriaPT` VARCHAR(45) NOT NULL,
  `nomeCategoriaEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeCategoriaES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCategoriaPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCategoriaEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCategoriaES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idCategoria`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblstatuscontrato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblstatuscontrato` (
  `idStatusContrato` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeStatusContrato` VARCHAR(45) NOT NULL COMMENT 'Status do contrato (Aguardando pagamento, Ativo,  Inativo, etc.)',
  `descricaoStatusContrato` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idStatusContrato`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblpropriedadeplano`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblpropriedadeplano` (
  `idPropPlano` INT(11) NOT NULL AUTO_INCREMENT,
  `idTipoPlano` INT(11) NOT NULL,
  `idPropriedadeChave` VARCHAR(100) NOT NULL COMMENT 'Descrição da propriedade do plano, como:\n- Número de obras permitidas\n- Espaço para biografia\n- Espaço para texto crítico\n- Sub-dominio',
  `idValorChave` VARCHAR(100) NOT NULL COMMENT 'Valor para as propriedades de acordo com o plano, por exemplo:\n\nPlano: Bronze\nPropriedade: Número de obras permitidas\nVALOR: 10',
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idPropPlano`, `idTipoPlano`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblusuariosfuncao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblusuariosfuncao` (
  `idFuncao` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `descricao` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idFuncao`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tbltipoplano`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tbltipoplano` (
  `idTipoPlano` INT(11) NOT NULL AUTO_INCREMENT,
  `idFuncao` INT(11) NOT NULL COMMENT 'Plano destinado a função. (1 = Artista, 2 = Parceiro, 3 = TI, 4 = Comercial, 5 = Financeiro, 6 = RH, 7 = Marketing, etc.)',
  `planoPT` VARCHAR(45) NOT NULL COMMENT 'Nome do plano: \nBronze - Trimestral, Bronze - Semestral, Bronze - Anual',
  `planoEN` VARCHAR(45) NULL DEFAULT NULL,
  `planoES` VARCHAR(45) NULL DEFAULT NULL,
  `periodicidade` INT(11) NOT NULL COMMENT 'Periodicidades dos planos: \n3, 6 ou 12 meses.',
  `vlPlano` VARCHAR(45) NOT NULL COMMENT 'Valor do plano em reais (sem desconto).',
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  INDEX `fkTipoPlanoxUsuariosFuncao_idx` (`idFuncao` ASC),
  INDEX `fkTipoPlanoxPropriedadePlano` (`idTipoPlano` ASC),
  CONSTRAINT `fkTipoPlanoxPropriedadePlano`
    FOREIGN KEY (`idTipoPlano`)
    REFERENCES `sysma`.`tblpropriedadeplano` (`idPropPlano`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fkTipoPlanoxUsuariosFuncao`
    FOREIGN KEY (`idFuncao`)
    REFERENCES `sysma`.`tblusuariosfuncao` (`idFuncao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblgeralpais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblgeralpais` (
  `idPais` INT(11) NOT NULL AUTO_INCREMENT,
  `iso` CHAR(2) NULL DEFAULT NULL,
  `iso3` CHAR(3) NULL DEFAULT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `descricao` VARCHAR(45) NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPais`))
ENGINE = InnoDB
AUTO_INCREMENT = 895
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblusuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblusuarios` (
  `idUsuario` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `senha` VARCHAR(20) NOT NULL,
  `idPaisResidente` INT(11) NOT NULL COMMENT 'Código do país residente (1 = Brasil, 2 = Portugal, etc.)',
  `telefoneCelular` VARCHAR(20) NULL DEFAULT NULL,
  `telefoneResidencial` VARCHAR(20) NULL DEFAULT NULL,
  `idFuncao` INT(11) NOT NULL COMMENT 'Função do usuário no site (1 = Artista, 2 = Parceiro, 3 = TI, 4 = Comercial, 5 = Financeiro, 6 = RH, 7 = Marketing, etc.)',
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fkUsuariosFuncao_idx` (`idFuncao` ASC),
  INDEX `fkGeralPais_idx` (`idPaisResidente` ASC),
  CONSTRAINT `fkUsuariosxGeralPais`
    FOREIGN KEY (`idPaisResidente`)
    REFERENCES `sysma`.`tblgeralpais` (`idPais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkUsuariosxUsuariosFuncao`
    FOREIGN KEY (`idFuncao`)
    REFERENCES `sysma`.`tblusuariosfuncao` (`idFuncao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblcontratos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblcontratos` (
  `idContrato` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Número do contrato',
  `idUsuario` INT(11) NOT NULL COMMENT 'Código do usuário',
  `idTipoPlano` INT(11) NOT NULL COMMENT 'Tipo de plano (1 = Bronze, 2 = Prata, 3 = Ouro, etc.)',
  `idStatusContrato` INT(11) NOT NULL COMMENT 'Código do status do contrato (1 = Aguardando pagamento, 2 = Ativo, 3 = Inativo, etc.)',
  `dtInicioContrato` DATETIME NOT NULL COMMENT 'Data do aceite do contrato.',
  `dtFimContrato` DATETIME NOT NULL COMMENT 'Data do fim do contrato de acordo com a periodicidade e o plano escolhido.',
  `observacao` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idContrato`),
  INDEX `fkContratosxUsuarios_idx` (`idUsuario` ASC),
  INDEX `fkContratosxTipoPlanos_idx` (`idTipoPlano` ASC),
  INDEX `fkContratosxStatusContrato_idx` (`idStatusContrato` ASC),
  CONSTRAINT `fkContratosxStatusContrato`
    FOREIGN KEY (`idStatusContrato`)
    REFERENCES `sysma`.`tblstatuscontrato` (`idStatusContrato`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fkContratosxTipoPlanos`
    FOREIGN KEY (`idTipoPlano`)
    REFERENCES `sysma`.`tbltipoplano` (`idTipoPlano`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkContratosxUsuarios`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `sysma`.`tblusuarios` (`idUsuario`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblcorpredescultura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblcorpredescultura` (
  `idCorPred` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeCorPredPT` VARCHAR(45) NOT NULL,
  `nomeCorPredEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeCorPredES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCorPredPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCorPredEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCorPredES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idCorPred`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblcorpredfotografia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblcorpredfotografia` (
  `idCorPred` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeCorPredPT` VARCHAR(45) NOT NULL,
  `nomeCorPredEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeCorPredES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCorPredPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCorPredEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCorPredES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idCorPred`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblcorpredpintura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblcorpredpintura` (
  `idCorPred` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeCorPredPT` VARCHAR(45) NOT NULL,
  `nomeCorPredEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeCorPredES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCorPredPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCorPredEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoCorPredES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idCorPred`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblformatoescultura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblformatoescultura` (
  `idFormato` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeFormatoPT` VARCHAR(45) NOT NULL,
  `nomeFormatoEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeFormatoES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoFormatoPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoFormatoEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoFormatoES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idFormato`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblpgartista`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblpgartista` (
  `idPgArtista` INT(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` INT(11) NOT NULL,
  `slugPagina` VARCHAR(200) NOT NULL,
  `nomeArtistico` VARCHAR(45) NOT NULL,
  `subtituloPgPT` VARCHAR(45) NOT NULL,
  `subtituloPgEN` VARCHAR(45) NULL DEFAULT NULL,
  `subtituloPgES` VARCHAR(45) NULL DEFAULT NULL,
  `idBiografia` INT(11) NOT NULL,
  `txtMsgVisitantePT` VARCHAR(250) NOT NULL,
  `txtMsgVisitanteEN` VARCHAR(250) NULL DEFAULT NULL,
  `txtMsgVisitanteES` VARCHAR(250) NULL DEFAULT NULL,
  `imgPerfil` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Caminho da imagem do perfil do artista (foto)',
  `imgBannerPg` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Caminho da imagem que representa o banner na página do artista.',
  `facebookID` VARCHAR(45) NULL DEFAULT NULL,
  `twitterID` VARCHAR(45) NULL DEFAULT NULL,
  `googlePlusID` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idPgArtista`, `idUsuario`),
  INDEX `fkPgArtistaxBiografia_idx` (`idBiografia` ASC),
  CONSTRAINT `fkPgArtistaxBiografia`
    FOREIGN KEY (`idBiografia`)
    REFERENCES `sysma`.`tblbiografiaartista` (`idBiografia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblstatusescultura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblstatusescultura` (
  `idStatusEscultura` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeStatusEsculturaPT` VARCHAR(45) NOT NULL,
  `nomeStatusEsculturaEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeStatusEsculturaES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoStatusEsculturaPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoStatusEsculturaEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoStatusEsculturaES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idStatusEscultura`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tbltecnicaescultura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tbltecnicaescultura` (
  `idTecnica` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeTecnicaPT` VARCHAR(45) NOT NULL,
  `nomeTecnicaEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeTecnicaES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoTecnicaPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoTecnicaEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoTecnicaES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idTecnica`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblescultura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblescultura` (
  `idObra` INT(11) NOT NULL AUTO_INCREMENT,
  `idPgArtista` INT(11) NOT NULL,
  `idCategoriaObras` INT(11) NOT NULL,
  `tituloPT` VARCHAR(45) NOT NULL,
  `tituloEN` VARCHAR(45) NULL DEFAULT NULL,
  `tituloES` VARCHAR(45) NULL DEFAULT NULL,
  `idTecnica` INT(11) NOT NULL,
  `altura` DECIMAL(15,2) NOT NULL COMMENT 'Altura da obra',
  `comprimento` DECIMAL(15,2) NOT NULL COMMENT 'Comprimento da obra',
  `largura` DECIMAL(15,2) NULL DEFAULT NULL COMMENT 'Largura da obra',
  `ladoMaior` INT(11) NULL DEFAULT NULL COMMENT 'Esta coluna deverá ser preenchida pelo sistema no momento do insert/update das obras, ela armazenará o lado maior da obra. Por exemplo, uma Escultura que foi cadastrada co',
  `dataCriacao` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Data de criação da obra',
  `descricaoPT` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Descrição da obra',
  `descricaoEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoES` VARCHAR(45) NULL DEFAULT NULL,
  `idFormato` INT(11) NOT NULL COMMENT 'Formato da obra (Horizontal, Vertical, etc.)',
  `precoEscultura` DECIMAL(15,2) NOT NULL COMMENT 'Preço da obra',
  `precoComDesconto` DECIMAL(15,2) NULL DEFAULT NULL COMMENT 'Preço com desconto aplicado',
  `idCorPred` INT(11) NOT NULL COMMENT 'Id da cor predominante',
  `idCategoria1` INT(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria2` INT(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria3` INT(11) NULL DEFAULT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `imgFoto1` VARCHAR(200) NOT NULL,
  `imgFoto2` VARCHAR(200) NULL DEFAULT NULL,
  `imgFoto3` VARCHAR(200) NULL DEFAULT NULL,
  `idStatusEscultura` INT(11) NOT NULL,
  `somatoriaNotas` INT(11) NULL DEFAULT NULL COMMENT 'Somatória das notas atribuidas pelos visitantes .. por exempl',
  `qtdVotos` INT(11) NULL DEFAULT NULL COMMENT 'Armazena quantidade de votos.',
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idObra`),
  INDEX `fkEsculturaxTecnica_idx` (`idTecnica` ASC),
  INDEX `fkEsculturaxFormato_idx` (`idFormato` ASC),
  INDEX `fkEsculturaxCorPredom_idx` (`idCorPred` ASC),
  INDEX `fkEsculturaxCategoria1_idx` (`idCategoria1` ASC),
  INDEX `fkEsculturaxCategoria2_idx` (`idCategoria2` ASC),
  INDEX `fkEsculturaxCategoria3_idx` (`idCategoria3` ASC),
  INDEX `fkEsculturaxStatusEscultura_idx` (`idStatusEscultura` ASC),
  INDEX `fkEsculturaxPgArtista_idx` (`idPgArtista` ASC),
  INDEX `fkEsculturaxCategoriaObra_idx` (`idCategoriaObras` ASC),
  CONSTRAINT `fkEsculturaxCategoria1`
    FOREIGN KEY (`idCategoria1`)
    REFERENCES `sysma`.`tblcategoriasescultura` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxCategoria2`
    FOREIGN KEY (`idCategoria2`)
    REFERENCES `sysma`.`tblcategoriasescultura` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxCategoria3`
    FOREIGN KEY (`idCategoria3`)
    REFERENCES `sysma`.`tblcategoriasescultura` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxCategoriaObra`
    FOREIGN KEY (`idCategoriaObras`)
    REFERENCES `sysma`.`tblcategoriaobras` (`idCategoriaObras`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxCorPredom`
    FOREIGN KEY (`idCorPred`)
    REFERENCES `sysma`.`tblcorpredescultura` (`idCorPred`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxFormato`
    FOREIGN KEY (`idFormato`)
    REFERENCES `sysma`.`tblformatoescultura` (`idFormato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxPgArtista`
    FOREIGN KEY (`idPgArtista`)
    REFERENCES `sysma`.`tblpgartista` (`idPgArtista`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxStatusEscultura`
    FOREIGN KEY (`idStatusEscultura`)
    REFERENCES `sysma`.`tblstatusescultura` (`idStatusEscultura`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxTecnica`
    FOREIGN KEY (`idTecnica`)
    REFERENCES `sysma`.`tbltecnicaescultura` (`idTecnica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblformatofotografia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblformatofotografia` (
  `idFormato` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeFormatoPT` VARCHAR(45) NOT NULL,
  `nomeFormatoEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeFormatoES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoFormatoPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoFormatoEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoFormatoES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idFormato`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblformatopintura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblformatopintura` (
  `idFormato` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeFormatoPT` VARCHAR(45) NOT NULL,
  `nomeFormatoEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeFormatoES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoFormatoPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoFormatoEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoFormatoES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idFormato`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblformulariocontato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblformulariocontato` (
  `idFormContato` INT(11) NOT NULL AUTO_INCREMENT,
  `idCategoriaObras` INT(11) NOT NULL COMMENT 'id da categoria da obra: Pintura, Escultura ou Fotografia',
  `idObra` INT(11) NOT NULL COMMENT 'id da obra, seja pintura, escultura ou fotografia',
  `nomeRemetente` VARCHAR(45) NOT NULL,
  `emailRemetente` VARCHAR(45) NOT NULL,
  `telefoneRemetente` VARCHAR(45) NULL DEFAULT NULL,
  `mensagemRemetente` VARCHAR(45) NOT NULL,
  `valorObra` VARCHAR(45) NOT NULL,
  `dataEnvio` VARCHAR(45) NOT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idFormContato`),
  INDEX `fkFormContato_idx` (`idCategoriaObras` ASC),
  CONSTRAINT `fkFormContatoxCategoriaObras`
    FOREIGN KEY (`idCategoriaObras`)
    REFERENCES `sysma`.`tblcategoriaobras` (`idCategoriaObras`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblstatusfotografia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblstatusfotografia` (
  `idStatusFotografia` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeStatusFotografiaPT` VARCHAR(45) NOT NULL,
  `nomeStatusFotografiaEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeStatusFotografiaES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoStatusFotografiaPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoStatusFotografiaEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoStatusFotografiaES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idStatusFotografia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tbltecnicafotografia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tbltecnicafotografia` (
  `idTecnica` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeTecnicaPT` VARCHAR(45) NOT NULL,
  `nomeTecnicaEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeTecnicaES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoTecnicaPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoTecnicaEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoTecnicaES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idTecnica`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblfotografia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblfotografia` (
  `idObra` INT(11) NOT NULL AUTO_INCREMENT,
  `idPgArtista` INT(11) NOT NULL,
  `idCategoriaObras` INT(11) NOT NULL,
  `tituloPT` VARCHAR(45) NOT NULL,
  `tituloEN` VARCHAR(45) NULL DEFAULT NULL,
  `tituloES` VARCHAR(45) NULL DEFAULT NULL,
  `idTecnica` INT(11) NOT NULL,
  `altura` DECIMAL(15,2) NOT NULL,
  `comprimento` DECIMAL(15,2) NOT NULL,
  `largura` DECIMAL(15,2) NULL DEFAULT NULL,
  `ladoMaior` INT(11) NULL DEFAULT NULL COMMENT 'Esta coluna deverá ser preenchida pelo sistema no momento do insert/update das obras, ela armazenará o lado maior da obra. Por exemplo, uma fotografia que foi cadastrada co',
  `dataCriacao` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Data de criação da obra',
  `descricaoPT` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Descrição da obra',
  `descricaoEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoES` VARCHAR(45) NULL DEFAULT NULL,
  `idFormato` INT(11) NOT NULL COMMENT 'Formato da obra (Horizontal, Vertical, etc.)',
  `precoFotografia` DECIMAL(15,2) NOT NULL COMMENT 'Preço da Fotografia',
  `precoComDesconto` DECIMAL(15,2) NULL DEFAULT NULL COMMENT 'Preço com desconto aplicado',
  `idCorPred` INT(11) NOT NULL COMMENT 'Id da cor predominante',
  `idCategoria1` INT(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria2` INT(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria3` INT(11) NULL DEFAULT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `imgFoto1` VARCHAR(200) NOT NULL,
  `imgFoto2` VARCHAR(200) NULL DEFAULT NULL,
  `imgFoto3` VARCHAR(200) NULL DEFAULT NULL,
  `idStatusFotografia` INT(11) NOT NULL,
  `somatoriaNotas` INT(11) NULL DEFAULT NULL COMMENT 'Somatória das notas atribuidas pelos visitantes .. por exempl',
  `qtdVotos` INT(11) NULL DEFAULT NULL COMMENT 'Armazena quantidade de votos.',
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idObra`),
  INDEX `fkFotografiaxTecnica_idx` (`idTecnica` ASC),
  INDEX `fkFotografiaxFormato_idx` (`idFormato` ASC),
  INDEX `fkFotografiaxCorPredom_idx` (`idCorPred` ASC),
  INDEX `fkFotografiaxCategoria1_idx` (`idCategoria1` ASC),
  INDEX `fkFotografiaxCategoria2_idx` (`idCategoria2` ASC),
  INDEX `fkFotografiaxCategoria3_idx` (`idCategoria3` ASC),
  INDEX `fkFotografiaxStatusFotografia_idx` (`idStatusFotografia` ASC),
  INDEX `fkFotografiaxPgArtista_idx` (`idPgArtista` ASC),
  INDEX `fkFotografiaxCategoriaObra_idx` (`idCategoriaObras` ASC),
  CONSTRAINT `fkFotografiaxCategoria1`
    FOREIGN KEY (`idCategoria1`)
    REFERENCES `sysma`.`tblcategoriasfotografia` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxCategoria2`
    FOREIGN KEY (`idCategoria2`)
    REFERENCES `sysma`.`tblcategoriasfotografia` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxCategoria3`
    FOREIGN KEY (`idCategoria3`)
    REFERENCES `sysma`.`tblcategoriasfotografia` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxCategoriaObra`
    FOREIGN KEY (`idCategoriaObras`)
    REFERENCES `sysma`.`tblcategoriaobras` (`idCategoriaObras`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxCorPredom`
    FOREIGN KEY (`idCorPred`)
    REFERENCES `sysma`.`tblcorpredfotografia` (`idCorPred`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxFormato`
    FOREIGN KEY (`idFormato`)
    REFERENCES `sysma`.`tblformatofotografia` (`idFormato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxPgArtista`
    FOREIGN KEY (`idPgArtista`)
    REFERENCES `sysma`.`tblpgartista` (`idPgArtista`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxStatusFotografia`
    FOREIGN KEY (`idStatusFotografia`)
    REFERENCES `sysma`.`tblstatusfotografia` (`idStatusFotografia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxTecnica`
    FOREIGN KEY (`idTecnica`)
    REFERENCES `sysma`.`tbltecnicafotografia` (`idTecnica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblgeralcupom`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblgeralcupom` (
  `idCupom` INT(11) NOT NULL AUTO_INCREMENT,
  `codCupom` VARCHAR(8) NOT NULL COMMENT 'Código do cupom, randômico e alfanumérico com oito caracteres.',
  `porcentagemDesconto` TINYINT(4) NOT NULL COMMENT 'Porcentagem de desconto que o cupom aplicará no valor total, de 1% até 100%.',
  `cupomAtivo` TINYINT(4) NOT NULL COMMENT 'Cupom ativo? 0 = Não, 1 = Sim.',
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idCupom`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblparceiros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblparceiros` (
  `idParceiro` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(70) NOT NULL,
  `cidade` VARCHAR(45) NOT NULL,
  `estado` VARCHAR(45) NOT NULL,
  `urlSite` VARCHAR(200) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `telefone` VARCHAR(20) NOT NULL,
  `idCategoriaParceiro` INT(11) NOT NULL,
  `imgLogo` VARCHAR(200) NULL DEFAULT NULL,
  `imgProduto1` VARCHAR(200) NULL DEFAULT NULL,
  `imgProduto2` VARCHAR(200) NULL DEFAULT NULL,
  `imgProduto3` VARCHAR(200) NULL DEFAULT NULL,
  `imgProduto4` VARCHAR(200) NULL DEFAULT NULL,
  `imgProduto5` VARCHAR(200) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idParceiro`),
  INDEX `fkParceirosxCategoriaParceiros_idx` (`idCategoriaParceiro` ASC),
  CONSTRAINT `fkParceirosxCategoriaParceiros`
    FOREIGN KEY (`idCategoriaParceiro`)
    REFERENCES `sysma`.`tblcategoriaparceiros` (`idCategoriaParceiro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblstatuspintura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblstatuspintura` (
  `idStatusPintura` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeStatusPinturaPT` VARCHAR(45) NOT NULL,
  `nomeStatusPinturaEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeStatusPinturaES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoStatusPinturaPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoStatusPinturaEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoStatusPinturaES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idStatusPintura`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tbltecnicapintura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tbltecnicapintura` (
  `idTecnica` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeTecnicaPT` VARCHAR(45) NOT NULL COMMENT 'Óleo sobre tela, Acrílica',
  `nomeTecnicaEN` VARCHAR(45) NULL DEFAULT NULL,
  `nomeTecnicaES` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoTecnicaPT` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoTecnicaEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoTecnicaES` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idTecnica`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblpintura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblpintura` (
  `idObra` INT(11) NOT NULL AUTO_INCREMENT,
  `idPgArtista` INT(11) NOT NULL,
  `idCategoriaObras` INT(11) NOT NULL,
  `tituloPT` VARCHAR(45) NOT NULL COMMENT 'Titulo da pintura, algo semelhante há:\n\n\"Mulher com crianças\", \"Figura abstrata\", \"Jogo de futebol\".',
  `tituloEN` VARCHAR(45) NULL DEFAULT NULL,
  `tituloES` VARCHAR(45) NULL DEFAULT NULL,
  `idTecnica` INT(11) NOT NULL COMMENT 'Técnica da pintura (Óleo, acrílico, desenho).',
  `altura` DECIMAL(15,2) NOT NULL COMMENT 'Altura da obra',
  `comprimento` DECIMAL(15,2) NOT NULL COMMENT 'Comprimento da obra',
  `largura` DECIMAL(15,2) NULL DEFAULT NULL COMMENT 'Largura da obra',
  `ladoMaior` INT(11) NULL DEFAULT NULL COMMENT 'Esta coluna deverá ser preenchida pelo sistema no momento do insert/update das obras, ela armazenará o lado maior da obra. Por exemplo, uma pintura que foi cadastrada com:\n\nAltura: 100\nComprimento: 150\nLargura: NULL\nLadoMaior: 150 (sempre o lado maior)\n\nEsta coluna facilitará no momento de realizar os filtros.',
  `dataCriacao` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Data de criação da obra',
  `descricaoPT` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Descrição da obra',
  `descricaoEN` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoES` VARCHAR(45) NULL DEFAULT NULL,
  `idFormato` INT(11) NOT NULL COMMENT 'Formato da obra (Horizontal, Vertical, etc.)',
  `precoPintura` DECIMAL(15,2) NOT NULL COMMENT 'Preço da pintura',
  `precoComDesconto` DECIMAL(15,2) NULL DEFAULT NULL COMMENT 'Preço com desconto aplicado',
  `idCorPred` INT(11) NOT NULL COMMENT 'Id da cor predominante',
  `idCategoria1` INT(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria2` INT(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria3` INT(11) NULL DEFAULT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `imgFoto1` VARCHAR(200) NOT NULL COMMENT 'Caminho da foto da pintura 1',
  `imgFoto2` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Caminho da foto da pintura 2',
  `imgFoto3` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Caminho da foto da pintura 3',
  `idStatusPintura` INT(11) NOT NULL COMMENT 'Status da pintura, exemplo:\n\n1 = À venda\n2 = Vendido\n',
  `somatoriaNotas` INT(11) NULL DEFAULT NULL COMMENT 'Somatória das notas atribuidas pelos visitantes .. por exemplo\n\nVisitante 1, deu nota 5 (5 estrelas)\nVisitante 2, deu nota 4 (4 estrelas)\nvisitante 3, deu nota 5 (5 estrelas)\n\nA nota armazenada neste campo será: 14\n\nPois NOTA / QTD. VOTOS = MÉDIA.\n\nNeste caso: 14 / 3 = 4,66',
  `qtdVotos` INT(11) NULL DEFAULT NULL COMMENT 'Armazena quantidade de votos.',
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idObra`),
  INDEX `fkPinturaxTecnica_idx` (`idTecnica` ASC),
  INDEX `fkPinturaxFormato_idx` (`idFormato` ASC),
  INDEX `fkPinturaxCorPredom_idx` (`idCorPred` ASC),
  INDEX `fkPinturaxCategoria1_idx` (`idCategoria1` ASC),
  INDEX `fkPinturaxCategoria2_idx` (`idCategoria2` ASC),
  INDEX `fkPinturaxCategoria3_idx` (`idCategoria3` ASC),
  INDEX `fkPinturaxStatusPintura_idx` (`idStatusPintura` ASC),
  INDEX `fkPinturaxPgArtista_idx` (`idPgArtista` ASC),
  INDEX `fkPinturaxCategoriaObra_idx` (`idCategoriaObras` ASC),
  CONSTRAINT `fkPinturaxCategoria1`
    FOREIGN KEY (`idCategoria1`)
    REFERENCES `sysma`.`tblcategoriaspintura` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxCategoria2`
    FOREIGN KEY (`idCategoria2`)
    REFERENCES `sysma`.`tblcategoriaspintura` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxCategoria3`
    FOREIGN KEY (`idCategoria3`)
    REFERENCES `sysma`.`tblcategoriaspintura` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxCategoriaObra`
    FOREIGN KEY (`idCategoriaObras`)
    REFERENCES `sysma`.`tblcategoriaobras` (`idCategoriaObras`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxCorPredom`
    FOREIGN KEY (`idCorPred`)
    REFERENCES `sysma`.`tblcorpredpintura` (`idCorPred`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxFormato`
    FOREIGN KEY (`idFormato`)
    REFERENCES `sysma`.`tblformatopintura` (`idFormato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxPgArtista`
    FOREIGN KEY (`idPgArtista`)
    REFERENCES `sysma`.`tblpgartista` (`idPgArtista`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxStatusPintura`
    FOREIGN KEY (`idStatusPintura`)
    REFERENCES `sysma`.`tblstatuspintura` (`idStatusPintura`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxTecnica`
    FOREIGN KEY (`idTecnica`)
    REFERENCES `sysma`.`tbltecnicapintura` (`idTecnica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tbltipopagamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tbltipopagamento` (
  `idTipoPagamento` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `descricao` VARCHAR(45) NULL DEFAULT NULL,
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  PRIMARY KEY (`idTipoPagamento`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tbltransacoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tbltransacoes` (
  `idTransacao` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Número da transação',
  `idContrato` INT(11) NOT NULL COMMENT 'Número do contrato',
  `codCupom` VARCHAR(8) NULL DEFAULT NULL,
  `idTipoPagamento` INT(11) NOT NULL COMMENT 'Forma de pagamento escolhida pelo cliente (1 = Deposito em conta, 2 = PagSeguro, 3 = Paypal, 4 = Boleto, etc..)',
  `valorTotal` DECIMAL(15,2) NOT NULL COMMENT 'Valor total do plano escolhido (sem aplicação do cupom de desconto).',
  `valorAPagar` DECIMAL(15,2) NOT NULL COMMENT 'Valor total a pagar pelo gerador da transação (com cupom de desconto aplicado).',
  `txtidentPagamento` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Explicação de como foi feito a identificação do pagamento, algo semelhante a:\n\"Pagamento reconhecido por Alcides, no dia 09/09 às 16h28 através de consulta ao sistema do PagSeguro.\"',
  `valorPago` DECIMAL(15,2) NULL DEFAULT NULL COMMENT 'Valor pago pelo gerador da transação.',
  `dtPagamentoTransacao` DATETIME NOT NULL,
  `observacao` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Observação geral.',
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` DATETIME NOT NULL,
  PRIMARY KEY (`idTransacao`),
  INDEX `fkTransacoesxContratos_idx` (`idContrato` ASC),
  INDEX `fkTransacoesxTipoPagamento_idx` (`idTipoPagamento` ASC),
  CONSTRAINT `fkTransacoesxContratos`
    FOREIGN KEY (`idContrato`)
    REFERENCES `sysma`.`tblcontratos` (`idContrato`)
    ON UPDATE NO ACTION,
  CONSTRAINT `fkTransacoesxTipoPagamento`
    FOREIGN KEY (`idTipoPagamento`)
    REFERENCES `sysma`.`tbltipopagamento` (`idTipoPagamento`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sysma`.`tblusuariosdados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sysma`.`tblusuariosdados` (
  `idUsuario` INT(11) NOT NULL,
  `nomeCompleto` VARCHAR(70) NOT NULL,
  `CPF` VARCHAR(15) NULL DEFAULT NULL COMMENT 'Somente números na coluna CPF',
  `RG` VARCHAR(15) NULL DEFAULT NULL,
  `RNE` VARCHAR(15) NULL DEFAULT NULL,
  `sexo` CHAR(1) NOT NULL,
  `telefoneComercial` VARCHAR(20) NULL DEFAULT NULL,
  `enderecoCEP` VARCHAR(9) NULL DEFAULT NULL,
  `enderecologradouro` VARCHAR(60) NOT NULL,
  `enderecoNumero` VARCHAR(12) NOT NULL,
  `enderecoComplemento` VARCHAR(45) NULL DEFAULT NULL,
  `enderecoBairro` VARCHAR(60) NULL DEFAULT NULL,
  `cidade` VARCHAR(45) NOT NULL,
  `estado` VARCHAR(45) NOT NULL,
  `publicacoesEmail` TINYINT(4) NOT NULL COMMENT 'Deseja receber publicações por e-mail? (0=Não/1=Sim)',
  `notificacaoAlertaSMS` TINYINT(4) NOT NULL COMMENT 'Receber notificação e alerta por SMS?  (0=Não/1=Sim)',
  `dtCriacao` DATETIME NOT NULL,
  `dtUltAlteracao` DATETIME NULL DEFAULT NULL,
  `statusRegistro` TINYINT(4) NOT NULL,
  INDEX `fkUsuariosDados_idx` (`idUsuario` ASC),
  CONSTRAINT `fkUsuariosDadosxUsuarios`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `sysma`.`tblusuarios` (`idUsuario`)
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

/* session CI */
CREATE TABLE IF NOT EXISTS  `sysma`.`ci_sessions` (
  session_id varchar(40) DEFAULT '0' NOT NULL,
  ip_address varchar(45) DEFAULT '0' NOT NULL,
  user_agent varchar(120) NOT NULL,
  last_activity int(10) unsigned DEFAULT 0 NOT NULL,
  user_data text NOT NULL,
  PRIMARY KEY (session_id),
  KEY `last_activity_idx` (`last_activity`)
);





CREATE TABLE `walla148_sysma`.`tblvotos` (
  `idVoto` INT NOT NULL AUTO_INCREMENT,
  `idObra` INT NOT NULL,
  `userIp` VARCHAR(45) NOT NULL,
  `dataCriacao` DATETIME NULL,
  `statusRegistro` TINYINT(4) NULL,
  PRIMARY KEY (`idVoto`));

CREATE TABLE `tblcontato` (
`idContato`  int NOT NULL AUTO_INCREMENT ,
`idUsuario`  int NOT NULL ,
`nomeContato`  varchar(45) NOT NULL ,
`emailContato`  varchar(45) NOT NULL ,
`telefoneContato`  varchar(45) NULL ,
`mensagemContato`  text NULL ,
`dtCriacao`  datetime NOT NULL ,
`dtAlteracao`  datetime NULL ,
`statusRegistro`  tinyint(5) NOT NULL ,
PRIMARY KEY (`idContato`)
);


CREATE TABLE `tblnewsletter` (
`idNewsletter`  int NOT NULL AUTO_INCREMENT ,
`nomeNewsletter`  varchar(45) NOT NULL ,
`emailNewsletter`  varchar(45) NOT NULL ,
`dtCriacao`  datetime NOT NULL ,
`dtAlteracao`  datetime NULL ,
`statusRegistro`  tinyint(5) NOT NULL ,
PRIMARY KEY (`idNewsletter`)
);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;