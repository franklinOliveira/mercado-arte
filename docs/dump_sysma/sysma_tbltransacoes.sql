CREATE DATABASE  IF NOT EXISTS `sysma` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: sysma
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbltransacoes`
--

DROP TABLE IF EXISTS `tbltransacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltransacoes` (
  `idTransacao` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Número da transação',
  `idContrato` int(11) NOT NULL COMMENT 'Número do contrato',
  `codCupom` varchar(8) DEFAULT NULL,
  `idTipoPagamento` int(11) NOT NULL COMMENT 'Forma de pagamento escolhida pelo cliente (1 = Deposito em conta, 2 = PagSeguro, 3 = Paypal, 4 = Boleto, etc..)',
  `valorTotal` decimal(15,2) NOT NULL COMMENT 'Valor total do plano escolhido (sem aplicação do cupom de desconto).',
  `valorAPagar` decimal(15,2) NOT NULL COMMENT 'Valor total a pagar pelo gerador da transação (com cupom de desconto aplicado).',
  `txtidentPagamento` varchar(255) DEFAULT NULL COMMENT 'Explicação de como foi feito a identificação do pagamento, algo semelhante a:\n"Pagamento reconhecido por Alcides, no dia 09/09 às 16h28 através de consulta ao sistema do PagSeguro."',
  `valorPago` decimal(15,2) DEFAULT NULL COMMENT 'Valor pago pelo gerador da transação.',
  `dtPagamentoTransacao` datetime NOT NULL,
  `observacao` varchar(255) DEFAULT NULL COMMENT 'Observação geral.',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` datetime NOT NULL,
  PRIMARY KEY (`idTransacao`),
  KEY `fkTransacoesxContratos_idx` (`idContrato`),
  KEY `fkTransacoesxTipoPagamento_idx` (`idTipoPagamento`),
  CONSTRAINT `fkTransacoesxContratos` FOREIGN KEY (`idContrato`) REFERENCES `tblcontratos` (`idContrato`) ON UPDATE NO ACTION,
  CONSTRAINT `fkTransacoesxTipoPagamento` FOREIGN KEY (`idTipoPagamento`) REFERENCES `tbltipopagamento` (`idTipoPagamento`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltransacoes`
--

LOCK TABLES `tbltransacoes` WRITE;
/*!40000 ALTER TABLE `tbltransacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltransacoes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-09 16:29:04
