CREATE DATABASE  IF NOT EXISTS `sysma` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: sysma
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblusuariosdados`
--

DROP TABLE IF EXISTS `tblusuariosdados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblusuariosdados` (
  `idUsuario` int(11) NOT NULL,
  `nomeCompleto` varchar(70) NOT NULL,
  `CPF` varchar(15) DEFAULT NULL COMMENT 'Somente números na coluna CPF',
  `RG` varchar(15) DEFAULT NULL,
  `RNE` varchar(15) DEFAULT NULL,
  `sexo` char(1) NOT NULL,
  `telefoneComercial` varchar(20) DEFAULT NULL,
  `enderecoCEP` varchar(9) DEFAULT NULL,
  `enderecologradouro` varchar(60) NOT NULL,
  `enderecoNumero` varchar(12) NOT NULL,
  `enderecoComplemento` varchar(45) DEFAULT NULL,
  `enderecoBairro` varchar(60) DEFAULT NULL,
  `cidade` varchar(45) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `publicacoesEmail` tinyint(4) NOT NULL COMMENT 'Deseja receber publicações por e-mail? (0=Não/1=Sim)',
  `notificacaoAlertaSMS` tinyint(4) NOT NULL COMMENT 'Receber notificação e alerta por SMS?  (0=Não/1=Sim)',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  KEY `fkUsuariosDados_idx` (`idUsuario`),
  CONSTRAINT `fkUsuariosDadosxUsuarios` FOREIGN KEY (`idUsuario`) REFERENCES `tblusuarios` (`idUsuario`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusuariosdados`
--

LOCK TABLES `tblusuariosdados` WRITE;
/*!40000 ALTER TABLE `tblusuariosdados` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblusuariosdados` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-09 16:29:05
