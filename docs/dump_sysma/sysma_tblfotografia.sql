CREATE DATABASE  IF NOT EXISTS `sysma` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: sysma
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblfotografia`
--

DROP TABLE IF EXISTS `tblfotografia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblfotografia` (
  `idObra` int(11) NOT NULL AUTO_INCREMENT,
  `idPgArtista` int(11) NOT NULL,
  `idCategoriaObras` int(11) NOT NULL,
  `tituloPT` varchar(45) NOT NULL,
  `tituloEN` varchar(45) DEFAULT NULL,
  `tituloES` varchar(45) DEFAULT NULL,
  `idTecnica` int(11) NOT NULL,
  `altura` decimal(15,2) NOT NULL,
  `comprimento` decimal(15,2) NOT NULL,
  `largura` decimal(15,2) DEFAULT NULL,
  `ladoMaior` int(11) DEFAULT NULL COMMENT 'Esta coluna deverá ser preenchida pelo sistema no momento do insert/update das obras, ela armazenará o lado maior da obra. Por exemplo, uma fotografia que foi cadastrada co',
  `dataCriacao` varchar(45) DEFAULT NULL COMMENT 'Data de criação da obra',
  `descricaoPT` varchar(255) DEFAULT NULL COMMENT 'Descrição da obra',
  `descricaoEN` varchar(45) DEFAULT NULL,
  `descricaoES` varchar(45) DEFAULT NULL,
  `idFormato` int(11) NOT NULL COMMENT 'Formato da obra (Horizontal, Vertical, etc.)',
  `precoFotografia` decimal(15,2) NOT NULL COMMENT 'Preço da Fotografia',
  `precoComDesconto` decimal(15,2) DEFAULT NULL COMMENT 'Preço com desconto aplicado',
  `idCorPred` int(11) NOT NULL COMMENT 'Id da cor predominante',
  `idCategoria1` int(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria2` int(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria3` int(11) DEFAULT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `imgFoto1` varchar(200) NOT NULL,
  `imgFoto2` varchar(200) DEFAULT NULL,
  `imgFoto3` varchar(200) DEFAULT NULL,
  `idStatusFotografia` int(11) NOT NULL,
  `somatoriaNotas` int(11) DEFAULT NULL COMMENT 'Somatória das notas atribuidas pelos visitantes .. por exempl',
  `qtdVotos` int(11) DEFAULT NULL COMMENT 'Armazena quantidade de votos.',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idObra`),
  KEY `fkFotografiaxTecnica_idx` (`idTecnica`),
  KEY `fkFotografiaxFormato_idx` (`idFormato`),
  KEY `fkFotografiaxCorPredom_idx` (`idCorPred`),
  KEY `fkFotografiaxCategoria1_idx` (`idCategoria1`),
  KEY `fkFotografiaxCategoria2_idx` (`idCategoria2`),
  KEY `fkFotografiaxCategoria3_idx` (`idCategoria3`),
  KEY `fkFotografiaxStatusFotografia_idx` (`idStatusFotografia`),
  KEY `fkFotografiaxPgArtista_idx` (`idPgArtista`),
  KEY `fkFotografiaxCategoriaObra_idx` (`idCategoriaObras`),
  CONSTRAINT `fkFotografiaxCategoria1` FOREIGN KEY (`idCategoria1`) REFERENCES `tblcategoriasfotografia` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxCategoria2` FOREIGN KEY (`idCategoria2`) REFERENCES `tblcategoriasfotografia` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxCategoria3` FOREIGN KEY (`idCategoria3`) REFERENCES `tblcategoriasfotografia` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxCategoriaObra` FOREIGN KEY (`idCategoriaObras`) REFERENCES `tblcategoriaobras` (`idCategoriaObras`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxCorPredom` FOREIGN KEY (`idCorPred`) REFERENCES `tblcorpredfotografia` (`idCorPred`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxFormato` FOREIGN KEY (`idFormato`) REFERENCES `tblformatofotografia` (`idFormato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxPgArtista` FOREIGN KEY (`idPgArtista`) REFERENCES `tblpgartista` (`idPgArtista`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxStatusFotografia` FOREIGN KEY (`idStatusFotografia`) REFERENCES `tblstatusfotografia` (`idStatusFotografia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkFotografiaxTecnica` FOREIGN KEY (`idTecnica`) REFERENCES `tbltecnicafotografia` (`idTecnica`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblfotografia`
--

LOCK TABLES `tblfotografia` WRITE;
/*!40000 ALTER TABLE `tblfotografia` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblfotografia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-09 16:29:02
