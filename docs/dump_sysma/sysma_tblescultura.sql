CREATE DATABASE  IF NOT EXISTS `sysma` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: sysma
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblescultura`
--

DROP TABLE IF EXISTS `tblescultura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblescultura` (
  `idObra` int(11) NOT NULL AUTO_INCREMENT,
  `idPgArtista` int(11) NOT NULL,
  `idCategoriaObras` int(11) NOT NULL,
  `tituloPT` varchar(45) NOT NULL,
  `tituloEN` varchar(45) DEFAULT NULL,
  `tituloES` varchar(45) DEFAULT NULL,
  `idTecnica` int(11) NOT NULL,
  `altura` decimal(15,2) NOT NULL COMMENT 'Altura da obra',
  `comprimento` decimal(15,2) NOT NULL COMMENT 'Comprimento da obra',
  `largura` decimal(15,2) DEFAULT NULL COMMENT 'Largura da obra',
  `ladoMaior` int(11) DEFAULT NULL COMMENT 'Esta coluna deverá ser preenchida pelo sistema no momento do insert/update das obras, ela armazenará o lado maior da obra. Por exemplo, uma Escultura que foi cadastrada co',
  `dataCriacao` varchar(45) DEFAULT NULL COMMENT 'Data de criação da obra',
  `descricaoPT` varchar(255) DEFAULT NULL COMMENT 'Descrição da obra',
  `descricaoEN` varchar(45) DEFAULT NULL,
  `descricaoES` varchar(45) DEFAULT NULL,
  `idFormato` int(11) NOT NULL COMMENT 'Formato da obra (Horizontal, Vertical, etc.)',
  `precoEscultura` decimal(15,2) NOT NULL COMMENT 'Preço da obra',
  `precoComDesconto` decimal(15,2) DEFAULT NULL COMMENT 'Preço com desconto aplicado',
  `idCorPred` int(11) NOT NULL COMMENT 'Id da cor predominante',
  `idCategoria1` int(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria2` int(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria3` int(11) DEFAULT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `imgFoto1` varchar(200) NOT NULL,
  `imgFoto2` varchar(200) DEFAULT NULL,
  `imgFoto3` varchar(200) DEFAULT NULL,
  `idStatusEscultura` int(11) NOT NULL,
  `somatoriaNotas` int(11) DEFAULT NULL COMMENT 'Somatória das notas atribuidas pelos visitantes .. por exempl',
  `qtdVotos` int(11) DEFAULT NULL COMMENT 'Armazena quantidade de votos.',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idObra`),
  KEY `fkEsculturaxTecnica_idx` (`idTecnica`),
  KEY `fkEsculturaxFormato_idx` (`idFormato`),
  KEY `fkEsculturaxCorPredom_idx` (`idCorPred`),
  KEY `fkEsculturaxCategoria1_idx` (`idCategoria1`),
  KEY `fkEsculturaxCategoria2_idx` (`idCategoria2`),
  KEY `fkEsculturaxCategoria3_idx` (`idCategoria3`),
  KEY `fkEsculturaxStatusEscultura_idx` (`idStatusEscultura`),
  KEY `fkEsculturaxPgArtista_idx` (`idPgArtista`),
  KEY `fkEsculturaxCategoriaObra_idx` (`idCategoriaObras`),
  CONSTRAINT `fkEsculturaxCategoria1` FOREIGN KEY (`idCategoria1`) REFERENCES `tblcategoriasescultura` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxCategoria2` FOREIGN KEY (`idCategoria2`) REFERENCES `tblcategoriasescultura` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxCategoria3` FOREIGN KEY (`idCategoria3`) REFERENCES `tblcategoriasescultura` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxCategoriaObra` FOREIGN KEY (`idCategoriaObras`) REFERENCES `tblcategoriaobras` (`idCategoriaObras`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxCorPredom` FOREIGN KEY (`idCorPred`) REFERENCES `tblcorpredescultura` (`idCorPred`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxFormato` FOREIGN KEY (`idFormato`) REFERENCES `tblformatoescultura` (`idFormato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxPgArtista` FOREIGN KEY (`idPgArtista`) REFERENCES `tblpgartista` (`idPgArtista`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxStatusEscultura` FOREIGN KEY (`idStatusEscultura`) REFERENCES `tblstatusescultura` (`idStatusEscultura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkEsculturaxTecnica` FOREIGN KEY (`idTecnica`) REFERENCES `tbltecnicaescultura` (`idTecnica`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblescultura`
--

LOCK TABLES `tblescultura` WRITE;
/*!40000 ALTER TABLE `tblescultura` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblescultura` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-09 16:29:01
