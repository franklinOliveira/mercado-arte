CREATE DATABASE  IF NOT EXISTS `sysma` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: sysma
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblpintura`
--

DROP TABLE IF EXISTS `tblpintura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpintura` (
  `idObra` int(11) NOT NULL AUTO_INCREMENT,
  `idPgArtista` int(11) NOT NULL,
  `idCategoriaObras` int(11) NOT NULL,
  `tituloPT` varchar(45) NOT NULL COMMENT 'Titulo da pintura, algo semelhante há:\n\n"Mulher com crianças", "Figura abstrata", "Jogo de futebol".',
  `tituloEN` varchar(45) DEFAULT NULL,
  `tituloES` varchar(45) DEFAULT NULL,
  `idTecnica` int(11) NOT NULL COMMENT 'Técnica da pintura (Óleo, acrílico, desenho).',
  `altura` decimal(15,2) NOT NULL COMMENT 'Altura da obra',
  `comprimento` decimal(15,2) NOT NULL COMMENT 'Comprimento da obra',
  `largura` decimal(15,2) DEFAULT NULL COMMENT 'Largura da obra',
  `ladoMaior` int(11) DEFAULT NULL COMMENT 'Esta coluna deverá ser preenchida pelo sistema no momento do insert/update das obras, ela armazenará o lado maior da obra. Por exemplo, uma pintura que foi cadastrada com:\n\nAltura: 100\nComprimento: 150\nLargura: NULL\nLadoMaior: 150 (sempre o lado maior)\n\nEsta coluna facilitará no momento de realizar os filtros.',
  `dataCriacao` varchar(45) DEFAULT NULL COMMENT 'Data de criação da obra',
  `descricaoPT` varchar(255) DEFAULT NULL COMMENT 'Descrição da obra',
  `descricaoEN` varchar(45) DEFAULT NULL,
  `descricaoES` varchar(45) DEFAULT NULL,
  `idFormato` int(11) NOT NULL COMMENT 'Formato da obra (Horizontal, Vertical, etc.)',
  `precoPintura` decimal(15,2) NOT NULL COMMENT 'Preço da pintura',
  `precoComDesconto` decimal(15,2) DEFAULT NULL COMMENT 'Preço com desconto aplicado',
  `idCorPred` int(11) NOT NULL COMMENT 'Id da cor predominante',
  `idCategoria1` int(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria2` int(11) NOT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `idCategoria3` int(11) DEFAULT NULL COMMENT 'O artista poderá escolher até 3 categorias para obra. Categoria = Tema, exemplo:\n\nCategorias:\n- Abstrato\n- Natureza\n- Paisagem\n- Urbano',
  `imgFoto1` varchar(200) NOT NULL COMMENT 'Caminho da foto da pintura 1',
  `imgFoto2` varchar(200) DEFAULT NULL COMMENT 'Caminho da foto da pintura 2',
  `imgFoto3` varchar(200) DEFAULT NULL COMMENT 'Caminho da foto da pintura 3',
  `idStatusPintura` int(11) NOT NULL COMMENT 'Status da pintura, exemplo:\n\n1 = À venda\n2 = Vendido\n',
  `somatoriaNotas` int(11) DEFAULT NULL COMMENT 'Somatória das notas atribuidas pelos visitantes .. por exemplo\n\nVisitante 1, deu nota 5 (5 estrelas)\nVisitante 2, deu nota 4 (4 estrelas)\nvisitante 3, deu nota 5 (5 estrelas)\n\nA nota armazenada neste campo será: 14\n\nPois NOTA / QTD. VOTOS = MÉDIA.\n\nNeste caso: 14 / 3 = 4,66',
  `qtdVotos` int(11) DEFAULT NULL COMMENT 'Armazena quantidade de votos.',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idObra`),
  KEY `fkPinturaxTecnica_idx` (`idTecnica`),
  KEY `fkPinturaxFormato_idx` (`idFormato`),
  KEY `fkPinturaxCorPredom_idx` (`idCorPred`),
  KEY `fkPinturaxCategoria1_idx` (`idCategoria1`),
  KEY `fkPinturaxCategoria2_idx` (`idCategoria2`),
  KEY `fkPinturaxCategoria3_idx` (`idCategoria3`),
  KEY `fkPinturaxStatusPintura_idx` (`idStatusPintura`),
  KEY `fkPinturaxPgArtista_idx` (`idPgArtista`),
  KEY `fkPinturaxCategoriaObra_idx` (`idCategoriaObras`),
  CONSTRAINT `fkPinturaxCategoria1` FOREIGN KEY (`idCategoria1`) REFERENCES `tblcategoriaspintura` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxCategoria2` FOREIGN KEY (`idCategoria2`) REFERENCES `tblcategoriaspintura` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxCategoria3` FOREIGN KEY (`idCategoria3`) REFERENCES `tblcategoriaspintura` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxCategoriaObra` FOREIGN KEY (`idCategoriaObras`) REFERENCES `tblcategoriaobras` (`idCategoriaObras`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxCorPredom` FOREIGN KEY (`idCorPred`) REFERENCES `tblcorpredpintura` (`idCorPred`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxFormato` FOREIGN KEY (`idFormato`) REFERENCES `tblformatopintura` (`idFormato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxPgArtista` FOREIGN KEY (`idPgArtista`) REFERENCES `tblpgartista` (`idPgArtista`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxStatusPintura` FOREIGN KEY (`idStatusPintura`) REFERENCES `tblstatuspintura` (`idStatusPintura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkPinturaxTecnica` FOREIGN KEY (`idTecnica`) REFERENCES `tbltecnicapintura` (`idTecnica`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpintura`
--

LOCK TABLES `tblpintura` WRITE;
/*!40000 ALTER TABLE `tblpintura` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblpintura` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-09 16:29:03
