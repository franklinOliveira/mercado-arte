CREATE DATABASE  IF NOT EXISTS `sysma` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sysma`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: sysma
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblgeralcupom`
--

DROP TABLE IF EXISTS `tblgeralcupom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblgeralcupom` (
  `idCupom` int(11) NOT NULL AUTO_INCREMENT,
  `codCupom` varchar(8) NOT NULL COMMENT 'Código do cupom, randômico e alfanumérico com oito caracteres.',
  `porcentagemDesconto` tinyint(4) NOT NULL COMMENT 'Porcentagem de desconto que o cupom aplicará no valor total, de 1% até 100%.',
  `cupomAtivo` tinyint(4) NOT NULL COMMENT 'Cupom ativo? 0 = Não, 1 = Sim.',
  `dtCriacao` datetime NOT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  `statusRegistro` tinyint(4) NOT NULL,
  PRIMARY KEY (`idCupom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblgeralcupom`
--

LOCK TABLES `tblgeralcupom` WRITE;
/*!40000 ALTER TABLE `tblgeralcupom` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblgeralcupom` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-09 16:29:06
